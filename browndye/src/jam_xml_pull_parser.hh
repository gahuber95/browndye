/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/


#ifndef __JAM_XM_PULL_PARSER_HH__
#define __JAM_XM_PULL_PARSER_HH__

/*
Top-level parsing classes used in the C++ code.
Implements a parser using the same model as the Ocaml version in
jam_xml_ml_pull_parser.ml. Implements two classes: the Parser and
the Node.  It wraps the Parser_1F class.
*/

#include <map>
#include <list>
#include <memory>
#include <queue>
#include "jam_xml_1f_parser.hh"

namespace JAM_XML_Pull_Parser{

/*************************************************************/
namespace JP = JAM_XML_Parser;
typedef JP::String String;

class State;
class Parser;

typedef JP::Attrs Attrs;

class Parser;

class Node{
public:
  const String& tag() const;
  const Attrs& attributes() const;
  bool is_traversed() const;

  Node* child( const std::string& tag);
  Node* child( const std::string&, const std::string&);
  Node* child( const std::string&, const std::string&, const std::string&);
  Node* child( const std::string&, const std::string&, const std::string&, const std::string&);
  Node* child( const std::string&, const std::string&, const std::string&, const std::string&, 
               const std::string&);
  Node* child( const std::string&, const std::string&, const std::string&, const std::string&, 
               const std::string&, const std::string&);
  Node* child( const std::string&, const std::string&, const std::string&, const std::string&, 
               const std::string&, const std::string&, const std::string&);

  std::list< Node*> children_of_tag( const std::string& tag);

  Node( const String& tag, const JP::Attrs&, Node* parent);

  const Vector< String>& data() const;

  /*****************************************************************/

private: 
  friend 
  void begin_tag( Parser&, const String&, const JP::Attrs&);

  friend 
  void end_tag( Parser&, const String&, Vector< String>&&);

  friend
  class Parser;

  String ttag;
  JP::Attrs attrs;

  //  std::unique_ptr< JP::Str_Stream> strm;
  Vector< String> sdata;
  std::multimap< String, std::unique_ptr< Node> > children;
  std::queue< std::unique_ptr< Node> > child_nodes;

  Node* parent = nullptr;
  Node* successor = nullptr;

  bool read_in = false;
  bool completed = false;
  bool traversed = false;
};

typedef Node* Node_Ptr;

/******************************************************/
class Parser{
public:
  explicit Parser( std::ifstream&);

  void next();
  bool done() const;
  void go_up();
  
  void complete_current_node();
  bool is_current_node_tag( const std::string&) const;

  Node* current_node();
  const Node* current_node() const;

  // throws exception if not found
  void find_next_tag( const std::string&);

  template< class Function>
  void apply_to_nodes_of_tag( const std::string& tag, Function& f);

private:

  friend
  void begin_tag( Parser&, const String& ctag, const JP::Attrs& attrs);

  friend 
  void end_tag( Parser&, const String&, Vector< String>&&);

  std::ifstream& stream;

  typedef JP::Parser_1F< Parser> JParser;
  std::unique_ptr< JParser> jparser;
  Node* current = nullptr;
  Node* leading = nullptr;
  bool parsing_done = false;
  bool traversal_done = false;
  std::unique_ptr< Node> top_node;
  
  void complete_node( Node*);

  void build_forward( Node*);
};

inline
Node* Parser::current_node(){
  return current;
}

inline
const Node* Parser::current_node() const{
  return current;
}

  // applies f to nodes within current node, advancing the current position
template< class Function>
void Parser::apply_to_nodes_of_tag( const std::string& tag, Function& f){

  auto current_level = [&]() -> size_t {
    size_t i = 0;
    Node* node = current;
    while( node){
      node = node->parent;
      ++i;
    }
    return i;
  };

  auto nl = current_level();

  while(true) {
    next();

    if (current_level() <= nl)
      break;

    else if (current->tag() == tag){
      complete_current_node();
      f( current);
    }
  }
}

}

#endif
