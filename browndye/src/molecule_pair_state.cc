/*
 * molecule_pair_state.cc
 *
 *  Created on: Sep 9, 2015
 *      Author: root
 */
#include "molecule_pair_state.hh"
#include "molecule_pair_common.hh"

void Molecule_Pair_State::copy_from( const Molecule_Pair_State& other){
  auto old_n = n;
  *this = other;
  n = old_n;
}

void Molecule_Pair_State::initialize( unsigned long int _number){
  n = _number;
  current_rxn_state = std::numeric_limits< size_type>::max();
  last_rxn = std::numeric_limits< size_type>::max();
  had_rxn = false;
  ffate = In_Action;
  n_full_steps = 0;
  n_succ_collisions = 0;
  wt = NAN;
  min_rxn_coord = Length( INFINITY);
  time = Time( 0.0);
}

Molecule_Pair_Common::size_type
Molecule_Pair_State::current_reaction_state() const{
  return current_rxn_state;
}

void Molecule_Pair_State::set_initial_reaction_state( const Molecule_Pair_Common& common){
  current_rxn_state = common.pathway->first_state();
  min_rxn_coord = Length( INFINITY);
  had_rxn = false;
  ffate = In_Action;
}

