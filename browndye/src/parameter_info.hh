/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

#ifndef __PARAMETERS_HH__
#define __PARAMETERS_HH__

#include <map>
#include <list>
#include "vector.hh"
#include "jam_xml_pull_parser.hh"
#include "units.hh"
#include "small_vector.hh"

/*
This code reads in data for the near-field interactions.

This implements the Parameter_Info class, which reads in its data
from an XML file.  The Parameter_Info object, in turn, is used 
in the function 

template< class Interface>
void get_atoms_from_file( const Parameter_Info* pinfo, 
                          const std::string& file, 
                          typename Interface::Spheres& atoms,
                          bool use_param_charges,
                          bool use_param_radii_hard,
                          bool use_param_radii_soft,
                          bool use_68);

which reads in a container of spheres from a file. The Interface
class has the following types and static functions:

Interface:

Spheres - containter of spheres (atoms)
size_type - type of integer index

void put_position( Spheres&, size_type i, const Length*);
void put_charge( Spheres&, size_type i, Charge);
void put_hard_radius( Spheres&, size_type i, Length);
void put_soft_radius( Spheres&, size_type i, Length);
void put_interaction_radius( Spheres&, size_type i, Length);
void put_type( Spheres&, size_type i, size_type type);
void resize( Spheres&, size_type);
void put_number( Spheres&, size_type, size_type);
void make_soft( Spheres&, size_type);
*/

namespace Near_Interactions{

typedef Vector< int>::size_type size_type;

struct Parameters{
  Energy epsilon;
  Length radius;
  Charge charge;
  size_type index;

  Parameters();
};

typedef std::string String;

class Parameter_Info{

public:
  typedef Vector< int>::size_type size_type;

  Parameter_Info( const std::string& file);
  size_type index( const std::string& residue, const std::string& atom) const;
  const Parameters& parameters( size_type) const;
  size_type n_parameters() const;

private:
  typedef std::pair< const String, const String> Key;
  typedef std::map< const Key, size_t> Dict;
  Dict dict;
  Vector< Parameters> parameter_array;

};

template< class Interface>
void get_atoms_from_file( const Parameter_Info* pinfo, 
                          const std::string& file,
                          typename Interface::Spheres&,
                          bool use_param_charges,
                          bool use_param_radii_hard,
                          bool use_param_radii_soft,
                          bool use_68);

inline
const Parameters& Parameter_Info::parameters( size_type i) const{
  return parameter_array[i];
}

inline
size_type Parameter_Info::n_parameters() const{
  return parameter_array.size();
}

namespace JP = JAM_XML_Pull_Parser;


struct Temp_Atom{
  size_type type;
  Vec3< Length> pos;
  Length hard_radius, soft_radius, interaction_radius;
  Charge charge;
  size_type number;
  bool soft;

  Temp_Atom();
};

typedef std::list< Temp_Atom> AList;

void get_residue_atoms( const Parameter_Info* pinfo, JP::Node_Ptr rnode, 
                        AList& atoms, 
                        bool use_param_charges,
                        bool use_param_radii_hard,
                        bool use_param_radii_soft,
                        bool use_68
                        );

class Residue_Getter{
public:  

  AList atoms;
  const Parameter_Info* pinfo;
  bool use_param_radii_hard, use_param_radii_soft, use_param_charges,
    use_68;

  Residue_Getter( const Parameter_Info* _pinfo):
    pinfo( _pinfo){}

  void operator()( JP::Node_Ptr rnode){
    get_residue_atoms( pinfo, rnode, atoms, use_param_charges, 
                       use_param_radii_hard, use_param_radii_soft, use_68);
  }

};

  // finds atom type if pinfo is non-null
template< class Interface>
void get_atoms_from_file( const Parameter_Info* pinfo, 
                          const std::string& file, 
                          typename Interface::Spheres& atoms,
                          bool use_param_charges,
                          bool use_param_radii_hard,
                          bool use_param_radii_soft,
                          bool use_68){


  typedef typename Interface::size_type atoms_size_type;

  std::ifstream input( file);

  if (!input.is_open())
    error( "file ", file, " could not be opened");

  JP::Parser parser( input);
  
  Residue_Getter rg( pinfo);
  rg.use_param_charges = use_param_charges;
  rg.use_param_radii_hard = use_param_radii_hard;
  rg.use_param_radii_soft = use_param_radii_soft;
  rg.use_68 = use_68;
  parser.apply_to_nodes_of_tag( "residue", rg);

  atoms_size_type n = rg.atoms.size();
  Interface::resize( atoms, n);

  atoms_size_type i = 0;
  for (AList::iterator itr = rg.atoms.begin(); itr != rg.atoms.end(); ++itr){
    Temp_Atom& tatom = *itr;
    Interface::put_position( atoms, i, tatom.pos);
    Interface::put_charge( atoms, i, tatom.charge);
    Interface::put_hard_radius( atoms, i, tatom.hard_radius);
    Interface::put_soft_radius( atoms, i, tatom.soft_radius);
    Interface::put_interaction_radius( atoms, i, tatom.interaction_radius);
    Interface::put_number( atoms, i, tatom.number);
    if (tatom.soft)
      Interface::make_soft( atoms, i); 
    if (pinfo != NULL)
      Interface::put_type( atoms, i, tatom.type);
    ++i;
  }
}

}

#endif
