#ifndef __ELLIPSOID_HH__
#define __ELLIPSOID_HH__

#include "linalg3.hh"

struct Ellipsoid{
  
  Vec3< Vec3< double> > axes;
  Vec3< Length> distances;
  Vec3< Length> center;

  Ellipsoid();
};

inline
Ellipsoid::Ellipsoid(){
  for( int i=0; i<3; i++)
    Linalg3::fill_with_nan( axes[i]);
  Linalg3::fill_with_nan( distances);
}


#endif
