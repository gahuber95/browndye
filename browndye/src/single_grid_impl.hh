/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
Implementation of Single_Grid::Grid. Included by "single_grid.hh".
 */

#include <stdarg.h>
#include "single_grid.hh"

namespace Single_Grid{

template< class Potential>
void Grid< Potential>::get_high_corner( Vec3< Length>& c) const{
  c[0] = low_corner[0] + hx*(double)(nx-1);
  c[1] = low_corner[1] + hy*(double)(ny-1);
  c[2] = low_corner[2] + hz*(double)(nz-1);
}

template< class Potential>
void Grid< Potential>::shift_position( const Vec3< Length>& offset){
  low_corner[0] += offset[0];
  low_corner[1] += offset[1];
  low_corner[2] += offset[2];
}

inline
void check_scanf( FILE* fp, unsigned int status0, unsigned int status, unsigned int line){
  if(ferror( fp) || (status0 != status)) {
    fclose( fp);
    error( "input error in file ", __FILE__, " at line ", line);
  }
}
  
inline
bool was_comment( FILE* fp){
  char c;
  int status = fscanf( fp, "%c", &c);
  check_scanf( fp, 1, status, __LINE__);

  if (c == '#'){
    do{
      int status = fscanf( fp, "%c", &c);
      check_scanf( fp, 1, status, __LINE__);
    }
    while (c != '\n');
    return true;
  }
  else
    return false;
}

template< class Potential>
Grid< Potential>::Grid( const char* filename){

  FILE *fp = fopen( filename, "r");
  if (fp == NULL)
    error( "cannot open file ", filename, " at line ", __LINE__);
 
  bool comments_done;
  do{
    comments_done = !was_comment( fp);
  }
  while (comments_done == false);
  
  unsigned int nx_in, ny_in, nz_in;
  int status; // ugly hack
  status = fscanf( fp, "bject 1 class gridpositions counts %u %u %u\n", 
                   &nx_in, &ny_in, &nz_in);
  
  check_scanf( fp, 3, status, __LINE__);

  nx = nx_in;
  ny = ny_in;
  nz = nz_in;

  nyz = ny*nz;
  
  nxm1 = nx-1;
  nym1 = ny-1;
  nzm1 = nz-1;

  double val0, val1, val2;
  status = fscanf( fp, "origin %lf %lf %lf\n", 
                   &val0, &val1, &val2);
  set_fvalue( low_corner[0], val0);
  set_fvalue( low_corner[1], val1);
  set_fvalue( low_corner[2], val2);

  check_scanf( fp, 3, status, __LINE__);  

  double val;
  double duma, dumb;
  status = fscanf( fp, "delta %lf %lf %lf\n", &val, &duma, &dumb);
  check_scanf( fp, 3, status, __LINE__);
  set_fvalue( hx, val); 

  status = fscanf( fp, "delta %lf %lf %lf\n", &duma, &val, &duma);
  check_scanf( fp, 3, status, __LINE__);
  set_fvalue( hy, val); 

  status = fscanf( fp, "delta %lf %lf %lf\n", &duma, &dumb, &val);
  check_scanf( fp, 3, status, __LINE__);
  set_fvalue( hz, val); 

  unsigned int iduma, idumb, idumc;
  status = fscanf( fp, "object 2 class gridconnections counts %u %u %u\n", 
          &iduma, &idumb, &idumc);
  check_scanf( fp, 3, status, __LINE__);

  status = 
    fscanf( fp, 
            "object 3 class array type double rank 0 items %u data follows", 
            &iduma);
  check_scanf( fp, 1, status, __LINE__);

  unsigned long n = nx*ny*nz;
  data.resize( n); 

  for( size_type i = 0; i < n; i++){
    float val;
    status = fscanf( fp, "%f ", &val);
    set_fvalue( data[i], val);
    check_scanf( fp, 1, status, __LINE__);
  }

  fclose( fp);
}

namespace JP = JAM_XML_Pull_Parser;

template< class V>
class D_Getter{
public:
  D_Getter( V* _data): data(_data){
    i = 0;
  }

  void operator()( JP::Node_Ptr node){
    auto& sdata = node->data();
    for (int k = 0; k<3; k++){
      set_fvalue( data[i], sdata[k]);
      ++i;
    }
  }
  
  typename Vector< V>::size_type i;
  V* data;
};


  // assume parser current node is "distances"; returns with
  // parser at end of "distances"
template< class Potential>
Grid< Potential>::Grid( JP::Parser& parser){

  parser.find_next_tag( "n");
  {
    parser.complete_current_node();
    auto& ndata = parser.current_node()->data();
    nx = stol( ndata[0]);
    ny = stol( ndata[1]);
    nz = stol( ndata[2]);
    nyz = ny*nz;
  }

  parser.find_next_tag( "spacing");
  {
    parser.complete_current_node();
    auto& ndata = parser.current_node()->data();
    set_fvalue( hx, stod( ndata[0]));
    set_fvalue( hy, stod( ndata[1]));
    set_fvalue( hz, stod( ndata[2]));
  }    

  parser.find_next_tag( "corner");
  {
    parser.complete_current_node();
    auto& ndata = parser.current_node()->data();
    set_fvalue( low_corner[0], stod( ndata[0]));
    set_fvalue( low_corner[1], stod( ndata[1]));
    set_fvalue( low_corner[2], stod( ndata[2]));
  }    

  parser.find_next_tag( "data");
  data = new SPotential [nx*nyz];

  D_Getter< SPotential> dgetter( data);
  parser.apply_to_nodes_of_tag( "d", dgetter);
  
  typename Vector< SPotential>::difference_type di = nx*ny*nz - dgetter.i;
  if (di == 1){
    parser.find_next_tag( "d1");
    parser.complete_current_node();
    auto& ndata = parser.current_node()->data();
    set_fvalue( data[ dgetter.i], stof( ndata[0]));
  }
  else if (di == 2){
    parser.find_next_tag( "d2");
    parser.complete_current_node();
    auto& ndata = parser.current_node()->data();
    set_fvalue( data[ dgetter.i], stof( ndata[0]));
    set_fvalue( data[ dgetter.i + 1], stof( ndata[1]));
  }
  parser.go_up();
}  

template< class Potential>
void Grid< Potential>::
get_gradient( long ix, long iy, long iz, 
              Length rx, Length ry, Length rz,
              Vec3< typename UQuot< Potential, Length>::Res>& g) const{

  size_type i = ix*nyz + iy*nz + iz;
    
    Potential vmmm = data[i];
    Potential vmmp = data[i+1];
    Potential vmpm = data[i+nz];
    Potential vmpp = data[i+nz+1];
    
    Potential vpmm = data[i+nyz];
    Potential vpmp = data[i+nyz+1];
    Potential vppm = data[i+nyz+nz];
    Potential vppp = data[i+nyz+nz+1];
    
    double ax = (rx - ((double)ix)*hx)/hx;
    double ay = (ry - ((double)iy)*hy)/hy;
    double az = (rz - ((double)iz)*hz)/hz;
    
    double apx = 1.0 - ax;
    double apy = 1.0 - ay;
    double apz = 1.0 - az;
    
    // z component 
    Potential_Gradient gzmm = (vmmp - vmmm)/hz;
    Potential_Gradient gzmp = (vmpp - vmpm)/hz;
    Potential_Gradient gzpm = (vpmp - vpmm)/hz;
    Potential_Gradient gzpp = (vppp - vppm)/hz;
    
    Potential_Gradient gzm = apy*gzmm + ay*gzmp;
    Potential_Gradient gzp = apy*gzpm + ay*gzpp;
    
    g[2] = apx*gzm + ax*gzp;
    
    // y component
    Potential_Gradient gymm = (vmpm - vmmm)/hy;
    Potential_Gradient gymp = (vmpp - vmmp)/hy;
    Potential_Gradient gypm = (vppm - vpmm)/hy;
    Potential_Gradient gypp = (vppp - vpmp)/hy;
    
    Potential_Gradient gym = apz*gymm + az*gymp;
    Potential_Gradient gyp = apz*gypm + az*gypp;
    
    g[1] = apx*gym + ax*gyp;
    
    // x component
    Potential_Gradient gxmm = (vpmm - vmmm)/hx;
    Potential_Gradient gxmp = (vpmp - vmmp)/hx;
    Potential_Gradient gxpm = (vppm - vmpm)/hx;
    Potential_Gradient gxpp = (vppp - vmpp)/hx;
    
    Potential_Gradient gxm = apz*gxmm + az*gxmp;
    Potential_Gradient gxp = apz*gxpm + az*gxpp;
    
    g[0] = apy*gxm + ay*gxp;
 
}

template< class Potential>
bool Grid< Potential>::in_range( const Vec3< Length>& pos) const{
  Length rx = pos[0] - low_corner[0];
  Length ry = pos[1] - low_corner[1];
  Length rz = pos[2] - low_corner[2];
 
  long ix = (long)(rx/hx);
  long iy = (long)(ry/hy);
  long iz = (long)(rz/hz);

  return ! ((ix >= (long)(nx-1)) || (iy >= (long)(ny-1)) || (iz >= (long)(nz-1)) 
            || (ix < 0) || (iy < 0) || (iz < 0));
}

template< class Potential>
void Grid< Potential>::
get_gradient( const Vec3< Length>& pos, 
              Vec3< typename UQuot< Potential, Length>::Res>& g, 
              bool& in_range) const{

  Length rx = pos[0] - low_corner[0];
  Length ry = pos[1] - low_corner[1];
  Length rz = pos[2] - low_corner[2];

  long ix = (long)(floor(rx/hx));
  long iy = (long)(floor(ry/hy));
  long iz = (long)(floor(rz/hz));

  in_range = ! ((ix >= (long)(nx-1)) || (iy >= (long)(ny-1)) || (iz >= (long)(nz-1)) 
                || (ix < 0) || (iy < 0) || (iz < 0));

  if (in_range)
    get_gradient( ix,iy,iz, rx,ry,rz, g);
}  

template< class Potential>
void Grid< Potential>::
get_gradient( const Vec3< Length>& pos, 
              Vec3< typename UQuot< Potential, Length>::Res>& g) const{

  Length rx = pos[0] - low_corner[0];
  Length ry = pos[1] - low_corner[1];
  Length rz = pos[2] - low_corner[2];

  long ix = (long)(rx/hx);
  long iy = (long)(ry/hy);
  long iz = (long)(rz/hz);

  get_gradient( ix,iy,iz, rx,ry,rz, g);
}  



template< class Potential>
Potential Grid< Potential>::potential( const Vec3< Length>& pos) const{
  Length rx = pos[0] - low_corner[0];
  Length ry = pos[1] - low_corner[1];
  Length rz = pos[2] - low_corner[2];
  
  long ix = (long)(rx/hx);
  long iy = (long)(ry/hy);
  long iz = (long)(rz/hz);

  return potential( ix, iy, iz, rx, ry, rz);
}

template< class Potential>
Potential Grid< Potential>::potential( long ix, long iy, long iz, 
                                       Length rx, Length ry, Length rz) const{

  size_type i = ix*nyz + iy*nz + iz;

  Potential vmmm = data[i];
  Potential vmmp = data[i+1];
  Potential vmpm = data[i+nz];
  Potential vmpp = data[i+nz+1];
  
  Potential vpmm = data[i+nyz];
  Potential vpmp = data[i+nyz+1];
  Potential vppm = data[i+nyz+nz];
  Potential vppp = data[i+nyz+nz+1];
  
  double ax = (rx - ((double)ix)*hx)/hx;
  double ay = (ry - ((double)iy)*hy)/hy;
  double az = (rz - ((double)iz)*hz)/hz;
  
  double apx = 1.0 - ax;
  double apy = 1.0 - ay;
  double apz = 1.0 - az;
  
  Potential vmm = apz*vmmm + az*vmmp;
  Potential vmp = apz*vmpm + az*vmpp;
  Potential vpm = apz*vpmm + az*vpmp;
  Potential vpp = apz*vppm + az*vppp;
  
  Potential vm = apy*vmm + ay*vmp;
  Potential vp = apy*vpm + ay*vpp;
  
  return apx*vm + ax*vp;
}

template< class Potential>
void Grid< Potential>::get_potential( const Vec3< Length>& pos, 
				      Potential& v, 
				      bool& in_range) const{
  Length rx = pos[0] - low_corner[0];
  Length ry = pos[1] - low_corner[1];
  Length rz = pos[2] - low_corner[2];
  
  long ix = (long)(floor(rx/hx));
  long iy = (long)(floor(ry/hy));
  long iz = (long)(floor(rz/hz));
  
  in_range = ! ((ix >= (long)nxm1) || (iy >= (long)nym1) || (iz >= (long)nzm1) || 
                (ix < 0) || (iy < 0) || (iz < 0));
  
  if (in_range){    
    v = potential( ix, iy, iz, rx, ry, rz);
  }
}

}

//********************************************************
// test code
/*
class GInterface{
public:
  typedef ::Potential Potential;
  typedef ::Potential_Gradient Potential_Gradient;

};

namespace JP = JAM_XML_Pull_Parser;

int main(){
  std::ifstream input( "bull");
  JP::Parser parser( input);

  Single_Grid::Grid< GInterface> grid( parser);

  Length x[3];
  Potential_Gradient f[3];
  bool in_range;
  grid.get_gradient( x, f, in_range);
  Potential v;
  grid.get_potential( x, v, in_range);

  grid.invert_data();

  return 0;
}
*/
