/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/


/* Resolves motion of particle moving near inner surface of an absorbing
sphere under a radial force.  Computes 1) whether particle is absorbed in
next time step, and 2) if not, its new radial position.

Assume that kT term is part of force, so F has units of 1/Length

Interface:

typedef ... Random_Number_Generator
double gaussian( RNG&) - gaussian with zero mean and unit variance
double uniform( RNG&) - uniform between 0 and 1

*/

#ifndef __STEP_NEAR_ABSORBING_SPHERE_HH__
#define __STEP_NEAR_ABSORBING_SPHERE_HH__

#include <math.h>
#include "pi.hh"

template< class I, class Length, class Force, class Diffusivity, class Time>
void step_near_absorbing_sphere( typename I::Random_Number_Generator& rng,
                                 Length r, Length R, Force F,
                                 Diffusivity D_radial, Diffusivity D_perp,
                                 bool& survives, 
                                 Length& new_r, Time& time
                                 ){

  auto b = F + 2.0*D_perp/(D_radial*r);
  Length x0 = R - r;
  Time t = x0*x0/D_radial;

  auto tau = t*D_radial;    

  auto st = sqrt( tau);
  auto bt = b*tau;
  auto erfmt = erf( (x0 - bt)/(2.0*st));
  double psurv = 0.5*( exp(b*x0)*(erf( (x0 + bt)/(2.0*st)) - 1.0) + 
                       erfmt + 1.0);

  survives = I::uniform( rng) < psurv;

  if (survives){
    Length x;

    bool found;
    do{
      do 
        x = x0 - b*tau + sqrt( 2.0)*st*I::gaussian( rng);
      while ( x < Length(0.0));
      
      auto t4 = 4.0*tau;
      auto p0 = exp(-sq(x - x0 + bt)/t4);
      auto p1 = exp( b*x0 - sq(x + x0 + bt)/t4);
      auto p2 = -2.0*exp(-(sq(x + bt) + x0*(x0 + 2.0*(x - bt)))/t4);
      auto p = p0 + p1 + p2;

      auto pu = p0/(0.5*(erfmt + 1.0));
      found = (I::uniform( rng) < p/pu);
    }
    while (!found);

    new_r = R - x;
    time = t;
  }

  else{ // !survives
    auto x02 = x0*x0;
    auto b2 = b*b;
    auto tau_max = fabs( b*x0) < 0.5 ? 
      x02*(1.0/6.0 - x02*b2/216.0) :  
      (sqrt( b2*x02 + 9.0) - 3.0)/b2;

    typedef decltype( tau) Tau;
    typedef decltype( 1.0/tau) Inv_Tau;
    auto pt = [&]( Tau tau) -> Inv_Tau {
      return x0*exp(-sq(x0 - b*tau)/(4.0*tau))/(2.0*sqrt(pi*tau*sq(tau)));
    };

    auto pmax = pt( tau_max);
    bool done = false;
    Tau tau_samp;
    do{
      tau_samp = tau*I::uniform( rng);
      auto p_samp = pmax*I::uniform( rng);
      done = p_samp < pt( tau_samp);
    }
    while (!done);

    time = tau_samp/D_radial;
    new_r = R;
  }
} 

#endif  

