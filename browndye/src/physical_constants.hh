/*
 * physical_constants.hh
 *
 *  Created on: Sep 11, 2015
 *      Author: ghuber
 */

#ifndef PHYSICAL_CONSTANTS_HH_
#define PHYSICAL_CONSTANTS_HH_

#include "units.hh"
const Viscosity water_viscosity( 0.243);
const Permittivity vacuum_permittivity( 0.000142);

#endif /* PHYSICAL_CONSTANTS_HH_ */
