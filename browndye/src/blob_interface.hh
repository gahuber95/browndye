#ifndef __BLOB_INTERFACE_HH__
#define __BLOB_INTERFACE_HH__

#include "units.hh"
#include "field_for_blob.hh"
#include "transform.hh"
#include "small_vector.hh"
#include "distance_finder.hh"

// CInterface refers to collision structure of field
template< class CInterface, class FInterface>
class Blob_Interface{
public:
  typedef typename FInterface::Charge Charge;
  typedef typename FInterface::Potential Potential;
  typedef ::Transform Transform;
  typedef Field_For_Blob< CInterface, FInterface> Field;

  static
  void translate( const Transform& trans, const Vec3< Length>& before, Vec3< Length>& after){
    trans.get_translated( before, after);
  }

  template< class U>
  static
  void rotate( const Transform& trans, const Vec3< U>& before, Vec3< U>& after){
    trans.get_rotated( before, after);
  }

  static
  Potential value( const Field& field, const Vec3< Length>& pos){
    return field.field->potential( pos, *(field.hint_ptr));
  }

  typedef typename UQuot< Potential, Length>::Res Potential_Gradient;

  static
  void get_gradient( const Field& field, const Vec3< Length>& pos, 
                     Vec3< Potential_Gradient>& grad){
    field.field->get_gradient( pos, grad, *(field.hint_ptr));
    
  } 

  static
  bool distance_less_than( const Field& field, Length r, 
                           const Vec3< Length>& pos){

    Distance_Finder< CInterface> df;
    return field.collision_structure->is_within( df, r, pos);
  }
};

#endif
