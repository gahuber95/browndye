/*
 * molecule_pair_fate.hh
 *
 *  Created on: Sep 8, 2015
 *      Author: root
 */

#ifndef MOLECULE_PAIR_FATE_HH_
#define MOLECULE_PAIR_FATE_HH_

enum Molecule_Pair_Fate {In_Action, Escaped, Final_Rxn, Stuck};

#endif /* MOLECULE_PAIR_FATE_HH_ */
