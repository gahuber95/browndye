/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
Implements classes and functions described in "parameter_info.hh"
*/


#include <math.h>
#include "node_info.hh"
#include "parameter_info.hh"
#include "newton_1d.hh"
#include <algorithm>

namespace Near_Interactions{

//****************************************************************
// Following code used for initializing

Parameters::Parameters(){
  radius = Length( NAN);
  epsilon = Energy( NAN);
  charge = Charge( NAN);
}

String node_string( const std::string& tag, JP::Node_Ptr node){
  JP::Node_Ptr nnode = node->child( tag);

  if (nnode == NULL)
    return String( "");
  else
    return nnode->data()[0];
}

String node_name( JP::Node_Ptr node){
  return node_string( "name", node);
}

double node_value( JP::Node_Ptr node, const std::string& tag){
  JP::Node_Ptr nnode = node->child( tag);
  if (nnode == NULL)
    return NAN;
  else 
    return stod( nnode->data()[0]);
}

typedef std::list< JP::Node_Ptr> NList;

class Param_Ptr_Comparer{
public:
  bool operator()( const Parameters* p0, const Parameters* p1) const{
    if (p0->epsilon != p1->epsilon)
      return p0->epsilon < p1->epsilon;
    else if (p0->radius != p1->radius)
      return p0->radius < p1->radius;
    else if (p0->charge != p1->charge)
      return p0->charge < p1->charge;
    else return false;
  } 
};
  
// constructor
Parameter_Info::Parameter_Info( const std::string& file){

  std::ifstream input( file);
  
  JP::Parser parser( input);
  parser.complete_current_node();
  JP::Node_Ptr top_node = parser.current_node();


  NList residue_nodes = top_node->children_of_tag( "residue");

  // get number of entries
  size_t nparams = 0;
  for( NList::iterator itr = residue_nodes.begin();
       itr != residue_nodes.end(); ++itr){

    JP::Node_Ptr rnode = *itr;
    const String residue_name = node_name( rnode);

    NList atom_nodes = rnode->children_of_tag( "atom");
    for( NList::iterator itr = atom_nodes.begin();
         itr != atom_nodes.end(); ++itr)
      ++nparams;
  }
  parameter_array.resize( nparams);

  // get the parameters
  size_t i = 0;  
  for( NList::iterator itr = residue_nodes.begin();
       itr != residue_nodes.end(); ++itr){

    JP::Node_Ptr rnode = *itr;
    const String residue_name = node_name( rnode);

    NList atom_nodes = rnode->children_of_tag( "atom");
    for( NList::iterator itr = atom_nodes.begin();
         itr != atom_nodes.end(); ++itr){
      
      JP::Node_Ptr anode = *itr;
      const String atom_name = node_name( anode);

      const Key key( residue_name, atom_name);
      std::pair< const Key, size_t> entry( key, i);
      std::pair< Dict::iterator, bool> result = dict.insert( entry);

      if (!result.second)
        error( "Parameter_Info: duplicate info ", residue_name.c_str(), " ",
               atom_name.c_str());

      Parameters& param = parameter_array[i];
      bool found;
      get_value_from_node( anode, "radius", param.radius, found);
      get_value_from_node( anode, "charge", param.charge, found);
      get_value_from_node( anode, "epsilon", param.epsilon, found);
      ++i;
      
    }
  }

  // get rid of duplicate info
  size_type n = parameter_array.size();
  Vector< Parameters*> param_ptrs( n);
  for( size_type i = 0; i < n; i++)
    param_ptrs[i] = &(parameter_array[i]);

  Param_Ptr_Comparer less_than;
  std::sort( param_ptrs.begin(), param_ptrs.end(), less_than);
  
  param_ptrs[0]->index = 0;
  size_type ip = 0;
  for( size_type j = 1; j < param_ptrs.size(); j++){
    Parameters& param0 = *(param_ptrs[j-1]);
    Parameters& param1 = *(param_ptrs[j]);
    if (less_than( &param0, &param1)){
      ++ip;
    }
    param1.index = ip;
  }
    
  Vector< Parameters> new_params( ip+1); 
  for( size_type j = 0; j < param_ptrs.size(); j++){
    Parameters& param = *(param_ptrs[j]);
    new_params[ param.index] = param;
  }

  Dict new_dict;
  for (Dict::iterator itr = dict.begin(); itr != dict.end(); ++itr){
    const Key& key = itr->first;
    size_t index = parameter_array[ itr->second].index;
    std::pair< const Key, size_t> entry( key, index);
    new_dict.insert( entry);
  }
  dict.swap( new_dict);

  parameter_array.swap( new_params);
}

size_t Parameter_Info::index( const std::string& residue, 
			      const std::string& atom) const{

  const String atom_name( atom);
  const String residue_name( residue);
  const Key key( residue_name, atom_name);

  Dict::const_iterator itr = dict.find( key);
  if (itr == dict.end())
    error( "Param_Info::index: not found ", residue, " ", atom);

  return itr->second;
}

  // constructor
Temp_Atom::Temp_Atom(){
  type = std::numeric_limits< size_type>::max();
  pos[0] = pos[1] = pos[2] = Length( NAN);
  hard_radius = Length( NAN);
  soft_radius = Length( NAN);
  charge = Charge( NAN);
  soft = false;
}

//***********************************************************
// Following code is used to find energy cut-off
  
class LJ_Info{
public:
  LJ_Info( Length _sig, Energy _eps, Energy _thresh, bool _use_68):
    sig(_sig), eps(_eps), thresh(_thresh), use_68(_use_68){}
  
  Energy f( Length x);
  void get_f_and_dfdx( Length x, Energy&, Force&);
  
  
private:
  const Length sig;
  const Energy eps;
  const Energy thresh;
  bool use_68;
};
  
Energy LJ_Info::f( Length x){
  double r = x/sig;
  double r2 = r*r;
  double r4 = r2*r2;
  double r6 = r2*r4;
  double r8 = r4*r4;
  double r12 = r6*r6;
  
  Energy v;
  if (use_68)
    v = eps*(3.0/r8 - 4.0/r6);
  else
    v = eps*( 1.0/r12 - 2.0/r6);
  return v - thresh;
}

void LJ_Info::get_f_and_dfdx( Length x, Energy& v, Force& dvdx){
  double r = x/sig;
  double r2 = r*r;
  double r4 = r2*r2;
  double r6 = r2*r4;
  double r8 = r4*r4;
  double r12 = r6*r6;
  
  if (use_68){
    v = eps*(3.0/r8 - 4.0/r6) - thresh;
    dvdx = -24.0*eps*( 1.0/r8 - 1.0/r6)/x;
  }
  else{
    v = eps*( 1.0/r12 - 2.0/r6) - thresh;
    dvdx = -12.0*eps*( 1.0/r12 - 1.0/r6)/x;
  }
}

class LJ_Interface{
public:
  typedef Length X;
  typedef Energy F;
  typedef Force DFDX;
  typedef LJ_Info Info;

  static
  void get_f_and_dfdx( LJ_Info& info, Length x, Energy& v, Force& dvdx){
    info.get_f_and_dfdx( x,v,dvdx);
  }  

  static
  Energy f( LJ_Info& info, Length x){
    return info.f( x);
  }  
};

Length bound_radius( Energy eps, Length sig, bool use_68){

  Length res;
  if (fvalue( sig) < 0.0){
    error( "cannot have negative radius in Lennard-Jones forces");
    res = Length( NAN);
  }

  else if (fvalue( sig) == 0.0)
    res = Length( 0.0);

  else{ 
    Energy thresh0( 0.01);
    Energy thresh = eps < thresh0 ? thresh0 : -thresh0;
    LJ_Info lj_info( sig, eps, thresh, use_68);
    
    if (eps < thresh){ // shallow well
      
      Length r0 = 0.25*sig;
      while (fvalue( lj_info.f( r0)) < 0.0){
        r0 /= 2.0;
      }
      
      res = Newton_Raphson::solution< LJ_Interface>( lj_info, 
                                                     r0, 0.5*sig, sig,
                                                     0.01*thresh0);
    }
    else if (eps == thresh){
      res = sig;
    }
    else{
      res =  Newton_Raphson::solution< LJ_Interface>( lj_info, 
                                                      sig, 2.0*sig, 5.0*sig,
                                                      0.01*thresh0);
    }
  }
  return res;    
}

//****************************************************************
void get_residue_atoms( const Parameter_Info* pinfo, JP::Node_Ptr rnode, 
                        AList& atoms, 
                        bool use_param_charges,
                        bool use_param_radii_hard, 
                        bool use_param_radii_soft,
                        bool use_68){

  String residue_name = node_string( "residue_name", rnode);

  NList anodes = rnode->children_of_tag( "atom");
  for( NList::iterator itr = anodes.begin(); itr != anodes.end(); ++itr){
    Temp_Atom atom;
    JP::Node_Ptr anode = *itr;
    atom.pos[0] = Length( node_value( anode, "x"));
    atom.pos[1] = Length( node_value( anode, "y"));
    atom.pos[2] = Length( node_value( anode, "z"));

    bool found;
    Length aradius;
    get_value_from_node( anode, "radius", aradius, found);
    get_value_from_node( anode, "charge", atom.charge, found);    
    get_value_from_node( anode, "atom_number", atom.number);

    String atom_name = node_string( "atom_name", anode);

    atom.hard_radius = aradius;
    atom.soft_radius = aradius;

    {
      JP::Node_Ptr snode = anode->child( "soft");
      if (snode != NULL){
        String res = string_from_node( snode);
        if (res == String( "true") || res == String( "True") || 
            res == String( "TRUE")){
          atom.soft = true;
        }
      }
    }

    if (!( atom_name.empty() && residue_name.empty()) && atom.soft && pinfo != nullptr){
      atom.type = pinfo->index( residue_name.c_str(), atom_name.c_str());

      const Parameters& params = pinfo->parameters( atom.type);
      if (use_param_radii_hard)
        atom.hard_radius = params.radius;

      if (use_param_radii_soft)
        atom.soft_radius = params.radius;
      
      if (use_param_charges)
        atom.charge = params.charge;
    }


    if (atom.soft){
      Energy epsilon = pinfo->parameters( atom.type).epsilon;
      Length bradius = bound_radius( epsilon, atom.soft_radius, use_68);

      atom.interaction_radius = max( atom.hard_radius, bradius);
    }
    else
      atom.interaction_radius = atom.hard_radius;

    atoms.push_back( atom);
  }
}
  
}

//******************************************************************
// Test code
/*
struct Atom{
  Length pos[3];
  Length radius;
  Charge charge;
  Vector< int>::size_type type;
  size_t number;
};

class Interface{
public:
  typedef Vector< Atom> Spheres;
  typedef Spheres::size_type size_type;

  static
  void resize( Vector< Atom>& atoms, size_type n){
    atoms.resize( n);
  }

  static
  void put_position( Spheres& atoms, size_type i, Length* pos){
    atoms[i].pos[0] = pos[0];
    atoms[i].pos[1] = pos[1];
    atoms[i].pos[2] = pos[2];
  }

  static
  void put_charge( Spheres& atoms, size_type i, Charge charge){
    atoms[i].charge = charge;
  }

  static
  void put_radius( Spheres& atoms, size_type i, Length radius){
    atoms[i].radius = radius;
  }

  static
  void put_type( Spheres& atoms, size_type i, 
                 Near_Interactions::size_type type){
    atoms[i].type = type;
  }

  static
  void put_number( Spheres& atoms, size_type i, 
                   Near_Interactions::size_type number){
    atoms[i].number = number;
  }


};

int main(){
  Near_Interactions::Parameter_Info pi( "vparam-amber-parm94.xml");

  Vector< Atom> atoms;
  Near_Interactions::get_atoms_from_file< Interface>( 
                pi, "t-atoms.pqrxml", atoms, false, true);


  size_t n = atoms.size();
  for( size_t i = 0; i < n; i++){
    size_t itype = atoms[i].type;
    Energy eps = pi.parameters( itype).epsilon;
    printf( "atom %d (%g %g %g) q %g r %g e %g\n",
            atoms[i].number,
            atoms[i].pos[0], atoms[i].pos[1], atoms[i].pos[2], 
            atoms[i].charge, atoms[i].radius, eps); 
  }

}
*/
