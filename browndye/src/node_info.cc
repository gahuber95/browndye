/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

// Implementation of "node_info" functions

#include "node_info.hh"

namespace JP = JAM_XML_Pull_Parser;

template< class Value>
void get_checked_value_from_node( JP::Node_Ptr node, 
                                  const std::string& tag, Value& value){
  JP::Node_Ptr cnode = node->child( tag);
  
  if (cnode == NULL)
    error( "get_value_from_node: tag ", tag, " not found"); 

  std::stringstream ss( cnode->data()[0]);
  ss >> value;
  if (ss.fail())
    error( "get_value_from_node: nothing found in ", tag, " tag");
}

double double_from_node( JP::Node_Ptr node, const std::string& tag){
  double res = NAN;
  get_double_from_node( node, tag, res);
  return res;
}

double double_from_node( JP::Node_Ptr node){
  double res = NAN;
  get_double_from_node( node, res);
  return res;
}

int int_from_node( JP::Node_Ptr node, const std::string& tag){
  int res;
  get_checked_value_from_node( node, tag, res);
  return res;
}

int int_from_node( JP::Node_Ptr node){
  int res;
  get_value_from_node( node, res);
  return res;
}

std::string string_from_node( JP::Node_Ptr node, const std::string& tag){
  std::string res;
  get_checked_value_from_node( node, tag, res);
  return res;
}

std::string string_from_node( JP::Node_Ptr node){
  std::string res;
  get_value_from_node( node, res);
  return res;
}

JAM_XML_Pull_Parser::Node_Ptr
checked_child( JAM_XML_Pull_Parser::Node_Ptr node, 
	      const std::string& tag){

  JP::Node_Ptr res = node->child( tag);
  if (res == NULL)
    error( "node of ", tag, " not found");
  
  return res;
}

