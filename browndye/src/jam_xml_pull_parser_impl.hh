/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
Implementation of inline and templated JAM_XML_Pull_Parser::Parser and 
Node functions; included by "jam_xml_pull_parser.hh".
*/

inline
Node* Parser::current_node(){
  if (entries.empty())
    return top_node.get();
  else{
    return entries.top()->front().get();
  }
}

inline
const Node* Parser::current_node() const{
  if (entries.empty())
    return top_node.get();
  else
    return entries.top()->front().get();
}


template< class Function>
void Parser::apply_to_nodes_of_tag( const std::string& tag, Function& f){
  typedef std::stack< Entry>::size_type size_type;
  const size_type level = entries.size()+1;
  
  Node* prev_node = NULL;
  next();

  bool done = false;
  while (true){
    while ((!is_current_node_tag( tag)) || (entries.size() != level) ||
           current_node() == prev_node){
      next();
      if (entries.size() < level){
        done = true;
        break;
      }
    }
    if (done)
      break;

    complete_current_node();
    Node* node = current_node();
    prev_node = node;
    f( node);
  }

}
