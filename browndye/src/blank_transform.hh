#ifndef __BLANK_TRANSFORM_HH__
#define __BLANK_TRANSFORM_HH__

class Blank_Transform{
  
public:
  void get_translation( Vec3< Length>& x) const{
    x[0] = x[1] = x[2] = Length( 0.0);
  }

  void get_translated( const Vec3< Length>& before, Vec3< Length>& after) const{
    Linalg3::copy( before, after);
  }

  template< class U>
  void get_rotated( const Vec3< U>& before, Vec3< U>& after) const{
    Linalg3::copy( before, after);
  }

  void get_rotation( Mat3< double>& rot){
    Linalg3::get_id_mat( rot);
  }

  template< class U>
  void get_transformed( const Vec3< U>& before, Vec3< U>& after) const{
    Linalg3::copy( before, after);
  }  
  
  void get_inverse( Blank_Transform& ) const {}

  void set_zero(){}
};

#endif
