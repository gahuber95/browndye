#ifndef __COL_INTERFACE_LARGE_HH__
#define __COL_INTERFACE_LARGE_HH__

#include "linalg3.hh"
#include "col_interface_gen.hh"
#include "col_interface_large_interface.hh"

class Col_Interface_Large: public Col_Interface_Gen< Col_Interface_Large_Interface>{
public:
  static const bool is_changeable = false;

  static
  void get_transformed_position( const Vec3< Length>& pos, const Blank_Transform& trans,
                                 Vec3< Length>& tpos){
    Linalg3::copy( pos, tpos);
  }
 
  static
  void clear_transform( Atoms&, Refs& refs){}
};

#endif
