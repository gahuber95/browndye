#include "large_molecule_common.hh"
#include "set_desolvation_chebybox.hh"
#include "initialize_field.hh"
#include "node_info.hh"
#include "get_atoms_from_file.hh"

// constructor
Large_Molecule_Common::Large_Molecule_Common(){
  total_charge = Charge( NAN);

  ks = Spring_Constant( NAN);
  equilib_len = Length( NAN);
  spring_atom0 = NULL;
}

template< class T>
inline
T min4( const T& a, const T& b, const T& c, const T& d){
  return min( min( a,b), min( b,c));
}

void Large_Molecule_Common::check_mpole_consistency() const{
  Vec3< Length> lowc, highc;
  v_field->get_corners( lowc, highc);

  Length dmin = Length( INFINITY);
  for (unsigned int i = 0; i < atoms.size(); i++){
    const Vec3< Length>& pos = atoms[i].pos;
    Length r = atoms[i].hard_radius;
    Length dxl = pos[0] - lowc[0] - r;
    Length dxh = highc[0] - pos[0] - r;
    Length dyl = pos[1] - lowc[1] - r;
    Length dyh = highc[1] - pos[1] - r;
    Length dzl = pos[2] - lowc[2] - r;
    Length dzh = highc[2] - pos[2] - r;
    dmin = min( min( min( min4( dxl,dxh,dyl,dyh), dzl), dzh), dmin);    
  }

  const Vec3< Length>& dists = hydro_ellipsoid.distances;
  Length eff_radius = max( max( dists[0], dists[1]), dists[2]);

  Length debye = v_field->debye_length();

  Length req_dist = min( 3.0*debye, 3.0*eff_radius);

  if ( dmin < req_dist){
    Length shortfall = req_dist - dmin;
    error( "outermost grid is too small: needs to be ", 
           fvalue( shortfall), " length units bigger");
  }
}

void Large_Molecule_Common::initialize( JAM_XML_Pull_Parser::Node_Ptr node, 
                                        Energy kT){

  //pair = _pair;
  Vec3< Length> offset; 
  initialize_super( node, offset);

  String atom_file = string_from_node( node, "atoms");
  get_atoms_from_file( pinfo.get(), atom_file.c_str(), atoms, 
                       hard_radii_from_param_file, soft_radii_from_param_file,
                       use_68);

  // shift molecule hydro center to origin
  for( size_type i = 0; i < atoms.size(); i++){
    Atom_Large& atom = atoms[i];
    atom.translate( offset);
  }

  collision_structure.reset( 
    new Collision_Detector::Structure< Col_Interface_Large>( atoms)
			     );

  collision_structure->set_max_number_per_cell( max_per_cell);
  collision_structure->load_objects();

  auto enode = node->child( "electric-field");
  if (enode != NULL){
    printf( "get electric-field\n");
    initialize_field( enode, v_field);
    v_field->shift_position( offset);
    v_hint0 = v_field->first_hint();
  }

  set_desolvation_chebybox_from_node( *this, node, offset);
  get_checked_double_from_node( node, "total-charge", total_charge);

  check_for_param_file( atoms, pinfo.get(), 0);

  auto tether_node = node->child( "tether");
  if (tether_node != NULL){
    auto el_node = checked_child( tether_node, "resting-length");
    equilib_len = Length( double_from_node( el_node));

    auto ed_node = checked_child( tether_node, "length-deviation");
    Length dl = Length( double_from_node( ed_node));
    ks = kT/(dl*dl);

    auto a0_node = checked_child( tether_node, "atom0");
    unsigned int num = int_from_node( a0_node);
    spring_atom0 = atom_of_number( atoms, num);
  }

  check_mpole_consistency();
}

Charge Large_Molecule_Common::charge() const{
  return total_charge;
}

