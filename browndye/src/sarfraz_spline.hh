/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/


#ifndef __SARFRAZ_HH__
#define __SARFRAZ_HH__

/*
Implements a spline, based on rational functions, that is guaranteed to
be monotonically increasing.

Author(s): Sarfraz M
Source: COMPUTERS & GRAPHICS-UK    Volume: 27    Issue: 1    Pages: 107-121    
Published: FEB 2003  
*/

#include "units.hh"
#include "found.hh"

namespace Sarfraz{

template< class X, class F>
class Spline{
public:
  typedef typename UQuot< F, X>::Res DFDX;

  F value( const X&) const;
  DFDX deriv( const X&) const;

  void initialize( const Vector< X>&, const Vector< F>&);

private:

  Vector< X> x;
  Vector< F> f;
  Vector< DFDX> del;
  Vector< X> h;
  Vector< DFDX> d;

  typedef typename UQuot< DFDX, X>::Res D2FDX2;
  typedef typename UProd< X, X>::Res X2;
  typedef typename UProd< X, X2>::Res X3;

  typedef typename UProd< F, F>::Res F2;
  typedef typename UQuot< F2, X>::Res AType;

  Vector< AType> a1;
  Vector< AType> a2;

  Vector< DFDX> b12;

};

  typedef Vector< double>::size_type size_type;

//**************************************************************
template< class X, class F>
F Spline< X, F>::value( const X& xx) const{

  size_type i = found( x, xx);  
  if (xx == x[i])
    return f[i];
  else{
    double the = (xx - x[i])/h[i];
    if (x.size() == 2){
      double thec = 1.0 - the;
      return thec*f[0] + the*f[1];
    }
    else{
      AType num = the*(a1[i] + the*a2[i]);
      DFDX den = del[i] + b12[i]*the*(1.0 - the);
      return f[i] + num/den; 
    }
  }
}

//**************************************************************
template< class T>
typename UProd< T, T>::Res sq( const T& t){
  return t*t;
}

inline
size_type min( size_type a, size_type b){
  return a < b ? a : b;
}

template< class X, class F>
typename UQuot< F, X>::Res Spline< X, F>::deriv( const X& xx) const{
  if (x.size() == 2)
    return del[0];
  else{
    size_type i = found( x, xx);  
    
    if (xx == x[i])
      return d[i];
    else{
     
      double the = (xx - x[i])/h[i];
      AType num = the*(a1[i] + the*a2[i]);
      DFDX den = del[i] + b12[i]*the*(1.0 - the);
        
      AType dnum = 2.0*a2[i]*the + a1[i];
      DFDX dden = b12[i]*(1.0 - 2.0*the);
      return (den*dnum - num*dden)/(sq( den)*h[i]);
    } 
  }
}

//**************************************************************
template< class T>
class Gt{
public:
  bool operator()( const T& t0, const T& t1) const{
    return t0 > t1;
  }
};

template< class T>
class Ge{
public:
  bool operator()( const T& t0, const T& t1) const{
    return t0 >= t1;
  }
};


template< class T, class Comp>
void test_order( const Vector< T>& xx, const Comp& comp){
  auto n = xx.size();

  for( auto i = 0u; i < n-1; i++){
    if (comp( xx[i], xx[i+1])){
      for( auto k = 0u; k < n-1; k++)
	std::cerr << xx[k] << "\n";
      error( "Sarfraz::Spline::initialize: input not monotone");
    }
  }
}

  
const double tol = 1.0e-6;
  
template< class X, class F>
void Spline< X, F>::initialize( const Vector< X>& xs, const Vector< F>& fs){
  auto n = min( xs.size(), fs.size());

  if (n < 2)
    error( "Sarfraz::Spline::initialize: not enough points");

  test_order( xs, Ge<X>());
  test_order( fs, Gt<F>());

  x = xs;
  f = fs;

  if (n == 2){
    X hh = x[1] - x[0];
    DFDX ddel = (f[1] - f[0])/hh;
    del.push_back( ddel);
    d.push_back( ddel);
    h.push_back( hh);
  }
  else{ // n > 2
    h.resize( n-1);
    for( auto i = 0u; i < n-1; i++)
      h[i] = x[i+1] - x[i];

    del.resize( n-1);
    for( auto i = 0u; i < n-1; i++)
      del[i] = (f[i+1] - f[i])/h[i];

    {
      d.resize( n);
      for( auto i=1u; i < n-1; i++)
        d[i] = (f[i+1] - f[i-1])/(x[i+1] - x[i-1]);
      
      double hexp = h[0]/(h[0] + h[1]);
      d[0] = del[0] * (pow((del[0]/del[1]), hexp));
      
      {
        DFDX dd = 
          del[n-2] + (del[n-2] - del[n-3])*h[n-2]/(h[n-2] + h[n-3]);
        if (dd < DFDX( 0.0))
          d[n-1] = DFDX( 0.0);
        else
          d[n-1] = dd;
      }
    }

    typedef typename URecip< F>::Res Inv_F;
    typedef typename URecip< X>::Res Inv_X;

    Vector< DFDX> new_d = d;
    Vector< Inv_F> nu( n-1, Inv_F( 0.0));
    Vector< D2FDX2> mu( n-1, D2FDX2( 0.0));

    while (true){
      for (size_type i=0; i<n-1; i++)
        nu[i] = 1.0/(del[i]*h[i]);
                     
      for (size_type i=0; i<n-1; i++)
        mu[i] = del[i]/h[i];
          
      for (size_type i=1; i<n-1; i++){
        Inv_F alp = nu[i-1] + nu[i]; 
        Inv_X bet;
        {           
          Inv_X ome = 1.0/h[i-1] + 1.0/h[i];
          bet = ome - nu[i-1]*d[i-1] - nu[i]*d[i+1]; 
        }

        D2FDX2 gam = mu[i-1] + mu[i];
        new_d[i] = 
          (bet + (sqrt ((bet*bet) + 4.0*alp*gam)))/(2.0*alp);
      }
          
      typedef typename UProd< DFDX, DFDX>::Res DFDX2;

      DFDX2 diff2( 0.0);
      DFDX2 norm2( 0.0);
      for( auto i = 0u; i<n; i++){    
        diff2 = diff2 +  sq( (new_d[i] - d[i]));
        norm2 = norm2 + sq( d[i]);
      }
      
      for( auto k=0u; k<n; k++)
        d[k] = new_d[k];

      double err = sqrt( diff2/norm2);
      if (err < tol)
        break;
    }
    
    a1.resize( n-1);
    for( auto i = 0u; i<n-1; i++)
      a1[i] = del[i]*h[i]*d[i];

    a2.resize( n-1);
    for( auto i = 0u; i<n-1; i++)
      a2[i] =  del[i]*h[i]*(del[i]-d[i]);

    b12.resize( n-1);
    for( auto i = 0u; i<n-1; i++)
      b12[i] =  d[i+1] + d[i] - 2.0*del[i];
  }
}
}

#endif

/*******************************************************************/
/*
// test code

int main(){

  double x_data[8] = {
    0.000000,
    0.161273,
    0.285001,
    0.357961,
    0.500415,
    0.665266,
    0.848838,
    1.000000};
  
  
  double y_data[8] = {
    0.000000,
    0.001000, 
    0.016998,
    0.079492,
    0.499900,
    0.952105,
    0.999800,
    1.000000
  };
  
  Vector< Time> xs( 8);
  Vector< Length> ys( 8);

  for( auto i = 0u; i<8; i++){
    xs[i] = Time( x_data[i]);
    ys[i] = Length( y_data[i]);
  }

  Sarfraz::Spline< Time, Length> spl;
  spl.initialize( xs, ys);

  for( int i = 1; i<=99; i++){
    Time x( 0.01*i);
    Length y = spl.value( x);
    Velocity dy = spl.deriv( x);
    printf( "%f %f %f\n", fvalue(x), fvalue(y), fvalue(dy));    
  }

}
*/
