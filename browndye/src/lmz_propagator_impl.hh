/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

// Implements LMZ::Propagator class


namespace LMZ{

  const double pi2 = 2*pi;

  inline
  double max( double a, double b){
    return a > b ? a : b;
  }

  inline
  double min( double a, double b){
    return a < b ? a : b;
  }

  //*********************************************
  template< class I>
  double exponential_dist( typename I::Random_Number_Generator& rng){
    double U = I::uniform( rng);
    return -log( 1 - U);
  }

  //*********************************************
  // G. Marsaglia and W. W. Tsang (2000) 
  // t^(a-1) exp(-t)
  template< class I, class Rng>
  double gamma_dist( Rng& rng, double alpha){
    if (0.99 < alpha && alpha < 1.01)
      return exponential_dist<I>( rng);

    else if (alpha < 1.0){
      double gam = gamma_dist<I>( rng, alpha + 1.0);
      double U = I::uniform( rng);
      return gam*pow( U, 1.0/alpha);
    }

    else{
      double d = alpha - 1.0/3.0;
    
      double v;
      bool done;
      do {
        double x = I::gaussian( rng);
        double U = I::uniform( rng);
        double crv = 1 + x/sqrt( 9*d);
        v = crv*crv*crv;
      
        done = log( U) < 0.5*x*x + d - d*v + d*log(v);
      }  
      while (!done);
    
      return d*v;
    }
  }

  //*********************************************
  template< class I>
  void Theta_Distribution<I>::get_sample_old( RNG& rng, double& theta, Time& time) const{

    double p = rng.uniform();
    theta = cml_prob.value( p);

    Time mean = mean_time.value( theta);
    Time sdev = sdev_time.value( theta);

    Time2 s2 = sdev*sdev;
    double alpha = mean*mean/s2;
    Time lambda = s2/mean;
  
    time = lambda*gamma_dist( rng, alpha);
  }

  template< class I>
  void Theta_Distribution<I>::get_sample( RNG& rng, double& theta, Time& time) const{

    double p = rng.uniform();
    theta = max( min( cml_prob.value( p), max_theta), 0.0);

    Time mean = mean_time.value( theta);
    Time sdev = sdev_time.value( theta);

    Time2 s2 = sdev*sdev;
    double alpha = mean*mean/s2 + 2;
    Inv_Time lambda = 1.0/(mean*(alpha - 1));
  
    Inv_Time inv_time = lambda*gamma_dist<I>( rng, alpha);
    time = 1.0/inv_time;
  }

  //*********************************************
  typedef std::list< JP::Node_Ptr> NList;

  template< class Unit>
  void get_data( NList& nodes, const std::string& ioro_tag, 
                 const std::string& tag, Vector< Unit>& data){
  
    typedef typename Vector< Unit>::size_type size_type;
    size_type n = 0;
    for( NList::iterator itr = nodes.begin(); itr != nodes.end(); ++itr){
      auto node = *itr;
      auto io_node = node->child( ioro_tag);
      if (io_node != NULL)
        ++n;
      else
        break;
    }
    data.resize( n);
    NList::iterator itr = nodes.begin();
    for( size_type i = 0; i<n; i++){
      auto node = *itr;
      auto io_node = node->child( ioro_tag);

      data[i] = Unit( double_from_node( io_node, tag));
      ++itr;
    }
  }

  //*********************************************
  // completed node labeled inner-distr
  template< class I>
  void Theta_Distribution<I>::initialize( JP::Node_Ptr node, 
					  const std::string& ioro_tag,
                                          const Vector< double>& thetas){

    auto sub_nodes = node->children_of_tag( "sub-distr");

    get_data( sub_nodes, ioro_tag, "cumul-prob", cml_prob_data);
    get_data( sub_nodes, ioro_tag, "mean-t", mean_time_data);
    get_data( sub_nodes, ioro_tag, "sdev-t", sdev_time_data);
  
    cml_prob.initialize( cml_prob_data, thetas);

    mean_time.initialize( thetas, mean_time_data);  
    sdev_time.initialize( thetas, sdev_time_data);

    max_theta = 0.999*thetas[ mean_time.size()-1];
  }


  //*********************************************
  // completed node labeled inner-distr
  template< class I>
  void Shell_Distribution<I>::initialize( JP::Node_Ptr node){
    prob_inner = double_from_node( node, "prob-inner");

    NList sub_nodes = node->children_of_tag( "sub-distr");
    size_type nth = sub_nodes.size();
    thetas.resize( nth);

    size_type i = 0;
    for( auto itr = sub_nodes.begin(); itr != sub_nodes.end(); ++itr){
      auto node = *itr;
      thetas[i] = fvalue( double_from_node( node, "theta"));
      ++i;
    }
  
    inner_dist.initialize( node, "inner", thetas);
    outer_dist.initialize( node, "outer", thetas);
  }

  //*********************************************
  template< class I>
  void Shell_Distribution<I>::propagate( RNG& rng, double theta0, double phi0, 
                                         double& theta1, double& phi1,
                                         In_Or_Out& in_or_out, Time& time) const{

    const Theta_Distribution<I>* dist;
    if (rng.uniform() < prob_inner){
      dist = &inner_dist;
      in_or_out = In;
    }
    else{
      dist = &outer_dist;
      in_or_out = Out;
    }

    Time t;
    double theta;
    dist->get_sample( rng, theta, t);
    double phi = pi2*rng.uniform();

    Vec3< double> axis2;
    axis2[0] = cos( phi0)*sin( theta0);
    axis2[1] = sin( phi0)*sin( theta0);
    axis2[2] = cos( theta0);

    Vec3< double> axis0, axis1;
    L::get_perp_axes( axis2, axis0, axis1);

    Vec3< double> u;
    L::get_scaled( cos( phi)*sin( theta), axis0, u);
    L::add_scaled( sin( phi)*sin( theta), axis1, u);
    L::add_scaled( cos( theta), axis2, u);

    phi1 = atan2( u[1], u[0]);
    theta1 = acos( u[2]);
    time += t;
  }


  //*********************************************
  // completed node
  template< class I>
  void Propagator<I>::initialize( JP::Node_Ptr node){

    get_checked_double_from_node( node, "inner-radius", inner_radius);
    get_checked_double_from_node( node, "charge0", q0);
    get_checked_double_from_node( node, "charge1", q1);
    get_checked_double_from_node( node, "hydro-radius0", a0);
    get_checked_double_from_node( node, "hydro-radius1", a1);
    get_checked_double_from_node( node, "vacuum-permittivity", vperm);
    get_checked_double_from_node( node, "dielectric", diel);
    get_checked_double_from_node( node, "debye-length", dL);
    get_checked_double_from_node( node, "rotational-diffusivity0", rdiff0);
    get_checked_double_from_node( node, "rotational-diffusivity1", rdiff1);
    get_checked_double_from_node( node, "viscosity", mu);


    {
      auto nhnode = node->child( "hydrodynamic-interactions");
      if (nhnode != NULL){
        typedef std::string String;
        String res = string_from_node( nhnode); 
        if (res == String( "false") || res == String( "False") || 
            res == String( "FALSE"))
          hi = false;        
        else
          hi = true;
      }
    }

    {
      auto nhnode = node->child( "hemisphere");
      if (nhnode != NULL){
        typedef std::string String;
        String res = string_from_node( nhnode); 
        if (res == String( "true") || res == String( "True") || 
            res == String( "TRUE"))
          hemisphere = true;        
        else
          hemisphere = false;
      }
    }

    NList inodes = node->children_of_tag( "inner-distr");
    shell_dists.resize( inodes.size());
  
    size_type i = 0;
    for( NList::iterator itr = inodes.begin(); itr != inodes.end(); ++itr){
      auto inode = *itr;
      shell_dists[i].initialize( inode);
      ++i;
    }

    auto onode = checked_child( node, "outer-distr");
    outer_prob_return = double_from_node( onode, "prob-return");
  }

  //*********************************************
  // time is added to
  template< class I>
  void Propagator<I>::propagate( RNG& rng, 
                                 const Vec3< Length>& r0b, 
                                 const Mat3< double>& rot0b, 
                                 const Vec3< Length>& r1b, 
                                 const Mat3< double>& rot1b, 
                                 bool& escaped, 
                                 Mat3< double>& rot0, 
                                 Vec3< Length>& r1, 
                                 Mat3< double>& rot1,
                                 Time& time) const{

    escaped = false;
    size_type nsh = shell_dists.size();

    Vec3< Length> r;
    L::get_diff( r1b, r0b, r);
    Vec3< double> u;
    L::get_normed( r, u);
  
    double thetab = acos( u[2]);
    double phib = atan2( u[1], u[0]);

    size_type ish = 0;
    Time t( 0.0);

    double thetae, phie;

    while (true){
      In_Or_Out ioro;
      if (ish == nsh){
        if (rng.uniform() < outer_prob_return){
          t = Time( INFINITY);
          --ish;

          if (nsh == 0){
            double tau0( INFINITY);
            double tau1( INFINITY);

            if (!hemisphere)
              add_diffusional_rotation<I>( rng, tau0, rot0b, rot0);
            else
              rot0 = rot0b;

            add_diffusional_rotation<I>( rng, tau1, rot1b, rot1);
          
            Vec3< double> u;
            Vec3< Length> new_r;   
            get_random_unit_vector<I>( rng, u);
            L::get_scaled( inner_radius, u, new_r);

            L::get_sum( r0b, new_r, r1);
            break;         
          }

        }
        else {
          ++ish;
          escaped = true;

          break;
        }
      }

      shell_dists[ ish].propagate( rng, thetab, phib, thetae, phie, ioro, t);

      thetab = thetae;
      phib = phie;

      if (ioro == In){
     
        if (ish == 0){
          escaped = false;
          double tau0 = rdiff0*t;
          double tau1 = rdiff1*t;

          if (!hemisphere)
            add_diffusional_rotation<I>( rng, tau0, rot0b, rot0);
          else
            rot0 = rot0b;

          add_diffusional_rotation<I>( rng, tau1, rot1b, rot1);
        
          Vec3< Length> new_r;
          double sth = sin( thetae);
        
          new_r[0] = inner_radius*sth*cos( phie);
          new_r[1] = inner_radius*sth*sin( phie);
          new_r[2] = inner_radius*cos( thetae);

          if (hemisphere)
            new_r[2] = fabs( new_r[2]);
        
          L::get_sum( r0b, new_r, r1);
          break;
        }
        else
          --ish;
      }
      else{  // ioro == Out
        ++ish;
      }
    }
    time += t;
  }

  template< class I>
  auto Propagator<I>::radial_force( Length r) const -> Force{
    return (q0*q1*exp(-r/dL)/(4.0*pi*vperm*diel))*(1.0/(dL*r) + 1.0/(r*r));
  }

  template< class I>
  auto Propagator<I>::radial_mobility( Length r) const -> Mobility{
    if (hi)
      return Rotne_Prager::parallel_mobility( mu, a0, a1, r); 
    else
      return Rotne_Prager::no_hi_mobility( mu, a0, a1);
  }

  template< class I>
  auto Propagator<I>::perpendicular_mobility( Length r) const -> Mobility{
    if (hi)
      return Rotne_Prager::perpendicular_mobility( mu, a0, a1, r); 
    else
      return Rotne_Prager::no_hi_mobility( mu, a0, a1);
  }

  template< class I>
  auto Propagator<I>::rotational_diffusivity0() const -> Inv_Time{
    return rdiff0;
  }

  template< class I>
  auto Propagator<I>::rotational_diffusivity1() const -> Inv_Time{
    return rdiff1;
  }

  template< class I>
  bool Propagator<I>::has_hemisphere() const{
    return hemisphere;
  }

}

//****************************************************************************
// test code
/*
  int main(){
  LMZ::Propagator prop;
  std::ifstream input( "bull.xml");
  JP::Parser parser( input);
  parser.complete_current_node();
  
  prop.initialize( parser.current_node());

  Browndye_RNG rng; 
  rng.set_seed( 1111127);

  const Length r0b[3] = {0.0, 0.0, 0.0}; 
  const double rot0b[3][3] = 
  { {1.0, 0.0, 0.0}, {0.0, 1.0, 0.0}, {0.0, 0.0, 1.0}};

  const Length r1b[3] = {0.0, 0.0, 11.0}; 
  const double rot1b[3][3] = 
  { {1.0, 0.0, 0.0}, {0.0, 1.0, 0.0}, {0.0, 0.0, 1.0}};

  bool escaped; 
  double rot0[3][3]; 
  Length r1[3]; 
  double rot1[3][3];
  Time time( 0.0);
  
  prop.propagate( rng, r0b, rot0b, r1b, rot1b, escaped, rot0, r1, rot1, time);

  printf( "escaped %d time %g\n", escaped, time);
  printf( "r1 %g %g %g\n", r1[0], r1[1], r1[2]);

  }
*/


/*
  void Propagator::propagate( Browndye_RNG& rng, 
  const Length r0b[], const double rot0b[][3], 
  const Length r1b[], const double rot1b[][3], 
  bool& escaped, double rot0[][3], Length r1[], double rot1[][3],
  Time& time) const{


*/

/*
  int main(){
  Browndye_RNG rng;
  rng.set_seed( 111111);

  int n = 100000;
  double sum = 0.0;
  double sum2 = 0.0;
  for (int i = 0; i<n; i++){
  double x = gamma_dist( rng, 0.5);
  sum += x;
  sum2 += x*x;
  }
  double mean = sum/n;
  double sdev = (sum2/n - mean*mean);

  printf( "mean %g sdev %g\n", mean, sdev);

  }
*/
