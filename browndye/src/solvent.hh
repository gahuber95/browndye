/*
 * solvent.hh
 *
 *  Created on: Sep 11, 2015
 *      Author: ghuber
 */

#ifndef SOLVENT_HH_
#define SOLVENT_HH_

#include "units.hh"

struct Solvent{
	Length debye_length;
	double dielectric;
	double relative_viscosity;
	Energy kT;

	Solvent(){
		debye_length = Length( INFINITY);
		dielectric = 1.0;
		relative_viscosity = 1.0;
		kT = Energy( 1.0);
	}
};


#endif /* SOLVENT_HH_ */
