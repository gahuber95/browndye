/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
NAM_Simulator reads in the data, carries out a one-trajectory-at-a-time
simulation, and outputs data.  Also takes care of the multithreading. 
(NAM stands for Northrup-Allison-McCammon, even though their
original algorithm is not used here.
*/

#ifndef __NAM_SIMULATOR_HH__
#define __NAM_SIMULATOR_HH__

#include "simulator.hh"
#include "nam_simulator_pre.hh"

class NAM_Simulator: public Simulator{
public:
  NAM_Simulator();

  void initialize( const std::string& file);

  void set_number_of_trajectories( unsigned int);

  void set_n_steps_per_output( unsigned int);
  void run();
  ~NAM_Simulator();

private:
  friend class NAM_Multi_Interface;

  void run_individual( Molecule_Pair&);

  unsigned long int n_trajs;
  unsigned long int n_stuck;
  unsigned long int n_escaped;
  unsigned long int n_steps_per_output;
  unsigned long int max_n_steps;
  unsigned long int n_trajs_per_output;
  std::string traj_file_name;
  bool do_output_rng_state;
  Multithread::Mover< NAM_Multi_Interface> multi_mover;
  pthread_mutex_t mutex;

  unsigned long int n_trajs_completed;
  unsigned long int n_trajs_started;
  Vector< unsigned long int> completed_rxns;  
  std::string min_rxn_coord_name;

  void print_state( const Molecule_Pair&, 
                    std::ofstream&, bool state_change ) const;
};

//**************************************************
template< class Func>
void NAM_Multi_Interface::apply_to_object_refs( Func& f, NAM_Simulator* sim){
  for( std::list< Molecule_Pair>::iterator itr = sim->pairs.begin();
       itr != sim->pairs.end(); ++itr){
    f( &(*itr));
  }
} 

inline
unsigned int NAM_Multi_Interface::size( NAM_Simulator* sim){
  return sim->pairs.size();
}

/* 

Input:

information for Molecule_Pair

number of trajectories
number of steps per output
maximum number of steps
output file
optional random number seed

Output:

number reacted vs. number of trajectories

*/


#endif
