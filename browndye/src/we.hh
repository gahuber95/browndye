/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
  This implements the Weighted_Ensemble_Dynamics::Sampler class, which
  is used to carry out weighed ensemble simulations.  Information about
  this class, including a description of the Interface class, is found
  in doc/we.html.

  For the multithreaded version, see "we_multithreaded.hh".
*/

#include <stdio.h>
#include <iostream>
#include <math.h>
#include "vector.hh"
#include <list>
#include <queue>
#include <limits>
#include <algorithm>
#include <memory>
#include "error_msg.hh"

namespace Weighted_Ensemble_Dynamics{

  typedef unsigned int uint;

  const uint max_int = std::numeric_limits< uint>::max();

  inline
  double round( double a){
    double low = floor( a);
    if (a > low + 0.5) 
      return low + 1.0;
    else
      return low;
  }

  template< class Interface>
  class Sampler{

  public:
    typedef typename Interface::Info Info;
    typedef typename Interface::Object Object;

    void set_user_info( Info);

    void set_number_of_objects( uint);
    void set_number_of_fluxes( uint);

    void set_number_of_moves_per_output( uint);

    void initialize_objects();
    void step_objects_forward();
    void update_fluxes();
    void renormalize_objects();
    void renormalize_objects_1D();

    // Function: f( Object, double);
    template< class Function>
    void apply_to_weighted_copies( Function& f);
  
    Sampler();
    ~Sampler();

    void generate_bins( std::list< double>&);

    Info user_info(){
      return info;
    }

  private: 
    static
    Object new_object( Info);
  
    static
    void initialize_object( Info, Object);

    static
    void step_objects_forward( Info);

    static
    void get_crossing( Info, Object, bool&, unsigned int&);

    static
    double uniform( Info);

    static
    uint bin( Info, Object, bool& exists);

    static
    uint bin( Info, Object);

    static
    double weight( Info, Object);

    static
    void set_weight( Info, double, Object);

    static
    uint number_of_bins( Info);

    static
    void get_duplicated_objects( Info, Object, uint n_copies, 
                                 std::list< Object>&);
  
    static
    void remove( Info, Object);

    static
    bool is_null( Info);

    static
    void set_null( Info&);

    static
    uint number_of_bin_neighbors( Info, uint);

    static
    bool have_population_reversal( Info, uint, double, uint, double);

    // f( uint)
    template< class Function>
    static
    void apply_to_bin_neighbors( Info, uint, Function&);

    static
    double reaction_coordinate( Info, Object);

    //################################
    class Obj_Wrapper{
    public:
      Obj_Wrapper( Info _info, Object _obj): info(_info),obj(_obj){}
      ~Obj_Wrapper(){
        remove( info, obj);
      }

      Info info;
      Object obj;
    };

    class Weighted_Object{
    public:      
      //RC_Ptr< Obj_Wrapper> rcobj;
      std::shared_ptr< Obj_Wrapper> rcobj;
      double weight;
      uint bin;

      Object object(){
        return rcobj->obj;
      }

      Weighted_Object( Info info, Object obj){
        rcobj.reset( new Obj_Wrapper( info, obj));
        bool exists;
        bin = Interface::bin( info, obj, exists);
        if (!exists)
          bin = max_int;
        weight = Interface::weight( info, obj);
      }
    };

    bool is_building_bins;

    std::list< Weighted_Object> wobjects;
    uint n_objects, n_fluxes;

    Info info;

    double time;
    uint n_steps_per_output;
    unsigned int istep;
    Vector< double> fluxes;

    typedef typename std::list< Weighted_Object>::iterator Obj_Itr;

    class Bin_Neighbor_Getter{
    public:
      uint n_nebors;
      Vector< int> nebors;

      Bin_Neighbor_Getter(){
        n_nebors = 0;
      }

      void operator()( uint bin){
        nebors[ n_nebors] = bin;
        ++n_nebors;
      }

      void setup( Info info, uint ib){
        uint num_nebors = Interface::number_of_bin_neighbors( info, ib);
        nebors.resize( num_nebors);
        n_nebors = 0;
        Interface::apply_to_bin_neighbors( info, ib, *this);
      }

    };

    class Object_Comparer{
    public:
      bool operator()( const Weighted_Object& obj0, const Weighted_Object& obj1){
        if (obj0.bin == obj1.bin)
          return obj0.weight < obj1.weight;
        else
          return obj0.bin < obj1.bin;
      }
    };

    void check_inputs();

  };

  //#########################################################
  template< class Interface>
  void Sampler< Interface>::set_number_of_moves_per_output( uint n){
    n_steps_per_output = n;
  }


  template< class Interface>
  typename Interface::Object 
  Sampler< Interface>::new_object( Info info){
    return Interface::new_object( info);
  }

  template< class Interface>
  void Sampler< Interface>::set_weight( Info info, double wt, Object obj){
    Interface::set_weight( info, wt, obj);
  }

  template< class Interface>
  double Sampler< Interface>::weight( Info info, Object obj){
    return Interface::set_weight( info, obj);
  }

  template< class Interface>
  void Sampler< Interface>::get_duplicated_objects( Info info, Object obj, 
                                                    uint n_copies, 
                                                    std::list< Object>& dups){
    Interface::get_duplicated_objects( info, obj, n_copies, dups);
  }


  template< class Interface>
  void Sampler< Interface>::initialize_object( Info info, Object obj){
    Interface::initialize_object( info, obj);
  }
                        
  template< class Interface>
  void Sampler< Interface>::get_crossing( Info info, Object obj, 
                                          bool& crossed, unsigned int& crossing){
    Interface::get_crossing( info, obj, crossed, crossing);
  }
  
  template< class Interface>
  void Sampler< Interface>::step_objects_forward( Info info){
    Interface::step_objects_forward( info);
  }
    
  template< class Interface>
  uint Sampler< Interface>::bin( Info info, Object obj, bool& exists){
    return Interface::bin( info, obj, exists);
  }      

  template< class Interface>
  uint Sampler< Interface>::bin( Info info, Object obj){
    return Interface::bin( info, obj);
  }      



  template< class Interface>
  uint Sampler< Interface>::number_of_bins( Info info){
    return Interface::number_of_bins( info);
  }
  

  template< class Interface>
  void Sampler< Interface>::remove( Info info, Object obj){
    Interface::remove( info, obj);
  }

  template< class Interface>
  void Sampler< Interface>::set_null( Info& info){
    Interface::set_null( info);
  }


  template< class Interface>
  bool Sampler< Interface>::is_null( Info info){
    return Interface::is_null( info);
  }

  template< class Interface>
  uint Sampler< Interface>::number_of_bin_neighbors( Info info, uint ib){
    return Interface::number_of_bin_neighbors( info, ib);
  }

  template< class Interface>
  bool Sampler< Interface>::have_population_reversal( Info info, uint ib0, double wt0, 
                                                      uint ib1, double wt1){
    return Interface::have_population_reversal( info, ib0, wt0, ib1, wt1);
  }

  // f( uint)
  template< class Interface>
  template< class Function>
  void Sampler< Interface>::apply_to_bin_neighbors( Info info, uint ib, Function& f){
    Interface:: apply_to_bin_neighbors( info, ib, f);
  }


  template< class Interface>
  double Sampler< Interface>::uniform( Info info){
    return Interface::uniform( info);
  }
  
  template< class Interface>
  double Sampler< Interface>::reaction_coordinate( Info info, Object obj){
    return Interface::reaction_coordinate( info, obj);
  }

  //################################################################
  template< class Interface>
  void Sampler< Interface>::set_number_of_objects( uint n){
    n_objects = n;
  }
  
  template< class Interface>
  void Sampler< Interface>::set_number_of_fluxes( uint n){
    n_fluxes = n;
    fluxes.resize( n, 0.0);
  }

  template< class Interface>
  void Sampler< Interface>::set_user_info( Info _info){
    info = _info;
  }

  template< class Interface>
  void Sampler< Interface>::update_fluxes(){

    for( Obj_Itr itr = wobjects.begin(); itr != wobjects.end(); ++itr){
      Weighted_Object& wobj = *itr;
      Object obj = wobj.object();

      bool crossed;
      unsigned int crossing = std::numeric_limits< unsigned int>::max();
      get_crossing( info, obj, crossed, crossing);
      if (crossed){
        fluxes[ crossing] += wobj.weight;
      }
    }

    if (istep == n_steps_per_output){
      for( unsigned int i=0; i<n_fluxes; i++){
        Interface::output( info, i, fluxes[i]);
      }
    
      for( unsigned int i=0; i<n_fluxes; i++)
        fluxes[i] = 0.0;
      istep = 0;
    }
    else
      ++istep;
  }

  template< class Interface>
  void Sampler< Interface>::renormalize_objects_1D(){
    check_inputs();

    // lump bins if needed
    uint n_bins = number_of_bins( info);

    Vector< double> bin_wts( n_bins, 0.0);

    for( Obj_Itr itr = wobjects.begin(); itr != wobjects.end(); ++itr){
      Weighted_Object& wobj = *itr;
      Object obj = wobj.object();
      uint ib = bin( info, obj);
      bin_wts[ib] += wobj.weight;
    }

    // figure out which bins are centers of lumped bins
    Vector< bool> is_forward_max( n_bins, false);
    uint n_lbins = 1;
    {
      double max_wt = bin_wts[0];
      is_forward_max[0] = true;
      for (uint i = 1; i < n_bins; i++){
        if (bin_wts[i] > max_wt){
          is_forward_max[i] = true;
          max_wt = bin_wts[i];
          ++n_lbins;
        }
      }
    }
  
    Vector< uint> lbins( n_lbins);
    {
      uint i_lbin = 0;
      for (uint i = 0; i < n_bins; i++){
        if (is_forward_max[i]){
          lbins[i_lbin] = i;
          ++i_lbin;
        }
      }
    }
  
    Vector< uint> bot_lbin( n_lbins+1);
    {
      bot_lbin[0] = 0;
      bot_lbin[n_lbins] = n_bins;
      for (uint i = 1; i < n_lbins; i++){
        bot_lbin[i] = (lbins[i-1] + lbins[i])/2 + 1;
      }
    }
  
    Vector< uint> lbin_info( n_bins, max_int);
    for (uint ilb = 0; ilb < n_lbins; ++ilb){
      for (uint i = bot_lbin[ilb]; i < bot_lbin[ilb+1]; i++)
        lbin_info[i] = ilb;
    }
  
    // assign each object a lumped bin
    for( Obj_Itr itr = wobjects.begin(); itr != wobjects.end(); ++itr){
      Weighted_Object& wobj = *itr;
      uint ibin = bin( info, wobj.object());
      wobj.bin = lbin_info[ ibin];
    }

    // ordered increasing according to bin, then weight
    wobjects.sort( Object_Comparer());

    Vector< uint> n_in_lbins( n_lbins, 0);
    Vector< double> lbin_wts( n_lbins, 0.0);

    for( Obj_Itr itr = wobjects.begin(); itr != wobjects.end(); ++itr){
      Weighted_Object& wobj = *itr;
      n_in_lbins[ wobj.bin] += 1;
      lbin_wts[ wobj.bin] += wobj.weight;
    }

    Vector< uint> nbins_in_lbin( n_lbins, 0);
    for( unsigned int i=0; i<n_bins; i++){
      ++(nbins_in_lbin[ lbin_info[i]]);
    }

    Obj_Itr begin_itr;
    Obj_Itr end_itr = wobjects.begin();
    for( unsigned int ib=0; ib<n_lbins; ib++){
      const uint n_in_lbin = n_in_lbins[ib];

      if (n_in_lbin == 0)
        continue;

      double ideal_n = std::max( 1.0, ((double)n_objects*nbins_in_lbin[ib])/n_bins);
      double ideal_wt = lbin_wts[ib]/ideal_n;

      begin_itr = end_itr;
      for( unsigned int i=0; i<n_in_lbin; i++) ++end_itr;

      std::list< Weighted_Object> wobjects_bin;
      wobjects_bin.splice( wobjects_bin.end(), wobjects, begin_itr, end_itr);
      Obj_Itr itrb = wobjects_bin.begin();

      // Combine small copies
      bool small_done = false;
      while(!small_done){
        const uint n_in_lbin_cur = wobjects_bin.size(); 
        if (n_in_lbin_cur < 2){
          small_done = true;
          break;
        }

        Vector< double> wts( n_in_lbin_cur);
        Vector< Obj_Itr> itrs( n_in_lbin_cur, wobjects_bin.end());
        uint nwts = 0;

        // Create next combined copy
        bool done = false;
        double totwt = 0.0;
        while (!done){
          double wt = itrb->weight;

          if (wt < 0.5*ideal_wt){
            totwt += wt;
            wts[nwts] = wt;
            itrs[nwts] = itrb;
            ++nwts;
            ++itrb;
            if (totwt > ideal_wt)
              done = true;
          }
          else{
            small_done = true;
            done = true;
            if (totwt + wt < 1.5*ideal_wt){
              totwt += wt;
              wts[nwts] = wt;
              itrs[nwts] = itrb;
              ++nwts;
              ++itrb;
            }
          }

          if (nwts == n_in_lbin_cur)
            done = true;
        }
      
        // Cumulative weights
        if (nwts > 0){
          Vector< double> cwts( nwts);
          cwts[0] = wts[0];
          for( unsigned int iwt=1; iwt<nwts; iwt++)
            cwts[ iwt] = cwts[ iwt-1] + wts[ iwt];
          for( unsigned int iwt=0; iwt<nwts; iwt++)
            cwts[ iwt] /= totwt;

          uint ichoice = 0;
          double choice = uniform( info);
          while( cwts[ichoice] < choice) ++ichoice;
          itrb = itrs[ ichoice];
          itrb->weight = totwt;
          set_weight( info, totwt, itrb->object());
        
          if (ichoice > 0){
            wobjects_bin.erase( itrs[0], itrs[ichoice]);
          }
          if (ichoice < nwts-1){
            Obj_Itr eitr = itrs[ nwts-1];
            ++eitr;
            wobjects_bin.erase( itrs[ ichoice+1], eitr);
          }
        
          ++itrb;
          wobjects_bin.sort( Object_Comparer());
        }
      }
    
      for (Obj_Itr itr = wobjects_bin.begin(); itr != wobjects_bin.end(); ++itr){
        Weighted_Object& wobj = *itr;
        if (wobj.weight > 1.5*ideal_wt){
          uint n_copies = (uint)(round( wobj.weight/ideal_wt))-1;
        
          wobj.weight /= (n_copies+1);
        
          typedef std::list< Object> Obj_List;
          Obj_List objs;
          get_duplicated_objects( info, wobj.object(), n_copies, objs);
          for( typename Obj_List::iterator oitr = objs.begin(); oitr != objs.end(); ++oitr){
            Object obj = *oitr;
            Weighted_Object new_wobj( info, obj);
            wobjects_bin.insert( itr, new_wobj);
          }
        }
      }
      wobjects.splice( end_itr, wobjects_bin, wobjects_bin.begin(),
                       wobjects_bin.end());
    }
    
  }
  
  
  template< class Interface>
  void Sampler< Interface>::renormalize_objects(){
    //printf( "begin renormalize\n");
    check_inputs();
  
    // lump bins if needed
    uint n_bins = number_of_bins( info);
  
    std::queue< uint> queu;

    Vector< double> bin_wts( n_bins, 0.0);

    for( Obj_Itr itr = wobjects.begin(); itr != wobjects.end(); ++itr){
      Weighted_Object& wobj = *itr;
      Object obj = wobj.object();
      uint ib = bin( info, obj);
      bin_wts[ib] += wobj.weight;
    }

    // figure out which bins are centers of lumped bins
    Vector< uint> lbin_info( n_bins, max_int);
    uint n_lbins = 1;

    for (uint ib = 0; ib < n_bins; ib++){
      Bin_Neighbor_Getter bng;
      bng.setup( info, ib);
      uint n_nebors = bng.n_nebors;
    
      if (bin_wts[ib] > 0.0){
        bool is_main_bin = true;
        for( unsigned int knb=0; knb < n_nebors; knb++){
          uint inb = bng.nebors[ knb];
          if (have_population_reversal( info,
                                        ib, bin_wts[ib], 
                                        inb, bin_wts[inb])){
            is_main_bin = false;
            break;
          }
        }
        if (is_main_bin){
          lbin_info[ib] = n_lbins;
          ++n_lbins;
        }
      }
    }
  
    // assign bins to lumped bins
    lbin_info[0] = 0;
    for (uint ib = 0; ib < n_bins; ib++){
      if (lbin_info[ib] != max_int){
        queu.push( ib);
      }
    }
  
    while( !queu.empty()){
      uint ib = queu.front();
      queu.pop();
    
      Bin_Neighbor_Getter bng;
      bng.setup( info, ib);
      uint n_nebors = bng.n_nebors;
    
      for( unsigned int knb=0; knb < n_nebors; knb++){
        uint inb = bng.nebors[ knb];
        if (lbin_info[inb] == max_int){
          lbin_info[inb] = lbin_info[ib];
          queu.push( inb);
        }
      }           
    }   
  
    // assign each object a lumped bin
    for( Obj_Itr itr = wobjects.begin(); itr != wobjects.end(); ++itr){
      Weighted_Object& wobj = *itr;
      uint ibin = bin( info, wobj.object());
      wobj.bin = lbin_info[ ibin];
    }    

    // ordered increasing according to bin, then weight
    wobjects.sort( Object_Comparer());

    Vector< uint> n_in_lbins( n_lbins, 0);
    Vector< double> lbin_wts( n_lbins, 0.0);

    for( Obj_Itr itr = wobjects.begin(); itr != wobjects.end(); ++itr){
      Weighted_Object& wobj = *itr;
      n_in_lbins[ wobj.bin] += 1;
      lbin_wts[ wobj.bin] += wobj.weight;
    }

    Vector< uint> nbins_in_lbin( n_lbins, 0);
    for( unsigned int i=0; i<n_bins; i++){
      ++(nbins_in_lbin[ lbin_info[i]]);
    }

    Obj_Itr begin_itr;
    Obj_Itr end_itr = wobjects.begin();
    for( unsigned int ib=0; ib<n_lbins; ib++){
      uint n_in_lbin = n_in_lbins[ib];

      if (n_in_lbin == 0)
        continue;

      double ideal_n = std::max( 1.0, ((double)n_objects*nbins_in_lbin[ib])/n_bins);
      double ideal_wt = lbin_wts[ib]/ideal_n;


      begin_itr = end_itr;
      for( unsigned int i=0; i<n_in_lbin; i++) ++end_itr;
    
      std::list< Weighted_Object> wobjects_bin;
      wobjects_bin.splice( wobjects_bin.end(), wobjects, begin_itr, end_itr);
      Obj_Itr itrb = wobjects_bin.begin();

      // Combine small copies 
      bool small_done = false;
      while(!small_done){      

        Vector< double> wts( n_in_lbin);
        Vector< Obj_Itr> itrs( n_in_lbin, wobjects_bin.end());
        uint nwts = 0;
      
        // Create next combined copy
        bool done = false;
        double totwt = 0.0;
        while (!done){
          double wt = itrb->weight;
          if (wt < 0.5*ideal_wt){
            totwt += wt;
            wts[nwts] = wt;
            itrs[nwts] = itrb;
            ++nwts;
            ++itrb;
            if (totwt > ideal_wt)
              done = true;
          }
          else{
            small_done = true;
            done = true;
            if (totwt + wt < 1.5*ideal_wt){
              totwt += wt;
              wts[nwts] = wt;
              itrs[nwts] = itrb;
              ++nwts;
              ++itrb;
            }
          }
          if (nwts == n_in_lbin)
            small_done = true;
        }

        // Cumulative weights
        if (nwts > 0){
          Vector< double> cwts( nwts);
          cwts[0] = wts[0];
          for( unsigned int iwt=1; iwt<nwts; iwt++)
            cwts[ iwt] = cwts[ iwt-1] + wts[ iwt];
          for( unsigned int iwt=0; iwt<nwts; iwt++)
            cwts[ iwt] /= totwt;
        
          uint ichoice = 0;
          double choice = uniform( info);
          while( cwts[ichoice] < choice) ++ichoice;
          itrb = itrs[ ichoice];
          itrb->weight = totwt;
          set_weight( info, totwt, itrb->object());

          if (ichoice > 0)
            wobjects_bin.erase( itrs[0], itrs[ichoice]);
          if (ichoice < nwts-1){
            Obj_Itr eitr = itrs[ nwts-1];
            ++eitr;
            wobjects_bin.erase( itrs[ ichoice+1], eitr);
          }
        
          ++itrb;
          wobjects_bin.sort( Object_Comparer());
        }
      }

      for (Obj_Itr itr = wobjects_bin.begin(); itr != wobjects_bin.end(); ++itr){
        Weighted_Object& wobj = *itr;
        if (wobj.weight > 1.5*ideal_wt){
          uint n_copies = (uint)(round( wobj.weight/ideal_wt))-1;
        
          wobj.weight /= (n_copies+1);

          typedef std::list< Object> Obj_List;
          Obj_List objs;
          get_duplicated_objects( info, wobj.object(), n_copies, objs);
          for( typename Obj_List::iterator oitr = objs.begin();
               oitr != objs.end(); ++oitr){
            Object obj = *oitr;
            Weighted_Object new_wobj( info, obj);
            wobjects_bin.insert( itr, new_wobj);
          }
        }
      }
      wobjects.splice( end_itr, wobjects_bin, wobjects_bin.begin(),
                       wobjects_bin.end());
    }
  }
  
  // constructor
  template< class Interface>
  Sampler< Interface>::Sampler(){
    n_objects = max_int;

    n_fluxes = 1;
    fluxes.resize( 1);
    fluxes[0] = 0.0;

    set_null( info);
    n_steps_per_output = 1;
    istep = 0;

    is_building_bins = false;
  }

  // destructor
  template< class Interface>
  Sampler< Interface>::~Sampler(){}

  template< class Interface>
  void Sampler< Interface>::check_inputs(){
    if (n_objects == max_int)
      error( "number of objects not set");

    if (is_null( info))
      error( "Sampler: user info not set");
  }

  template< class Interface>
  void Sampler< Interface>::initialize_objects(){

    if (n_objects == max_int)
      error( "initialize_objects: Need to set number of objects");

    if (is_null( info))
      error( "initialize_objects: need to set user info");


    for( unsigned int i=0; i<n_objects; i++){
      Object obj = new_object( info);
      initialize_object( info, obj);
      Weighted_Object wobj( info, obj);
      wobjects.push_back( wobj);
    }
  }

  template< class Interface>
  void Sampler< Interface>::step_objects_forward(){

    check_inputs();
    step_objects_forward( info);    
  } 

  template< class Interface>
  struct Obj_W_Coord{
    typename Interface::Object obj;
    double rxn_coord;
  };


  template< class Interface>
  class OWC_Less_Than{
  public:
    typedef Obj_W_Coord< Interface> OWC;

    bool operator()( const OWC& owc0, const OWC& owc1) const{
      return owc0.rxn_coord < owc1.rxn_coord;
    }  
  };

  // partitions initially empty. At end, reaction coordinates are
  // ordered from highest to zero 
  template< class Interface>
  void Sampler< Interface>::generate_bins( std::list< double>& partitions){
    typedef Obj_W_Coord< Interface> OWC;
    typedef OWC_Less_Than< Interface> OWC_LT;

    Vector< OWC> objs_w_coord( n_objects);
    for( uint i=0; i < n_objects; i++){
      Object obj = new_object( info);

      OWC& owc = objs_w_coord[i];
      owc.obj = obj;
      owc.rxn_coord = reaction_coordinate( info, obj);
    }

    while (true){
      step_objects_forward( info);

      for( uint i=0; i < n_objects; i++){
        OWC& owc = objs_w_coord[i];
        Object obj = owc.obj;
        owc.rxn_coord = reaction_coordinate( info, obj);
      } 

      std::sort( objs_w_coord.begin(), objs_w_coord.end(), OWC_LT());

      uint nkeep = 4*n_objects/5;
      uint ndiscard = n_objects - nkeep;
      for( uint i = 0; i < ndiscard; i++){
        uint id = i + nkeep;
        Object obj = objs_w_coord[ id].obj;
        remove( info, obj);
      
        uint ik = (uint)( nkeep*uniform( info));

        std::list< Object> objs;
        get_duplicated_objects( info, objs_w_coord[ik].obj, 1, objs);
      
        objs_w_coord[ id].obj = objs.front();
      }

      double rxn_coord = objs_w_coord[0].rxn_coord;

      if (rxn_coord <= 0.0)
        break;

      if (!partitions.empty()){
        printf( "rxn coord %g\n", rxn_coord);
        fflush( stdout);
      }

      if (partitions.empty() || rxn_coord < partitions.front())
        partitions.push_front( rxn_coord);
    }
    partitions.push_front( 0.0);
  }



}

