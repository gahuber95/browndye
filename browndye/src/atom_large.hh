#ifndef __ATOM_LARGE_HH__
#define __ATOM_LARGE_HH__

#include "units.hh"
#include "small_vector.hh"
#include "vector.hh"
#include "linalg3.hh"

struct Atom_Large{

  Atom_Large();

  template< class TForm>
  void get_transformed_position( const TForm& tform, Vec3< Length>& tpos) const{
    tform.get_transformed( pos, tpos);
  }
  
  void translate( const Vec3< Length>& trans){
    Linalg3::get_sum( pos, trans, pos);
  }

  void get_position( Vec3< Length>& _pos) const{
    Linalg3::copy( pos, _pos);
  }

  void set_position( const Vec3< Length>& _pos){
    Linalg3::copy( _pos, pos);
  }

  Length interaction_radius() const;

  typedef Vector< int>::size_type size_type;

  Length hard_radius, soft_radius, interac_radius;
  Vec3< Length> pos;
  bool soft;
  unsigned int number;
  size_type type;
};

typedef typename Vector< Atom_Large>::iterator Atom_Large_Ref;

inline
Length Atom_Large::interaction_radius() const{
  return interac_radius;
}


// constructor
inline
Atom_Large::Atom_Large(){
  pos[0] = pos[1] = pos[2] = Length( NAN);
  hard_radius = Length( NAN);
  soft_radius = Length( NAN);
  interac_radius = Length( NAN);
  soft = false;
  number = std::numeric_limits< unsigned int>::max();
  type = std::numeric_limits< size_type>::max();

}


#endif
