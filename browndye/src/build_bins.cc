/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

// Actual program that is called for build_bins

#include "we_simulator.hh"
#include "get_args.hh"

int main( int argc, char* argv[]){

  if (argc < 2){
    error( "build_bins: need input file name");
  }
  const char* input = argv[1];

  //const char* input = string_arg( argc, argv, "-in");

  Browndye_RNG rng;
  WE_Simulator wes;
  wes.building_bins = true;
  wes.initialize( input);

  wes.generate_bins();
}
