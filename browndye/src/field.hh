/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

#ifndef __FIELD_HH__
#define __FIELD_HH__


/*
The Field class contains nested grids (Single_Grid::Grid) 
and the multipole field (Multipole_Field::Field) beyond the outermost
grid. It is initialized with the names of the data files, and is
used to get the potential and its gradient at a point.

It is templated with a user-defined Interface class, which has
the following: Potential, Charge, Permittivity, Potential_Gradient

This allows it to be used for the desolvation fields as well as
electric fields.

*/

#include <algorithm>
#include <memory>
#include <iostream>
#include "error_msg.hh"
#include "single_grid.hh"
#include "multipole_field.hh"
#include "sfield_ptr_tree.hh"

namespace Field{

template< class Interface>
class Field{
public:
  typedef typename Interface::Potential Potential;
  typedef Single_Grid::Grid< Potential> SGrid;
  typedef typename Interface::Charge Charge;
  typedef typename Interface::Permittivity Permittivity;
  typedef typename Interface::Potential_Gradient Potential_Gradient;
  typedef typename Interface::String String;

  class SField_Interface;

  typedef typename SField_Ptr::Tree< SField_Interface>::Hint Hint;

  // Names of files.
  void initialize( const String& mpole, const Vector< String>& sfiles);

  // with no multipole field
  void initialize( const Vector< String>& sfiles);

  void get_gradient( const Vec3< Length>& pos, 
                     Vec3< Potential_Gradient>& f, Hint&) const;
  Potential potential( const Vec3< Length>& pos, Hint&) const;

  Length debye_length() const;
  Permittivity vacuum_permittivity() const;
  double solvent_dielectric() const;
  Charge charge() const;

  void shift_position( const Vec3< Length>&);

  bool has_solvent_info() const;

  Hint first_hint() const;

  void get_corners( Vec3< Length>&, Vec3< Length>&) const;

#include "field_innards.hh"
};

#include "field_impl.hh"

#endif
