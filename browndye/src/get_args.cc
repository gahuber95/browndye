/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
Extracts arguments from the command-line input to a program,
using flags to denote the arguments.
*/ 

#include "string.h"
#include "error_msg.hh"

void get_double_arg( int argc, char* argv[], const char* flag, double& _res){
  for( int i = 0; i < argc-1; i++){
    if (strcmp( flag, argv[i]) == 0){
      char* str = argv[i+1];
      char* pend;
      double res = strtod( str, &pend);
      if (pend == str)
        error( "double_arg: not a floating point number at ", flag);
      else {
        _res = res;
        break;
      }
    }
  }
}

void get_int_arg( int argc, char* argv[], const char* flag, int& _res){
  for( int i = 0; i < argc-1; i++){
    if (strcmp( flag, argv[i]) == 0){
      char* str = argv[i+1];
      char* pend;
      int res = strtol( str, &pend, 10);
      if (pend == str)
        error( "int_arg: not an integer at ", flag);
      else {
        _res = res;
        break;
      }
    }
  }
}

void get_string_arg( int argc, char* argv[], const char* flag, char* res){
  for( int i = 0; i < argc-1; i++){
    if (strcmp( flag, argv[i]) == 0){
      strcpy( res, argv[i+1]);
      break;
    }
  }
}



