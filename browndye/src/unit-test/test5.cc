#include <random>
#include <iostream>
#include "rotations.hh"


class Interface{
public:
  typedef std::default_random_engine Random_Number_Generator;
  
  static
  double gaussian( Random_Number_Generator& gen){
    std::normal_distribution< double> g( 0.0, 1.0);
    return g( gen);
  }
};

double fnorm( const Mat3< double>& mat){
  double sum = 0.0;
  for( int i = 0; i < 3; i++)
    for( int j = 0; j < 3; j++)
      sum += mat[i][j]*mat[i][j];

  return sqrt( sum);
}

double norm( const SVec< double, 4>& quat){
  double sum = 0.0;
  for( int i = 0; i < 4; i++)
    sum += quat[i]*quat[i];

  return sqrt( sum);
}

int main(){
  std::default_random_engine gen;

  Mat3< double> rot0, rot1, drot;

  SVec< double, 4> quat0;

  unsigned int nsamples = 1'000'000;
  double max_ratio = 0.0;
  for (auto i = 0u; i < nsamples; i++){

    get_random_rotation< Interface>( gen, rot0);

    mat_to_quat( rot0, quat0);
    quat_to_mat( quat0, rot1);
    
    for( int i = 0; i < 3; i++)
      for( int j = 0; j < 3; j++)
        drot[i][j] = rot1[i][j] - rot0[i][j];
   
    double ratio = fnorm( drot)/fnorm( rot0);
    if (ratio > 1.0e-8){
      std::cout << "failed " << ratio << "\n";
      exit(0);
    }
 
    SVec< double, 4> quat1, quat2, inv_quat1, quat3, dquat;
    get_random_rotation< Interface>( gen, rot1);
    mat_to_quat( rot1, quat1);
    multiply_quats( quat0, quat1, quat2);
    invert_quat( quat1, inv_quat1);
    multiply_quats( quat2, inv_quat1, quat3);

    for( int i = 0; i < 4; i++)
      dquat[i] = quat3[i] - quat0[i];

    double qratio = norm( dquat)/norm( quat0);

    if (qratio > 1.0e-8){
      std::cout << "failed q " << ratio << "\n";
      exit(0);
    }


    max_ratio = max( max_ratio, qratio);
  }

  std::cout << "passed\n";

}
