#include <random>
#include <algorithm>
#include <iostream>
#include "units.hh"
#include "vector.hh"
#include "small_vector.hh"
#include "linalg3.hh"
#include "two_sphere_brownian.hh"

const Length radius0( 30.0);
const Length radius1( 20.0);

template< class T>
auto sq( T t){
  return t*t;
}

class Interface{
public:
  typedef SVec< Vec3< double>, 2> Spheres;

  static
  void get_positions_and_rotations( const Spheres& sphs, 
                                    Vec3< double>& pos0,
                                    Mat3< double>& rot0,
                                    Vec3< double>& pos1,
                                    Mat3< double>& rot1
                                    ){  
    namespace L = Linalg3;
    pos0 = sphs[0];
    pos1 = sphs[1];
    L::get_id_mat( rot0);
    L::get_id_mat( rot1);
  }

  static
  void get_forces_and_torques( const Spheres& sphs, 
                               Vec3< double>& f0, Vec3< double>& t0, 
                               Vec3< double>& f1, Vec3< double>& t1){
    namespace L = Linalg3;
    L::zero( f0);
    L::zero( f1);
    L::zero( t0);
    L::zero( t1);

  }

  static
  double gap( const Spheres& sphs){
    double r0 = 1.0;
    double r1 = radius1/radius0;
    return Linalg3::distance( sphs[0], sphs[1]) - (r1 + r0);
  }
  
  static
  void put_positions_and_rotations( const Vec3< double>& r0,
                                    const Mat3< double>& rot0,
                                    const Vec3< double>& r1, 
                                    const Mat3< double>& rot1,
                                    Spheres& sphs){    
    sphs[0] = r0;
    sphs[1] = r1;
  }
   
};

// scaled by 6 pi mu / kT, d is pos 1 - pos 0
void get_rp_matrix( Length a0, Length a1, const Vec3< Length>& d,
                    Vector< Vector< Inv_Length> >& mat){

  namespace L = Linalg3;

  auto r = L::norm( d);
  auto r2 = r*r;
  auto r3 = r2*r;
  auto a2 = 0.5*( a0*a0 + a1*a1);
  Mat3< Inv_Length> m;
  
  auto delta = []( int i, int j) -> double{
    return (i == j) ? 1.0 : 0.0;   
  };

  for( int i = 0; i < 3; i++)
    for( int j = 0; j < 3; j++)
      m[i][j] = 
        0.75*(delta(i,j)/r + d[i]*d[j]/r3 + 
              (2.0*a2/(3.0*r3))*( delta(i,j) - 3.0*d[i]*d[j]/r2));
   
  for( int i = 0; i < 3; i++)
    for( int j = 0; j < 3; j++){
      mat[i][j] = delta(i,j)/a0;
      mat[i+3][j+3] = delta(i,j)/a1;

      mat[i][j+3] = m[i][j];
      mat[i+3][j] = m[i][j];
    }
}

template< class T>
void init_mat( Vector< Vector<T> >& mat, size_t n){
  mat.resize( n);
  for( size_t i = 0; i < n; i++){
    mat[i].resize( n);
    for( size_t j = 0; j < n; j++)
      mat[i][j] = T( 0.0);
  }
}

void main1(  std::default_random_engine& gen){
  namespace L = Linalg3;

  Vec3< Length> pos0, pos1;
  L::zero( pos0);

  auto R = radius0 + radius1;
  {
    std::normal_distribution< double> gaussian( 0.0, 1.0);
    Vec3< double> d;
    for( int k = 0; k < 3; k++)
      d[k] = gaussian( gen);
    auto dd = L::norm( d);
    L::get_scaled( R/dd, d, pos1);
  }

  pos1[2] = radius0 + radius1;

  Vector< Vector< Inv_Length> > amat;
  init_mat( amat, 6);

  Vec3< Length> d01;
  L::get_diff( pos1, pos0, d01);

  get_rp_matrix( radius0, radius1, d01, amat);

  Vector< Vector< Length2> > acov;
  init_mat( acov, 6);

  Energy kT( 1.0);
  Viscosity mu( 1.0);

  Time dt( 1.0);

  for( int i = 0; i < 6; i++)
    for( int j = 0; j < 6; j++)
      acov[i][j] = 2.0*dt*kT*amat[i][j]/(6.0*pi*mu);
    
  // browndye
  unsigned int n_samples = 1'000'000;
  Two_Sphere_Brownian::Mover< Interface> mover;
  mover.set_radius( radius1/radius0);
  //mover.set_no_hi();

  SVec< double, 12> w;
  SVec< Vec3< double>, 2> spheres;

  auto& sph0 = spheres[0];
  auto& sph1 = spheres[1];

  Vector< Vector< Length2> > cov;
  init_mat( cov, 6);
  
  std::normal_distribution< double> gaussian( 0.0, 1.0);

  for( auto is = 0u; is < n_samples; is++){
    L::get_scaled( 1.0/radius0, pos0, sph0);
    L::get_scaled( 1.0/radius0, pos1, sph1);

    for( int i = 0; i < 12; i++)
      w[i] = gaussian( gen);
    
    auto a3 = sq( radius0)*radius0;
    mover.move_spheres( dt*kT/(a3*mu), w, spheres);

    Vec3< Length> d0, d1;
    Vec3< Length> ssph0, ssph1;
    L::get_scaled( radius0, sph0, ssph0);
    L::get_scaled( radius0, sph1, ssph1);
    L::get_diff( ssph0, pos0, d0);
    L::get_diff( ssph1, pos1, d1);
    
    for( int i = 0; i < 3; i++)
      for( int j = 0; j < 3; j++){
        cov[i][j] += d0[i]*d0[j];
        cov[i+3][j] += d1[i]*d0[j];
        cov[i][j+3] += d0[i]*d1[j];
        cov[i+3][j+3] += d1[i]*d1[j];
      }
  }
  
  for( int i = 0; i < 6; i++)
    for( int j = 0; j < 6; j++)
      cov[i][j] /= n_samples;
  
  Length4 sum( 0.0), dsum( 0.0);
  for( int i = 0; i < 6; i++)
    for( int j = 0; j < 6; j++){
      dsum += sq(cov[i][j] - acov[i][j]); 
      sum += sq( acov[i][j]);
    }  
  
  Length2 fnorm = sqrt( sum);
  Length2 dfnorm = sqrt( dsum);
  
  double ratio =  dfnorm/fnorm;
  std::cout << ratio << "\n";

  if (ratio > 0.01){
    std::cerr << "failed\n";
    exit(1);
  }
}

int main(){
  std::default_random_engine gen;
  for( int i = 0; i < 10; i++)
    main1( gen);

  std::cout << "passed\n";
  return 0;
}
