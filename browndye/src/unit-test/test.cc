#include "molecule.hh"
#include "rotations.hh"
#include "compare.hh"

/*
mock headers:

field.hh
charged_blob_simple.hh
jam_xml_pull_parser.hh

 */

template< class C0, class FI, class Mol0, class TForm0, 
	  class Mol1, class TForm1>
void add_cheby_and_grid_forces( 
             const Field_For_Blob< C0, FI>& field0, 
             const Charged_Blob::Blob< 4, Blob_Interface< C0, FI> >& blob1,
             Mol0& mol0, const TForm0& tform0, 
             Mol1& mol1, const TForm1& tform1, double factor);
  

template< class T>
void prvec( const Vec3<T>& v){
  std::cout << v[0] << " " << v[1] << " " << v[2] << "\n";
}

template< class A, class B, class TForm0, class TForm1>
void get_actual_torques( Field::Field<A>& efield, 
			 Charged_Blob::Blob< 4, B>& blob,
			 const TForm0& tform0, const TForm1& tform1, 
			 Vec3< Torque>& torque0, Vec3< Torque>& torque1){

  namespace L = Linalg3;
 
  Vector< Atom> batoms;
  for( auto& atom: blob.atoms){
    Atom batom;
    tform1.get_transformed( atom.pos, batom.pos);
    batom.q = atom.q;
    batoms.push_back( batom);
  }

  Vec3< Length> bcenter, fcenter;
  tform0.get_translation( fcenter);
  tform1.get_translation( bcenter);
  
  L::zero( torque0);
  L::zero( torque1);
  
  for( auto& batom: batoms){
    auto eps = Permittivity(1.0);
    auto qb = batom.q;
    auto& bpos = batom.pos;
    
    Vec3< Force> f;
    L::zero( f);
    for( auto& fatom: efield.atoms){
      auto& fpos = fatom.pos;
      auto qf = fatom.q;
      Vec3< Length> d;
      L::get_diff( bpos, fpos, d);
      
      auto r = L::norm( d);
      auto factor = qf*qb/( eps*r*r*r);
      Vec3< Force> ff;
      L::get_scaled( factor, d, ff);

      Vec3< Torque> t;
      Vec3< Length> larm;
      L::get_diff( fpos, fcenter, larm);
      L::get_cross( ff, larm, t); // reverse force
      L::get_sum( torque0, t, torque0);
      

      L::get_sum( ff, f, f);
    }
    
    Vec3< Torque> t;
    Vec3< Length> larm;
    L::get_diff( bpos, bcenter, larm);
    L::get_cross( larm, f, t);
    
    L::get_sum( torque1, t, torque1);
  }

}

void randomize_transform( std::default_random_engine& gen, Transform& tform){
  tform.set_zero();

  std::uniform_real_distribution<double> distr(-1.0,1.0);
  Vec3< Length> d;
  for( int k = 0; k < 3; k++)
    d[k] = distr( gen);
  tform.set_translation( d);

  class Interface{
  public:
    typedef std::default_random_engine Random_Number_Generator;

    static
    double gaussian( std::default_random_engine& gen){
      std::normal_distribution< double> distr( 0.0, 1.0);
      return distr( gen);
    }
  };
  
  Mat3< double> rot;
  get_random_rotation< Interface>( gen, rot);
  tform.set_rotation( rot);

}

void randomize_transform( std::default_random_engine&, Blank_Transform&){}

template< class TForm0, class Mol0, class TForm1, class Mol1, class BInterface, class FInterface> 
void main1(){
  namespace L = Linalg3;

  std::default_random_engine gen;

  for( int itest = 0; itest < 10; itest++){
      Mol0 mol0;
      Mol1 mol1;
      
      Field_For_Blob< BInterface, FInterface> field;
      field.field = new Field::Field< FInterface>( gen);
      
      Charged_Blob::Blob< 4, Blob_Interface< BInterface, FInterface> > blob( gen);
      
      L::zero( mol0.force);
      L::zero( mol0.torque);
      L::zero( mol1.force);
      L::zero( mol1.torque);
      
      TForm0 tform0;
      TForm1 tform1;
      randomize_transform( gen, tform1);
      
      add_cheby_and_grid_forces( field, blob, mol0, tform0, mol1, tform1, 1.0);
      
      std::cout << "forces\n";
      prvec( mol0.force);
      prvec( mol1.force);
      
      std::cout << "torques\n";
      prvec( mol0.torque);
      prvec( mol1.torque);
      
      Vec3< Torque> atorque0, atorque1;
      
      get_actual_torques( *(field.field), blob, tform0, tform1, atorque0, atorque1);
      
      std::cout << "actual torques\n";
      prvec( atorque0);
      prvec( atorque1);
      
      compare( mol0.torque, atorque0);
      compare( mol1.torque, atorque1);
      
  }
}

int main(){
  main1< Blank_Transform, Large_Molecule_State, Transform, Small_Molecule_State, Col_Interface_Large, V_Field_Interface>();

  main1< Transform, Small_Molecule_State, Blank_Transform, Large_Molecule_State, Col_Interface_Small, Born_Field_Interface>();

  std::cout << "passed\n";
  return 0;
}
