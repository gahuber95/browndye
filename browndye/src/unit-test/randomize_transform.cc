#include "randomize_transform.hh"
#include "rotations.hh"

void randomize_transform( std::default_random_engine& gen, Transform& tform){
  tform.set_zero();

  std::uniform_real_distribution<double> distr(-1.0,1.0);
  Vec3< Length> d;
  for( int k = 0; k < 3; k++)
    d[k] = Length( distr( gen));
  tform.set_translation( d);

  class Interface{
  public:
    typedef std::default_random_engine Random_Number_Generator;

    static
    double gaussian( std::default_random_engine& gen){
      std::normal_distribution< double> distr( 0.0, 1.0);
      return distr( gen);
    }
  };
  
  Mat3< double> rot;
  get_random_rotation< Interface>( gen, rot);
  tform.set_rotation( rot);

}
