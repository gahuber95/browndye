#include "random_atoms.hh"

void get_random_atoms( std::default_random_engine& gen,
		       Charge max_q,
		       Vector< Atom>& atoms){

  double max_qr = max_q/Charge( 1.0);
  std::uniform_real_distribution<double> 
    qdistr(-max_qr, max_qr), distr( -1.0,1.0);

  for( auto& atom: atoms){
    for( int k = 0; k < 3; k++){
      atom.pos[k] = Length( distr( gen));
      atom.q = Charge( qdistr( gen));
    }
  }

}
