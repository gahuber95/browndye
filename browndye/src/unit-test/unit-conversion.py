# just to reassure myself that I did the unit conversions correctly
# Water viscosity and vacuum permittivity in units of 
# Angstroms, kT, picoseconds, elementary charge

kT = 1.38064852e-23*298 # J
mu = 0.001 # Pa * s = J*s/m^3

m_per_A = 1.0e-10
ps_per_s = 1.0e12

muf = mu*ps_per_s*m_per_A**3/kT
print muf

vperm = 8.854187817e-12 # Farad/m = C^2/(J m)
ec_per_coulomb = 1.0/1.60217662e-19

vpermf = vperm*ec_per_coulomb**2*m_per_A*kT
print vpermf



