#include <random>
#include "molecule.hh"
#include "randomize_transform.hh"
#include "compare.hh"
#include "back_door.hh"

template< class T>
void prvec( const Vec3<T>& v){
  std::cout << v[0] << " " << v[1] << " " << v[2] << "\n";
}

void Back_Door::test2_main1( std::default_random_engine& gen){

  Large_Molecule_Common molc0;
  Large_Molecule_State mols0;
  Small_Molecule_Common molc1;
  Small_Molecule_Thread molt1;
  Small_Molecule_State mols1;                 
  bool has_collision;

  // Parameters
  Length mol0_radius( 1.1), mol1_radius( 1.2);
  Large_Molecule_Common::Spring_Constant ks( 1.1);
  Length equilib_len( 1.1);
  double mol0_desolve_fudge = 0.7;
  double mol1_desolve_fudge = 0.6;
  Energy eps0( 2.2), eps1( 1.1);
  Length displacement( 2.0);
  Charge max_q( 1.0);
  
  // Large_Molecule_Common 0
  molc0.born_field.reset( new Field::Field< Born_Field_Interface>( gen, max_q));
  molc0.desolve_fudge = mol0_desolve_fudge;
  molc0.pinfo.reset( new Near_Interactions::Parameter_Info( eps0));

  molc0.v_field.reset( new Field::Field< V_Field_Interface>(gen, max_q));

  molc0.atoms.resize( 4);
  {
    std::uniform_real_distribution<double> distr(-1.0,1.0);

    for( auto& atom: molc0.atoms){
      for( int k = 0; k < 3; k++)
        atom.pos[k] = Length( distr( gen));
      atom.soft_radius = mol0_radius;
      atom.hard_radius = mol0_radius;
      atom.interac_radius = 10.0*mol0_radius;
      atom.type = 0;
      atom.soft = true;
    }
  }

  molc0.collision_structure.reset( new Collision_Detector::Structure< Col_Interface_Large>( molc0.atoms));
  molc0.collision_structure->set_max_number_per_cell( 1000);
  molc0.collision_structure->load_objects();

  molc0.q2_blob.reset( new Charged_Blob::Blob< 4, Large_Molecule_Common::Q2_Blob_Interface>( gen, max_q));

  molc0.ks = ks;
  molc0.equilib_len = equilib_len;
  molc0.spring_atom0 = &(molc0.atoms[0]);
  //molc0.spring_atom0 = nullptr;

  // Large_Molecule_State 0
  randomize_transform( gen, mols0.transform);

  // Small_Molecule_Common 1
  molc1.born_field.reset( new Field::Field< Born_Field_Interface>( gen, max_q));
  molc1.desolve_fudge = mol1_desolve_fudge;
  molc1.pinfo.reset( new Near_Interactions::Parameter_Info( eps1));

  molc1.q2_blob.reset( new Charged_Blob::Blob< 4, Small_Molecule_Common::Q2_Blob_Interface>( gen, max_q));
  molc1.q_blob.reset( new Charged_Blob::Blob< 4, Small_Molecule_Common::Q_Blob_Interface>( gen, max_q));

  // Small_Molecule_Thread 1
  molt1.atoms.resize( 4);
  {
    std::uniform_real_distribution<double> distr(-1.0,1.0);

    for( auto& atom: molt1.atoms){
      for( int k = 0; k < 3; k++)
        atom.pos[k] = Length( distr( gen));
      atom.soft_radius = mol1_radius;
      atom.hard_radius = mol1_radius;
      atom.interac_radius = 10.0*mol1_radius;
      atom.type = 0;
      atom.soft = true;
    }
  }
  molt1.spring_atom1 = &(molt1.atoms[0]);

  molt1.collision_structure.reset( new Collision_Detector::Structure< Col_Interface_Small>( molt1.atoms));
  molt1.collision_structure->set_max_number_per_cell( 1000);
  molt1.collision_structure->load_objects();

  // Small_Molecule_State 1
  randomize_transform( gen, mols1.transform);
  {
    Vec3< Length> trans1;
    mols1.transform.get_translation( trans1);
    trans1[2] += displacement;
    mols1.transform.set_translation( trans1);
  }

  compute_forces_and_torques( molc0, mols0, molc1, molt1, mols1, has_collision);

  std::cout << "Software\n";
  prvec( mols0.force);
  prvec( mols0.torque);
  prvec( mols1.force);
  prvec( mols1.torque);
  std::cout << "\n";

  // Actual

  // LJ : Atom_Large, Atom_Small
  // Tether: Atom_Large, Atom_Small
  // Electrostatic: Atom, Atom
  // Born 0->1: Atom, Atom
  // Born 1->0: Atom, Atom

  namespace L = Linalg3;
  
  Vec3< Length> center0, center1;
  mols0.transform.get_translation( center0);
  mols1.transform.get_translation( center1);

  Vec3< Force> aforce0, aforce1;
  Vec3< Torque> atorque0, atorque1;

  L::zero( aforce0);
  L::zero( aforce1);
  L::zero( atorque0);
  L::zero( atorque1);

  auto compute_cforces = [&]( const auto& fc, 
			      double fudge,
                              const auto& atoms0, 
                              const auto& atoms1){
    
    auto& tform0 = mols0.transform;
    auto& tform1 = mols1.transform;

    for( auto& atom0: atoms0){
      Vec3< Length> pos0;
      tform0.get_transformed( atom0.pos, pos0);
      Vec3< Length> larm0;
      L::get_diff( pos0, center0, larm0);
      for( auto& atom1: atoms1){
        Vec3< Length> pos1;
        tform1.get_transformed( atom1.pos, pos1);
        Vec3< Length> larm1;
        L::get_diff( pos1, center1, larm1);
        
        Vec3< Length> d;
        L::get_diff( pos1, pos0, d);
        auto r2 = L::dot( d,d);
        auto r = sqrt( r2);
        
        auto f = fc( atom0, atom1, r);

        Vec3< Force> F1, F0;
        L::get_scaled( fudge*f/r, d, F1);
        L::get_scaled( -1.0, F1, F0);

        L::get_sum( F0, aforce0, aforce0);
        L::get_sum( F1, aforce1, aforce1);

        Vec3< Torque> T0, T1;
        L::get_cross( larm0, F0, T0);
        L::get_cross( larm1, F1, T1);

        L::get_sum( T0, atorque0, atorque0);
        L::get_sum( T1, atorque1, atorque1);
      }
    }
  };

  auto cc = [&]( const Atom& atom0, const Atom& atom1, Length r) -> Force{
    auto eps = Permittivity( 1.0);
    auto r2 = r*r;
    return atom0.q*atom1.q/(eps*r2);    
  };

  compute_cforces( cc, 1.0,
                   molc0.v_field->atoms, 
                   molc1.q_blob->atoms);

  compute_cforces( cc, mol1_desolve_fudge,
                   molc0.born_field->atoms, 
                   molc1.q2_blob->atoms);
  compute_cforces( cc, mol0_desolve_fudge,
                   molc0.q2_blob->atoms, 
                   molc1.born_field->atoms);     
            
  auto ljc = [&]( const Atom_Large& atom0, const Atom_Small& atom1, Length d)
    -> Force {

    auto eps = sqrt( eps0*eps1);
    auto s0 = atom0.soft_radius;
    auto s1 = atom1.soft_radius;
    auto s = s0 + s1;
    
    auto r = d/s;
    auto r2 = r*r;
    auto r4 = r2*r2;
    auto r6 = r2*r4;
    auto r12 = r6*r6;
    
    return 12.0*eps*(1.0/r12 - 1.0/r6)/d;
  };

  compute_cforces( ljc, 1.0, molc0.atoms, molt1.atoms);

  {
    Vec3< Length> pos0, pos1;
    mols0.transform.get_transformed( molc0.atoms[0].pos, pos0);
    mols1.transform.get_transformed( molt1.atoms[0].pos, pos1);

    Vec3< Length> d;
    L::get_diff( pos1, pos0, d);
    auto r = L::norm( d);
    auto f = ks*(equilib_len - r);
    Vec3< Force> F0, F1;
    L::get_scaled( f/r, d, F1);
    L::get_scaled( -1.0, F1, F0);
    L::get_sum( F0, aforce0, aforce0);
    L::get_sum( F1, aforce1, aforce1);

    Vec3< Length> larm0, larm1;
    L::get_diff( pos0, center0, larm0);
    L::get_diff( pos1, center1, larm1);

    Vec3< Torque> T0, T1;
    L::get_cross( larm0, F0, T0);
    L::get_cross( larm1, F1, T1);
    
    L::get_sum( T0, atorque0, atorque0);
    L::get_sum( T1, atorque1, atorque1);
  }


  std::cout << "Actual\n";
  prvec( aforce0);
  prvec( atorque0);
  prvec( aforce1);
  prvec( atorque1);
  std::cout << "\n";

  compare( mols0.force, aforce0);
  compare( mols1.force, aforce1);

  compare( mols0.torque, atorque0);
  compare( mols1.torque, atorque1);
}

int main(){

  std::default_random_engine gen;
  for( int i = 0; i < 100; i++)
    Back_Door::test2_main1( gen);

  std::cout << "Passed\n";
}

/*
Fakes:

Parameter_Info
Field
Charged_Blob_Simple
XML_Pull_Parser
Molecule_Pair

 */
