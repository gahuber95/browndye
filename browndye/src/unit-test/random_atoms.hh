#ifndef __RANDOM_ATOMS_HH__
#define __RANDOM_ATOMS_HH__


#include <random>
#include "units.hh"
#include "vector.hh"
#include "small_vector.hh"

struct Atom{
  Vec3< Length> pos;
  Charge q;
};

void get_random_atoms( std::default_random_engine& gen, Charge max_q, 
		       Vector< Atom>& atoms);


#endif
