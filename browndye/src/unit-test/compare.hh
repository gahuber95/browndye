#ifndef __COMPARE_HH__
#define __COMPARE_HH__

template< class T>
void compare( const Vec3<T>& a, const Vec3<T>& b){
  namespace L = Linalg3;

  Vec3<T> sum, c, d;
  L::get_sum( a,b,sum);
  L::get_scaled( 0.5, sum,c);
  L::get_diff( a, b, d);

  auto cnorm = L::norm( c);
  if (cnorm > T(1.0e-20)){
    auto dnorm = L::norm( d);
    if (dnorm > 0.001*cnorm){
      std::cout << "failed\n";
      exit(0);
    }
  }
}

#endif
