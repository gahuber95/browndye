#include <random>
#include "units.hh"
#include "../step_near_absorbing_sphere.hh"

class Interface{
public:

  typedef std::default_random_engine Random_Number_Generator;

  static
  double gaussian( std::default_random_engine& gen){
    std::normal_distribution< double> gs( 0.0, 1.0);
    return gs( gen);
  }

  static
  double uniform( std::default_random_engine& gen){
    std::uniform_real_distribution< double> nd( 0.0, 1.0);
    return nd( gen);
  }

};

template< class T>
double rel_diff( T a, T b){
  return fabs( a - b)/(0.5*(a + b));
}


void main2( Force F, Diffusivity D, Length r0){
  std::default_random_engine gen;

  auto R = Length( 10.0);
  //auto r0 = Length( 9.0);
  //auto F = Force( -1.5);
  //auto D_radial = Diffusivity( 2.0);
  auto D_radial = D;
  auto D_perp = D_radial;

  unsigned int n_samples = 1e6;

  Length r_mean, r_sdev;
  Time t_mean, t_sdev;
  double sur_prob;
  
  {
    auto rsum = Length( 0.0);
    auto r2sum = Length2( 0.0);
    auto tsum = Time( 0.0);
    auto t2sum = Time2( 0.0);
    unsigned int sur_sum = 0;
    const auto t0 = sq( r0 - R)/D_radial;
    for( auto i = 0u; i < n_samples; i++){
      std::normal_distribution< double> gs( 0.0, 1.0);
      Length x,y,z;
      auto sqrt_D = sqrt( 2.0*D_radial);
      
      x = Length( 0.0);
      y = Length( 0.0);
      z = Length( r0);
      Time t = t0;
      Length r = r0;
      auto dt_min = Time( 1.0e-9);

      while (t > 0.0 && r < R){
	auto dt = min( t, max( dt_min, 0.05*sq( r - R)/D_radial));
        
        auto fx = F*x/r;
        auto fy = F*y/r;
        auto fz = F*z/r;
	x += D_radial*fx*dt + sqrt_D*sqrt(dt)*gs( gen);
	y += D_radial*fy*dt + sqrt_D*sqrt(dt)*gs( gen);
	z += D_radial*fz*dt + sqrt_D*sqrt(dt)*gs( gen);
	
	r = sqrt( x*x + y*y + z*z);
	
	t -= dt;
      }
      if (t > 0.0){
        auto te = t0 - t;
	tsum += te;
	t2sum += te*te;
      }
      else{
	rsum += r;
	r2sum += r*r;
	sur_sum++;
      }
    }
    sur_prob = ((double)sur_sum)/n_samples;
    r_mean = rsum/sur_sum;
    r_sdev = sqrt( r2sum/sur_sum - sq( r_mean));
    t_mean = tsum/(n_samples - sur_sum);
    t_sdev = sqrt( t2sum/(n_samples - sur_sum ) - sq( t_mean));
  }
  std::cout << sur_prob << " " << r_mean << " " << r_sdev << " " << t_mean << " " << t_sdev << "\n";

  Length r_meana, r_sdeva;
  Time t_meana, t_sdeva;
  double sur_proba;

  {
    auto rsum = Length( 0.0);
    auto r2sum = Length2( 0.0);
    auto tsum = Time( 0.0);
    auto t2sum = Time2( 0.0);
    unsigned int sur_sum = 0;
    for( auto i = 0u; i < n_samples; i++){
      bool survives;
      Length r;
      Time t;
      step_near_absorbing_sphere< Interface>( gen, r0, R, F, D_radial, D_perp, 
                                              survives, r, t);

      if (!survives){
	tsum += t;
	t2sum += t*t;
      }
      else{
	rsum += r;
	r2sum += r*r;
	sur_sum++;
      }
      
    }
    sur_proba = ((double)sur_sum)/n_samples;
    r_meana = rsum/sur_sum;
    r_sdeva = sqrt( r2sum/sur_sum - sq( r_meana));
    t_meana = tsum/(n_samples - sur_sum);
    t_sdeva = sqrt( t2sum/(n_samples - sur_sum) - sq( t_meana));
  }
  std::cout << sur_proba << " " << r_meana << " " << r_sdeva << " " << t_meana << " " << t_sdeva << "\n";
  
  double tol = 0.02;
  if (rel_diff( sur_prob, sur_proba) > tol || 
      rel_diff( r_mean, r_meana) > Length( tol) ||
      rel_diff( r_sdev, r_sdeva) > Length( tol) ||
      rel_diff( t_mean, t_meana) > Time( tol) ||
      rel_diff( t_sdev, t_sdeva) > Time( tol)){
    
    std::cout << "failed\n";
    exit(1);
  }

}


int main(){
  main2( 1.5, 2.3, 9.0);
  main2( 1.5, 2.3, 9.5);
  main2( -1.5, 2.3, 9.0);
  main2( -1.5, 2.3, 9.5);


  std::cout << "passed\n";
  return 0;
}
