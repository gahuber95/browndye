#include <vector>
#include <random>
#include "units.hh"
#include "small_vector.hh"
#include "sfield_ptr_tree.hh"

struct Box{
  Vec3< Length> upper, lower;
  unsigned int level;

  Box(){
    level = 0;
  }
};

void shrink( Box& box){
  auto& upper = box.upper;
  auto& lower = box.lower;
  
  Vec3< Length> c;
  for( int i = 0; i < 3; i++)
    c[i] = 0.5*(upper[i] + lower[i]);
  
  for( int i = 0; i < 3; i++){
    lower[i] += 0.2*(c[i] - lower[i]);
    upper[i] -= 0.2*(upper[i] - c[i]);
  }
}

std::vector< Box*> subdivided( std::default_random_engine& gen,
                               Box& parent){

  //  chance of having children
  std::uniform_real_distribution< double> cdist( 0.0, 2.0);

  std::vector< Box*> res;
  res.push_back( &parent);
  if (cdist( gen) > 1.0 && parent.level < 20){
    auto& upper = parent.upper;
    auto& lower = parent.lower;
    auto dx = upper[0] - lower[0];
    auto dy = upper[1] - lower[1];
    auto dz = upper[2] - lower[2];
    
    Vec3< Length> c;
    for( int i = 0; i < 3; i++)
      c[i] = 0.5*(upper[i] + lower[i]);

    auto md = max( max( dx, dy), dz);
    int dir = 0;
    if (dy == md)
      dir = 1;
    else if (dz == md)
      dir = 2;
    
    Box* child0 = new Box( parent);
    Box* child1 = new Box( parent);
    child0->upper[dir] = c[dir];
    child1->lower[dir] = c[dir];
    child0->level = parent.level+1;
    child1->level = parent.level+1;
    shrink( *child0);
    shrink( *child1);
    
    auto children0 = subdivided( gen, *child0);
    auto children1 = subdivided( gen, *child1);
    res.insert( res.begin(), children0.begin(), children0.end());
    res.insert( res.begin(), children1.begin(), children1.end());
  }
  return res;
}

bool encloses( const Box& box0, const Box& box1){
  bool res = true;
  for( int i = 0; i < 3; i++){
    res &= (box0.lower[i] < box1.lower[i]);
    res &= (box0.upper[i] > box1.upper[i]);
  }
  return res;
}

bool is_inside( const Box& box, const Vec3< Length>& pos){
  bool res = true;
  for( int i = 0; i < 3; i++){
    res &= (box.lower[i] < pos[i]);
    res &= (box.upper[i] > pos[i]);
  }
  return res;
}

bool overlap( const Box& box0, const Box& box1){
       
  auto ioverlap = []( Length a, Length b, Length x, Length y) -> bool{
    bool res =  (b > x) && (a < y);
    return res;
  };

  bool res = true;
  for( int i = 0; i < 3; i++)
    res &= ioverlap( box0.lower[i], box0.upper[i], 
                     box0.lower[i], box1.upper[i]);
  return res;
}

const Box* lowest_box( const std::vector< Box* >& boxes, 
                       Vec3< Length>& pos){
  const Box* res = nullptr;
  for( auto& box_ptr: boxes){
    auto& box = *box_ptr;
    if (is_inside( box, pos)){
      if (res == nullptr)
        res = &box;
      else{
        if (encloses( *res, box))
          res = &box;
      }
    }
  }
  return res;
}

class Interface{
public:
  typedef Box Contained;
  typedef Vec3< Length> Point;

  static 
  bool overlap( const Box& box0, const Box& box1){
    return ::overlap( box0, box1);
  }

  static
  bool encloses( const Box& box0, const Box& box1){
    return ::encloses( box0, box1);
  }

  static
  bool is_inside( int dum, const Point& pos, const Box& box){
    return ::is_inside( box, pos);
  }

  static
  void error( const char* msg){
    std::cout << msg << "\n";
    std::exit(1);
  }
};

void main1( std::default_random_engine& gen){
  Box* box = new Box();
  for( int i = 0; i < 3; i++)
    box->upper[i] = Length( 1.0);
  for( int i = 0; i < 3; i++)
    box->lower[i] = Length( 0.0);
  
  auto boxes = subdivided( gen, *box);

  SField_Ptr::Tree< Interface> tree;
  for( auto& box_ptr: boxes)
    tree.insert( box_ptr);

  tree.check_for_loops();

  SField_Ptr::Tree< Interface>::Hint hint = tree.first_hint();

  std::uniform_real_distribution< double> pdist( 0.0001, 0.9999);

  unsigned int nsamples = 10000;
  for( auto i = 0u; i < nsamples; i++){
    Vec3< Length> pos;
    for( int k = 0; k < 3; k++)
      pos[k] = pdist( gen);

    int dum = 0;
    auto tbox = tree.lowest_found( dum, pos, hint);
    auto abox = lowest_box( boxes, pos);

    //std::cout << i << " " << tbox << "\n";

    if (tbox != abox){
      std::cout << "failed\n";
      exit(1);
    }
  }
}

int main(){
  std::seed_seq seeds = {11111,22222};
  std::default_random_engine gen( seeds);

  unsigned int bsamples = 1000;
  for( auto i = 0u; i < bsamples; i++){
    main1( gen);
  }

  std::cout << "passed\n";
  return 0;
}
