#include <random>
#include <iostream>
#include "rotations.hh"
#include "transform.hh"

class Interface{
public:
  typedef std::default_random_engine Random_Number_Generator;
  
  static
  double gaussian( Random_Number_Generator& gen){
    std::normal_distribution< double> g( 0.0, 1.0);
    return g( gen);
  }
};

double fnorm( const Mat3< double>& mat){
  double sum = 0.0;
  for( int i = 0; i < 3; i++)
    for( int j = 0; j < 3; j++)
      sum += mat[i][j]*mat[i][j];

  return sqrt( sum);
}

template< class T>
T norm( const Vec3< T>& v){
  decltype( T()*T()) sum = 0.0;
  for( int i = 0; i < 3; i++)
    sum += v[i]*v[i];

  return sqrt( sum);
}

int main(){
  std::default_random_engine gen;

  std::uniform_real_distribution< double> unif( -10.0, 10.0);

  unsigned int nsamples = 1'000'000;
  double max_ratio = 0.0;
  for (auto i = 0u; i < nsamples; i++){

    Mat3< double> rot0, rot1, rot0a, drot;
    get_random_rotation< Interface>( gen, rot0);
    get_random_rotation< Interface>( gen, rot1);

    Vec3< Length> pos0, pos1, pos0a, dpos;
    for( int i = 0; i < 3; i++)
      pos0[i] = unif( gen);

    for( int i = 0; i < 3; i++)
      pos1[i] = unif( gen);


    Transform tform0, tform1, tform01, tform0a, inv_tform1;
    tform0.set_translation( pos0);
    tform0.set_rotation( rot0);

    tform1.set_translation( pos1);
    tform1.set_rotation( rot1);
    
    tform1.get_inverse( inv_tform1);

    get_product( tform0, tform1, tform01);
    get_product( tform01, inv_tform1, tform0a);
    
    tform0a.get_translation( pos0a);
    tform0a.get_rotation( rot0a);

    for( int i = 0; i < 3; i++)
      for( int j = 0; j < 3; j++)
        drot[i][j] = rot0a[i][j] - rot0[i][j];
   
    double ratio = fnorm( drot)/fnorm( rot0);
    if (ratio > 1.0e-9){
      std::cout << "failed " << ratio << "\n";
      exit(0);
    }
 
    for( int i = 0; i < 3; i++)
      dpos[i] = pos0a[i] - pos0[i];

    double pratio = norm( dpos)/norm( pos0);
   if (pratio > 1.0e-9){
      std::cout << "failed " << pratio << "\n";
      exit(0);
    }
 
    max_ratio = max( max_ratio, ratio);
  }

  std::cout << "passed\n";
}
