#include <random>
#include "multipole_field.hh"
#include "pi.hh"

class Back_Door{
public:
  Back_Door( Multipole_Field::Field& _field): field(_field){}
  
  void test( std::default_random_engine& );
  void test2();

  Multipole_Field::Field& field;
};

template< class T>
double rel_diff( T a, T b){
  return fabs( a - b)/(0.5*(a + b));
}

void Back_Door::test2(){
  field.cx = 0.0;
  field.cy = 0.0;
  field.cz = 0.0;

  field.pol1 = 3.54702;
  
  field.polx = 0.0;
  field.poly = 0.0;
  field.polz = 0.0;

  field.polqm2 = 0.0; 
  field.polqm1 = 0.0;
  field.polq0 = 0.0;
  field.polq1 = 0.0;
  field.polq2 = 0.0;

  field.debye = 1.0e40;
  field.vperm = 1.0;
  field.solvdi = 1.0;
  field.fit_radius = 1.0;

  auto q = field.pol1/sqrt( 4.0*pi);

  Vec3< double> pos;
  pos[0] = 0.0;
  pos[1] = 0.0;
  pos[2] = 1.0;

  auto actual_V = q/(4.0*pi);
  
  auto V = field.potential( pos);
  if (rel_diff( V, actual_V) > 1.0e5){
    std::cout << actual_V << " " << V << "\n";
    std::cout << "failed\n";
    exit(0);
  }
}

void Back_Door::test( std::default_random_engine& gen){
  std::uniform_real_distribution< double> udist( 0.0, 10.0);

  field.cx = 10.0 - 2.0*udist( gen);
  field.cy = 10.0 - 2.0*udist( gen);
  field.cz = 10.0 - 2.0*udist( gen);

  field.pol1 = 10.0*udist( gen);
  
  field.polx = 10.0*udist( gen);
  field.poly = 10.0*udist( gen);
  field.polz = 10.0*udist( gen);

  field.polqm2 = 10.0*udist( gen);
  field.polqm1 = 10.0*udist( gen);
  field.polq0 = 10.0*udist( gen);
  field.polq1 = 10.0*udist( gen);
  field.polq2 = 10.0*udist( gen);

  field.debye = 1.0 + 10.0*udist( gen);
  field.vperm = 1.0 + 10.0*udist( gen);
  field.solvdi = 1.0 + 10.0*udist( gen);
  field.fit_radius = 1.0 + 10.0*udist( gen);

  Vec3< double> grad;

  Vec3< double> pos;
  pos[0] = 1.0;
  pos[1] = 2.0;
  pos[2] = 3.0;

  field.get_gradient( pos, grad);

  Vec3< double> fe_grad;
  {
    const double h = 1.0e-4;

    auto dV = [&]( int i) -> double {
      Vec3< double> new_pos = pos;
      new_pos[i] += h;
      auto Vp = field.potential( new_pos);
      new_pos[i] -= 2.0*h;
      auto Vm = field.potential( new_pos);
      new_pos[i] = pos[0];
      return (Vp - Vm)/(2*h);
    };
    

    for( int i = 0; i < 3; i++)
      fe_grad[i] = dV(i);
  }

  auto rd = [&]( int i) -> bool {
    return (rel_diff( grad[i], fe_grad[i]) > 1.0e-4);
  };

  for( int i = 0; i < 3; i++){
    if (rd(0) || rd(1) || rd(2)){
      std::cout << grad[i] << " " << fe_grad[i] << "\n";
      std::cout << "failed\n";
      exit(0);
    }
  }
}

int main(){
  Multipole_Field::Field field;
  Back_Door bd( field);

  std::default_random_engine gen;

  bd.test2();
  for( int i = 0; i < 10000; i++)
    bd.test( gen);

  std::cout << "passed\n";
}
