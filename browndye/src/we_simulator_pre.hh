/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/


#include <set>
#include "we_multithread.hh"
#include "molecule_pair.hh"
#include "simulator.hh"


class WE_Simulator;

class WE_Interface{
public:
  typedef Molecule_Pair_State* Object;
  typedef WE_Simulator* Info;

  static
  void initialize_object( WE_Simulator* sim,  
                          Molecule_Pair_State*);

  static
  void step_forward( WE_Simulator* sim, unsigned int ithread,
                     Molecule_Pair_State* state);

  static
  unsigned int bin( WE_Simulator* sim, Molecule_Pair_State* state, bool& exists);

  static
  unsigned int bin( WE_Simulator* sim, Molecule_Pair_State* state);


  static
  Molecule_Pair_State* new_object( WE_Simulator*);

  static
  void get_duplicated_objects( WE_Simulator*, 
                               Molecule_Pair_State*, unsigned int n_copies,
                               std::list< Molecule_Pair_State*>&);

  static
  void remove( WE_Simulator*, Molecule_Pair_State* state);

  static
  void get_crossing( WE_Simulator* sim, Molecule_Pair_State* state, bool& crossed, unsigned int& crossing);

  static
  unsigned int number_of_bins( WE_Simulator* sim);

  static
  unsigned int number_of_bin_neighbors( WE_Simulator* sim, unsigned int bin);

  template< class Function> 
  static
  void apply_to_bin_neighbors( WE_Simulator* sim, unsigned int bin, 
                               Function& f);

  // low bins are closer to reaction
  static
  bool have_population_reversal( WE_Simulator* sim, unsigned int bin0, double wt0, 
                                 unsigned int bin1, double wt1);

  typedef WE_Simulator* WE_Simulator_Ptr;

  static
  void set_null( WE_Simulator_Ptr& ref){
    ref = NULL;
  }

  static
  bool is_null( const WE_Simulator* ref){
    return (ref == NULL);
  }

  static
  double uniform( WE_Simulator* sim);

  static
  void output( WE_Simulator* sim, unsigned int crossing, double flux);

  static
  double reaction_coordinate( WE_Simulator* sim, Molecule_Pair_State* state);

  static
  unsigned int number_of_threads( WE_Simulator* sim);

  static
  double weight( WE_Simulator* sim, Molecule_Pair_State* state){
    return state->weight();
  }

  static
  void set_weight( WE_Simulator* sim, double wt, Molecule_Pair_State* state){
    state->set_weight( wt);
  }

  template< class F>
  static
  void apply_to_object_refs( F& f, WE_Simulator* sim);
};

