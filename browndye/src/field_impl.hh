/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

// Implementation of the Field class

#include "field.hh"
#include "error_msg.hh"
#include "pi.hh"
#include "jam_xml_pull_parser.hh"

namespace Field{

namespace JP = JAM_XML_Pull_Parser;

  //**************************************************************
/*
This class allows the Field< Interface>::potential to be compiled
with UNITS turned on, avoiding a units type clash between the
electrostatic multipole class and the desolvation field.
*/
template< class Potential, class Potential_Gradient>
class Selector{
public:
  static
  Potential potential( const Multipole_Field::Field* mfield,
                      const Vec3< Length>& pos){
    return Potential( 0.0);
  }

  static
  void get_gradient( const Multipole_Field::Field* mfield,
                     const Vec3< Length>& pos,
                     Vec3< Potential_Gradient>& grad){
    grad[0] = grad[1] = grad[2] = Potential_Gradient( 0.0);
  }
};

template<>
class Selector< EPotential, EPotential_Gradient>{
public:
  static
  EPotential potential( const Multipole_Field::Field* mfield,
                        const Vec3< Length>& pos){
    if (mfield)
      return mfield->potential( pos);
    else
      return EPotential( 0.0);
  }

  static
  void get_gradient( const Multipole_Field::Field* mfield,
                     const Vec3< Length>& pos,
                     Vec3< EPotential_Gradient>& grad){
    if (mfield)
      mfield->get_gradient( pos, grad);
    else{ // no mpoles
      grad[0] = grad[1] = grad[2] = EPotential_Gradient( 0.0);
    }
  }
};
  //**************************************************************
template< class Interface>
typename Interface::Potential 
Field< Interface>::potential( const Vec3< Length>& pos,
                              typename Field< Interface>::Hint& hint) const{  

  Potential v;
  const SGrid* sfield = sfields.lowest_found( v, pos, hint);

  if (sfield != NULL)
    return v;
  else
    return Selector< Potential, Potential_Gradient>::
      potential( mfield.get(), pos);
}

template< class Interface>
void Field< Interface>::
get_gradient( const Vec3< Length>& pos,
              Vec3< typename Interface::Potential_Gradient>& grad,
              typename Field< Interface>::Hint& hint
              ) const{  
  
  const SGrid* sfield = sfields.lowest_found( grad, pos, hint);

  if (sfield == NULL)
    Selector< Potential, Potential_Gradient>::
      get_gradient( mfield.get(), pos, grad);
}

  // automatically ordered the correct way re nesting
template< class Interface>
void Field< Interface>::initialize( 
  const Vector< typename Interface::String>& files){
  typedef typename Vector< String>::size_type size_type;

  if (files.empty())
    error( "no files for electric field");

  size_type nf = files.size();
  for( size_type i = 0; i<nf; i++){
    const String& file = files[i];
    SGrid* grid = new SGrid( file.c_str());
    sfields.insert( grid); 
  }

  sfields.check_consistency();
}

template< class Interface>
void Field< Interface>::
initialize( const typename Interface::String& mpole, 
            const Vector< typename Interface::String>& files){
  
  mfield.reset( new MField( mpole));
  initialize( files);
}

template< class Interface>
Length Field< Interface>::debye_length() const{
  if (!mfield)
    error( "Field::debye_length: need multipole for this");

  return mfield->debye_length();
}

template< class Interface>
bool Field< Interface>::has_solvent_info() const {
  return bool(mfield);
}

template< class Interface>
double Field< Interface>::solvent_dielectric() const{
  if (!mfield)
    error( "Field::solvent_dielectric: need multipole for this");

  return mfield->solvent_dielectric();
}

template< class Interface>
typename Interface::Permittivity 
Field< Interface>::vacuum_permittivity() const{
  if (!mfield)
    error( "Field::vacuum_permittivity: need multipole for this");

  return mfield->vacuum_permittivity();
}

  /*
template< class Interface>
typename Interface::Charge 
Field< Interface>::charge() const{
  return mfield->eff_charge()*4.0*pi*vacuum_permittivity()*solvent_dielectric();
}
  */

template< class SGrid>
class Shifter{
public:
  Shifter( const Vec3< Length>& offset): offset( offset){} 
  
  void operator()( SGrid& sgrid) const{
    sgrid.shift_position( offset);
  }
  
  const Vec3< Length>& offset;
};

template< class Interface>
void Field< Interface>::shift_position( const Vec3< Length>& offset){

  if (mfield)
    mfield->shift_position( offset);

  Shifter< SGrid> shifter( offset);
  sfields.apply( shifter);
}

template< class Interface>
typename Field< Interface>::Hint Field< Interface>::first_hint() const{
  return sfields.first_hint();
}

template< class I>
void Field<I>::get_corners( Vec3< Length>& low, Vec3< Length>& high) const{
  const SGrid& top = sfields.top_item();
  top.get_low_corner( low);
  top.get_high_corner( high);
}

}
