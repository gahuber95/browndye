/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
Implementation of Transform class.

I used Strassen matrix multiplication because I was too lazy to code 
up a Gaussian elimination routine or mess with linking with LAPACK.
*/

#include "transform.hh"

typedef Transform::Mat2 Mat2;

void zero_2x2( Mat2& a){
  a[0][0] = a[0][1] = a[1][0] = a[1][1] = 0.0;
}                            

void id_2x2( Mat2& a){
  a[0][0] = a[1][1] = 1.0;
  a[0][1] = a[1][0] = 0.0;
}

void Transform::set_zero(){
  zero_2x2( m01);
  zero_2x2( m10);
  id_2x2( m00);
  id_2x2( m11);
}

Transform::Transform(){
  set_zero();
}

void Transform::get_translated( const Vec3< Length>& r, Vec3< Length>& rt) const{
  rt[0] = r[0] + Length( m01[0][1]);
  rt[1] = r[1] + Length( m01[1][1]);
  rt[2] = r[2] + Length( m11[0][1]);
}


void Transform::get_transformed( const Vec3< Length>& r, Vec3< Length>& rt) const{  
  Vec3< Length> rm;
  get_rotated( r, rm);
  get_translated( rm, rt);
}

void Transform::get_rotation( Mat3< double>& rot) const{
  rot[0][0] = m00[0][0];
  rot[0][1] = m00[0][1];
  rot[0][2] = m01[0][0];

  rot[1][0] = m00[1][0];
  rot[1][1] = m00[1][1];
  rot[1][2] = m01[1][0];

  rot[2][0] = m10[0][0];
  rot[2][1] = m10[0][1];
  rot[2][2] = m11[0][0];
}

void Transform::set_rotation( const Mat3< double>& rot){
  m00[0][0] =  rot[0][0];
  m00[0][1] =  rot[0][1];
  m01[0][0] =  rot[0][2];
                 
  m00[1][0] =  rot[1][0];
  m00[1][1] =  rot[1][1];
  m01[1][0] =  rot[1][2];
                 
  m10[0][0] =  rot[2][0];
  m10[0][1] =  rot[2][1];
  m11[0][0] =  rot[2][2];
}

void Transform::get_translation( Vec3< Length>& tr) const{
  tr[0] = Length( m01[0][1]);
  tr[1] = Length( m01[1][1]);
  tr[2] = Length( m11[0][1]);
}

void Transform::set_translation( const Vec3< Length>& tr){
  m01[0][1] = fvalue( tr[0]);
  m01[1][1] = fvalue( tr[1]);
  m11[0][1] = fvalue( tr[2]);
}



void get_2x2_inverse( const Mat2& a, Mat2& ainv){
  double det = a[0][0]*a[1][1] - a[0][1]*a[1][0];
  ainv[0][0] = a[1][1]/det;
  ainv[0][1] = -a[0][1]/det;
  ainv[1][0] = -a[1][0]/det;
  ainv[1][1] = a[0][0]/det;
}

#define P2( i, j) c[i][j] = a[i][0]*b[0][j] + a[i][1]*b[1][j] 

void get_2x2_prod( const Mat2& a, const Mat2& b, Mat2& c){
  P2( 0, 0);
  P2( 1, 0);
  P2( 0, 1);
  P2( 1, 1);
}

#define D2( i, j) c[i][j] = a[i][j] - b[i][j]

void get_2x2_diff( const Mat2& a, const Mat2& b, Mat2& c){
  D2( 0,0);
  D2( 0,1);
  D2( 1,0);
  D2( 1,1);
}

#define SM2( i, j) c[i][j] = a[i][j] + b[i][j]

void get_2x2_sum( const Mat2& a, const Mat2& b, Mat2& c){
  SM2( 0,0);
  SM2( 0,1);
  SM2( 1,0);
  SM2( 1,1);
}


#define S2( i, j) c[i][j] = b*a[i][j]
void get_2x2_scaled( const Mat2& a, double b, Mat2& c){
  S2( 0,0);
  S2( 0,1);
  S2( 1,0);
  S2( 1,1);
}

void Transform::get_inverse( Transform& inv_trans) const{
  Mat2 R1, R2, R3, R4, R5, R6, R7;

  get_2x2_inverse( m00, R1);
  get_2x2_prod( m10, R1, R2);
  get_2x2_prod( R1, m01, R3);
  get_2x2_prod( m10, R3, R4);
  get_2x2_diff( R4, m11, R5);
  get_2x2_inverse( R5, R6);
  get_2x2_prod( R3, R6, inv_trans.m01);
  get_2x2_prod( R6, R2, inv_trans.m10);
  get_2x2_prod( R3, inv_trans.m10, R7);
  get_2x2_diff( R1, R7, inv_trans.m00);
  get_2x2_scaled( R6, -1.0, inv_trans.m11);
}

#define PP2( i, j)                                      \
  get_2x2_prod( trans0.m##i##0, trans1.m0##j, w0);      \
  get_2x2_prod( trans0.m##i##1, trans1.m1##j, w1);      \
  get_2x2_sum( w0, w1, trans01.m##i##j)


void get_product( const Transform& trans0, const Transform& trans1,
                  Transform& trans01){

  Mat2 w0, w1;
  PP2( 0,0);
  PP2( 0,1);
  PP2( 1,0);
  PP2( 1,1);
}

void copy_2x2( const Mat2& a, Mat2& b){
  b[0][0] = a[0][0];
  b[0][1] = a[0][1];
  b[1][0] = a[1][0];
  b[1][1] = a[1][1];
}

/*
void Transform::copy_from( const Transform& other){
  copy_2x2( other.m00, m00);
  copy_2x2( other.m01, m01);
  copy_2x2( other.m10, m10);
  copy_2x2( other.m11, m11);
}
*/
