/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
Implementation of Pathway class. Included by "pathway.hh"

 */

//*****************************************************************
namespace Pathways{

template< class Interface>
bool 
Pathway< Interface>::is_satisfied( typename Interface::Molecule_Pair& mols,
                                   const Criterion& criterion) const{

  size_type n_satis = 0;

  const Vector< Rxn_Pair>& pairs = criterion.pairs;
  for( Vector< Rxn_Pair>::const_iterator itr = pairs.begin();
       itr != pairs.end(); ++itr){
    
    const Rxn_Pair& pair = *itr;
    
    if (Interface::distance( mols, pair.atom0, pair.atom1) < pair.distance){
      ++n_satis;
      if (n_satis >= criterion.n_needed)
        return true;
    } 
  }

  const Vector< Criterion*>& criteria = criterion.criteria;
  for( Vector< Criterion*>::const_iterator itr = criteria.begin();
       itr != criteria.end(); ++itr){

    const Criterion* sub_criterion = *itr;
    if (is_satisfied( mols, *sub_criterion)){
      ++n_satis;
      if (n_satis >= criterion.n_needed)
        return true;
    }
  }

  return false;
}

//********************************************************************
template< class Interface>
Length Pathway< Interface>::
reaction_coordinate( const Criterion& criterion, 
                     typename Interface::Molecule_Pair& mols) const{
  
  size_type np = criterion.pairs.size();
  size_type nc = criterion.criteria.size();

  Vector< Length> distances( np + nc);

  for( size_type ip = 0; ip < np; ip++){
    const Rxn_Pair& pair = criterion.pairs[ip];
    distances[ip] = 
      Interface::distance( mols, pair.atom0, pair.atom1) - pair.distance;
 
    if (distances[ip] < Length( 0.0))
      distances[ip] = Length( 0.0);
  }
  

  for( size_type ic = 0; ic < nc; ic++){
    const Criterion& sub_criterion = *(criterion.criteria[ic]);
    distances[ np+ic] = reaction_coordinate( sub_criterion, mols);
  }
   
  size_type nn = criterion.n_needed;

  if (nn > np)
    return Length( INFINITY);

  else{
    std::partial_sort( distances.begin(), distances.begin() + nn, 
                       distances.end());   
    return distances[ nn-1];
  }
}

//********************************************************************
template< class Interface>
Length Pathway< Interface>::
reaction_coordinate( const Reaction& reaction, 
                     typename Interface::Molecule_Pair& mols) const{

  return reaction_coordinate( *(reaction.criterion), mols);
}

//********************************************************************
template< class Interface>
Length Pathway< Interface>::
reaction_coordinate( typename Interface::Molecule_Pair& mols, size_type i) const{

  return reaction_coordinate( *(reactions[i]), mols);
}

//********************************************************************
template< class Interface>
bool Pathway< Interface>::
is_satisfied( size_type irxn, typename Interface::Molecule_Pair& mols) const{

  return is_satisfied( mols, *(reactions[irxn]->criterion));
}
        
template< class Interface>
void Pathway< Interface>::
get_next_completed_reaction( size_type istate, 
                             typename Interface::Molecule_Pair& mols,
                             bool& completed, size_type& jrxn) const{

  const State& state = states[ istate];
  for( size_type k=0; k < state.rxns_from.size(); ++k){
    size_type irxn = state.rxns_from[k];
    if (is_satisfied( irxn, mols)){
      jrxn = irxn;
      completed = true;
      return;
    }
  }
  jrxn = (size_type)(-1);
  completed = false;
}

//********************************************************************
template< class Interface>
template< class F>
void Pathway< Interface>::
apply_to_pairs( const F& f, typename Interface::Molecule_Pair& mpair, 
                const Criterion& crit) const{

  size_type n = crit.pairs.size();
  for( size_type i=0; i<n; i++){
    const Rxn_Pair& pair = crit.pairs[i];
    f( mpair, pair.atom0, pair.atom1);
  }

  size_type nc = crit.criteria.size();
  for( size_type i=0; i<nc; i++)
    apply_to_pairs( f, mpair, crit.criteria[i]);
}

template< class Interface>
template< class F>
void Pathway< Interface>::
apply_to_pairs( const F& f, typename Interface::Molecule_Pair& mpair, 
                size_type irxn) const{
  const Reaction& rxn = reactions[irxn];
  apply_to_pairs( f, mpair, rxn.criterion);
}

//********************************************************************
template< class Interface>
template< class F>
void Pathway< Interface>::
apply_to_pairs_of_successors( const F& f, 
                              typename Interface::Molecule_Pair& mpair,
                              size_type istate) const{
  
  const State& state = states[ istate];
  size_type n = state.rxns_from.size();
  for( size_type i=0; i<n; i++){
    size_type irxn = state.rxns_from[i];
    apply_to_pairs( f, mpair, irxn);
  }
}

//********************************************************************

inline
size_type Pathway_Base::state_before_reaction( size_type irxn) const{
  return reactions[irxn]->state_before;
}

inline
size_type Pathway_Base::state_after_reaction( size_type irxn) const{
  return reactions[irxn]->state_after;
}

inline
size_type Pathway_Base::n_reactions() const{
  return reactions.size();
}

inline
size_type Pathway_Base::n_states() const{
  return states.size();
}

inline
size_type Pathway_Base::n_reactions_from( size_type istate) const{
  return states[ istate].rxns_from.size();
}

inline
size_type Pathway_Base::reaction_from( size_type istate, size_type i) const{
  return states[ istate].rxns_from[i];
}

inline
const String& Pathway_Base::reaction_name( size_type i) const{
  return reactions[i]->name;
}

inline
const String& Pathway_Base::state_name( size_type i) const{
  return states[i].name;
}

}
