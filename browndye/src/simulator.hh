/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

#ifndef __SIMULATOR_HH__
#define __SIMULATOR_HH__

/*
Simulator is the base class for NAM_Simulator and WE_Simulator.
It holds the molecule data and provides code for reading in 
data from the XML file input to "nam_simulation", we_simulation",
and "build_bins".

*/

#include <set>
#include "molecule_pair.hh"

class Simulator{
public:
  typedef JAM_XML_Pull_Parser::Node_Ptr Node_Ptr;
  typedef Vector< int>::size_type size_type;

  Node_Ptr initialized_node( JAM_XML_Pull_Parser::Parser&);
  ~Simulator();
  Vector< uint32_t> initialize_rngs( uint32_t);

  std::ofstream& output_stream();
  void input_rng_states( Node_Ptr); 

  void output_rng_states( std::ofstream&, unsigned int n_spaces) const;

protected:
  void input_rng_state( Node_Ptr, size_type); 
  void output_rng_state( std::ofstream&, size_type irng, unsigned int n_spaces) const;

  typedef std::list< Node_Ptr> NList;

  void begin_output( std:: ofstream&);

  void renormalize_reactions();

  class MP_Comparer{
  public:
    bool operator()( const Molecule_Pair_State* mp0, const Molecule_Pair_State* mp1){
      return mp0->number() < mp1->number();
    }
  };
  
  Molecule_Pair_Common common;
  Vector< Molecule_Pair_Thread> mthreads;
  std::set< Molecule_Pair_State*, MP_Comparer> states;
  std::list< Molecule_Pair> pairs;
  
  std::string output_name;
  
  Vector< uint32_t> extra_seed;
};


#endif
