/*
 * molecule_pair_thread.cc
 *
 *  Created on: Sep 12, 2015
 *      Author: ghuber
 */
#include "molecule_pair_thread.hh"

void Molecule_Pair_Thread::initialize( Molecule_Pair_Common& common,
                                       JAM_XML_Pull_Parser::Node_Ptr node, unsigned int n_in){

  n = n_in;
  pthread_mutex_lock( &common.mutex);

  printf( "molecule pair thread initialize\n"); fflush( stdout);

  JAM_XML_Pull_Parser::Node_Ptr mol1_node = checked_child( node, "molecule1");
  mol1.initialize( common.mol1, mol1_node);

  pthread_mutex_unlock( &common.mutex);
}

void Molecule_Pair_Thread::set_seed( const Vector< uint32_t>& seed){
  rng.set_seed( seed);
}
