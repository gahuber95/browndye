#include "trajectory_outputter.hh"

// Implements Trajectory_Outputter class

std::ostream& operator<<( std::ostream& out, const std::string& str){
  out << str.c_str();
  return out;
}

void Trajectory_Outputter::initialize( std::ostream& _out,
                                       const Vec3< Length>& center0, 
                                       const Vec3< Length>& center1,
                                       std::ostream& _index_out
                                       ){
  out = &_out;
  index_out = &_index_out;

  (*out) << "<trajectories>" << std::endl;

  (*out) << "  <center-molecule0>" << std::endl;
  (*out) << "    " << fvalue( center0[0]) << " " << fvalue( center0[1]) << " " << fvalue( center0[2]) << "" << std::endl; 
  (*out) << "  </center-molecule0>" << std::endl;

  (*out) << "  <center-molecule1>" << std::endl;
  (*out) << "    " << fvalue( center1[0]) << " " << fvalue( center1[1]) << " " << fvalue( center1[2]) << "" << std::endl; 
  (*out) << "  </center-molecule1>" << std::endl;
  (*out) << "  <n-states-per-record> " << nstates << " </n-states-per-record>" << std::endl;

  (*out) << "  <fields>" << std::endl;
  (*out) << "    <field> time 1 </field>" << std::endl;
  (*out) << "    <field> translation 3</field>" << std::endl;
  (*out) << "    <field> quaternion 3 </field>" << std::endl;
  (*out) << "    <field> near-field 1 </field>" << std::endl;
  (*out) << "    <field> coulombic 1 </field>" << std::endl;
  (*out) << "    <field> desolvation 1 </field>" << std::endl;
  (*out) << "  </fields>" << std::endl;

  itraj = 0;
  isubtraj = 0;
  istate = 0;
  last_pos = out->tellp();
  (*out) << "</trajectories>" << std::endl; 

  (*index_out) << "<trajectories>" << std::endl;
  last_index_pos = index_out->tellp();
  (*index_out) << "</trajectories>" << std::endl;
}

void Trajectory_Outputter::end_trajectories(){}

void print_space( std::ostream* out, int n){
  for (int i = 0; i < n; i++)
    (*out) << " ";
}

void print_fate( Molecule_Pair_Fate fate, int ns, std::ostream* out){
  print_space( out, ns);
  if (fate == Final_Rxn)
    (*out) << "<fate> reacted </fate>" << std::endl;
  else if (fate == Escaped)
    (*out) << "<fate> escaped </fate>" << std::endl;
  else if (fate == Stuck)
    (*out) << "<fate> stuck </fate>" << std::endl;
  else
    (*out) << "<fate> in-progress </fate>" << std::endl;
}

void Trajectory_Outputter::end_trajectory( Molecule_Pair_Fate fate, const String& rxn, 
                                           const String& rxn_state, Length min_rxn_coord){
  ++itraj;

  out->seekp( last_pos);
  print_fate( fate, 4, out);
  (*out) << "    <rxn-state> " << rxn_state << " </rxn-state>" << std::endl;
  if (fate == Final_Rxn)
    (*out) << "    <rxn> " << rxn << " </rxn>" << std::endl;
  (*out) << "    <min-rxn-coord> " << fvalue( min_rxn_coord) << "</min-rxn-coord>" << std::endl;
  (*out) << "  </trajectory>" << std::endl;
  last_pos = out->tellp();
  (*out) << "</trajectories>" << std::endl;

  index_out->seekp( last_index_pos);
  (*index_out) << "  </trajectory>" << std::endl;
  last_index_pos = index_out->tellp();
  (*index_out) << "</trajectories>" << std::endl;
}

void Trajectory_Outputter::print_end_subtrajectory( Molecule_Pair_Fate fate, 
                                                    const String& rxn,
                                                    const String& rxn_state,
                                                    std::ostream* sout){
  print_fate( fate, 6, sout);
  (*sout) << "      <rxn-state-end> " << rxn_state << " </rxn-state-end>" << std::endl;
  
  if (not ((fate == Escaped) || (fate == Stuck)))
    (*sout) << "      <rxn> " << rxn << " </rxn>" << std::endl; 
}
 
void Trajectory_Outputter::end_subtrajectory( Molecule_Pair_Fate fate, 
                                              const String& rxn,  
                                              const String& rxn_state){
  ++isubtraj;
  write_states();
  out->seekp( last_pos);
  print_end_subtrajectory( fate, rxn, rxn_state, out);

  (*out) << "    </subtrajectory>" << std::endl;   

  last_pos = out->tellp();
  (*out) << "  </trajectory>" << std::endl;
  (*out) << "</trajectories>" << std::endl;
  
  index_out->seekp( last_index_pos);
  print_end_subtrajectory( fate, rxn, rxn_state, index_out);
  (*index_out) << "    </subtrajectory>" << std::endl;
  last_index_pos = index_out->tellp();
  (*index_out) << "  </trajectory>" << std::endl;
  (*index_out) << "</trajectories>" << std::endl;
}

void Trajectory_Outputter::start_trajectory(){
  istate = 0;
  isubtraj = 0;
  out->seekp( last_pos);
  traj_start = out->tellp();
  (*out) << "  <trajectory>" << std::endl;
  (*out) << "    <n-traj> " << itraj << " </n-traj>" << std::endl;
  last_pos = out->tellp();

  index_out->seekp( last_index_pos);
  (*index_out) << "  <trajectory>" << std::endl;
  (*index_out) << "    <n-traj> " << itraj << " </n-traj>" << std::endl;
  (*index_out) << "    <start> " << traj_start << " </start>" << std::endl;
  last_index_pos = index_out->tellp();
}

void Trajectory_Outputter::start_subtrajectory( const String& rxn_state){
  out->seekp( last_pos);
  subtraj_start = out->tellp();
  (*out) << "    <subtrajectory>" << std::endl;
  (*out) << "      <n-subtraj> " << isubtraj << " </n-subtraj>" << std::endl;
  (*out) << "      <rxn-state> " << rxn_state.c_str() << " </rxn-state>" << std::endl;
  last_pos = out->tellp();
  (*out) << "    </subtrajectory>" << std::endl;
  (*out) << "  </trajectory>" << std::endl;
  (*out) << "</trajectories>" << std::endl;

  index_out->seekp( last_index_pos);
  (*index_out) << "    <subtrajectory>" << std::endl;
  (*index_out) << "      <n-subtraj> " << isubtraj << " </n-subtraj>" << std::endl;
  (*index_out) << "      <start> " << subtraj_start << " </start>" << std::endl;
  (*index_out) << "      <rxn-state> " << rxn_state << " </rxn-state>" << std::endl;
  last_index_pos  = index_out->tellp();
  (*index_out) << "    </subtrajectory>" << std::endl;
  (*index_out) << "  </trajectory>" << std::endl;
  (*index_out) << "</trajectories>" << std::endl;
}

void Trajectory_Outputter::write_states(){

  if (istate > 0){
    uint k = 0;
    for (uint kstate=0; kstate < istate; ++kstate){
      const State& state = states[ kstate];
      state_output[ k+0] = fvalue( state.time);
      state_output[ k+1] = fvalue( state.trans[0]);
      state_output[ k+2] = fvalue( state.trans[1]);
      state_output[ k+3] = fvalue( state.trans[2]);
      state_output[ k+4] = state.quat[1];
      state_output[ k+5] = state.quat[2];
      state_output[ k+6] = state.quat[3];
      state_output[ k+7] = fvalue( state.near_field);
      state_output[ k+8] = fvalue( state.coulombic);
      state_output[ k+9] = fvalue( state.desolvation);
      k += 10;
    }
    std::string str = string_of_floats( state_output, k);
    out->seekp( last_pos);
    
    if (istate < nstates){
      (*out) << "      <s>" << std::endl;
      (*out) << "        <n> " << istate << " </n>" << std::endl;
      (*out) << "<data>" << str.c_str() << "</data>" << std::endl;
      (*out) << "      </s>" << std::endl;
    }
    else
      (*out) << "<s>" << str.c_str() << "</s>" << std::endl;
    
    last_pos = out->tellp();
    (*out) << "    </subtrajectory>" << std::endl;
    (*out) << "  </trajectory>" << std::endl;
    (*out) << "</trajectories>" << std::endl;
  }
}

void Trajectory_Outputter::
output_state( Time time, const Vec3< Length>& trans, const Mat3< double>& rot, 
              Energy near_field, Energy coulombic, Energy desolvation){

  State& state = states[ istate];
  state.time = time;
  for( uint i = 0; i < 3; i++)
    state.trans[i] = trans[i];

  mat_to_quat( rot, state.quat); 
  state.near_field = near_field;
  state.coulombic = coulombic;
  state.desolvation = desolvation;

  ++istate;
  if (istate == nstates){
    write_states();
    istate = 0;
  }
}

// constructor
Trajectory_Outputter::Trajectory_Outputter(){
  states.resize( nstates);
  state_output.resize( nstates*nelements);
  istate = 0;
  out = NULL;
  index_out = NULL;
  last_pos = -1;
  last_index_pos = -1;
  traj_start = -1;
  subtraj_start = -1;
}

