/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
The transform class contains the affine rigid body transformation
(rotation and translation), implemented as a 4x4 matrix.
*/

#ifndef __TRANSFORM_HH__
#define __TRANSFORM_HH__

#include <fstream>
#include "units.hh"
#include "small_vector.hh"

class Transform{
public:
  typedef SMat< double, 2,2> Mat2;

  Transform();
  void set_zero();
  void get_transformed( const Vec3< Length>&, Vec3< Length>&) const;
  void get_inverse( Transform&) const;
  void get_translated( const Vec3< Length>&, Vec3< Length>&) const;

  template< class U>
  void get_rotated( const Vec3< U>&, Vec3< U>&) const;

  template< class U>
  void rotate( Vec3< U>&) const;

  void get_translation( Vec3< Length>& tr) const;
  void get_rotation( Mat3< double>& rot) const;
  void set_translation( const Vec3< Length>& tr);
  void set_rotation( const Mat3< double>& rot);

  // in order of matrix multiplication
  friend 
  void get_product( const Transform&, const Transform&, Transform&);
  
private:
  Mat2 m00, m01, m10, m11;
};


template< class U>
void Transform::get_rotated( const Vec3< U>& r, Vec3< U>& rt) const{
  rt[0] = m00[0][0]*r[0] + m00[0][1]*r[1] + m01[0][0]*r[2];
  rt[1] = m00[1][0]*r[0] + m00[1][1]*r[1] + m01[1][0]*r[2];
  rt[2] = m10[0][0]*r[0] + m10[0][1]*r[1] + m11[0][0]*r[2];
}

template< class U>
void Transform::rotate( Vec3< U>& r) const{
  Vec3< U> new_r;
  get_rotated( r, new_r);
  r = new_r;
}



#endif

