/*
Defines various auxilliary classes used by Wiener::Process.
Included by "wiener.hh".
*/

#include <stdlib.h>
#include <math.h>
#include <limits>
#include <forward_list>
#include <cstddef>

namespace Wiener{

template< class Interface>
struct W_Info{
  typedef typename Interface::Vector Vector;
  typedef typename Interface::Time Time;
  typedef typename Interface::Sqrt_Time Sqrt_Time;

  W_Info( const W_Info< Interface>&);
  W_Info();

  Vector w;
  Time dt;
  Time t;

private:
  W_Info& operator=( const W_Info< Interface>&);
};

  // constructor
template< class Interface>
W_Info< Interface>::W_Info(): dt( NAN), t( NAN){}

  // copy constructor
template< class Interface>
W_Info< Interface>::W_Info( const W_Info< Interface>& other){
  Interface::copy( other.w, w);
  t = other.t;
  dt = other.dt;
}
}
