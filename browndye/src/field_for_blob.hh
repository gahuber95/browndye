#ifndef __FIELD_FOR_BLOB_HH__
#define __FIELD_FOR_BLOB_HH__

#include "field.hh"
#include "collision_detector.hh"

template< class CInterface, class FInterface>
struct Field_For_Blob{
  Field::Field< FInterface>* field;

  Collision_Detector::Structure< CInterface>* 
    collision_structure;

  typename Field::Field< FInterface>::Hint* hint_ptr;

  Field_For_Blob(){
    field = nullptr;
    collision_structure = nullptr;
    hint_ptr = nullptr;
  }
};

#endif
