/*
 * molecule_pair_state.hh
 *
 *  Created on: Sep 8, 2015
 *      Author: root
 */

#ifndef MOLECULE_PAIR_STATE_HH_
#define MOLECULE_PAIR_STATE_HH_

#include "units.hh"
#include "vector.hh"
#include "small_molecule_state.hh"
#include "large_molecule_state.hh"
#include "molecule_pair_fate.hh"
#include "atom_small.hh"

class Molecule_Pair_Common;

class Molecule_Pair_State{
public:
  typedef Vector< Atom_Small>::size_type size_type;

  Molecule_Pair_State();
  Molecule_Pair_State( unsigned long int);
  size_t current_reaction_state() const;
  // puts molecule 0 at origin
  void initialize( unsigned long int);
  unsigned long int number() const;
  double weight() const;
  void set_weight( double);
  bool had_reaction() const;
  Molecule_Pair_Fate fate() const;
  void set_initial_reaction_state( const Molecule_Pair_Common&);
  size_t last_reaction() const;

  // retains number
  void copy_from( const Molecule_Pair_State&);

private:
  friend class Molecule_Pair;
  friend class Stepper_Interface;
  friend class Mover_Interface;
  friend class Rxn_Interface;

  // data
  Large_Molecule_State mol0;
  Small_Molecule_State mol1;

  size_type current_rxn_state;
  size_type last_rxn;
  bool had_rxn;
  Molecule_Pair_Fate ffate;

  unsigned long int n_full_steps;
  unsigned long int n_succ_collisions;

  double wt;
  Length min_rxn_coord;
  Time time;
  unsigned long int n;
  Diffusivity diffu;
  Time last_dt;
};

// constructor
inline
Molecule_Pair_State::Molecule_Pair_State(){
  initialize(0);
}

inline
Molecule_Pair_State::Molecule_Pair_State( unsigned long int _number){
  initialize(_number);
}

inline
unsigned long int Molecule_Pair_State::number() const{
  return n;
}

inline
double Molecule_Pair_State::weight() const{
  return wt;
}

inline
void Molecule_Pair_State::set_weight( double _wt){
  wt = _wt;
}


inline
bool Molecule_Pair_State::had_reaction() const{
  return had_rxn;
}

inline
Molecule_Pair_Fate Molecule_Pair_State::fate() const{
  return ffate;
}

inline
size_t Molecule_Pair_State::last_reaction() const{
  return last_rxn;
}


#endif /* MOLECULE_PAIR_STATE_HH_ */
