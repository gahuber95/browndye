/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
The Pathway class implements the reaction network and is used to
determine when a reaction takes place.  It also reads in its data
from an XML node. The Interface class has the
following types and static functions:

Molecule_Pair - type

Length distance( Molecule_Pair&, size_type atom0, size_type atom1) - 
distance between two atoms

"size_type" is an alias for Vector< int>::size_type.

 */

#ifndef __PATHWAYS_HH__
#define __PATHWAYS_HH__

#include "pathways_pre.hh"

namespace Pathways{

  template< class Interface>
  class Pathway: public Pathway_Base{
  public:
    /* 
    From base class:

    size_type n_states() const;
    size_type n_reactions() const;

    size_type state_before_reaction( size_type irxn) const;
    size_type state_after_reaction( size_type irxn) const;

    size_type n_reactions_from( size_type istate) const;
    size_type reaction_from( size_type istate, size_type i) const;

    void remap_molecule0( const std::vector< size_type>&);
    void remap_molecule1( const std::vector< size_type>&);

    size_type first_state() const;
    const String& reaction_name( size_type) const;
    const String& state_name( size_type) const;

    void initialize( const std::string& file);

    void initialize_from_node( JP::Node_Ptr node);
     */

    typedef typename Interface::Molecule_Pair Molecule_Pair;
    
    Length reaction_coordinate( Molecule_Pair& mols, size_type irxn) const;
    
    bool is_satisfied( size_type irxn, Molecule_Pair& mols) const;
    
    void get_next_completed_reaction( size_type istate, Molecule_Pair& mols,
                                      bool& completed, size_type& irxn) const;
    
    // f( mpair, atom0, atom1) - all atom pairs define criterion of irxn
    template< class F>
    void apply_to_pairs( const F& f, Molecule_Pair& mpair, 
                         size_type irxn) const;
    
    // apply_to_pairs applied to reactions leaving istate
    template< class F>
    void apply_to_pairs_of_successors( const F& f, Molecule_Pair& mpair, 
                                       size_type istate) const;

    //*******************
  private:
    bool is_satisfied( Molecule_Pair& mols, const Criterion& criterion) const;

    template< class F>
    void apply_to_pairs( const F& f, Molecule_Pair& mpair, 
                         const Criterion& crit) const;

    Length reaction_coordinate( const Criterion& criterion, 
                                Molecule_Pair& mols) const;

    Length reaction_coordinate( const Reaction& reaction, 
                                Molecule_Pair& mols) const;
  }; 

}

#include "pathways_impl.hh"

/*
<roottag>
  <first-state> ... </first-state>

  <reactions>    
    <reaction>
      <number>0</number>
      <state-to> 3 </state-to>
      <state-from> 4 </state-from>

      <criterion>
        <n-needed> 3 </n-needed>
        <pair>
          <atoms> i0 i1 </atoms>
          <distance> 2.1 </distance>
        </pair>
        ....

        <criterion> .. </criterion>
        ....

      </criterion>
    </reaction>
    ...
  </reactions>

  <states>
    <state>
      <number> 0 </number>
      <rxn-to> 1 </rxn-to>
      ...
      <rxn-from> 2 </rxn-from>
      ...
    </state>
    ...
  </states>
</roottag>

*/

#endif

