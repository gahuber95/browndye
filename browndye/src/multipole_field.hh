/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
Implements the multipole expansion of the electric field outside
of the outer-most grid.

*/

#ifndef __MULTIPOLE_FIELD_HH__
#define __MULTIPOLE_FIELD_HH__

#include "jam_xml_pull_parser.hh"
#include "units.hh"
#include "small_vector.hh"

class Back_Door;

namespace Multipole_Field{

namespace JP = JAM_XML_Pull_Parser;
typedef JP::String String;

class Field{
public:
  Field( const String& file);
  Field( std::ifstream&);
  Field( JAM_XML_Pull_Parser::Parser&);
  Field();

  EPotential potential( const Vec3< Length>& pos) const;
  void get_gradient( const Vec3< Length>& pos, Vec3< EPotential_Gradient>& g) const;
  Length debye_length() const;
  Permittivity vacuum_permittivity() const;
  double solvent_dielectric() const;

  // real charge divided by 4 pi eps alpha; eps - vacuum permittivity,
  // alpha - solvent dielectric
  Charge eff_charge() const; 
  void get_center( Vec3< Length>&) const;
  void shift_position( const Vec3< Length>&);

  friend class ::Back_Door;

private: 
  typedef UProd< Charge, Length>::Res EDipole;
  typedef UProd< Dipole, Length>::Res EQuadrupole;

  void initialize( JP::Parser&);
  void initialize( std::ifstream&);
  void get_m3_from_node( JP::Node_Ptr node, const String& tag);    

  void get_potential_n_grads( const Vec3< Length>& pos, EPotential& phi_out,
                              Vec3< EPotential_Gradient>& grad,
                              bool get_grad) const;

  void init_variables();

  Length cx,cy,cz;
  Charge pol1;
  EDipole polx,poly,polz;
  EQuadrupole polqm2, polqm1, polq0, polq1, polq2;
  Length debye;
  Permittivity vperm;
  double solvdi;
  Length fit_radius;
};

}

#include "multipole_field_impl.hh"

#endif
