/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/


#ifndef __JAM__VECTOR_HH__
#define __JAM__VECTOR_HH__

/* 
The Vector class wraps the std::vector class, giving
bounds-checking and useful default values if DEBUG is #define'd.
*/


#include <vector>

#include "error_msg.hh"

#include "default_values.hh"

//*********************************************************************
template< class T>
class Vector: public std::vector<T>{
public:
  typedef typename std::vector<T>::size_type size_type;
  typedef typename std::vector<T>::difference_type difference_type;
  typedef std::vector<T> Super;
  typedef typename Super::reference TRef;
  typedef typename Super::const_reference const_TRef;

  TRef operator[]( size_type i){
#ifdef DEBUG
    if (i >= this->size()){
      error( "Vector: out of bounds: ", i, " ", this->size());
    }
#endif

    return Super::operator[](i);
  }

  const_TRef operator[]( size_type i) const{
#ifdef DEBUG
    if (i >= this->size()){
      error( "Vector: out of bounds: ", i, " ", this->size());
    }
#endif
    return Super::operator[](i);
  }

  typedef JAM_Vector::Default_Value< T> DV;

  explicit Vector( size_type n = 0): Super( n){
    if (DV::is_specialized)
      for( size_type i = 0; i<n; i++)
        DV::set_value( Super::operator[](i));
  }

  Vector( size_type n, const T& t): Super( n, t){}

  Vector( const T* data, size_type n){
    resize( n);
    for( size_type i=0; i<n; i++)
      (*this)[i] = data[i]; 
  }

  Vector( const Vector< T>& v): Super( v){}

  Vector<T>& operator=( const Vector< T>& v){
    return static_cast< Vector< T>& >(Super::operator=( v));
  }

  void resize( size_type n){
    size_type old_n = Super::size();
    Super::resize( n);
    if (DV::is_specialized){
      if (n > old_n)
        for( size_type i = old_n; i<n; i++)
          DV::set_value( Super::operator[](i));
    }
  }

  void resize( size_type n, const T& t){
    Super::resize( n, t);
  }

};


#endif


