/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
  Various auxilliary classes used by the Propagator class. Included by
  "lmz_propagator.hh".
*/

#include <cmath>
#include "spline.hh"
#include "sarfraz_spline.hh"
#include "browndye_rng.hh"
#include "node_info.hh"
#include "linalg3.hh"
#include "rotations.hh"
#include "rotne_prager.hh"
#include "pi.hh"


namespace LMZ{

  namespace L = Linalg3;
  namespace JP = JAM_XML_Pull_Parser;

  //#######################################################################
  template< class I>
  class Theta_Distribution{
  public:
    typedef typename I::Length Length;
    typedef typename I::Time Time;
    typedef typename I::Random_Number_Generator RNG;

    void initialize( JP::Node_Ptr node, const std::string& ioro_tag,
                     const Vector< double>& thetas);
    void get_sample( RNG& rng, double& theta, Time& time) const;
    void get_sample_old( RNG& rng, double& theta, Time& time) const;

  private:
    typedef Vector< double>::size_type size_type;

    Sarfraz::Spline< double, double> cml_prob;
    Spline< double, Time> mean_time;
    Spline< double, Time> sdev_time;

    Vector< double> thetas, cml_prob_data;
    Vector< Time> mean_time_data, sdev_time_data;
    double max_theta;
  };

  //###########################################################################
  enum In_Or_Out{In, Out};

  template< class I>
  class Shell_Distribution{
  public:
    typedef typename I::Length Length;
    typedef typename I::Time Time;
    typedef typename I::Random_Number_Generator RNG;

    void propagate( RNG& rng, double theta_in, double phi_in, 
                    double& theta, double& phi,
                    In_Or_Out& in_or_out, Time& t) const;

    void initialize( JP::Node_Ptr node);

  private:
    typedef Vector< double>::size_type size_type;
    typedef decltype( Time()*Time()) Time2;

    Theta_Distribution<I> inner_dist, outer_dist;
    double prob_inner;
    Vector< double> thetas;
  };

}
