#ifndef __SMALL_MOLECULE_THREAD_HH__
#define __SMALL_MOLECULE_THREAD_HH__

#include "units.hh"
#include "jam_xml_pull_parser.hh"
#include "vector.hh"
#include "small_molecule_common.hh"
#include "atom_small.hh"
#include "blob_interface.hh"
#include "col_interface_large.hh"
#include "col_interface_small.hh"
#include "collision_detector.hh"
#include "born_field_interface.hh"
#include "parameter_info.hh"

class Small_Molecule_Thread{
public:
  typedef Vector< int>::size_type size_type;
  typedef UQuot< Energy, Volume>::Res Vol_Potential;

  Small_Molecule_Thread();
  void set_up_collision_structure();
  void get_atoms( const char* file);
  void initialize( const Small_Molecule_Common&, JAM_XML_Pull_Parser::Node_Ptr);

  Atom_Small& atom( size_type i);
  const Atom_Small& atom( size_type i) const;
  Vector< size_t> atom_indices() const;

private:
  friend class Back_Door;

  typedef Blob_Interface< Col_Interface_Large,
                          Born_Field_Interface> Q2_Blob_Interface;

  Vector< Atom_Small> atoms;
  std::shared_ptr< Collision_Detector::Structure< Col_Interface_Small> > 
  collision_structure; 
  const Near_Interactions::Parameter_Info* pinfo;

  const Atom_Small* spring_atom1;

  friend
  void add_forces_and_torques(
                              const Large_Molecule_Common& molc0,
                              Large_Molecule_State& mol0,
                              const Small_Molecule_Common& molc1,
                              Small_Molecule_Thread& molt1,
                              Small_Molecule_State& mol1,

                              bool& has_collision,
                              Length& violation,
                              Atom_Large_Ref& aref0,
                              Atom_Small_Ref& aref1
                              );

  friend
  void get_potential_energy(
                            const Large_Molecule_Common& molc0,
                            Large_Molecule_State& mol0,
                            const Small_Molecule_Common& molc1,
                            Small_Molecule_Thread& molt1,
                            Small_Molecule_State& mol1,
                            Energy& vnear, Energy& vcoul, Energy& vdesolv
                             );
};

inline
Atom_Small& Small_Molecule_Thread::atom( Vector< int>::size_type i){
  return atoms[i];
}

inline
const Atom_Small& Small_Molecule_Thread::atom( Vector< int>::size_type i) const{
  return atoms[i];
}


inline
Vector< size_t> Small_Molecule_Thread::atom_indices() const{
	return gen_atom_indices( atoms);
}

#endif
