#ifndef __SFIELD_PTR_TREE_HH__
#define __SFIELD_PTR_TREE_HH__

#include <list>
#include "error_msg.hh"
#include <list>
#include <iostream>
#include <memory>
#include <string.h>

/* 
Tree owns all pointers given to it.

Interface static members:
typename Contained;
typename Point;

returns true if t0 encloses t1
bool encloses( const T& t0, const T& t1)

template< class Info>
bool is_inside( Info&, const Point&, const T& t)

bool overlap( const T& t0, const T& t1)

void error( const char* msg)

*/

namespace SField_Ptr{

template< class Interface>
class Tree{
public:
  typedef typename Interface::Contained T;
  typedef typename Interface::Point Point;
  typedef Interface I;

  struct Hint{
    typedef Tree<I> Tr;
    const Tree<I>* data;

#ifdef DEBUG
    Hint(){
      data = nullptr;
    }
#endif
  };

  void insert( T* t);

  //Hint* first_hint() const;
  Hint first_hint() const;

  template< class Info>
  T* lowest_found( Info& info, const Point&, Hint&) const;

  Tree();

  void check_consistency() const;
  void check_for_loops() const;

  template< class F>
  void apply( const F& f);

  const T& top_item() const;

private:
  //typedef typename std::list< Tree< I>*>::iterator citerator;
  //typedef typename std::list< Tree< I>*>::const_iterator cciterator;

  std::unique_ptr<T> self;
  std::list< std::shared_ptr< Tree<I> > > children;
  Tree<I>* parent;

  void check_consistency_rec( const Tree<I>* ) const;
};

template< class I>
inline
const typename I::Contained& Tree< I>::top_item() const{
  return *self;
}

template< class I>
template< class F>
void Tree< I>::apply( const F& f){
  f( *self);
  //  for( citerator itr = children.begin(); itr != children.end(); ++itr)
  for( auto& child: children)
    child->apply( f);
}
  
template< class I>
inline
typename Tree<I>::Hint Tree<I>::first_hint() const{
  Hint hint;
  hint.data = this;
  return hint;
}

inline
int srep( int* t){
  if (t == nullptr)
    return -1;
  else
    return *t;
}

template< class I>
void Tree<I>::check_consistency_rec( const Tree<I>* top) const{
  const char* head = "sfield_ptr_tree consistency check: ";
  char msg[1000];
  strcpy( msg, head);

  if (self == nullptr){
    if (this != top){
      strcat( msg, "non top has null self" );
      I::error( msg);
    }
  }
  else{
    for( auto& ptr: children){
      const Tree<I>& child = *ptr;
      if (!I::encloses( *self, *(child.self))){
        strcat( msg, "child not contained by parent");
        I::error( msg);
      }
    }
  }
  
  for( auto& iptr: children){
    const Tree<I>& child = *iptr;
    for( auto& jptr: children){
      if (jptr != iptr){
        const Tree<I>& child2 = *jptr;
        if (I::encloses( *(child.self), *(child2.self))){ 
          strcat( msg, "one is contained by other on same level");
          I::error( msg);
        }
      }
    }
  }

  for( auto& ptr: children){
    const Tree<I>& child = *ptr;
    if (child.parent != this){
      strcat( msg, "parent connectivity is wrong");
      I::error( msg);
    }
  }

  for( auto& ptr: children){
    const Tree<I>& child = *ptr;
    child.check_consistency_rec( top);
  }

}

// call on top only
template< class I>
void Tree<I>::check_consistency() const{
  check_consistency_rec( this);
}

template< class I>
void Tree<I>::check_for_loops() const{
  for( auto& ptr: children){
    if (ptr.use_count() > 1){
      std::cerr << "count = " << ptr.use_count() << "\n";
      exit(1);
    }
    ptr->check_for_loops();
  }
}

  // constructor
template< class I>
Tree<I>::Tree(){
  self = nullptr;
  parent = nullptr;
}


template< class I>
void Tree<I>::insert( typename I::Contained* t){

  if (self == nullptr || I::encloses( *self, *t)){
    bool encloses_children = false;
    bool encloses_all = true;

    for( auto& ptr: children){
      Tree< I>& child = *ptr;
      const T& child_item = *(child.self);
      if (I::overlap( *(child.self), *t) && 
          !(I::encloses( child_item, *t) || I::encloses( *t, child_item)))
        I::error( "sfield_ptr_tree: should not have overlap");
    }

    for( auto& ptr: children){
      Tree< I>& child = *ptr;
      if (I::encloses( *(child.self), *t)){      
        child.insert( t);
        return;
      }
      else if (I::encloses( *t, *(child.self)))
        encloses_children = true;       
      
      else
        encloses_all = false;
    }

    if (!self && encloses_all){
      self.reset( t);
      return;
    }

    else{
      std::shared_ptr< Tree<I> > new_tree( new Tree<I>());
      new_tree->self.reset( t);
      new_tree->parent = this;

      if (encloses_children){
        std::list< std::shared_ptr< Tree<I> > > grandchildren;

        /*
        auto itr = children.begin();
        while( itr != children.end()){
          Tree< I>& grandchild = **itr;
          if (I::encloses( *t, *(grandchild.self))){
            grandchild.parent = &(*new_tree);
            grandchildren.push_back( *itr);
            itr = children.erase( itr);
          } 
          else
            ++itr;
        }
        */

        
        for( auto& ptr: children){
          Tree< I>& grandchild = *ptr;
          if (I::encloses( *t, *(grandchild.self))){
            grandchild.parent = &(*new_tree);
            grandchildren.push_back( ptr);
          }     
        }
        
        new_tree->children = grandchildren;
        for( auto& ptr: grandchildren){
          children.remove( ptr);
        }
        
      } 
      children.push_back( new_tree);
    }
  }

  else if (I::encloses( *t, *self)) {
    std::shared_ptr< Tree<I> > new_tree( new Tree<I>());
    new_tree->self = move( self);
    new_tree->parent = this;
    self.reset( t);
    for( auto& child_ptr: children){
      new_tree->children.push_back( child_ptr);
      child_ptr->parent = &(*new_tree);
    }

    children.clear();
    children.push_back( new_tree);
  }

  else{
    if (I::overlap( *self, *t))
      I::error( "sfield_ptr_tree: should not have overlap");

    std::shared_ptr< Tree<I> > new_tree( new Tree<I>());

    new_tree->self = move( self);
    for( auto& child_ptr: children){
      new_tree->children.push_back( child_ptr);
      child_ptr->parent = &(*new_tree);
    }
    new_tree->parent = this;

    self.release(); // perhaps not needed
    children.clear();

    std::shared_ptr< Tree<I> > new_tree2( new Tree<I>());
    new_tree2->self.reset( t);
    new_tree2->parent = this;
    children.push_back( new_tree);
    children.push_back( new_tree2);
  }
}


template< class I> template< class Info>
typename I::Contained* 
Tree<I>::lowest_found( Info& info, const typename I::Point& pt, 
                       typename Tree<I>::Hint& hint) const{

  typedef Tree<I> Tr;
  const Tr* current = hint.data;
  
#ifdef DEBUG
  if (current == nullptr)
    I::error( "lowest_found: no hint");
#endif

  while ( !(current == nullptr || I::is_inside( info, pt, *(current->self)))) {
    current = current->parent;
  }
  if (current == nullptr)
    return nullptr;

  else {
    while(true){
      if (current->children.empty()){
        hint.data = current;
        return current->self.get();   
      }
      else{
        bool no_child_contains = true;
        auto& children = current->children;
        for( auto& child_ptr: children){
          if (I::is_inside( info, pt, *(child_ptr->self))){
            current = &(*child_ptr);
            no_child_contains = false;
            break;
          }
        }
        if (no_child_contains){
          hint.data = current;
          return current->self.get(); 
        }
      }
    }
  }
}

}


#endif
