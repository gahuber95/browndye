/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/


/*
Implements force computation on large groups of point charges using
the Chebyshev interpolation method described in the paper.

Classes:

// contains all the information for a charged molecule
// order is the maximum order of Chebyshev interpolation
template< unsigned int order, class Interface>
class Blob;

// simple charged point
template< class Charge>
class Point;

// base class of boxes
template< class Charge>
class Box;

// contains Chebyshev coefficients and references to child boxes
template< unsigned int order, class Charge>
struct Coef_Box: public Box< Charge>;

// contains point charges at bottom
template< class Charge>
class Point_Box: public Box< Charge>;

Interface class (template argument to Blob) has the following members:

  typedef Field;
  typedef Transform;

  // assume that Charge and Potential are derived from the units in units.hh
  typedef Charge;
  typedef Potential;

  static 
  void translate( const Transform&, const Vec3< Length>&, Vec3< Length>&);

  template< class U>
  static 
  void rotate( const Transform&, const Vec3< U>&, Vec3< double>&);

  // Length from units.hh
  static
  Potential value( const Field& field, const Vec3< Length>& pos);

  // typedef UQuot< Potential, Length>::Res Potential_Gradient;
  // where UQuot is defined in units.hh
  static
  void get_gradient( const Field& field, const Vec3< Length>& pos, 
                     Vec3< Potential_Gradient>& grad);

  static
  bool distance_less_than( const Field& field, Length r, 
                           const Vec3< Length>& pos);
};
*/


#ifndef __CHARGED_BLOB_SIMPLE_HH__
#define __CHARGED_BLOB_SIMPLE_HH__

#include <stdlib.h>
#include <math.h>
#include "vector.hh"
#include <limits>
#include <string>
#include "jam_xml_pull_parser.hh"
#include "units.hh"
#include "linalg3.hh"
#include "node_info.hh"

namespace Charged_Blob{

namespace JP = JAM_XML_Pull_Parser;
namespace L = Linalg3;

template< class Charge>
class Box;

template< unsigned int order, class Charge>
struct Coef_Box;

template< class Charge>
class Point_Box;

  // simple charged point
template< class Charge>
class Point{
public:
  Point();
  Point( const Point&);
  Point& operator=( const Point&);

  Vec3< Length> pos;
  Charge q;

private:
  void copy( const Point< Charge>&);

};

// contains all the information for a charged molecule
template< unsigned int order, class Interface>
class Blob{
public:
  typedef typename Interface::Field Field;
  typedef typename Interface::Transform Transform;

  typedef typename Interface::Charge Charge;
  typedef typename Interface::Potential Potential;
  // require that Force = Charge * Potential / Length, or
  // Energy = Charge*Potential

  typedef typename UProd< Potential, Charge>::Res Energy;
  typedef typename UQuot< Potential, Length>::Res Potential_Gradient;

  Blob();
  ~Blob();

  void get_force_and_torque( const Transform& tform, 
                             const typename Interface::Field& field, 
                             Vec3< Force>& force, 
                             Vec3< Torque>& torque) const;

  void initialize( const std::string& filename);
  void set_distance_threshhold( double); // default 2

  void shift_position( const Vec3< Length>&);

  Charge total_charge() const;

  Energy potential_energy( const Transform& tform, 
                           const Field& field) const;

  Energy potential_energy_brute( const Transform& tform, 
                                 const Field& field) const;

  void get_charges( Vector< Point< Charge> >& points) const{
    top_box->get_box_charges( points);
  }

private:
  double distance_threshhold;
  Box< Charge>* top_box;
  Coef_Box< order-1, Charge>* top_box3;
  Charge charge;

  void add_force_and_torque_on_box(
                                   const Box< Charge>& box,
                                   const Transform& trans,
                                   const Field& field,
                                   Vec3< Force>& force, 
                                   Vec3< Torque>& torque) const;

  template< unsigned int cbox_order>
  void add_force_and_torque_on_cbox(
                                   const Coef_Box< cbox_order, Charge>& cbox,
                                   const Transform& trans,
                                   const Field& field,
                                   Vec3< Force>& force, 
                                   Vec3< Torque>& torque) const;

  void add_force_and_torque_on_pbox(
                                   const Point_Box< Charge>& pbox,
                                   const Transform& trans,
                                   const Field& field,
                                   Vec3< Force>& force, 
                                   Vec3< Torque>& torque) const;

  Energy potential_energy_of_box(
                                 const Box< Charge>& box,
                                 const Transform& trans,
                                 const Field& field) const;
  
  Energy potential_energy_brute_of_box(
                                       const Box< Charge>& box,
                                       const Transform& trans,
                                       const Field& field) const;
  
  template< unsigned int cbox_order>
  Energy potential_energy_of_cbox(
                                  const Coef_Box< cbox_order, Charge>& cbox,
                                  const Transform& trans,
                                  const Field& field) const;

  template< unsigned int cbox_order>
  Energy potential_energy_brute_of_cbox(
                                  const Coef_Box< cbox_order, Charge>& cbox,
                                  const Transform& trans,
                                  const Field& field) const;
  
  Energy potential_energy_of_pbox(
                                  const Point_Box< Charge>& pbox,
                                  const Transform& trans,
                                  const Field& field) const;



  //*************************************************
  static 
  void translate( const Transform& trans, const Vec3< Length>& in, Vec3< Length>& out){
    Interface::translate( trans, in, out);
  }
  
  template< class U>
  static 
  void rotate( const Transform& trans, const Vec3< U>& in, Vec3< U>& out){
    Interface::rotate( trans, in, out);
  }
  
  static
  Potential value( const Field& field, const Vec3< Length>& pos){
    return Interface::value( field, pos);
  }
  
  static
  void get_gradient( const Field& field, const Vec3< Length>& pos, 
                     Vec3< Potential_Gradient>& grad){
    Interface::get_gradient( field, pos, grad);
  }
  
  static
  bool distance_less_than( const Field& field, Length r, 
                           const Vec3< Length>& pos){
    return Interface::distance_less_than( field, r, pos);
  }
};

template< unsigned int order, class Interface>
void Blob< order, Interface>::shift_position( const Vec3< Length>& center){

  top_box->shift_position( center);
  if (top_box3 != NULL)
    top_box3->shift_position( center);
}

  // constructor
template< unsigned int order, class Interface>
Blob< order, Interface>::Blob(){
  top_box = NULL;
  top_box3 = NULL;
  distance_threshhold = 2.0;
  charge = Charge( 0.0);
}

  // destructor
template< unsigned int order, class Interface>
Blob< order, Interface>::~Blob(){
  if (top_box != NULL){
    delete top_box;
  }

  if (top_box3 != NULL){
    delete top_box3;
  }
}

template< class U>
inline
typename UProd< U, U>::Res sq( U x){
  return x*x;
}

template< unsigned int order, class Interface>
void Blob< order, Interface>::set_distance_threshhold( double th){
  distance_threshhold = th;
}

template< unsigned int order, class Interface>
void Blob< order, Interface>::
get_force_and_torque(
                     const typename Interface::Transform& trans,
                     const typename Interface::Field& field,
                     Vec3< Force>& force, Vec3< Torque>& torque) const{
  L::zero( force);
  L::zero( torque);
  
  if (top_box->is_coef_box()){
    const Coef_Box< order, Charge>* cbox = 
      static_cast< const Coef_Box< order, Charge>* >( top_box);
   
    const double th = distance_threshhold;
    const Length d = cbox->diameter;
    
    Vec3< Length> c0, c;
    rotate( trans, top_box3->center, c0);
    translate( trans, c0, c);
    
    if (distance_less_than( field, th*d, c)){
      add_force_and_torque_on_box( *top_box, trans, field, force, torque);
    }
    else{
      add_force_and_torque_on_cbox( *top_box3, trans, field, force, torque);
    }
  }
  else{
    const Point_Box< Charge>* pbox = 
      static_cast< const Point_Box< Charge>* >( top_box);

    add_force_and_torque_on_pbox( *pbox, trans, field, force, torque);
  }
}

template< unsigned int order, class Interface>
typename Blob< order, Interface>::Energy
Blob< order, Interface>::
potential_energy(
                 const typename Interface::Transform& trans,
                 const typename Interface::Field& field) const{
  
  if (top_box->is_coef_box()){
    const Coef_Box< order, Charge>* cbox = 
      static_cast< const Coef_Box< order, Charge>* >( top_box);
   
    const double th = distance_threshhold;
    const Length d = cbox->diameter;
    
    Vec3< Length> c0, c;
    rotate( trans, top_box3->center, c0);
    translate( trans, c0, c);
    
    if (distance_less_than( field, th*d, c)){
      return potential_energy_of_box( *top_box, trans, field);
    }
    else{
      return potential_energy_of_cbox( *top_box3, trans, field);
    }
  }
  else{
    const Point_Box< Charge>* pbox = 
      static_cast< const Point_Box< Charge>* >( top_box);

    return potential_energy_of_pbox( *pbox, trans, field);
  }

}

template< unsigned int order, class Interface>
typename Blob< order, Interface>::Energy
Blob< order, Interface>::
potential_energy_brute(
                 const typename Interface::Transform& trans,
                 const typename Interface::Field& field) const{
  
  if (top_box->is_coef_box()){
    return potential_energy_brute_of_box( *top_box, trans, field);
  }
  else{
    const Point_Box< Charge>* pbox = 
      static_cast< const Point_Box< Charge>* >( top_box);

    return potential_energy_of_pbox( *pbox, trans, field);
  }
}

template< class Charge>
inline
void Point< Charge>::copy( const Point& pt){
  pos[0] = pt.pos[0];
  pos[1] = pt.pos[1];
  pos[2] = pt.pos[2];
  q = pt.q;
}

template< class Charge>
inline 
Point< Charge>::Point( const Point& pt){
  copy( pt);
}

template< class Charge>
inline
Point< Charge>& Point< Charge>::operator=( const Point& pt){
  copy( pt);
  return *this;
}

template< class Charge>
inline
Point< Charge>::Point(){
  q = Charge( NAN);
}

template< class U>
inline
void put_nan( U& u){
  set_fvalue( u, NAN);
}

template< class U>
inline
void put_nan( Vec3< U>& v){
  put_nan( v[0]);
  put_nan( v[1]);
  put_nan( v[2]);
}

template< class U, unsigned int n>
inline
void fill_with_nans( SVec< U,n>& v){
  for( unsigned int i = 0; i<n; i++)
    put_nan( v[i]);
}

template< class U, unsigned int nr, unsigned int nc>
inline
void fill_with_nans( SMat< U,nr,nc>& v){
  for( unsigned int i = 0; i<nr; i++)
    for( unsigned int j = 0; j<nc; j++)
      put_nan( v[i][j]);
}

template< class U, unsigned int nr, unsigned int nc, unsigned int nz>
inline
void fill_with_nans( SArr< U,nr,nc,nz>& v){
  for( unsigned int i = 0; i<nr; i++)
    for( unsigned int j = 0; j<nc; j++)
      for( unsigned int k = 0; k<nz; k++)
        put_nan( v[i][j][k]);
}

// base class of boxes
template< class Charge>
class Box{
public:
  virtual ~Box(){};
  virtual bool is_coef_box() const = 0;
  virtual void shift_position( const Vec3< Length>& pos) = 0;
  virtual Charge total_charge() const = 0;
  virtual void get_box_charges( Vector< Point< Charge> >&) const = 0;
};


// contains Chebyshev coefficients and references to child boxes
template< unsigned int order, class Charge>
struct Coef_Box: public Box< Charge>{
public: 
  Coef_Box();
  ~Coef_Box();

  bool is_coef_box() const{ return true;}
  Charge total_charge() const;

  typedef typename UQuot< Charge, Length>::Res FType; 

  // coefficients for energy, force,and torque
  SArr< Charge, order,order,order> vcoefs;
  SArr< Vec3< FType>, order,order,order> fcoefs;
  SArr< Vec3< Charge>, order,order,order> tcoefs;
  // locations of interpolation points
  SMat< Length, 3, order> zeros;

  Box< Charge>* child0;
  Box< Charge>* child1;

  Vec3< Length> center;
  Length diameter;
  Charge charge;

  void shift_position( const Vec3< Length>& pos);
  void get_box_charges( Vector< Point< Charge> >&) const;
};

template< unsigned int order, class Charge>
void Coef_Box< order, Charge>::get_box_charges( Vector< Point< Charge> >& points) const{
  child0->get_box_charges( points);
  child1->get_box_charges( points);
}

template< unsigned int order, class Charge>
inline
Charge Coef_Box< order, Charge>::total_charge() const{
  return charge;
}

template< unsigned int order, class Charge>
void Coef_Box< order, Charge>::shift_position( const Vec3< Length>& offset){

  for( unsigned int i = 0; i<3; i++)
    for( unsigned int k = 0; k < order; k++)
      zeros[i][k] += offset[i];

  for( unsigned int ix = 0; ix < order; ix++)
    for( unsigned int iy = 0; iy < order; iy++)
      for( unsigned int iz = 0; iz < order; iz++){
        auto& tcoef = tcoefs[ix][iy][iz];
        auto& fcoef = fcoefs[ix][iy][iz];
        Vec3< Charge> dtcoef;
        L::get_cross( offset, fcoef, dtcoef);
        L::get_sum( tcoef, dtcoef, tcoef);
      }

  if (child0 != NULL)
    child0->shift_position( offset);
  if (child1 != NULL)
    child1->shift_position( offset);

  L::get_sum( center, offset, center);
}

  // constructor
template< unsigned int order, class Charge>
Coef_Box< order, Charge>::Coef_Box(){
  fill_with_nans( center);
  diameter = Length( NAN);

  child0 = NULL;
  child1 = NULL;
  fill_with_nans( fcoefs);
  fill_with_nans( tcoefs);
  fill_with_nans( zeros);
  charge = Charge( NAN);
}


  // destructor
template< unsigned int order, class Charge>
Coef_Box< order, Charge>::~Coef_Box(){
  if (child0 != NULL)
    delete child0;
  if (child1 != NULL)
    delete child1;
}

// contains point charges at bottom
template< class Charge>
class Point_Box: public Box< Charge>{
public:
  ~Point_Box(){}
  bool is_coef_box() const{ return false;}
  void shift_position( const Vec3< Length>&);
  Charge total_charge() const;
  typedef typename Vector< Point< Charge> >::size_type size_type;

  void get_box_charges( Vector< Point< Charge> >&) const;
  
  Vector< Point< Charge> > points;
};

template< class Charge>
void Point_Box< Charge>::get_box_charges( Vector< Point< Charge> >& opoints) const{
  
  for( size_type i = 0; i < points.size(); i++){
    const Point< Charge>& point = points[i];
    opoints.push_back( point);
  }
}

template< class Charge>
Charge Point_Box< Charge>::total_charge() const{
  Charge sum( 0.0);
  for( size_type i = 0; i < points.size(); i++)
    sum += points[i].q;
  return sum;
}

template< class Charge>
void Point_Box< Charge>::shift_position( const Vec3< Length>& offset){
  for( size_type i = 0; i < points.size(); i++){
    Point< Charge>& point = points[i];
    L::get_sum( point.pos, offset, point.pos);
  }
}

// add force and torque from "cbox" due to "field"
template< unsigned int order, class I>
void add_ft_of_coef_box( const Coef_Box< order, typename I::Charge>& cbox,
                         const typename I::Transform& trans,
                         const typename I::Field& field,
                         Vec3< Force>& force, Vec3< Torque>& torque){
  
  typedef typename I::Charge Charge;
  typedef typename I::Potential Potential;
  typedef typename UQuot< Charge, Length>::Res FType; 
  typedef Charge TType;

  Vec3< Length> pos, pos0, pos1;
  Vec3< Force> force0, force1;
  Vec3< Torque> torque0, torque1;
  L::zero( force0);
  L::zero( torque0);

  typedef SArr< Vec3< FType>, order,order,order> FCoefs;
  typedef SArr< Vec3< TType>, order,order,order> TCoefs;
  
  const FCoefs& fcoefs = cbox.fcoefs;
  const TCoefs& tcoefs = cbox.tcoefs;
  for( unsigned int ix = 0; ix < order; ix++){
    typedef typename SMat< Vec3< FType>, order, order>::Const_View FCoefsx;
    typedef typename SMat< Vec3< TType>, order, order>::Const_View TCoefsx;

    const FCoefsx fcoefsx = fcoefs[ix];
    const TCoefsx tcoefsx = tcoefs[ix];
    pos0[0] = cbox.zeros[0][ix];
    
    for( unsigned int iy = 0; iy < order; iy++){
      typedef typename SVec< Vec3< FType>, order>::Const_View FCoefsy;
      typedef typename SVec< Vec3< TType>, order>::Const_View TCoefsy;
      
      const FCoefsy fcoefsxy = fcoefsx[iy];
      const TCoefsy tcoefsxy = tcoefsx[iy];
      pos0[1] = cbox.zeros[1][iy];
      
      for( unsigned int iz = 0; iz < order; iz++){
        const Vec3< FType>& fcoefsxyz = fcoefsxy[iz]; 
        const Vec3< TType>& tcoefsxyz = tcoefsxy[iz]; 
        pos0[2] = cbox.zeros[2][iz];
        
        I::rotate( trans, pos0, pos1);
        I::translate( trans, pos1, pos);
        Potential val = I::value( field, pos);
        
        L::add_scaled( val, fcoefsxyz, force0);
        L::add_scaled( val, tcoefsxyz, torque0);
      }
    }
  }
  I::rotate( trans, force0, force1);
  I::rotate( trans, torque0, torque1);
  L::get_sum( force1, force, force);
  L::get_sum( torque1, torque, torque);
}


template< unsigned int order, class I>
typename UProd< typename I::Charge, typename I::Potential>::Res 
potential_energy_of_coef_box( const Coef_Box< order, typename I::Charge>& cbox,
                              const typename I::Field& field,
                              const typename I::Transform& trans){
  
  typedef typename I::Charge Charge;
  typedef typename I::Potential Potential;
  typedef typename UProd< Charge, Potential>::Res VType; 

  Vec3< Length> pos, pos0, pos1;

  typedef SArr< Charge, order,order,order> VCoefs;

  const VCoefs& vcoefs = cbox.vcoefs;
  VType epot( 0.0);
  for( unsigned int ix = 0; ix < order; ix++){
    typedef typename SMat< Charge, order, order>::Const_View VCoefsx;

    const VCoefsx vcoefsx = vcoefs[ix];
    pos0[0] = cbox.zeros[0][ix];
    
    for( unsigned int iy = 0; iy < order; iy++){
      typedef typename SVec< Charge, order>::Const_View VCoefsy;

      const VCoefsy vcoefsxy = vcoefsx[iy];
      pos0[1] = cbox.zeros[1][iy];
      
      for( unsigned int iz = 0; iz < order; iz++){
        const Charge vcoefsxyz = vcoefsxy[iz]; 
        pos0[2] = cbox.zeros[2][iz];

        I::rotate( trans, pos0, pos1);
        I::translate( trans, pos1, pos);
        Potential val = I::value( field, pos);
        
        epot += val*vcoefsxyz;
      }
    }
  }
  return epot;
}  

template< unsigned int blob_order, class I>
template< unsigned int order>
void Blob< blob_order, I>::
add_force_and_torque_on_cbox( 
                             const Coef_Box< order, typename I::Charge>& cbox,
                             const typename I::Transform& trans,
                             const typename I::Field& field,
                             Vec3< Force>& force, Vec3< Torque>& torque) const{
  
  Length d = cbox.diameter*distance_threshhold;
  Vec3< Length> c0, c;
  rotate( trans, cbox.center, c0);
  translate( trans, c0, c);

  if (distance_less_than( field, d, c)){
    add_force_and_torque_on_box( *(cbox.child0), trans, field, 
                                 force, torque);
    add_force_and_torque_on_box( *(cbox.child1), trans, field, 
                                 force, torque);   
  } 
  else
    add_ft_of_coef_box< order, I>( cbox, trans, 
                                   field, force, torque);
}
  
template< unsigned int blob_order, class I>
template< unsigned int order>
typename Blob< blob_order, I>::Energy
Blob< blob_order, I>::
potential_energy_of_cbox( 
                         const Coef_Box< order, typename I::Charge>& cbox,
                         const typename I::Transform& trans,
                         const typename I::Field& field
                         ) const{
  
  Length d = cbox.diameter*distance_threshhold;
  Vec3< Length> c0, c;
  rotate( trans, cbox.center, c0);
  translate( trans, c0, c);

  if (distance_less_than( field, d, c)){
    Energy v0 = potential_energy_of_box( *(cbox.child0), trans, field);
    Energy v1 = potential_energy_of_box( *(cbox.child1), trans, field);
    return v0 + v1;
  } 
  else
    return potential_energy_of_coef_box< order, I>( cbox, field, trans);
}

  // brute force method; just sums over all charges  
template< unsigned int blob_order, class I>
template< unsigned int order>
typename Blob< blob_order, I>::Energy
Blob< blob_order, I>::
potential_energy_brute_of_cbox( 
                          const Coef_Box< order, typename I::Charge>& cbox,
                          const typename I::Transform& trans,
                          const typename I::Field& field
                          ) const{
  
  Vec3< Length> c0, c;
  rotate( trans, cbox.center, c0);
  translate( trans, c0, c);

  Energy v0 = potential_energy_brute_of_box( *(cbox.child0), trans, field);
  Energy v1 = potential_energy_brute_of_box( *(cbox.child1), trans, field);
  return v0 + v1; 
}
  
template< unsigned int order, class I>
void Blob< order, I>::
add_force_and_torque_on_pbox(
                             const Point_Box< typename I::Charge>& pbox,
                             const typename I::Transform& trans,
                             const typename I::Field& field,
                             Vec3< Force>& force, Vec3< Torque>& torque) const{
  
  Vec3< Length> pos0, pos, origin0, origin;
  L::zero( origin0);
  translate( trans, origin0, origin);
  
  typedef typename I::Charge Charge;
  const Vector< Point< Charge> >& points = pbox.points;
  for( typename Vector< Point< Charge> >::const_iterator itr = points.begin(); 
       itr != points.end(); ++itr){
    const Point< Charge>& point = *itr;
    rotate( trans, point.pos, pos0);
    translate( trans, pos0, pos);

    Vec3< Force> f;
    Vec3< Torque> t;
    Vec3< Potential_Gradient> g;
    get_gradient( field, pos, g);

    Charge q = point.q;
    f[0] = -q*g[0];
    f[1] = -q*g[1];
    f[2] = -q*g[2];

    L::get_sum( f, force, force);
   
    L::get_diff( pos, origin, pos);
    L::get_cross( pos, f, t);
    L::get_sum( t, torque, torque);
  }
}

template< unsigned int order, class I>
typename Blob< order, I>::Energy
Blob< order, I>::
potential_energy_of_pbox(
                         const Point_Box< typename I::Charge>& pbox,
                         const typename I::Transform& trans,
                         const typename I::Field& field
                         ) const{
  
  Vec3< Length> pos0, pos;
  
  typedef typename I::Charge Charge;
  const Vector< Point< Charge> >& points = pbox.points;
  Energy energy( 0.0);
  for( typename Vector< Point< Charge> >::const_iterator itr = points.begin(); 
       itr != points.end(); ++itr){
    const Point< Charge>& point = *itr;
    rotate( trans, point.pos, pos0);
    translate( trans, pos0, pos);

    energy += point.q*value( field, pos);
  }
  return energy;
}

template< unsigned int order, class I>
void Blob< order, I>::
add_force_and_torque_on_box(
                            const Box< typename I::Charge>& box,
                            const typename I::Transform& trans,
                            const typename I::Field& field,
                            Vec3< Force>& force, Vec3< Torque>& torque) const{

  typedef typename I::Charge Charge;

#ifdef DEBUG
  if (&box == NULL)
    error( "add_force_and_torque_on_box: box does not exist");
#endif

  if (box.is_coef_box()){
    const Coef_Box< order, Charge>& cbox = 
      static_cast< const Coef_Box< order, Charge>& >( box);

    add_force_and_torque_on_cbox( cbox, trans, field, force, torque);

  }
  else{
    const Point_Box< Charge>& pbox = 
      static_cast< const Point_Box< Charge>& >( box);

    add_force_and_torque_on_pbox( pbox, trans, field, force, torque);   
  }
}

template< unsigned int order, class Interface>
typename Blob< order, Interface>::Energy
Blob< order, Interface>::
potential_energy_of_box(
                        const Box< typename Interface::Charge>& box,
                        const typename Interface::Transform& trans,
                        const typename Interface::Field& field) const{

  typedef typename Interface::Charge Charge;

  if (box.is_coef_box()){
    const Coef_Box< order, Charge>& cbox = 
      static_cast< const Coef_Box< order, Charge>& >( box);

    return potential_energy_of_cbox( cbox, trans, field);

  }
  else{
    const Point_Box< Charge>& pbox = 
      static_cast< const Point_Box< Charge>& >( box);

    return potential_energy_of_pbox( pbox, trans, field);   
  }
}

  // brute force approach by including every point charge
template< unsigned int order, class Interface>
typename Blob< order, Interface>::Energy
Blob< order, Interface>::
potential_energy_brute_of_box(
                        const Box< typename Interface::Charge>& box,
                        const typename Interface::Transform& trans,
                        const typename Interface::Field& field) const{

  typedef typename Interface::Charge Charge;

  if (box.is_coef_box()){
    const Coef_Box< order, Charge>& cbox = 
      static_cast< const Coef_Box< order, Charge>& >( box);

    return potential_energy_brute_of_cbox( cbox, trans, field);

  }
  else{
    const Point_Box< Charge>& pbox = 
      static_cast< const Point_Box< Charge>& >( box);

    return potential_energy_of_pbox( pbox, trans, field);   
  }
}

// Parsing code

template< class V>
void read_array( JP::Node_Ptr node, unsigned int n, V a);


template< class U>
void read_v3( JP::Node_Ptr node, Vec3< U>& v){

  set_fvalue( v[0], double_from_node( node, "x"));
  set_fvalue( v[1], double_from_node( node, "y"));
  set_fvalue( v[2], double_from_node( node, "z"));
}

template< class U>
void read_float( JP::Node_Ptr node, U& v){
  set_fvalue( v, double_from_node( node));
}

template< unsigned int n, class T> 
void read_coefs(  void (read)( JP::Node_Ptr, T&), 
                  JP::Node_Ptr node, SArr< T,n,n,n>& coefs){

  typedef std::list< JP::Node_Ptr> NList;

  NList nodes_x = node->children_of_tag( "xc");

  unsigned int ix = 0;
  for( auto itr = nodes_x.begin();
       itr != nodes_x.end(); 
       ++itr){
    
    auto node_x = *itr;
    auto nodes_y = node_x->children_of_tag( "yc");

    unsigned int iy = 0;
    for( NList::iterator itr = nodes_y.begin(); 
         itr != nodes_y.end(); 
         ++itr){

      auto node_y = *itr;
      auto nodes_z = node_y->children_of_tag( "zc");

      unsigned int iz = 0;
      for( NList::iterator itr = nodes_z.begin(); 
           itr != nodes_z.end(); 
           ++itr){
        
        auto node_z = *itr;
        read( node_z, coefs[ix][iy][iz]);
        ++iz;
      }
      ++iy;
    }
    ++ix;
  }  
}

template< class Charge>
void read_point( JP::Node_Ptr node, Point< Charge>& point);

template< unsigned int order, class Charge>
Box< Charge>* new_box( JP::Parser& parser);

// assume parser is pointed to "cbox" node
template< unsigned int order, class Charge>
Coef_Box< order, Charge>* new_coef_box( JP::Parser& parser){

  Coef_Box< order, Charge>* box = new Coef_Box< order, Charge>; 

  auto node = parser.current_node();

  while( !node->is_traversed()){
    parser.next();

    if (parser.is_current_node_tag( "corners")){
      parser.complete_current_node();
      JP::Node_Ptr conode = parser.current_node();

      JP::Node_Ptr lnode = checked_child( conode, "low");
      Vec3< Length> lc;
      read_v3( lnode, lc);
      
      JP::Node_Ptr hnode = checked_child( conode, "high");
      Vec3< Length> hc;
      read_v3( hnode, hc);

      box->diameter = sqrt( sq( hc[0] - lc[0]) +  
                            sq( hc[1] - lc[1]) +  
                            sq( hc[2] - lc[2]) 
                            );  
      box->center[0] = 0.5*( lc[0] + hc[0]);
      box->center[1] = 0.5*( lc[1] + hc[1]);
      box->center[2] = 0.5*( lc[2] + hc[2]);
      
    }
    else if (parser.is_current_node_tag( "coefs")){
      parser.complete_current_node();
      JP::Node_Ptr cnode = parser.current_node();
      
      read_coefs( read_v3, checked_child( cnode, "force"), box->fcoefs);
      read_coefs( read_v3, checked_child( cnode, "torque"), box->tcoefs);
      read_coefs( read_float, 
                  checked_child( cnode, "potential"), box->vcoefs);
            
    }
    else if (parser.is_current_node_tag( "positions")){
      parser.complete_current_node();
      JP::Node_Ptr pnode = parser.current_node();
      read_array( checked_child( pnode, "xc"), order, box->zeros[0]);
      read_array( checked_child( pnode, "yc"), order, box->zeros[1]);
      read_array( checked_child( pnode, "zc"), order, box->zeros[2]);
    }
    else if (parser.is_current_node_tag( "children")){
      JP::Node_Ptr cnode = parser.current_node();

      bool read0 = false;
      bool read1 = false;
      while (!cnode->is_traversed()){
        parser.next();
        if (parser.is_current_node_tag( "cbox") || 
            parser.is_current_node_tag( "pbox")){
          if (read0){
            box->child1 = new_box< order, Charge>( parser);
            read1 = true;
          }
          else if (!read1){
            box->child0 = new_box< order, Charge>( parser);
            read0 = true;
          }
        }
      }
    }
  }

  box->charge = Charge( 0.0);
  for (unsigned int ix = 0; ix < order; ix++)
    for (unsigned int iy = 0; iy < order; iy++)
      for (unsigned int iz = 0; iz < order; iz++)
        box->charge += box->vcoefs[ix][iy][iz];
  
  return box;
}

template< class Charge>
Point_Box< Charge>* new_point_box( JP::Parser& parser);

// assume parser is pointed to "cbox" or "pbox" node
template< unsigned int order, class Charge>
Box< Charge>* new_box( JP::Parser& parser){

  Box< Charge>* box = NULL;

  if (parser.is_current_node_tag( "cbox"))
    box = new_coef_box< order, Charge>( parser);
  else if (parser.is_current_node_tag( "pbox"))
    box = new_point_box< Charge>( parser);
  
  return box;
}

template< unsigned int order, class Interface>
typename Interface::Charge Blob< order, Interface>::total_charge() const{
  return charge;
}

template< unsigned int order, class Interface>
void Blob< order, Interface>::initialize( const std::string& filename){

  typedef typename Interface::Charge Charge;
  std::ifstream input( filename);

  if (!input.is_open())
    error( "file ", filename, " could not be opened");

  JP::Parser parser( input);

  while (!(parser.is_current_node_tag( "om1_box") ||
           parser.is_current_node_tag( "o_box")))
    parser.next();
  
  if (parser.is_current_node_tag( "om1_box")){
    while( !(parser.is_current_node_tag( "cbox")))
      parser.next();
    top_box3 = new_coef_box< order-1, Charge>( parser);

    while( !parser.is_current_node_tag( "o_box"))
      parser.next();
  }

  while( !((parser.is_current_node_tag( "cbox")) ||
           (parser.is_current_node_tag( "pbox")))
         )
    parser.next();

  top_box = new_box< order, Charge>( parser);

  charge = top_box->total_charge();
}


template< class V>
void read_array( JP::Node_Ptr node, unsigned int n, V a){
 
  auto& data = node->data();
  for( auto i = 0u; i < n; i++){
    set_fvalue( a[i], stod( data[i]));
  }
}


template< class Charge>
void read_point( JP::Node_Ptr node, Point< Charge>& point){

  JP::Node_Ptr pnode = checked_child( node, "pos");
  read_v3( pnode, point.pos);

  JP::Node_Ptr qnode = checked_child( node, "q");

  set_fvalue( point.q, double_from_node( qnode));
}

  /*
template< class Charge>
class Point_Box_Getter{
public:
  Point_Box_Getter( JP::Parser& _parser, Vector< Point< Charge> >& _points):
    parser(_parser),points(_points), n(_points.size()), i(0){}

  typedef typename Vector< Point< Charge> >::size_type size_type;

  JP::Parser& parser;
  Vector< Point< Charge> >& points;
  size_type n, i;

  void operator()( JP::Node_Ptr node){
    if (i == n)
      error( "new_point_box: too many points"); 

    read_point( node, points[i]);
    ++i;
  }
};
  */

  // assume current pointer is at pbox
template< class Charge>
Point_Box< Charge>* new_point_box( JP::Parser& parser){

  Point_Box< Charge>* box = new Point_Box< Charge>;

  parser.complete_current_node();
  auto pnode = parser.current_node();

  auto ponode = checked_child( pnode, "points");
  
  size_t np = int_from_node( ponode, "n");
  box->points.resize( np);

  auto poinodes = ponode->children_of_tag( "point");
  if (poinodes.size() != np)
    error( "new_points_box: not enough points", poinodes.size(), np);

  {
    size_t i = 0;
    for( auto& poinode: poinodes){
      read_point( poinode, box->points[i]);
      ++i;
    }
  }

  return box;

  /*
  const JP::Node_Ptr parent = parser.current_node();

  while (!parser.is_current_node_tag( "points"))
    parser.next();

  size_type np;
  {
    while (!parser.is_current_node_tag( "n"))
      parser.next();

    JP::Node_Ptr nnode = parser.current_node();
    if (nnode == parent)
      error( "new_point_box: need n for number of points");
    nnode->stream() >> np;
  }
  box->points.resize( np);

  parser.go_up();

  Point_Box_Getter< Charge> pbg( parser, box->points);
  parser.apply_to_nodes_of_tag( "point", pbg);
  if (pbg.i != box->points.size()){
    fprintf( stderr, "%d %d\n", (int)(pbg.i), (int)(box->points.size()));
    error( "new_points_box: not enough points");
  }
  parser.next();
  
  return box;
  */
}

}

#endif
