#ifndef __COL_INTERFACE_SMALL_HH__
#define __COL_INTERFACE_SMALL_HH__

#include "small_vector.hh"
#include "col_interface_gen.hh"
#include "col_interface_small_interface.hh"

class Col_Interface_Small: public Col_Interface_Gen< Col_Interface_Small_Interface>{
public:
  static const bool is_changeable = true;

  static
  void get_transformed_position( const Vec3< Length>& pos, const Transform& trans,
                                 Vec3< Length>& tpos){
    trans.get_transformed( pos, tpos);
  }
 
  static
  void clear_transform( Atoms&, Refs& refs){
    for( size_type i = 0; i < refs.size(); ++i){
      refs[i]->clear_transformed();
    }
  }
};

#endif
