/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/


#include <stdlib.h>
#include "vector.hh"
#include "spline.hh"
//#include "jam_xml_pull_parser.hh"
#include "small_vector.hh"
#include "rotne_prager.hh"
#include "pi.hh"

namespace Two_Sphere_Brownian{

  //namespace JP = JAM_XML_Pull_Parser;
  
  //double read_radius( JP::Node_Ptr node);

  void move_single_sphere( double a, Vec3< double>& r, Mat3< double>& rot,
                           const Vec3< double>& f, const Vec3< double>& t,
                           const SVec< double,6>& w, double dt);

  void move_rotne_prager_spheres( 
                                 double a2,                    
                                 Vec3< double>& r1, Mat3< double>& rot1, 
                                 const Vec3< double>& f1, const Vec3< double>& t1, 
                                 Vec3< double>& r2, Mat3< double>& rot2, 
                                 const Vec3< double>& f2, const Vec3< double>& t2,
                                 const SVec< double,12>& w, double dt, double gap);  
}
