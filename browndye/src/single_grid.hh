/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
Single_Grid::Grid holds grid data. It can be initialized from a DX file or
and XML file (such as output by aux/grid_distances).  It uses trilinear
interpolation to get values at arbitrary points.

 */

#ifndef __SINGLE_GRID_HH__
#define __SINGLE_GRID_HH__

#include <stdio.h>
#include <string>
#include "jam_xml_pull_parser.hh"
#include "units.hh"
#include "vector.hh"
#include "small_vector.hh"

namespace Single_Grid{

template< class Potential>
class Grid{
public:
  typedef typename UQuot< Potential, Length>::Res Potential_Gradient;

  bool in_range( const Vec3< Length>& pos) const;

  void get_gradient( const Vec3< Length>& pos, Vec3< Potential_Gradient>& f, 
                     bool& in_range) const;
  void get_gradient( const Vec3< Length>& pos, Vec3< Potential_Gradient>& f) const;

  void get_potential( const Vec3< Length>& pos, Potential& v, bool& in_range) const;
  Potential potential( const Vec3< Length>& pos) const;

  Grid( const char* filename);

  // assume parser current node is "distances"; returns with
  // parser at end of "distances"
  Grid( JAM_XML_Pull_Parser::Parser& parser);

  void get_low_corner( Vec3< Length>&) const;
  void get_high_corner( Vec3< Length>&) const;
  void get_dimensions( unsigned long&, unsigned long&, unsigned long&) const;
  
  void shift_position( const Vec3< Length>&);

private:
  typedef typename Short_Version< Potential>::Res SPotential;
  typedef typename Vector< SPotential>::size_type size_type;

  Grid( const Grid&){}
  Grid& operator=( const Grid&){ return *this;}
  Potential potential( long, long, long, 
                       Length, Length, Length) const;
  void get_gradient( long, long, long, 
                     Length, Length, Length, 
                     Vec3< Potential_Gradient>& f
                     ) const;

  unsigned long nx,ny,nz, nyz;
  unsigned long nxm1,nym1,nzm1;
  Vec3< Length> low_corner;
  Length hx,hy,hz;

  Vector< SPotential> data;
};

template< class Interface>
inline
void Grid< Interface>::get_low_corner( Vec3< Length>& c) const{
  c[0] = low_corner[0];
  c[1] = low_corner[1];
  c[2] = low_corner[2];
}

template< class Interface>
inline
void Grid< Interface>::get_dimensions( unsigned long& _nx, 
                                       unsigned long& _ny, unsigned long& _nz) const{
  _nx = nx;
  _ny = ny;
  _nz = nz;
}

}

#include "single_grid_impl.hh"

#endif
