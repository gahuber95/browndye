/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

// Implementation of Two_Sphere_Brownian::Mover class

#include <stdio.h>
#include <math.h>
#include "two_sphere_brownian.hh"
#include "node_info.hh"
#include "rotations.hh"
#include "linalg3.hh"

#define FOR( i, nb, ne) for( unsigned int i=(nb); i<(ne); i++)

namespace Two_Sphere_Brownian{

  /*
double float_of_node( JP::Node_Ptr node){
  double res;
  node->stream() >> res;
  return res;
}
  */

/*************************************************************/ 
/* 
double read_radius( JP::Node_Ptr node){
  return float_of_node( checked_child( node, "radius"));
}
*/

void cross( const Vec3< double>& a, const Vec3< double>& b, Vec3< double>& c){
  c[0] = a[1]*b[2] - a[2]*b[1];
  c[1] = a[2]*b[0] - a[0]*b[2];
  c[2] = a[0]*b[1] - a[1]*b[0];
}

double dot( const Vec3< double>& a, const Vec3< double>& b){
  return a[0]*b[0] + a[1]*b[1] + a[2]*b[2];
}

double norm( const Vec3< double>& a){
  return sqrt( dot( a,a));
}

void normalize( Vec3< double>& a){
  double aa = norm( a);
  a[0] /= aa;
  a[1] /= aa;
  a[2] /= aa;
}

double sq( double x){
  return x*x;
}

void normalize_quat( SVec< double,4>& q){
  double nrm = sqrt( sq( q[0]) + sq( q[1]) + sq( q[2]) + sq( q[3]));
  q[0] /= nrm;
  q[1] /= nrm;
  q[2] /= nrm;
  q[3] /= nrm;
}

// a, b are normalized
void get_rot_axis( const Vec3< double>& a, const Vec3< double>& b, 
                   Vec3< double>& axis, double& sin_alpha){
  
  cross( a, b, axis);
  normalize( axis);

  double cos_phi = dot( a, b);
  sin_alpha = sqrt( (1.0 - cos_phi)/2.0);
}

void get_back_rot( Vec3< double>& r1, Vec3< double>& r2, SVec< double,4>& quat){
  
  Vec3< double> d;
  d[0] = r2[0] - r1[0];
  d[1] = r2[1] - r1[1];
  d[2] = r2[2] - r1[2];
  normalize( d);

  Vec3< double> z_axis; // = {0.0, 0.0, 1.0};
  z_axis[0] = z_axis[1] = 0.0;
  z_axis[2] = 1.0;

  Vec3< double> axis;
  double sin_alpha;
  get_rot_axis( d, z_axis, axis, sin_alpha);

  if (d[0] == 0.0 && d[1] == 0.0){
    quat[0] = 1.0;
    quat[1] = quat[2] = quat[3] = 0.0;
  }
  else{
    double cos_alpha = sqrt( 1.0 - sin_alpha*sin_alpha);
    quat[0] = cos_alpha;
    quat[1] = axis[0]*sin_alpha;
    quat[2] = axis[1]*sin_alpha;
    quat[3] = axis[2]*sin_alpha;
  }
}  

// dom is in frame rotated to z-axis
void apply_dom_to_sphere( const SVec< double,4>& backq, 
                          const Mat3< double>& rot, 
                          const Vec3< double>& dom, 
                          Mat3< double>& rot_res){

  SVec< double, 4> q;
  mat_to_quat( rot, q);

  SVec< double, 4> dq;
  dom_to_quat( dom, dq);

  SVec< double, 4> qres;
  multiply_quats( backq, q, qres);

  multiply_quats( dq, qres, qres);
  SVec< double, 4> inv_backq;
  invert_quat( backq, inv_backq);
  multiply_quats( inv_backq, qres, qres);
  normalize_quat( qres);

  quat_to_mat( qres, rot_res);
}

template< unsigned int na, unsigned int nb>
void copy3( const SVec< double, na>& a, unsigned int offset,
            SVec< double, nb>& b){

  b[ offset] = a[0];
  b[ offset+1] = a[1];
  b[ offset+2] = a[2];
}


void scale_vec12( SVec< double,12>& a, double c){
  FOR( i, 0, 12)
    a[i] = a[i]*c;
}

void rotate_single_sphere( double a, Mat3< double>& rot, 
                           const Vec3< double>& tq, const Vec3< double>& w, double dt){

  double rmob = 1.0/(pi8*a*a*a);
  double sqdt = sqrt( 2.0*dt);

  double sqrmobdt = sqdt*sqrt( rmob);

  Vec3< double> dom;  
  FOR( i, 0, 3){
    dom[i] = rmob*tq[i]*dt + sqrmobdt*w[i];
  }

  SVec< double, 4> q, dq;
  mat_to_quat( rot, q);
  dom_to_quat( dom, dq);
  
  multiply_quats( dq, q, q);
  quat_to_mat( q, rot);
}

void move_single_sphere( double a, Vec3< double>& r, Mat3< double>& rot,
                         const Vec3< double>& f, const Vec3< double>& tq,
                         const SVec< double,6>& w, double dt){

  double tmob = 1.0/(pi6*a);
  double sqdt = sqrt( 2.0*dt);

  double sqtmobdt = sqdt*sqrt( tmob);  
  
  FOR( i, 0, 3){
    r[i] += tmob*f[i]*dt + sqtmobdt*w[i]; 
  }

  rotate_single_sphere( a, rot, tq, w.view<3,6>(), dt);

}

namespace L = Linalg3;

void move_rotne_prager_spheres( 
                               double a2,                      
                               Vec3< double>& r1, Mat3< double>& rot1, 
                               const Vec3< double>& f1, const Vec3< double>& t1, 
                               Vec3< double>& r2, Mat3< double>& rot2, 
                               const Vec3< double>& f2, const Vec3< double>& t2,
                               const SVec< double, 12>& w, double dt, double gap){

  double sqdt = sqrt( 2.0*dt);
  double r = 1.0 + a2 + gap;
  double mu = 1.0;
  double d11 = Rotne_Prager::single_mobility( mu, 1.0);
  double d22 = Rotne_Prager::single_mobility( mu, a2);

  if (gap < 0.0)
    error( "negative gap\n");

  double d12xx = Rotne_Prager::xx_mobility( mu, 1.0, a2, r);
  double d12zz = Rotne_Prager::zz_mobility( mu, 1.0, a2, r);

  double s11 = sqrt( d11);
  double s12xx = d12xx/s11;
  double s12zz = d12zz/s11;

  double s22xx = sqrt( d22 - s12xx*s12xx); 
  double s22zz = sqrt( d22 - s12zz*s12zz); 

  Vec3< double> dr;
  L::get_diff( r2,r1, dr);
  Vec3< double> uz;
  L::get_normed( dr, uz);
  
  Vec3< double> ux, uy;
  L::get_perp_axes( uz, ux, uy);

  double f1x = dot( f1, ux);
  double f1y = dot( f1, uy);
  double f1z = dot( f1, uz);

  double f2x = dot( f2, ux);
  double f2y = dot( f2, uy);
  double f2z = dot( f2, uz);

  Vec3< double> dr1, dr2;

  const SVec< double,6> w1 = w.view<0,6>();
  const SVec< double,6> w2 = w.view<6,12>();

  const Vec3< double> w1t = w1.view<0,3>();
  const Vec3< double> w2t = w2.view<0,3>();

  Vec3< double> w1p, w2p;
  w1p[0] = dot( w1t, ux);
  w1p[1] = dot( w1t, uy);
  w1p[2] = dot( w1t, uz);

  w2p[0] = dot( w2t, ux);
  w2p[1] = dot( w2t, uy);
  w2p[2] = dot( w2t, uz);

  double s11st = s11*sqdt;
  dr1[0] = (d11*f1x + d12xx*f2x)*dt + s11st*w1p[0];
  dr1[1] = (d11*f1y + d12xx*f2y)*dt + s11st*w1p[1];
  dr1[2] = (d11*f1z + d12zz*f2z)*dt + s11st*w1p[2];

  dr2[0] = (d22*f2x + d12xx*f1x)*dt + (s12xx*w1p[0] + s22xx*w2p[0])*sqdt;
  dr2[1] = (d22*f2y + d12xx*f1y)*dt + (s12xx*w1p[1] + s22xx*w2p[1])*sqdt;
  dr2[2] = (d22*f2z + d12zz*f1z)*dt + (s12zz*w1p[2] + s22zz*w2p[2])*sqdt;

  L::add_scaled( dr1[0], ux, r1);
  L::add_scaled( dr1[1], uy, r1);
  L::add_scaled( dr1[2], uz, r1);

  L::add_scaled( dr2[0], ux, r2);
  L::add_scaled( dr2[1], uy, r2);
  L::add_scaled( dr2[2], uz, r2);

  rotate_single_sphere( 1, rot1, t1, w1.view<3,6>(), dt);
  rotate_single_sphere( a2, rot2, t2, w2.view<3,6>(), dt);
}


}

