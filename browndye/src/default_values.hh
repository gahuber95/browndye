#ifndef __DEFAULT_VALUES_HH__
#define __DEFAULT_VALUES_HH__

/*
These classes are used to provide default values for uninitialized
vector and array types.  For example, the default for a "double"
is "NAN" and that for an integer is the maximum value. 
This helps catch bugs.

*/


#include <vector>
#include <limits>

//*********************************************************************
namespace JAM_Vector{


template< class T, bool has_quiet_nan, bool has_inf, bool is_bounded>
class DVV{
public:
  static const bool is_specialized = false;

  typedef typename std::vector<T>::reference TRef;

  static 
  void set_value( TRef){}
};

template< class T, bool has_quiet_nan, bool has_inf, bool is_bounded>
class DVV< T*, has_quiet_nan, has_inf, is_bounded>{
public:
  static const bool is_specialized = true;
  typedef typename std::vector<T*>::reference TRef;

  static
  void set_value( TRef t){
    t = NULL;
  }
};

template<>
class DVV< bool, false, false, true>{
public:
  static const bool is_specialized = true;
  typedef std::vector< bool>::reference TRef;

  static
  void set_value( TRef t){
    t = false;
  }
};


template< class T, bool has_inf, bool is_bounded>
class DVV< T, true, has_inf, is_bounded>{
public:
  static const bool is_specialized = true;

  typedef typename std::vector<T>::reference TRef;

  static 
  void set_value( TRef t){
    t = std::numeric_limits< T>::quiet_NaN();
  }

};

template< class T, bool is_bounded>
class DVV< T, false, true, is_bounded>{
public:
  static const bool is_specialized = true;

  typedef typename std::vector<T>::reference TRef;

  static
  void set_value( TRef t){
    t = std::numeric_limits< T>::infinity();
  }
};

template< class T>
class DVV< T, false, false, true>{
public:
  static const bool is_specialized = true;

  typedef typename std::vector<T>::reference TRef;

  static
  void set_value( TRef t){
    t = std::numeric_limits< T>::max();
  }
};


template< class T>
class Default_Value{
public:
  static const bool has_quiet_nan = std::numeric_limits< T>::has_quiet_NaN;
  static const bool has_infinity = std::numeric_limits< T>::has_infinity;
  static const bool is_bounded = std::numeric_limits< T>::is_bounded;

  typedef DVV< T, has_quiet_nan, has_infinity, is_bounded> DVVA;
  static const bool is_specialized = DVVA::is_specialized;

  typedef typename std::vector<T>::reference TRef;

  static
  void set_value( TRef t){
    DVVA::set_value( t);
  }

};


}

#endif
