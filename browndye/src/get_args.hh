/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

#ifndef __GET_ARGS_HH__
#define __GET_ARGS_HH__

/*
Extracts arguments from the command-line input to a program,
using flags to denote the arguments.

For example, if the user calls
program -f 12.3
and inside "program" is the command 
get_double_arg( argc, argv, "-f", ff);

where argc and argv are passed into "main",
then "ff" will have the value 12.3. 

*/

void get_double_arg( int argc, char* argv[], const char* flag, double& _res);
void get_int_arg( int argc, char* argv[], const char* flag, int& _res);
void get_string_arg( int argc, char* argv[], const char* flag, char* res);

#endif
