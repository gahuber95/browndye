/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
Top-level parsing classes used in the C++ code.
Implements a parser using the same model as the Ocaml version in
jam_xml_ml_pull_parser.ml. Implements two classes: the Parser and
the Node.  It wraps the Parser_1F class.
*/

#include "jam_xml_pull_parser.hh"

namespace JAM_XML_Pull_Parser{

const String& Node::tag() const{
  return ttag;
}

const JP::Attrs& Node::attributes() const{
  return attrs;
}

const Vector< String>& Node::data() const{
  return sdata;
}

Node* Node::child( const std::string& tag){
  auto itr = children.find( tag);

  if (itr == children.end())
    return nullptr;
  else 
    return itr->second.get();
} 

Node* Node::child( const std::string& tag0, const std::string& tag1){
  Node* cnode = child( tag0);
  if (cnode == nullptr)
    return nullptr;
  else
    return cnode->child( tag1);
}

Node* Node::child( const std::string& tag0, const std::string& tag1, 
                   const std::string& tag2){
  Node* cnode = child( tag0, tag1);
  if (cnode == nullptr)
    return nullptr;
  else
    return cnode->child( tag2);
}

Node* Node::child( const std::string& tag0, const std::string& tag1, 
                   const std::string& tag2, const std::string& tag3){
  Node* cnode = child( tag0, tag1, tag2);
  if (cnode == nullptr)
    return nullptr;
  else
    return cnode->child( tag3);
}

Node* Node::child( const std::string& tag0, const std::string& tag1, 
                   const std::string& tag2, const std::string& tag3, 
                   const std::string& tag4){
  Node* cnode = child( tag0, tag1, tag2, tag3);
  if (cnode == nullptr)
    return nullptr;
  else
    return cnode->child( tag4);
}

Node* Node::child( const std::string& tag0, const std::string& tag1, 
                   const std::string& tag2, const std::string& tag3, 
                   const std::string& tag4, const std::string& tag5){
  Node* cnode = child( tag0, tag1, tag2, tag3, tag4);
  if (cnode == nullptr)
    return nullptr;
  else
    return cnode->child( tag5);
}

Node* Node::child( const std::string& tag0, const std::string& tag1, 
                   const std::string& tag2, const std::string& tag3, 
                   const std::string& tag4, const std::string& tag5,
                   const std::string& tag6){
  Node* cnode = child( tag0, tag1, tag2, tag3, tag4, tag5);
  if (cnode == nullptr)
    return nullptr;
  else
    return cnode->child( tag6);
}

std::list< Node*> Node::children_of_tag( const std::string& tag){
  std::list< Node*> tchildren;
  auto rpair = children.equal_range( tag);
  auto begin = rpair.first;
  auto end = rpair.second;
  for( auto itr = begin; itr != end; ++itr){
    tchildren.push_back( itr->second.get());
  }
  return tchildren;
}

  /*
void Node::reset_streams(){
  strm.reset();
  for( auto itr = children.begin(); itr != children.end(); ++itr){
    (*itr).second->reset_streams();
  } 
}
  */

bool Node::is_traversed() const{
  return traversed;
}

// constructor
Node:: Node( const String& tag, const JP::Attrs& _attrs, Node* _parent){
  ttag = tag;
  attrs = _attrs;
  parent = _parent;
}


/******************************************************************/
bool Parser::done() const{
  return traversal_done;
}

bool Parser::is_current_node_tag( const std::string& tag) const{
  return (tag == (current_node()->tag()));
}

void begin_tag( Parser& parser, const String& tag, const JP::Attrs& attrs){

  auto pnode = parser.leading;
  auto node = new Node( tag, attrs, pnode);
  std::unique_ptr< Node> unode( node);

  parser.leading = node;
  
  if (!(parser.top_node)){
    parser.current = node;
    parser.top_node = std::move( unode);
  }
  else{
    auto& cnodes = pnode->child_nodes;

    if (!cnodes.empty())
      cnodes.back()->successor = node;

    cnodes.push( std::move( unode));
  }
}

void end_tag( Parser& parser, const String& ctag, Vector< String>&& data){
  auto node = parser.leading;
  node->sdata = data;
  node->read_in = true;
  parser.leading = node->parent;
}

  // constructor
Parser::Parser( std::ifstream& _stream): 
  stream( _stream)
{
  jparser.reset( new JParser( *this, begin_tag, end_tag));
  
  if (!stream.is_open())
    error( "Parser constructor: stream not open");

  while (!top_node)
    jparser->parse_some( stream, parsing_done);
}

void Parser::complete_node( Node* node){
  
  if (node->completed)
    error( "Parser::complete_node: already completed");

  while (!node->read_in)
    jparser->parse_some( stream, parsing_done);

  while (!node->child_nodes.empty()){
    std::unique_ptr< Node>& cnode_uptr = node->child_nodes.front();
    auto cnode = cnode_uptr.get();
    auto tnpair = make_pair( cnode->ttag, std::move( cnode_uptr));
    node->child_nodes.pop();
    node->children.insert( std::move( tnpair));
    complete_node( cnode);
  }
  node->completed = true;
}

void Parser::complete_current_node(){
  complete_node( current);
}

// ensure that node's successor is in place
void Parser::build_forward( Node* node){
  while( node == leading && (!leading->read_in))
    jparser->parse_some( stream, parsing_done);
  
  while( node->child_nodes.empty() && !(node->read_in))
    jparser->parse_some( stream, parsing_done);
  
  if (node->parent)
    while( !(node->successor) && !(node->parent->read_in))
      jparser->parse_some( stream, parsing_done); 
}

void Parser::next(){
  build_forward( current);

  if (current->completed || current->child_nodes.empty()){
    if (current->successor){ // go over
      current->traversed = true;
      auto parent = current->parent;
      current = current->successor;
      parent->child_nodes.pop();
    }
    else { // go up
      current = current->parent;
      if (current) {
        current->child_nodes.pop();
        current->traversed = true;
      }
      else{
        traversal_done = true;
        top_node.release();
      }
    }
  }
  else  // go down 
    current = current->child_nodes.front().get();
}  


void Parser::go_up(){
  if (current == top_node.get())
    error( "Parser::go_up: already at top");

  current = current->parent;
  current->child_nodes.pop();
}


void Parser::find_next_tag( const std::string& tag){
  do {
    if (traversal_done)
      error( "find_next_tag: ", tag, " not found");
    next();
  }
  while( current->tag() != tag);    
}
  

}

