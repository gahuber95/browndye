/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/


#include "barrier.hh"

/*
Implements a multithreaded barrier.  The object is initialized
with argument "n". When a process calls "wait", it is held up
until "n" processes have called wait. Then, they are all released.
*/

void Barrier::initialize( unsigned int nthreads){
  this->count = nthreads;
  this->cycle = false;
  this->threshhold = nthreads;
  pthread_mutex_init( &(this->mutex), NULL);
  pthread_cond_init( &(this->cond), NULL);
}

void Barrier::release(){
  this->cycle = !this->cycle;
  this->count = this->threshhold;
  pthread_cond_broadcast( &(this->cond));  
}

void Barrier::wait(){
  int cancel, tmp;

  pthread_mutex_lock( &(this->mutex));

  bool old_cycle = this->cycle;
  --this->count;
  if (this->count == 0)
    release();
  else{

    pthread_setcancelstate( PTHREAD_CANCEL_DISABLE, &cancel);
    while (old_cycle == this->cycle)
      pthread_cond_wait( &(this->cond), &(this->mutex));
    pthread_setcancelstate( cancel, &tmp);
  }
  pthread_mutex_unlock( &(this->mutex));
}
