#include "small_molecule_thread.hh"
#include "node_info.hh"
#include "get_atoms_from_file.hh"

// constructor
Small_Molecule_Thread::Small_Molecule_Thread(){
  pinfo = NULL;
  spring_atom1 = NULL;
}

void Small_Molecule_Thread::initialize( const Small_Molecule_Common& molc, 
                                        JAM_XML_Pull_Parser::Node_Ptr node){

  printf( "molecule thread initialize %p\n", &molc); fflush( stdout);
  
  pinfo = molc.pinfo.get();
  auto atom_file = string_from_node( node, "atoms");
  get_atoms_from_file( pinfo, atom_file.c_str(), atoms, 
                       molc.hard_radii_from_param_file, 
                       molc.soft_radii_from_param_file,
                       molc.use_68);

  // shift molecule hydro center to origin
  Vec3< Length> offset;
  {
    namespace L = Linalg3;

    Vec3< Length> center;
    L::copy( molc.hydro_ellipsoid.center, center);
    L::get_scaled( -1.0, center, offset);
  }

  for( size_type i = 0; i < atoms.size(); i++){
    Atom_Small& atom = atoms[i];
    atom.translate( offset);
  }

  collision_structure.reset(  
    new Collision_Detector::Structure< Col_Interface_Small>( atoms));

  collision_structure->set_max_number_per_cell( max_per_cell);
  collision_structure->load_objects();

  check_for_param_file( atoms, pinfo, 1);

  auto tether_node = node->child( "tether");
  if (tether_node != NULL){
    auto a1_node = checked_child( tether_node, "atom1");
    unsigned int num = int_from_node( a1_node);
    spring_atom1 = atom_of_number( atoms, num);
  }
}
