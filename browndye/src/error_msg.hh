/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

#ifndef __ERROR_MSG_HH__
#define __ERROR_MSG_HH__

/*
  Error-handling code.  There is a templated and overloaded "error" function that
  takes any number of printable arguments, concatenates them into a message, and
  throws an exception. 

*/


#include <iostream>
#include <sstream>
#include <stdexcept>

class Jam_Exception: public std::runtime_error{
public:
  Jam_Exception( const std::string& msg): std::runtime_error( msg){}
};

inline
void error_inner( std::stringstream& sst){
  std::string message = sst.str();
  std::cerr << message << "\n";
  throw Jam_Exception( message);
}

template< typename T, typename ... Args>
void error_inner( std::stringstream& sst, const T& s, Args ... args){
  sst << s;
  error_inner( sst, args...);
}

template< typename ... Args>
void error( Args ... args){
  std::stringstream sst;
  error_inner( sst, args...);
}

// kludge to get it to compile
inline
void error( const char* msg){
  error( std::string( msg));
}

#endif

