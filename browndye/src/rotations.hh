/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
Various functions to handle infinitesimal and finite rotations.

Finite rotations are handled by computing and storing ahead of time
the analytical solution of
(Furry, WH, "Isotropic Rotational Brownian Motion", 
Physical Review 107 (1) 7-13 (1957))
at a few well-defined multiples of the rotational time scale,
and resolving rotations at arbitrary times by composing several
finite rotations.
*/

#ifndef __ROTATIONS_HH__
#define __ROTATIONS_HH__

#include "linalg3.hh"
#include "spline.hh"
#include "small_vector.hh"

void quat_to_mat( const SVec< double,4>& quat, Mat3< double>& mat);

void mat_to_quat( const Mat3< double>& mat, SVec< double,4>& quat);

template< class I>
void get_random_rotation( typename I::Random_Number_Generator& rng, 
                          Mat3< double>& rot);

template< class I>
void get_random_unit_vector( typename I::Random_Number_Generator& rng, 
                             Vec3< double>& u);

void multiply_quats( const SVec< double,4>& q0, 
                     const SVec< double,4>& q1, 
                     SVec< double,4>& q01);

void invert_quat( const SVec< double,4>& q, 
                  SVec< double,4>& qinv);

void dom_to_quat( const Vec3< double>& dom, SVec< double,4>& dq);

template< class I>
void get_diffusional_rotation( typename I::Random_Number_Generator& rng, 
                               double t, SVec< double,4>& quat);

template< class I>
void add_diffusional_rotation( typename I::Random_Number_Generator& rng, 
                               double t, 
                               const Mat3< double>& rot0, Mat3< double>& rot);

#include "rotations_impl.hh"

#endif
