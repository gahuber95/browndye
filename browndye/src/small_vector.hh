#ifndef __SMALL_VECTOR_HH__
#define __SMALL_VECTOR_HH__

/*
Used to define a small array whose length is fixed and
known at compile-time.  It is used instead of ordinary
C arrays for two reasons. First, bounds checking can be
turned on by compiling with the DEBUG flag. Second,
default values, like NaN, are automatically put in 
when the DEBUG flag is set; this helps debugging.

Useful classes:

template< class Type, unsigned int n> SVec;

specialized to 3-D
template< class T> Vec3; 

Matrix
template< class Type, unsigned nr, unsigned int nc> SMat;

3-Tensor
template< class Type, unsigned n0, unsigned int n1, unsigned int n2> SArr;

 */

#include "error_msg.hh"
#include "default_values.hh"

enum VType {Actual, View, Const_View};

template< unsigned int n>
class Base{
protected:
  void check_bounds( unsigned int i) const{
#ifdef DEBUG
    if (i >= n)
      error( "Small_Vector: out of bounds: ", i, " ", n);
#endif

  }
};

/***********************************************************/
namespace Small_Vector{
  template< class T, unsigned int n>
  class SVec_Const_View;
  
  template< class T, unsigned int n>
  class SVec_View;
}


template< class T, unsigned int n>
class SVec: public Base< n>{
public:
  typedef JAM_Vector::Default_Value< T> DV;

#ifdef DEBUG
  typedef Small_Vector::SVec_View< T,n> View;
  typedef Small_Vector::SVec_Const_View< T,n> Const_View;
#else
  typedef T* View;
  typedef const T* Const_View;
#endif

  const T& operator[]( unsigned int i) const{
    this->check_bounds(i);
    return _data[i];
  }
  
  T& operator[]( unsigned int i){
    this->check_bounds(i);
    return _data[i];
  }

  // constructor
  SVec(){
#ifdef DEBUG
    if (DV::is_specialized)
      for( unsigned int i = 0; i<n; i++)
        DV::set_value( _data[i]);
#endif
  }

#ifdef DEBUG
  // constructor
  SVec( const Small_Vector::SVec_Const_View< T, n>& view){
    const T* data_in = view.data();
    for (unsigned int i = 0; i < n; i++)
      _data[i] = data_in[i];
  }

  explicit SVec( const T* data_in){
    for (unsigned int i = 0; i < n; i++)
      _data[i] = data_in[i];
  }

  template< unsigned int ib, unsigned int ie>
  Small_Vector::SVec_Const_View< T, ie - ib> view() const{
    return Small_Vector::SVec_Const_View< T, ie-ib>( _data + ib);
  }

#else
  SVec( const T* data_in){
    for (unsigned int i = 0; i < n; i++)
      _data[i] = data_in[i];
  }

  template< unsigned int ib, unsigned int ie>
  const T* view() const{
    return _data + ib;
  }


#endif


  T* data(){
    return _data;
  }

  const T* data() const{
    return _data;
  }
  
  unsigned int size() const{
    return n; 
  }

protected:
  T _data[n];
};

namespace Small_Vector{

  template< class T, unsigned int n>
  class SVec_View: public Base< n>{
  public:
    
    const T& operator[]( unsigned int i) const{
      this->check_bounds(i);
      return _data[i];
    }
    
    T& operator[]( unsigned int i){
      this->check_bounds(i);
      return _data[i];
    }
    
    SVec_View( SVec< T,n>& vec): _data( vec.data()){}
    SVec_View( T* data_in): _data( data_in){}
    
    T* data(){
      return _data;
    }
    
    const T* data() const{
      return _data;
    }
    
  protected:
    T* _data;
  };
  
  template< class T, unsigned int n>
  class SVec_Const_View: public Base< n>{
  public:
    
    const T& operator[]( unsigned int i) const{
      this->check_bounds(i);
      return _data[i];
    }
    
    // constructor
    SVec_Const_View( const SVec< T,n>& vec): _data( vec.data()){}
    SVec_Const_View( const T* data_in): _data( data_in){}
    
    const T* data() const{
      return _data;
    }
    
  protected:
    const T* _data;
  };
  
}

template< class T>
class Vec3: public SVec< T,3>{
public:
  typedef SVec< T,3> Super;
  typedef typename Super::View View;

  // constructor
  Vec3(){}
  Vec3( const T& t0, const T& t1, const T& t2){
    Super::_data[0] = t0;
    Super::_data[1] = t1;
    Super::_data[2] = t2;
  }
#ifdef DEBUG
  Vec3( const Small_Vector::SVec_Const_View< T, 3>& view): SVec<T,3>( view){}
#else
  Vec3( const T* data_in): Super( data_in){}
#endif

};


/*****************************************************************/
namespace Small_Vector{
  template< class T, unsigned int nr, unsigned int nc>
  class SMat_Const_View;
  
  template< class T, unsigned int nr, unsigned int nc>
  class SMat_View;
}

#ifdef DEBUG
template< class T, unsigned int nr, unsigned int nc>
class SMat: public Base< nr>{
public:
  typedef JAM_Vector::Default_Value< T> DV;
  typedef T Data_Type[nr][nc];
  typedef Small_Vector::SMat_View< T,nr,nc> View;
  typedef Small_Vector::SMat_Const_View< T,nr,nc> Const_View;

  Small_Vector::SVec_Const_View< T,nc> operator[]( unsigned int i) const{
    this->check_bounds(i);
    return Small_Vector::SVec_Const_View< T,nc>( _data[i]);
  }
  
  Small_Vector::SVec_View< T,nc> operator[]( unsigned int i){
    this->check_bounds(i);
    return Small_Vector::SVec_View< T,nc>( _data[i]);
  }

  // constructor
  SMat(){
    if (DV::is_specialized)
      for (unsigned int i = 0; i < nr; i++)
        for (unsigned int j = 0; j < nc; j++)
          DV::set_value( _data[i][j]);    
  }

  // constructor
  SMat( const Small_Vector::SMat_Const_View< T, nr,nc>& view){
    const Data_Type& data_in = view.data();
    for (unsigned int i = 0; i < nr; i++)
      for (unsigned int j = 0; j < nc; j++)
        _data[i][j] = data_in[i][j];
  }

  explicit SMat( const Data_Type& data_in){
    for (unsigned int i = 0; i < nr; i++)
      for (unsigned int j = 0; j < nc; j++)
        _data[i][j] = data_in[i][j];
  }

  Data_Type& data(){
    return _data;
  }

  const Data_Type& data() const{
    return _data;
  }

private:
  T _data[nr][nc];
};

namespace Small_Vector{
  template< class T, unsigned int nr, unsigned int nc>
  class SMat_View: public Base< nr>{
  public:
    typedef T Data_Type[nr][nc];
    
    SVec_Const_View< T,nc> operator[]( unsigned int i) const{
      this->check_bounds(i);
      return SVec_Const_View< T,nc>( _data[i]);
    }
    
    SVec_View< T,nc> operator[]( unsigned int i){
      this->check_bounds(i);
      return SVec_View< T,nc>( _data[i]);
    }
    
    SMat_View( SMat< T,nr,nc>& mat): _data( mat.data()){}
    SMat_View( Data_Type& data_in): _data( data_in){}
    
    Data_Type& data(){
      return _data;
    }
    
    const Data_Type& data() const{
      return _data;
    }
    
  private:
    Data_Type& _data;
  };
  
  template< class T, unsigned int nr, unsigned int nc>
  class SMat_Const_View: public Base< nr>{
  public:
    typedef const T Data_Type[nr][nc];
    
    SVec_Const_View< T,nc> operator[]( unsigned int i) const{
      this->check_bounds(i);
      return SVec_Const_View< T,nc>( _data[i]);
    }
    
    SMat_Const_View( const SMat< T,nr,nc>& mat): _data( mat.data()){}
    SMat_Const_View( Data_Type& data_in): _data( data_in){}
    
    Data_Type& data() const{
      return _data;
    }
    
  private:
    Data_Type& _data;
  };
}  

#else

template< class T, unsigned int nr, unsigned int nc>
class SMat{
public:
  typedef T Data_Type[nr][nc];
  typedef Data_Type& View;
  typedef const Data_Type& Const_View;

  const T* operator[]( unsigned int i) const{
    return _data[i];
  }
  
  T* operator[]( unsigned int i){
    return _data[i];
  }

  // constructor
  SMat(){}

  // constructor
  explicit SMat( const Data_Type& data_in){
    for (unsigned int i = 0; i < nr; i++)
      for (unsigned int j = 0; j < nc; j++)
        _data[i][j] = data_in[i][j];
  }

  Data_Type& data(){
    return _data;
  }

  const Data_Type& data() const{
    return _data;
  }

private:
  T _data[nr][nc];
};

#endif

template< class T>
class Mat3: public SMat< T,3,3>{};


/*****************************************************************/
#ifdef DEBUG

template< class T, unsigned int nr, unsigned int nc, unsigned int nz>
class SArr: public Base< nr>{
public:
  typedef JAM_Vector::Default_Value< T> DV;

  Small_Vector::SMat_View< T,nc,nz> operator[]( unsigned int i){
    this->check_bounds( i);
    return Small_Vector::SMat_View< T,nc,nz>(_data[i]);
  }

  const Small_Vector::SMat_Const_View< T,nc,nz> operator[]( unsigned int i) const{
    this->check_bounds( i);
    return Small_Vector::SMat_Const_View< T,nc,nz>(_data[i]);
  }

  SArr(){
    if (DV::is_specialized)
      for (unsigned int kr = 0; kr < nr; kr++)
        for (unsigned int kc = 0; kc < nc; kc++)
          for (unsigned int kz = 0; kz < nz; kz++)          
            DV::set_value( _data[kr][kc][kz]);    
  }


private:
  T _data[nr][nc][nz];
};

#else

template< class T, unsigned int nr, unsigned int nc, unsigned int nz>
class SArr{
public:
  typedef T Row_Type[nc][nz];

  Row_Type& operator[]( unsigned int i){
    return _data[i];
  }

  const Row_Type& operator[]( unsigned int i) const{
    return _data[i];
  }

  SArr(){}


private:
  T _data[nr][nc][nz];
};
#endif

#endif
