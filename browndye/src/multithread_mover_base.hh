/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

#ifndef __MULTITHREAD_MOVER_BASE_HH__
#define __MULTITHREAD_MOVER_BASE_HH__


/*
Implements Mover_Base class.

On several threads, this code will execute a function on 
a copy of an object on each thread.
This code runs only one object per thread.  See multithread_mover.hh
for code that can run arbitrary numbers of objects per thread.

The Interface class has the following types and static functions:

Exception - the type of exception to be thrown if needed.
Objects_Ref - reference to a container of objects
Object_Ref - reference to an object

template< class Func>
void apply_to_object_refs( Func& f, Objects_Ref objects) -
  applies function object "f" to each object in "objects".
  "Func" has overloaded void ()( Object_Ref) operator.

unsigned int size( Objects_Ref) - number of objects

void do_move( Objects_Ref objects, Object object) - moves "object"

void set_null( Objects_Ref& objects) - sets reference to null (no object)

bool is_null( Objects_Ref& objects) - returns "true" if reference is null.

*/

#include <string>
#include <vector>
#include "barrier.hh"
#include "error_msg.hh"

namespace Multithread{

template< class Interface>
class Mover_Base{
public:
  typedef typename Interface::Exception Exception;
  typedef typename Interface::Objects_Ref Objects;
  typedef typename Interface::Object_Ref Object;
  typedef void (*Function)( Objects, Object);

  void do_moves();
  Mover_Base();
  ~Mover_Base();
  void set_objects( Objects);
  
  unsigned int n_threads() const{
    return nthreads;
  }

private:
  template< class Func>
  static void apply_to_object_refs( Func& f, Objects objects){
    Interface::apply_to_object_refs( f, objects);
  }

  static unsigned int size( Objects objects){
    return Interface::size( objects);
  }

  static void do_move( Objects objects, Object obj){
    Interface::do_move( objects, obj);
  }

  static void set_null( Objects& objects){
    Interface::set_null( objects);
  }

  static bool is_null( Objects objects){
    return Interface::is_null( objects);
  }

  //####################################
  static void* start_thread( void* arg);

  class Thread_Info{
  public:
    unsigned int n;
    Mover_Base* sampler;
    Function do_move;
    Objects objects;
    Object object;
  };

  class Object_Getter{
  public:
    std::vector< Thread_Info>& thread_info;
    unsigned int n;

    Object_Getter( std::vector< Thread_Info>& info):
      thread_info( info){
      n = 0;
    }

    void operator()( Object obj){
      thread_info[n].object = obj;
      ++n;
    }
  };

  Barrier barrier, barrier2;

  bool done;
  bool begun;
  int error_thread;
  unsigned int nthreads;
  std::vector< pthread_t> threads;
  std::vector< Thread_Info> thread_info;
  Exception exception;
  pthread_mutex_t error_mutex;

  Objects objects;

};

template< class Interface>
void Mover_Base< Interface>::set_objects( Objects objs){

  nthreads = size( objs);
  objects = objs;

  threads.resize( nthreads);
  thread_info.resize( nthreads);

  for( unsigned int i=0; i<nthreads; i++){
    Thread_Info& info = thread_info[i];
    info.n = i;
    info.sampler = this;
    info.do_move = do_move;
    info.objects = objs;
  }
 
  Object_Getter object_getter( thread_info);
  apply_to_object_refs( object_getter, objs);
}

template< class Interface>
Mover_Base< Interface>::Mover_Base(){
  nthreads = 0;
  done = false;
  begun = false;
  error_thread = -1;
  set_null( objects);
}

template< class Interface>
Mover_Base< Interface>::~Mover_Base(){
  done = true;

  if (begun){
    barrier2.wait();
    
    void* final_i;
    for( unsigned int i=0; i<nthreads; i++){
      pthread_join( threads[i], &final_i);
    }
  }
  if (nthreads > 1)
    pthread_mutex_destroy( &error_mutex);
}

template< class Interface>
void* Mover_Base< Interface>::start_thread( void* arg){

  Thread_Info* tinfo = (Thread_Info*)arg;
  unsigned int ith = tinfo->n;
  unsigned int* ithad = &(tinfo->n);
  Mover_Base< Interface>* sampler = tinfo->sampler;
  Object object = tinfo->object;
  Objects objects = tinfo->objects;

  while(!sampler->done){

    try{
      do_move( objects, object);
    }
    catch (Exception message){

      if (sampler->nthreads > 1){
        if (sampler->error_thread == -1){
          pthread_mutex_lock( &(sampler->error_mutex));
          sampler->error_thread = ith;
          sampler->done = true;
          sampler->begun = false;
          sampler->exception = message;
          
          std::vector< pthread_t>& threads = sampler->threads;
          for( unsigned int k=0; k < sampler->nthreads; k++){
            if (!pthread_equal( threads[ith], threads[k])){
              pthread_cancel( threads[k]);
            }
          }
          sampler->barrier.release();
          pthread_mutex_unlock( &(sampler->error_mutex));
          pthread_exit( (void*)ithad);
        }
      }
      else{ // nthreads = 1
        throw message;
      }
    }

    if (sampler->nthreads == 1)
      return ((void*)0);

    else {
      if (sampler->error_thread == -1)
        sampler->barrier.wait();
            
      if (sampler->error_thread == -1)
        sampler->barrier2.wait();

      if (sampler->error_thread > -1)
        pthread_exit( (void*)ithad);
    }    
  }

  if (sampler->nthreads > 1){
    pthread_exit( (void*)ithad);
    return ((void*)0); // avoid compiler warning
  }
  else
    return ((void*)0);
}

template< class Interface>
void Mover_Base< Interface>::do_moves(){

  if (is_null( objects)){
    error( "objects not set\n");
    exit(1);
  }

  if (nthreads > 1){

    error_thread = -1;
    if (!begun){
      begun = true;
      
      pthread_mutex_init( &error_mutex, NULL);
      barrier.initialize( nthreads+1);
      barrier2.initialize( nthreads+1);
      
      for( unsigned int i=0; i<nthreads; i++)
        pthread_create( &(threads[i]), NULL, start_thread, 
                        &(thread_info[i]));  
    }
    
    else{
      barrier2.wait();
    }  
    
    if (error_thread == -1)
      barrier.wait();
    
    if (error_thread > -1){
      
      void* final_i;
      pthread_join( threads[ error_thread], &final_i);
      
      throw exception;
    }
  }
  else {  // nthread = 1
    start_thread( &(thread_info[0]));
  }

}

}

#endif
