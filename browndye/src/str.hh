#ifndef __STR_HH__
#define __STR_HH__

// Simple typedef for XML parsers

#include <expat.h>
#include <string>

namespace JAM_XML_Parser{
  typedef std::string String;
}

#endif
