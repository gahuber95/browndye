/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
Implementation of Pathway class.
 */

#include <list>
#include <math.h>
#include <map>
#include <set>
#include "jam_xml_pull_parser.hh"
#include "pathways.hh"
#include "node_info.hh"

namespace Pathways{

Criterion::~Criterion(){
  for( size_type i = 0; i < criteria.size(); ++i)
    delete criteria[i];
}

Reaction::~Reaction(){
  delete criterion;
}

Rxn_Pair::Rxn_Pair( size_type _atom0, size_type _atom1, Length _distance):
  atom0(_atom0), atom1(_atom1), distance(_distance)
{}

Rxn_Pair::Rxn_Pair():
  atom0( std::numeric_limits<size_type>::max()), 
  atom1( std::numeric_limits<size_type>::max()), distance( NAN)
{}

Rxn_Pair pair_from_node( JP::Node_Ptr node){
  Length distance;
  JP::Node_Ptr anode = checked_child( node, "atoms");

  auto i0 = stoul( anode->data()[0]);
  auto i1 = stoul( anode->data()[1]);

  auto dnode = checked_child( node, "distance");
  set_fvalue( distance, stod( dnode->data()[0]));
  return Rxn_Pair( i0, i1, distance);
}

typedef std::list< JP::Node_Ptr> NList;

Criterion* criterion_from_node( JP::Node_Ptr node){
  auto pnodes = node->children_of_tag( "pair");
  auto cnodes = node->children_of_tag( "criterion");

  Criterion* criterion = new Criterion();

  const size_type np = pnodes.size();
  criterion->pairs.resize( np);
  size_type ip = 0;
  for( NList::iterator itr = pnodes.begin(); itr != pnodes.end(); ++itr){
    auto pnode = *itr;
    criterion->pairs[ip] = pair_from_node( pnode);
    ++ip;
  }

  const size_type nc = cnodes.size();
  criterion->criteria.resize( nc);
  size_type ic = 0;
  for( NList::iterator itr = cnodes.begin(); itr != cnodes.end(); ++itr){
    auto cnode = *itr;
    criterion->criteria[ic] =  criterion_from_node( cnode);
    ++ic;
  }

  JP::Node_Ptr nnode = checked_child( node, "n-needed");
  criterion->n_needed = stoul( nnode->data()[0]);

  return criterion;
}

Reaction* reaction_from_node( JP::Node_Ptr node){
  Reaction* reaction = new Reaction();
  auto cnode = checked_child( node, "criterion");
  reaction->criterion = criterion_from_node( cnode);
  reaction->name = string_from_node( node, "name");
  reaction->state_before_name = string_from_node( node, "state-before");
  reaction->state_after_name = string_from_node( node, "state-after");
  return reaction;
}

size_type anumber( std::map< size_type, size_type>& map, size_type i){
  std::map< size_type, size_type>::iterator itr = map.find( i);
  if (itr == map.end())
    error( "pathways: atom number ", i,  " does not exist");
  return itr->second;
}

void remap_criterion( std::map< size_type, size_type>& imap,
                      Criterion* criterion, bool zero){

  Vector< Rxn_Pair>& pairs = criterion->pairs;
  for (Vector< Rxn_Pair>::iterator itr = pairs.begin();
       itr != pairs.end(); ++itr){
    
    Rxn_Pair& pair = *itr;

    if (zero)      
      pair.atom0 = anumber( imap, pair.atom0);
    else
      pair.atom1 = anumber( imap, pair.atom1);
  }

  Vector< Criterion*>& criteria = criterion->criteria;
  for (Vector< Criterion*>::iterator itr = criteria.begin();
       itr != criteria.end(); ++itr)
    remap_criterion( imap, *itr, zero);
}

// e.g, maps 2 4 6 8 to 0 1 2 3
void Pathway_Base::remap_molecule(  const std::vector< size_type>& numbers, 
                                    bool zero){

  std::map< size_type, size_type> imap;
  for (size_type i = 0; i < numbers.size(); i++)
    imap.insert( std::pair< size_type, size_type>( numbers[i], i));
  
  for (Vector< Reaction*>::iterator riter = reactions.begin();
       riter != reactions.end(); riter++){
    Reaction* rxn = *riter;
    remap_criterion( imap, rxn->criterion, zero);    
  }
}

void Pathway_Base::remap_molecule0( const std::vector< size_type>& numbers){
  remap_molecule( numbers, true);
}

void Pathway_Base::remap_molecule1( const std::vector< size_type>& numbers){
  remap_molecule( numbers, false);
}

size_type Pathway_Base::first_state() const{
  return first_state_number_data;
}

// constructor
Pathway_Base::Pathway_Base(): 
  first_state_name( "0"),
  first_state_number_data( std::numeric_limits< size_type>::max())
{}

// destructor
Pathway_Base::~Pathway_Base(){
  for( size_type i = 0; i < reactions.size(); ++i)
    delete reactions[i];
}


/* starts with only reactions and their input string data;
   generates states, and puts integer data into states and reactions.
*/

void Pathway_Base::determine_states(){

  std::set< String> state_names;
  for (size_type i = 0; i < reactions.size(); i++){
    Reaction* rxn = reactions[i];
    rxn->n = i;
    reaction_str2int[ rxn->name] = i;
    state_names.insert( rxn->state_before_name);
    state_names.insert( rxn->state_after_name);
  }

  states.resize( state_names.size());
  size_type i = 0;
  for (std::set< String>::iterator itr = state_names.begin();
       itr != state_names.end(); ++itr){
    states[i].name = *itr;
    states[i].n = i;
    state_str2int[ *itr] = i;
    ++i;
  }
  
  for (size_type i = 0; i < reactions.size(); i++){
    Reaction* rxn = reactions[i];
    size_type isb = state_str2int[ rxn->state_before_name];
    size_type isa = state_str2int[ rxn->state_after_name];
    rxn->state_before = isb;
    rxn->state_after = isa;
    states[ isb].n = isb;
    states[ isb].rxns_from.push_back( rxn->n);
    states[ isa].n = isa;
    states[ isa].rxns_to.push_back( rxn->n);
  }

  first_state_number_data = state_str2int[ first_state_name];
}

//********************************************************************
// initializer
void Pathway_Base::initialize( const std::string& file){

  std::ifstream input( file);

  if (!input.is_open())
    error( "file ", file, " could not be opened");

  JP::Parser parser( input);
  parser.complete_current_node();
  initialize_from_node( parser.current_node());
}

typedef std::list< JP::Node_Ptr> NList;

// assume completed node
void Pathway_Base::initialize_from_node( JP::Node_Ptr node){


  JP::Node_Ptr rsnode = checked_child( node, "reactions");

  NList rnodes = rsnode->children_of_tag( "reaction");
  size_type nr = rnodes.size();
  reactions.resize( nr);
  size_type ir = 0;
  for ( NList::iterator itr = rnodes.begin();
        itr != rnodes.end(); ++itr){
    JP::Node_Ptr rnode = *itr;
    reactions[ ir] = reaction_from_node( rnode);
    ++ir;
  }

  JP::Node_Ptr frnode = node->child( "first-state");
  if (frnode != NULL){
    first_state_name = string_from_node( frnode);
  }
  
  determine_states();
}

}

