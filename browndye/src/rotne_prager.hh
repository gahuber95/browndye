#ifndef __ROTNE_PRAGER_HH__
#define __ROTNE_PRAGER_HH__

#include "pi.hh"

//const double pi = 3.1415926;

namespace Rotne_Prager{

  template< class Length, class Viscosity>
  decltype( 1.0/(Length()*Viscosity()))
    single_mobility( Viscosity mu, Length a){
    return 1.0/(6.0*pi*mu*a);
  }

  template< class Length, class Viscosity>
  decltype( 1.0/(Length()*Length()*Length()*Viscosity()))
    single_rotational_mobility( Viscosity mu, Length a){
    return 1.0/(8.0*pi*mu*a*a*a);
  }


  template< class Length, class Viscosity>
  decltype( 1.0/(Length()*Viscosity()))
    zz_mobility( Viscosity mu, Length a0, Length a1, Length r){
    auto asq = 0.5*(a0*a0 + a1*a1);
    return (1.5 - (asq/(r*r)))/(6.0*pi*mu*r);
  }

  template< class Length, class Viscosity>
  decltype( 1.0/(Length()*Viscosity()))
    xx_mobility( Viscosity mu, Length a0, Length a1, Length r){
    auto asq = 0.5*(a0*a0 + a1*a1);
    return (0.75 + 0.5*( asq/(r*r)))/(6.0*pi*mu*r);
  }

  template< class Length, class Viscosity>
  decltype( 1.0/(Length()*Viscosity()))
    no_hi_mobility( Viscosity mu, Length a0, Length a1){

    return (1.0/a0 + 1.0/a1)/(6.0*pi*mu);
  }

  template< class Length, class Viscosity>
  decltype( 1.0/(Length()*Viscosity()))
    parallel_mobility( Viscosity mu, Length a0, Length a1, Length r){

    return no_hi_mobility( mu,a0,a1) - 2.0*zz_mobility( mu,a0,a1,r);
  }

  template< class Length, class Viscosity>
  decltype( 1.0/(Length()*Viscosity()))
    perpendicular_mobility( Viscosity mu, Length a0, Length a1, Length r){

    return no_hi_mobility( mu,a0,a1) - 2.0*xx_mobility( mu,a0,a1,r);
  }

}

#endif
