/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
Implements interface to Multithread::Mover class. Included by
"nam_simulator.hh".
*/

#include "multithread_mover.hh"

class NAM_Simulator;

class NAM_Multi_Interface{
public:
  typedef std::string Exception;
  typedef NAM_Simulator* Objects_Ref;
  typedef Molecule_Pair* Object_Ref;

  template< class Func>
  static void apply_to_object_refs( Func& f, Objects_Ref objects);
 
  static unsigned int size( Objects_Ref objects);
  static void do_move( Objects_Ref objects, unsigned int, Object_Ref obj);
  static void set_null( Objects_Ref& objects);
  static bool is_null( Objects_Ref objects);

};
                                                
inline
void NAM_Multi_Interface::set_null( NAM_Simulator*& sim){
  sim = NULL;
}

inline
bool NAM_Multi_Interface::is_null( NAM_Simulator* sim){
  return (sim == NULL);
}


