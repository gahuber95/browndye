#include "linalg3.hh"

/*
Implements simple linear algebra functions of 3-D systems.
*/

namespace Linalg3{

void get_perp_axes( const Vec3< double>& axis2, 
                    Vec3< double>& axis0, Vec3< double>& axis1){
 
  if (axis2[0] == 0.0 && axis2[1] == 0.0){
    axis0[0] = 1.0;
    axis0[1] = axis0[2] = 0.0;
  }
  else{
    Vec3< double> uz, r0;
    uz[0] = 0;
    uz[1] = 0;
    uz[2] = 1;

    get_cross( axis2, uz, r0);
    get_normed( r0, axis0);
  }
  {
    Vec3< double> r1;;
    get_cross( axis2, axis0, r1);
    get_normed( r1, axis1);
  }
}
}  
