/*
 * molecule_pair_common.cc
 *
 *  Created on: Sep 8, 2015
 *      Author: root
 */

#include "molecule_pair_common.hh"
#include "outtag.hh"
#include "molecule_pair_thread.hh"

namespace L = Linalg3;
namespace JP = JAM_XML_Pull_Parser;
typedef std::string String;

double angle_from_succ_rots( const Mat3< double>& rot0,
                             const Mat3< double>& rot1){
  SVec< double, 4> quat0, quat1;
  mat_to_quat( rot0, quat0);
  mat_to_quat( rot1, quat1);
  SVec< double,4> inv_quat0;
  invert_quat( quat0, inv_quat0);
  SVec< double, 4> dquat;
  multiply_quats( quat1, inv_quat0, dquat);
  return 0.5*acos( dquat[0]);
}



// constructor
Molecule_Pair_Common::Molecule_Pair_Common(){
  b_rad = Length( NAN);
  q_rad = Length( NAN);
//  dt_min = Time( NAN);
  //dtr_min = Time( NAN);

  building_bins = false;
  start_at_site = false;
  no_hi = false;
}

template< class T>
inline
typename UProd< typename UProd< T, T>::Res, T>::Res
cube( T x){
  return x*x*x;
}

// assume node is completed
void Molecule_Pair_Common::initialize( JP::Node_Ptr node,
                                       const std::string& solvent_file){

  pthread_mutex_init( &mutex, NULL);

  if (!solvent_file.empty()){
    std::ifstream sinput( solvent_file);

    JP::Parser parser( sinput);
    parser.complete_current_node();
    JP::Node_Ptr snode = parser.current_node();

    get_double_from_node( snode, "relative-viscosity",
                          solvent.relative_viscosity);

    get_double_from_node( snode, "kT", solvent.kT);
    get_double_from_node( snode, "debye-length", solvent.debye_length);
    get_double_from_node( snode, "dielectric", solvent.dielectric);
  }

  printf(  "initialize molecule 0\n");
  JP::Node_Ptr mol0_node = checked_child( node, "molecule0");
  mol0.initialize( mol0_node, solvent.kT);

  printf(  "initialize molecule 1\n");
  JP::Node_Ptr mol1_node = checked_child( node, "molecule1");
  mol1.initialize( mol1_node);

  printf(  "get reaction-file\n");
  String rxn_file = string_from_node( node, "reaction-file");
  pathway.reset( new Pathways::Pathway< Rxn_Interface>());
  pathway->initialize( rxn_file.c_str());


  mover.reset( new Two_Sphere_Brownian::Mover< Mover_Interface>());
  mover->set_radius( mol1.hydro_radius()/mol0.hydro_radius());

  printf( "get BD propagation file\n");
  String lmz_file = string_from_node( node, "BD-propagation-file");

  full_stepper.reset( new Full_Stepper::Doer< Stepper_Interface>);
  full_stepper->initialize( lmz_file.c_str());

  printf ("get radii\n");
  b_rad = Length( double_from_node( node, "b-radius"));
  q_rad = qb_factor()*b_rad;


  printf(  "get dt data\n");

  ts_params.initialize( *this, node);

  JP::Node_Ptr stnode = node->child( "start-at-site");
  if (stnode != NULL){
    String res = string_from_node( stnode);
    if (res == String( "true") || res == String( "True") ||
        res == String( "TRUE")){

      start_at_site = true;
      Vec3< Length> pos0, pos1;
      mol0.get_hydro_center( pos0);
      mol1.get_hydro_center( pos1);

      L::get_diff( pos1, pos0, starting_site_offset);
    }
  }

  JP::Node_Ptr nhnode = node->child( "hydrodynamic-interactions");
  if (nhnode != NULL){
    String res = string_from_node( nhnode);

    if (res == String( "false") || res == String( "False") ||
        res == String( "FALSE")){
      mover->set_no_hi();
      no_hi = true;
    }
  }

  JP::Node_Ptr dnode = node->child( "desolvation-parameter");
  if (dnode != NULL){
    double fudge = double_from_node( dnode);
    mol0.set_desolve_fudge( fudge);
    mol1.set_desolve_fudge( fudge);
  }

  JP::Node_Ptr node68 = node->child( "use-6-8-force");
  if (node68 != NULL){
    String res = string_from_node( node68);
    if (res == String( "true") || res == String( "True") ||
        res == String( "TRUE")){
      mol0.set_use_68();
      mol1.set_use_68();
    }
  }
}

template< class T>
void ot( std::ofstream& outp, const std::string& tag, T t){
  outtag( outp, tag, t, 4);
}

void Molecule_Pair_Common::output( std::ofstream& outp){

  outp << "  <solvent>\n";
  ot( outp, "debye-length", solvent.debye_length);
  ot( outp, "dielectric", solvent.dielectric);
  ot( outp, "vacuum-permittivity", vacuum_permittivity);
  ot( outp, "water-viscosity", water_viscosity);
  ot( outp, "relative-viscosity", solvent.relative_viscosity);
  outp << "  </solvent>\n";

  outp << "  <molecule-info>\n";

  ot( outp,"charge-molecule0", mol0.charge());
  ot( outp,"charge-molecule1", mol1.charge());

  ot( outp,"b-radius", b_rad);
  ot( outp,"radius-molecule0", mol0.hydro_radius());
  ot( outp,"radius-molecule1", mol1.hydro_radius());

  ot( outp, "hydrodynamic-interactions", no_hi ? "false" : "true");

  outp << "  </molecule-info>\n";

  outp << "  <time-step-tolerances>\n";
  ot( outp,"minimum-dt", minimum_dt());
  ot( outp, "minimum-dx", ts_params.dx_min);
  ot( outp, "force", ts_params.ftol);
  ot( outp, "reaction", ts_params.rtol);
  ot( outp, "collision", ts_params.gtol);
  outp << "  </time-step-tolerances>\n";

  const size_type nrxns = n_reactions();
  ot( outp,"n-reactions", nrxns);
}

/*
template< class Atom>
const Vector<  Vector< int>::size_type > atom_indices( const Vector< Atom>& atoms){

  typedef Vector< int>::size_type size_type;

  size_type n = atoms.size();
  Vector< size_type> indices( n);
  for( size_type i = 0; i < n; i++)
    indices[i] = atoms[i].number;
  return indices;
}
*/

// necessary to renumber atoms because only surface atoms are included.
void Molecule_Pair_Common::renormalize_reactions( const Molecule_Pair_Thread& mthread){
  //auto& atoms0 = mol0.atoms;
  auto indices0 = mol0.atom_indices();

  //auto& atoms1 = mthread.mol1.atoms;
  auto indices1 = mthread.mol1.atom_indices();

  pathway->remap_molecule0( indices0);
  pathway->remap_molecule1( indices1);
}
