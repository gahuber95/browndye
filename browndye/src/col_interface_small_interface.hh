#ifndef __COL_INTERFACE_SMALL_INTERFACE_HH__
#define __COL_INTERFACE_SMALL_INTERFACE_HH__

#include "vector.hh"
#include "transform.hh"
#include "atom_small.hh"

class Col_Interface_Small_Interface{
public:
  typedef Atom_Small Atom;
  typedef Vector< Atom_Small> Atoms;
  typedef Atoms::iterator Ref;
  typedef Vector< Ref> Refs;
  typedef Atoms::size_type size_type;
  typedef ::Transform Transform; 
};

#endif
