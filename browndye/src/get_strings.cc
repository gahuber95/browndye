#include "get_strings.hh"

void get_strings( JAM_XML_Pull_Parser::Node_Ptr node, std::string tag, 
                  Vector< std::string>& carrays){

  namespace JP = JAM_XML_Pull_Parser;
  typedef std::string String;

  typedef Vector< String>::size_type size_type;

  std::list< JP::Node_Ptr> cnodes = node->children_of_tag( tag);
  size_type n = cnodes.size();
  carrays.resize( n);

  std::list< JP::Node_Ptr>::iterator itr = cnodes.begin();
  for( size_type i=0; i<n; i++){
    JP::Node_Ptr cnode = *itr;
    carrays[i] = cnode->data()[0];
    ++itr;
  }
}


