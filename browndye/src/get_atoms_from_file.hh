#ifndef __GET_ATOMS_FROM_FILE_HH__
#define __GET_ATOMS_FROM_FILE_HH__

#include "error_msg.hh"
#include "parameter_info.hh"
#include "pinfo_interface.hh"

template< class Atom>
void get_atoms_from_file( const Near_Interactions::Parameter_Info* pinfo, 
                          std::string file, Vector< Atom>& atoms, 
                          bool hard_radii_from_param, 
                          bool soft_radii_from_param,
                          bool use_68){
  
  typedef Pinfo_Interface< Atom> Interface;
  Near_Interactions::get_atoms_from_file< Interface>( pinfo, file,
                                                      atoms, false, 
                                                      hard_radii_from_param, 
                                                      soft_radii_from_param, 
                                                      use_68);
}

template< class Atom>
void check_for_param_file( const Vector< Atom>& atoms,
                           const Near_Interactions::Parameter_Info* pinfo,
                           int i){
  for (auto itr = atoms.begin(); itr != atoms.end(); ++itr){
    const Atom& atom = *itr;
    if (atom.soft && pinfo == NULL)
      error( "molecule ", i, 
             " must have a LJ parameter file since is has soft atoms");
  }
  
}

template< class Atom>
const Atom* atom_of_number( const Vector< Atom>& atoms, unsigned int number){
  for( auto itr = atoms.begin(); itr != atoms.end(); ++itr){
    const Atom& atom = *itr;
    if (atom.number == number){
      return &atom;
    }
  }
  error( "atom number ", number, " for tether not found");
  return NULL;
}


#endif
