#include <fstream>
#include <list>
#include "molecule_common.hh"
#include "node_info.hh"
#include "initialize_field.hh"

namespace JP = JAM_XML_Pull_Parser;
typedef std::string String;

template< class U>
void get_vector3( JP::Node_Ptr node, Vec3< U>& a){

  auto& data = node->data();
  for( auto k = 0; k < 3; k++)
    set_fvalue( a[k], stod( data[k]));
}

void get_ecenter( JP::Node_Ptr top_node, const String& name, Ellipsoid& ellipsoid){
  JP::Node_Ptr node = checked_child( top_node, name.c_str());
  get_vector3( node, ellipsoid.center);
} 

void get_edistances( JP::Node_Ptr top_node, String name, Ellipsoid& ellipsoid){
  JP::Node_Ptr node = checked_child( top_node, name.c_str());
  get_vector3( node, ellipsoid.distances);
} 

Near_Interactions::Parameter_Info* initialized_param_info( JP::Node_Ptr node){
  JP::Node_Ptr pnode = node->child( "short-ranged-force-parameters");

  Near_Interactions::Parameter_Info* res;
  if (pnode != NULL){
    String file_name = string_from_node( pnode);
    res =  new Near_Interactions::Parameter_Info( file_name.c_str());
  }
  else
    res =  NULL;
  return res;
}

// constructor
Molecule_Common::Molecule_Common(){

  h_radius = Length( NAN);
  soft_radii_from_param_file = true; 
  hard_radii_from_param_file = false;
  desolve_fudge = 1.0;
  use_68 = false;
  //pair = NULL;
}

void Molecule_Common::set_h_radius( String file){
  std::ifstream input( file);
  if (!input.is_open())
    error( "file ", file, " could not be opened");

  JP::Parser parser( input);

  parser.complete_current_node();
  JP::Node_Ptr top_node = parser.current_node();
  h_radius = Length( double_from_node( top_node));
}

void Molecule_Common::set_ellipsoids( String file){
  std::ifstream input( file);

  if (!input.is_open())
    error( "file ", file, " could not be opened");

  JP::Parser parser( input);

  parser.complete_current_node();
  JP::Node_Ptr top_node = parser.current_node();

  get_ecenter( top_node, "hydro-center", hydro_ellipsoid);
  get_edistances( top_node, "hydro-radii", hydro_ellipsoid);  

  JP::Node_Ptr anode = checked_child( top_node, "axes");
  std::list< JP::Node_Ptr > vnodes = anode->children_of_tag( "axis");
  std::list< JP::Node_Ptr >::iterator iter = vnodes.begin();
  for( int i = 0; i<3; i++){
    get_vector3( *iter, hydro_ellipsoid.axes[i]);
    ++iter;
  }
}

void Molecule_Common::initialize_super( JP::Node_Ptr node, Vec3< Length>& offset){
  printf( "molecule common initialize %p\n", this);
  printf( "get ellipsoids\n");
  String ellipsoid_file = string_from_node( node, "ellipsoids");
  set_ellipsoids( ellipsoid_file.c_str());

  printf( "get hydro-radius\n");
  String hradius_file = string_from_node( node, "hydro-radius");
  set_h_radius( hradius_file.c_str());

  Linalg3::get_scaled( -1.0, hydro_ellipsoid.center, offset);

  JP::Node_Ptr dnode = node->child( "desolvation-field");
  if (dnode != NULL){
    printf( "get desolvation-field\n");
    initialize_field( dnode, born_field);
    born_field->shift_position( offset);
    born_hint0 = born_field->first_hint();
  }  

  pinfo.reset( initialized_param_info( node));

  JP::Node_Ptr rnode = node->child( "radii");
  if (rnode != NULL){
    JP::Node_Ptr hnode = rnode->child( "hard");
    if (hnode != NULL){
      String hstring = string_from_node( hnode);
      if (hstring == String( "parameter-file"))
        hard_radii_from_param_file = true;
      else if (hstring == String( "pqrxml-file"))
        hard_radii_from_param_file = false;
      else
        error( "radii hard tag: ", hstring.c_str(), 
               " not an option: must be either \"parameter-file\" or \"pqrxml-file\"");
    }
    JP::Node_Ptr snode = rnode->child( "soft");
    if (snode != NULL){
      String sstring = string_from_node( snode);
      if (sstring == String( "parameter-file"))
        soft_radii_from_param_file = true;
      else if (sstring == String( "pqrxml-file"))
        soft_radii_from_param_file = false;
      else
        error( "radii soft tag: ", sstring.c_str(), 
               " not an option: must be either \"parameter-file\" or \"pqrxml-file\"");
    }

    if (pinfo == NULL && (hard_radii_from_param_file || soft_radii_from_param_file))
      error( "initializing molecule: radii tag: must have parameter file");
        
  }
}
