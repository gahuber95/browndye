#ifndef __COL_INTERFACE_GEN_HH__
#define __COL_INTERFACE_GEN_HH__

#include "vector.hh"
#include "partition_contents_ext.hh"

const unsigned int max_per_cell = 8;

template< class Interface>
class Col_Interface_Gen{
public:
  typedef typename Interface::Atom Atom;
  typedef Vector< Atom> Atoms;
  typedef Atoms Container;
  typedef typename Vector< Atom>::iterator Ref;
  typedef typename Interface::Refs Refs;
  typedef typename Interface::Transform Transform;
  typedef typename Atoms::size_type size_type;
  typedef typename Atoms::difference_type difference_type;

  static
  void copy_references( Atoms& atoms, Refs& arefs){
    const size_type n = atoms.size();
    arefs.reserve( n);
    for( size_type i=0; i<n; i++){
      auto& atom = atoms[i];
      if (atom.interaction_radius() > 0.0)
        arefs.push_back( atoms.begin() + i);
    }
  }

  static
  void copy_references( const Atoms& atoms, const Refs& arefs0,
                        Refs& arefs1){
    const size_type n = arefs0.size();
    arefs1.resize( n);
    for( size_type i=0; i<n; i++)
      arefs1[i] = arefs0[i];
  }

  static
  size_type size( const Atoms& atoms){
    return atoms.size();
  }

  static
  size_type size( const Atoms& atoms, const Refs& refs){
    return refs.size();
  }

  static
  void get_position( const Atoms&, const Refs& refs, size_type i, Vec3< Length>& pos){
    refs[i]->get_position( pos);
  }

  static
  Length radius( const Atoms&, const Refs& refs, size_type i){
    return refs[i]->interaction_radius();
  }

  template< class F>
  static
  void partition_contents( const Atoms&, const Refs& refs, const F& f,
                           Refs& refs0, Refs& refs1){
    partition_contents_ext< F, Interface>( refs, f, refs0, refs1);
  }

  static
  void empty_container( Atoms&, Refs& refs){
    refs.resize( 0);
  }

  static
  void swap( Atoms&, Refs& refs, size_type i, size_type j){
    typename Refs::value_type ref = refs[i];
    refs[i] = refs[j];
    refs[j] = ref;
  }
};

#endif
