/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
Estimates the distance between two ellipsoids.  The "distances" 
arguments are the distances of the surface from the center along
the corresponding principal axis.



*/

#ifndef _ELLIPSOID_DISTANCES_HH__
#define _ELLIPSOID_DISTANCES_HH__

#include "units.hh"

Length ellipsoid_distance( const SVec< Vec3<double>, 3>& axes0, 
                           const Vec3< Length>& distances0, 
                           const Vec3< Length>& center0,
                           const SVec< Vec3<double>, 3>& axes1,
                           const Vec3< Length>& distances1,  
                           const Vec3< Length>& center1);

#endif
