/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/



/*
Generic code for a Cartesian-based multipole implementor.  See
doc/multipole.html for details.

Interface:

typedef size_type

unsigned int Interface::maximum_multipole_order( const Container&, const Refs&);

template< class Function>
void Interface::apply_multipole_function( const Container&, const Refs&, Function& f, 
                                       double* r, double* mpole);

f has operator() overloaded: ( const unsigned int order)

Before each time f is called, the user must place the
object position in 'r' and its multipole in 'mpole'.
Then, f is called with the multipole order placed in the
argument 'order'.

template< class Function>
void Interface::apply_potential( Container&, Refs&, Function& f)

f has operator() overloaded: double ( const double* r)

The position is placed in r, and the potential is returned.

template< class Function>
void Interface::apply_field( Container&, Refs&, Function& f)

f has operator() overloaded: void ( const double* r, double* field)  

void Interface::process_bodies( Container&, Refs&)

void Interface::process_bodies( Container&, Refs&, Refs&)


void Interface::split_container( Container&, Refs&, unsigned int dir,
                                 Refs* const cube[][2][2])


void Interface::empty_references( Container&, Refs&);

void Interface::copy_references( Container&, Refs&);
void Interface::copy_references( Container&, Refs&, Refs&);

size_type Interface::size( Container&,const Refs&);
size_type Interface::n_sources( Container&,const Refs&);
size_type Interface::n_receivers( Container&,const Refs&);


void Interface::get_bounds( Container&, const Refs&,  
                         double* low, double* high)


Must provide derivatives up to "order"
void Interface::get_r_derivatives( Container& unsigned int order, double r, double* derivs)


double Interface::near_potential( Container&, Refs&, const double* r);

void Interface::get_near_field( Container&, Refs&, const double* r,
                             double* F);

*/

#include "small_vector.hh"
#include "vector.hh"

namespace Cartesian_Multipole{

static const unsigned int X = 0;
static const unsigned int Y = 1;
static const unsigned int Z = 2;

enum Contained_Type {Sources, Receivers, Both, None};

inline
void zero( Vector< double>& v){
  typedef Vector< double>::size_type size_type;
  for ( size_type i = 0; i < v.size(); i++)
    v[i] = 0.0;
}

typedef Vector< unsigned int> Ivec1;
typedef Vector< Ivec1> Ivec2;
typedef Vector< Ivec2> Ivec3;


class Index3{
public:
  Index3(){
    data[X] = 0;
    data[Y] = 0;
    data[Z] = 0;
  }

  unsigned int operator[]( unsigned int i) const{
    return data[i];
  }

  unsigned int& operator[]( unsigned int i){
    return data[i];
  }

private:
  SVec< unsigned int, 3> data;
  //unsigned int data[3];
};

class Deriv_Term{
public:
  unsigned int f_deriv;
  unsigned int r_power;
  unsigned int xyz_power;
  int scale;

  Deriv_Term(){
    f_deriv = 0;
    r_power = 0;
    xyz_power = 0;
    scale = 0;
  }
};

unsigned int n_indices( unsigned int order);

class Multipole_Info{

public:
  Multipole_Info( unsigned int _order);
  
  void generate_scaled_rfactors( const Vec3< double>& r, 
                                 Vector< double>& factors) const;

  void generate_rfactors( const Vec3< double>& r, 
                          Vector< double>& factors) const;

  void add_shifted_polynomial( unsigned int lorder, const Vector< double>& cps,
                               const Vector< double>& vs0,
                               Vector< double>& vs) const;
  
  void add_taylor_expansion( const Vector< double>& qs,
                             const Vector< double>& ds,
                             Vector< double>& vs) const;

  void add_taylor_expansion( unsigned int lorder,  
                             const Vector< double>& qs,
                             const Vector< double>& ds,
                             Vector< double>& vs) const;
  
  template< class Moments>
  void add_shifted_multipoles( const unsigned int lorder,
                               const Vector< double>& cps,
                               const Moments& qs0,
                               Vector< double>& qs) const;

  void generate_greens_derivatives( const Vector< double>& rfactors,
                                    const Vector< double>& fderivs,
                                    Vector< double>& derivs
                                    ) const;

  //void get_indices( const unsigned int i, unsigned int* k) const;

  unsigned int number_of_indices() const{
    return ni;
  }
  
  void print_info( FILE* fp) const;

  // default 20
  void set_max_number_per_cell( unsigned int n){
    n_per_cell = n;
  }

  unsigned int max_number_per_cell() const{
    return n_per_cell;
  }

  // diameter to center-to-center distance
  // default 
  void set_distance_ratio( double r){
    ratio = r;
  }

  double distance_ratio() const{
    return ratio;
  }

  void generate_reversed_cs( const Vector< double>& cs0,
                             Vector< double>& cs) const;

  unsigned int multipole_order() const{
    return order;
  }

 static double factorials[];

private:
  void generate_indices();
  void generate_responsibilities();
  void setup_inv_indices();
  void generate_convsum_array();
  void generate_trifactorials();
  void generate_minus_one_powers();
  void generate_derivative_coefficients();      

  const unsigned int order;
  const unsigned int ni;
  Vector< Index3> indices;
  Vector< Index3> resps;
  Ivec3 inv_indices;
  Vector< double> trifacts;
  Ivec2 convsum;
  Vector< Vector< Deriv_Term> > deriv_coefs;
  Vector< double> m1powers;

  unsigned int n_per_cell;
  double ratio;  

 
};

template< class Moments>
void Multipole_Info::add_shifted_multipoles( const unsigned int lorder,
                                             const Vector< double>& cps,
                                             const Moments& qs0,
                                             Vector< double>& qs) const{
   
  const unsigned int ni = n_indices( lorder);

  for (unsigned int m = 0; m < ni; m++){
    //FOR( m, 0, ni){
    const Ivec1& cvsm = convsum[m];
    const double q0 = qs0[ m];
    for (unsigned int p = 0; p < cvsm.size(); p++){
      //FOR( p, 0, cvsm.size()){
      qs[ cvsm[p]] += q0*cps[p];  
    }
  }
}



template< class Interface>
class Far_Nebor_Interaction_Info{
public:
  typedef Vector< double> DS;
  //typedef Vector< Vector< Vector< DS> > > Cube;
  typedef SVec< SVec< SVec< DS, 7>, 7>, 7> Cube;

  Vector< Cube> ds_cubes;
  
  void set_up( const Multipole_Info& info, 
               typename Interface::Container& cont, 
               unsigned int n_levels, double toph){

    // later change to avoid recalculating with each setup
    if (n_levels > 2){
      ds_cubes.resize( n_levels-2);

      double h = toph/4;
      for (unsigned int ilevel = 2; ilevel < n_levels; ++ilevel){
        resize_ds_cube( ilevel);
        Cube& ds_cube = ds_cubes[ ilevel-2];

        //double diff[3];
        Vec3< double> diff;
        for (int ix = -3; ix <= 3; ix++){
          diff[X] = ix*h;
          for (int iy = -3; iy <= 3; iy++){
            diff[Y] = iy*h;
            for (int iz = -3; iz <= 3; iz++){
              diff[Z] = iz*h;

              if (ix != 0 || iy != 0 || iz != 0){
                double r = sqrt( sq( diff[0]) + sq( diff[1]) + sq( diff[2]));
                // double r = L::norm( diff);

                Vector< double> rs;
                info.generate_rfactors( diff, rs);


                Vector< double> fderivs( info.multipole_order()+1);
                //double* fderivs_data = &(fderivs[0]);
                
                Interface::get_r_derivatives( cont, info.multipole_order(), 
                                              r, fderivs);
                
                DS& ds = ds_cube[ix+3][iy+3][iz+3];
                ds.resize( info.number_of_indices());
                info.generate_greens_derivatives( rs, fderivs, ds);

              }
            }
          }
        }       
        
        h /= 2;
      }
    }
  }

  void resize_ds_cube( unsigned int ilevel){
    /*
    Cube& cube = ds_cubes[ ilevel-2];
    cube.resize( 7);
    for (unsigned int i = 0; i < 7; i++){
      cube[i].resize( 7);
      for (unsigned int j = 0; j < 7; j++)
        cube[i][j].resize( 7);
    }
    */
  }
  
  static double sq( double x){
    return x*x;
  }

};


template< class T>
class Cube_2x2{
public:
  const T& operator()( unsigned int ix, unsigned int iy, unsigned int iz) const{
    check( ix,iy,iz);
    return data[ix][iy][iz];
  }

  T& operator()( unsigned int ix, unsigned int iy, unsigned int iz){
    check( ix,iy,iz);
    return data[ix][iy][iz];
  }

private:
  void check( unsigned int ix, unsigned int iy, unsigned int iz) const{
#ifdef DEBUG
    if (ix > 2 || iy > 2 || iz > 2){
      fprintf( stderr, "Cube_2x2 out of bounds %d %d %d\n", ix, iy, iz);
      exit(1);
    }
#endif
  }

  T data[2][2][2];
};


template< class Interface>
class Calculator;

//**************************************************************
template< class Interface>
class Cell{
public:
  typedef Cell< Interface> MC;
  typedef Multipole_Info MI;
  typedef typename Interface::Container Cr;
  typedef Far_Nebor_Interaction_Info< Interface> Fnii;

  Cell( const MI&);

  void create_top_cell( const MI&, Cr& contents, double padding);
  void compute_expansion( const MI& info, const Fnii&, Cr& conr);
  void compute_interactions_with_potential( const MI&, Cr&);
  void compute_interactions_with_field( const MI&, Cr&);
  void reload_contents( Cr&);
  void even_bottoms( const MI&, Cr& contents);
  MC* neighbor( int, int, int);

  ~Cell();

  void print( FILE*) const;

private:
  typedef typename Interface::Refs Refs;
  typedef typename Interface::Container Container;
  friend class Calculator< Interface>;

  void split_cells( const MI&, Cr&);
  void split_this_cell( const MI&, Cr&);
  void compute_center_and_diameter();
  static void link_cousins( const MI& info, MC& cell0, MC& cell1);
  void link_brothers( const MI& info);

  void do_sideways_sweep( const MI& info, const Fnii&);
  void do_upward_sweep( const MI& info, Cr&);

  void split_cells( const MI& info);

  void do_downward_sweep( const MI& info);
  void even_bottoms_inner( const MI&, Cr& contents, bool&);

  bool is_bottom() const{
    bool res = true;
    for (unsigned int ix = 0; ix < 2; ix++)
      for (unsigned int iy = 0; iy < 2; iy++)
        for (unsigned int iz = 0; iz < 2; iz++)
          if (children(ix,iy,iz) != NULL){
            res = false;
            goto loopend;
          }
  loopend:
    return res;    
  }

  void reload_cells( Cr&);

  double potential_at_point( const Multipole_Info& info,
                             Container& conr,
                             Vector< double>& rs,
                             Vector< double>& fderivs,
                             Vector< double>& ds,
                             Vector< double>& vs,
                             const double* r) const;

  void get_field_at_point( const Multipole_Info& info,
                           Container& conr,
                           Vector< double>& rs,
                           Vector< double>& fderivs,
                           Vector< double>& ds,
                           Vector< double>& vs,
                           const double* r,
                           double* F) const;

  unsigned int depth() const;


  void apply_with_nephews( Container& conr, 
                           Cell< Interface>* cousin);
    
  void set_contained_type( Container& conr);

  bool has_receivers() const{
    return contained == Receivers || contained == Both;
  }

  bool has_sources() const{
    return contained == Sources || contained == Both;
  }

  bool do_pair( const Cell< Interface>* cell1) const;

  void zero_out_sums();

  struct Far_Nebor_Info{
    Vector< double> ds;
    MC* nebor;

    Far_Nebor_Info(){
      nebor = NULL;
    }
  };

  //*********************************
  Refs contents;

  //MC* children[2][2][2];
  Cube_2x2< MC*> children;

  Vector< double> qs;
  Vector< double> vs;
  Vector< double> cps; // from child to parent

  //double center[3];
  Vec3< double> center;
  double length;

  int idx,idy,idz;
  unsigned int level; // 0 is top
  MC* parent;
  Contained_Type contained;

  //*********************************
  struct Particle_Multipole_Getter{
    Vector< double> cps;
    
    MC* cell;
    const Multipole_Info* info;
    //double r[3];
    Vec3< double> r;

    Particle_Multipole_Getter(){
      cell = NULL;
      info = NULL;
      double nan = std::numeric_limits<double>::quiet_NaN();
      for( int k = 0; k < 3; k++)
        r[k] = nan;
    }

    template< class L3, class Moments>
    void operator()( const unsigned int order, const L3& pos, 
                     const Moments& moments){
   
      for( int k = 0; k < 3; k++)
        r[k] = pos[k] - cell->center[k];

      info->generate_scaled_rfactors( r, cps);
      info->add_shifted_multipoles( order, cps, moments, cell->qs);
    }
  };


  struct Taylor_Applier{
    const MC& cell;
    const MI& info;
    Vector< double> cps;
    Vector< double> vs;
    //double dr[3];
    Vec3< double> dr;

    Taylor_Applier( const MI& _info, const MC& _cell): 
      cell(_cell), info(_info), cps( _info.number_of_indices()), vs( 4){}
    
    
    template< class L3>
    void Do( unsigned int order, const L3& r){
      for( int k = 0; k < 3; k++)  
        dr[k] = r[k] - cell.center[k];
      for (int i = 0; i < 4; i++)
        vs[i] = 0.0;
      
      info.generate_scaled_rfactors( dr, cps);
      info.add_shifted_polynomial( order, cps, cell.vs, vs);
    }
  };
  
  struct Field_Applier: public Taylor_Applier{
    Field_Applier( const MI& info, const MC& cell):
      Taylor_Applier( info, cell){}

    void operator()( const double* r, double* F){
      this->Do( 1, r);
      for( int k = 0; k < 3; k++)  
        F[k] = this->vs[k+1];
    }
  };
  
  struct Potential_Applier: public Taylor_Applier{

    Potential_Applier( const MI& info, const MC& cell):
      Taylor_Applier( info, cell){}

    template< class L3>
    double operator()( const L3& r){
      this->Do( 0, r);
      return this->vs[0];
    }
  };


  template< class T>
  static
  T max( T x, T y){
    return x > y ? x : y;
  }
 


};

//*************************************************************************
// depth = 1 for bottom

template< class Interface>
unsigned int Cell< Interface>::depth() const{

  unsigned int ndep = 1;
  for (unsigned int ix = 0; ix < 2; ix++)
    for (unsigned int iy = 0; iy < 2; iy++)
      for (unsigned int iz = 0; iz < 2; iz++){
        //MC* child = children[ix][iy][iz];
        MC* child = children( ix,iy,iz);
        if (child != NULL)
          ndep = max( ndep, 1 + child->depth());
      }         
  return ndep;
}


template< class Interface>
Cell< Interface>::~Cell(){
    for (unsigned int ix = 0; ix < 2; ix++)
      for (unsigned int iy = 0; iy < 2; iy++)
        for (unsigned int iz = 0; iz < 2; iz++){
          //MC* child = children[ix][iy][iz];
          MC* child = children( ix,iy,iz);
          if (child != NULL)
            delete child;
        }
}
    

template< class Interface>
void Cell< Interface>::
compute_expansion( const Multipole_Info& info,
                   const Far_Nebor_Interaction_Info< Interface>& fn_info,
                   typename Interface::Container& conr){

  this->zero_out_sums();
  this->do_upward_sweep( info, conr);
  this->do_sideways_sweep( info, fn_info);
  this->do_downward_sweep( info);
}


template< class Interface>
Cell< Interface>* Cell< Interface>::neighbor( int kx, int ky, int kz){
  if (kx == 0 && ky == 0 && kz == 0)
    return this;

  else if (level == 0)
    return NULL;

  else{
    int jdx = idx + kx;
    int jdy = idy + ky;
    int jdz = idz + kz;

    int idx1 = jdx >> 1; 
    int idy1 = jdy >> 1; 
    int idz1 = jdz >> 1;

    int idx0 = parent->idx;
    int idy0 = parent->idy;
    int idz0 = parent->idz;
    
    MC* uncle = parent->neighbor( idx1 - idx0, idy1 - idy0, idz1 - idz0);
    if (uncle != NULL){
      
      int kx1 = jdx - (idx1 << 1); 
      int ky1 = jdy - (idy1 << 1); 
      int kz1 = jdz - (idz1 << 1); 
      
      //return uncle->children[kx1][ky1][kz1];
      return uncle->children(kx1,ky1,kz1);
    }
    else
      return NULL;   
  }
}


template< class Interface>
void Cell< Interface>::
apply_with_nephews( typename Interface::Container& conr, 
                    Cell< Interface>* cousin){

  if (cousin->is_bottom()){
    Interface::process_bodies( conr, contents, cousin->contents);
  }
  else{
    for (unsigned int ix = 0; ix < 2; ix++)
      for (unsigned int iy = 0; iy < 2; iy++)
        for (unsigned int iz = 0; iz < 2; iz++){
          MC* child = cousin->children(ix,iy,iz);
          if (child != NULL){
            apply_with_nephews( conr, child);
          }
        }
  }
}

template< class Interface>
void Cell< Interface>::do_sideways_sweep( const Multipole_Info& info,
                                    const Far_Nebor_Interaction_Info< Interface>& fn_info){

  if (has_sources()){
    if (parent != NULL){
      for (int jx = -1; jx <= 1; jx++)
        for (int jy = -1; jy <= 1; jy++)
          for (int jz = -1; jz <= 1; jz++)       
            if (jx != 0 || jy != 0 || jz != 0){
              MC* uncle = parent->neighbor( jx,jy,jz);
              if (uncle != NULL){
                for( unsigned int kx = 0; kx <= 1; kx++)
                  for( unsigned int ky = 0; ky <= 1; ky++)
                    for( unsigned int kz = 0; kz <= 1; kz++){
                      MC* cousin = uncle->children(kx,ky,kz);
                      if (cousin != NULL){                
                        int dx = cousin->idx - idx;
                        int dy = cousin->idy - idy;
                        int dz = cousin->idz - idz;
                        
                        if (abs( dx) > 1 || abs( dy) > 1 ||
                            abs( dz) > 1){
                          
                          if (cousin->has_receivers()){

                            const Vector< double>& ds = 
                              fn_info.ds_cubes[ level-2][dx+3][dy+3][dz+3];
                            
                            info.add_taylor_expansion( qs, ds, cousin->vs);
                          }
                        }               
                        
                      }
                    }
              }
              
            }
    }    
    
    for (unsigned int ix = 0; ix < 2; ix++)
      for (unsigned int iy = 0; iy < 2; iy++)
        for (unsigned int iz = 0; iz < 2; iz++){
          MC* child = children(ix,iy,iz);
          if (child != NULL)
            child->do_sideways_sweep( info, fn_info);
          
        }
  }
}



template< class Interface>
void Cell< Interface>::
compute_interactions_with_potential( const Multipole_Info& info,
                                     typename Interface::Container& conr){

  if (is_bottom()){
    Potential_Applier pa( info, *this);
    Interface::apply_potential( conr, contents, pa);
    Interface::process_bodies( conr, contents);    

    for (int jx = -1; jx <= 1; jx++)
      for (int jy = -1; jy <= 1; jy++)
        for (int jz = -1; jz <= 1; jz++){
          if (jx != 0 || jy != 0 || jz != 0){

            MC* nebor = neighbor( jx,jy,jz);
            if (nebor != NULL){       
              if (nebor->is_bottom()){
                bool doi;
                int jxyz = jx + jy + jz;
                if (jxyz > 0)
                  doi = true;
                else if (jxyz < 0)
                  doi = false;
                else {
                  int jxy = jx + jy;
                  if (jxy > 0)
                    doi = true;
                  else if (jxy < 0)
                    doi = false;
                  else
                    doi = (jx > 0);
                }

                if (doi){
                  Interface::process_bodies( conr, contents, nebor->contents);
                } 
              }
              else { // not bottome
                apply_with_nephews( conr, nebor);
              }
            }
          }
        }
  }
  else { // not bottom
    for (unsigned int ix = 0; ix < 2; ix++)
      for (unsigned int iy = 0; iy < 2; iy++)
        for (unsigned int iz = 0; iz < 2; iz++){
          MC* child = children(ix,iy,iz);
          if (child != NULL){
            child->compute_interactions_with_potential( info, conr);
          }
        }
  }
}

template< class Interface>
bool Cell< Interface>::do_pair(  const Cell< Interface>* cell1) const{
  
  bool r0 = has_receivers();
  bool r1 = cell1->has_receivers();
  bool s0 = has_sources();
  bool s1 = cell1->has_sources();
  return ((r0 && s1) || (r1 && s0));
}
  

  // genericize later
template< class Interface>
void Cell< Interface>::
compute_interactions_with_field( const Multipole_Info& info,
                                 typename Interface::Container& conr){

  if (is_bottom()){
    
    if (has_receivers()){
      Field_Applier fa( info, *this);
      Interface::apply_field( conr, contents, fa);
      if (has_sources())
        Interface::process_bodies( conr, contents);    
    }

    for (int jx = -1; jx <= 1; jx++)
      for (int jy = -1; jy <= 1; jy++)
        for (int jz = -1; jz <= 1; jz++){
          if (jx != 0 || jy != 0 || jz != 0){
            
            MC* nebor = neighbor( jx,jy,jz);
            if (nebor != NULL){       
              if (nebor->is_bottom()){
                bool doi;
                int jxyz = jx + jy + jz;
                if (jxyz > 0)
                  doi = true;
                else if (jxyz < 0)
                  doi = false;
                else {
                  int jxy = jx + jy;
                  if (jxy > 0)
                    doi = true;
                  else if (jxy < 0)
                    doi = false;
                  else
                    doi = (jx > 0);
                }
                
                
                if (doi){
                  if (do_pair( nebor))
                    Interface::process_bodies( conr, contents, nebor->contents);
                } 
              }
              else { // not bottom
                if (do_pair( nebor))
                  apply_with_nephews( conr, nebor);
              }
            }
          }
        }
  }
  else { // not bottom
    for (unsigned int ix = 0; ix < 2; ix++)
      for (unsigned int iy = 0; iy < 2; iy++)
        for (unsigned int iz = 0; iz < 2; iz++){
          MC* child = children(ix,iy,iz);
          if (child != NULL){
            child->compute_interactions_with_field( info, conr);
          }
        }
  }
}

template< class Interface>
void Cell< Interface>::
do_downward_sweep( const Multipole_Info& info){
  
  if (has_receivers()){

    if (!is_bottom()){
      for (unsigned int ix = 0; ix < 2; ix++)
        for (unsigned int iy = 0; iy < 2; iy++)
          for (unsigned int iz = 0; iz < 2; iz++){
            MC* child = children(ix,iy,iz);
            if (child != NULL){
              info.add_shifted_polynomial( info.multipole_order(),
                                           child->cps, vs, child->vs);
              child->do_downward_sweep( info);
            }      
          }
    }
  }
}


template< class Interface>
void Cell< Interface>::
do_upward_sweep( const Multipole_Info& info, 
                 typename Interface::Container& conr){

  //  zero( vs);
  // zero( qs);
  
  if (has_sources()){

    if (is_bottom()){
      Particle_Multipole_Getter pmg;
      pmg.cell = this;
      pmg.info = &info;
      pmg.cps.resize( info.number_of_indices());   
      Interface::apply_multipole_function( conr, contents, pmg);
    }
    
    else{
      for (unsigned int ix = 0; ix < 2; ix++)
        for (unsigned int iy = 0; iy < 2; iy++)
          for (unsigned int iz = 0; iz < 2; iz++){
            MC* child = children(ix,iy,iz);
            if (child != NULL){
              child->do_upward_sweep( info, conr);
              info.add_shifted_multipoles( info.multipole_order(), 
                                           child->cps, 
                                           child->qs, 
                                           qs);     
            }
          }
    }
  }
}

unsigned int ncell();

// constructor
template< class Interface>
Cell< Interface>::Cell( const Multipole_Info& info){

  contained = None;
  parent = NULL;
  level = 0;
  idx = idy = idz = 0;

  for (unsigned int ix = 0; ix < 2; ix++)
    for (unsigned int iy = 0; iy < 2; iy++)
      for (unsigned int iz = 0; iz < 2; iz++)
        children(ix,iy,iz) = NULL;

  const double nan = std::numeric_limits<double>::quiet_NaN();

  for( int k = 0; k < 3; k++)
    center[k] = nan;
  
  length = nan;
  qs.resize( info.number_of_indices());
  vs.resize( info.number_of_indices());
  parent = NULL;
}

template< class Interface>
void Cell< Interface>::
split_this_cell( const Multipole_Info& info,
                 typename Interface::Container& conr){
  
  
  for (unsigned int ix = 0; ix < 2; ix++)
    for (unsigned int iy = 0; iy < 2; iy++)
      for (unsigned int iz = 0; iz < 2; iz++)
        children(ix,iy,iz) = new MC( info);
  
  //Refs* refs_cube[2][2][2];
  Cube_2x2< Refs*> refs_cube;
  for (unsigned int ix = 0; ix < 2; ix++)
    for (unsigned int iy = 0; iy < 2; iy++)
      for (unsigned int iz = 0; iz < 2; iz++)
        refs_cube(ix,iy,iz) = &(children(ix,iy,iz)->contents);
  
  Interface::split_container( conr, contents, center, refs_cube);
  
  for (int ix = 0; ix < 2; ix++)
    for (int iy = 0; iy < 2; iy++)
      for (int iz = 0; iz < 2; iz++){
        MC* child = children(ix,iy,iz);
        if (Interface::size( conr, child->contents) == 0){
          delete child;
          children(ix,iy,iz) = NULL;
        }
        else{
          child->idx = (idx << 1) + ix;
          child->idy = (idy << 1) + iy;
          child->idz = (idz << 1) + iz;
          child->level = level + 1;
          child->length = length/2;
          double h = length/4;
          child->center[X] = center[X] + h*(2*ix - 1); 
          child->center[Y] = center[Y] + h*(2*iy - 1); 
          child->center[Z] = center[Z] + h*(2*iz - 1);
          child->parent = this;
        }
        
      }
  
  
  for (unsigned int ix = 0; ix < 2; ix++)
    for (unsigned int iy = 0; iy < 2; iy++)
      for (unsigned int iz = 0; iz < 2; iz++){
        MC* child = children(ix,iy,iz);
        if (child != NULL){
          //double c[3];
          Vec3< double> c;
          c[0] = child->center[0] - center[0];
          c[1] = child->center[1] - center[1];
          c[2] = child->center[2] - center[2];
          info.generate_scaled_rfactors( c, child->cps);
        }
      }  
}
  
template< class Interface>
void Cell< Interface>::
split_cells( const Multipole_Info& info,
             typename Interface::Container& conr){

  if (Interface::size( conr, contents) > info.max_number_per_cell()){ 
    split_this_cell( info, conr);
 
    for (unsigned int ix = 0; ix < 2; ix++)
      for (unsigned int iy = 0; iy < 2; iy++)
        for (unsigned int iz = 0; iz < 2; iz++){
          MC* child = children(ix,iy,iz);
          if (child != NULL)
            child->split_cells( info, conr);
        }
   
    Interface::empty_references( conr, contents);


  }
}

template< class Interface>
void Cell< Interface>::
create_top_cell( const Multipole_Info& info, 
                 typename Interface::Container& conr,
                 double padding){
  //double low[3], high[3];
  Vec3< double> low, high;
  Interface::get_bounds( conr, low, high);
  
  for( int k = 0; k < 3; k++)
    center[k] = 0.5*( low[k] + high[k]);

  length = max( max( high[0] - low[0], high[1] - low[1]), high[2] - low[2]) + 2*padding;

  Interface::copy_references( conr, contents);

  bool is_empty = (Interface::size( conr, contents) == 0);
  
  if (!is_empty &&
      (Interface::size( conr, contents) > info.max_number_per_cell())){ 

    split_cells( info, conr);
  } 
}


template< class Interface>
void Cell< Interface>::even_bottoms( 
                                    const Multipole_Info& info, 
                                    typename Interface::Container& conr){
  bool changed;
  do{
    changed = false;
    even_bottoms_inner( info, conr, changed);
  }
  while (changed);
}


template< class Interface>
void Cell< Interface>::even_bottoms_inner( 
                           const Multipole_Info& info, 
                           typename Interface::Container& conr,
                           bool& changed){

  if (is_bottom()){

    for (int kx = -1; kx <= 1; kx++)
      for (int ky = -1; ky <= 1; ky++)
        for (int kz = -1; kz <= 1; kz++)
          if (kx != 0 || ky != 0 || kz != 0){
            MC* nebor = neighbor( kx, ky, kz);
            if (nebor != NULL && nebor->depth() > 2){
                
              changed = true;
              split_this_cell( info, conr);
              
              for (unsigned int ix = 0; ix < 2; ix++)
                for (unsigned int iy = 0; iy < 2; iy++)
                  for (unsigned int iz = 0; iz < 2; iz++){
                    MC* child = children(ix,iy,iz);
                    if (child != NULL)
                      child->even_bottoms_inner( info, conr, changed);
                    
                  }
            }
          }
  }

  else{ // not bottom
    for (unsigned int ix = 0; ix < 2; ix++)
      for (unsigned int iy = 0; iy < 2; iy++)
        for (unsigned int iz = 0; iz < 2; iz++){
          MC* child = children(ix,iy,iz);
          if (child != NULL)
            child->even_bottoms_inner( info, conr, changed);
        }
  }   
        
}  
          
template< class Interface>
void Cell< Interface>::set_contained_type( typename Interface::Container& conr){
  bool has_sources, has_receivers;

  if (is_bottom()){
    has_receivers = (Interface::n_receivers( conr, contents) > 0);
    has_sources = (Interface::n_sources( conr, contents) > 0);
      
  }
  else{
    has_receivers = false;
    has_sources = false;

    for (unsigned int ix = 0; ix < 2; ix++)
      for (unsigned int iy = 0; iy < 2; iy++)
        for (unsigned int iz = 0; iz < 2; iz++){
          MC* child = children(ix,iy,iz);
          if (child != NULL){
            child->set_contained_type( conr);
            has_receivers |= child->has_receivers();
            has_sources |= child->has_sources();
          }    
        }
  }  
  
  if (has_receivers && has_sources) contained = Both;
  else if (has_receivers && !has_sources) contained = Receivers;
  else if (!has_receivers && has_sources) contained = Sources;
  else
    contained = None;
}

template< class Interface>
void Cell< Interface>::zero_out_sums(){
  zero( vs);
  zero( qs);

  for (unsigned int ix = 0; ix < 2; ix++)
    for (unsigned int iy = 0; iy < 2; iy++)
      for (unsigned int iz = 0; iz < 2; iz++){
        MC* child = children(ix,iy,iz);
        if (child != NULL){
          child->zero_out_sums();
        }
      }
}

//*******************************************************************
template< class Interface>
class Calculator{

public:
  typedef typename Interface::Container Container;
  typedef typename Interface::size_type size_type;

  Calculator( unsigned int order, Container& contents);

  void setup_cells();

  void compute_expansion();
  void compute_interactions_with_potential();
  void compute_interactions_with_field();

  // default 20
  void set_max_number_per_cell( size_type n){
    info.set_max_number_per_cell( (unsigned int)n);
  }

  size_type max_number_per_cell() const{
    return (size_type)(info.max_number_per_cell());
  }

  // diameter to (center-to-center distance) ratio
  // default 0.5
  void set_distance_ratio( double r){
    info.set_distance_ratio( r);
  }

  double distance_ratio() const{
    return info.distance_ratio();
  }

  void reload_contents();

  // default 0.0
  void set_padding( double p){
    padding = p;
  }

  template< class L3>
  double potential_at_point( const L3& r) const;

  template< class L3, class F3>
  void get_field_at_point( const L3& r, F3& F) const;

private:
  Far_Nebor_Interaction_Info< Interface> fn_info;
  Container& contents;
  Multipole_Info info;
  Cell< Interface> top_cell;
  double padding;
};



template< class Interface>
Calculator< Interface>::
Calculator( unsigned int order, typename Interface::Container& _contents):
  contents( _contents), info( order) , top_cell( info){
  padding = 0.0;
}

template< class Interface>
template< class L3>
double Calculator< Interface>::potential_at_point( const L3& r)const{

  const unsigned int ni = info.number_of_indices();
  Vector< double> fderivs( info.multipole_order()+1);
  Vector< double> rs( ni);
  Vector< double> ds( ni);
  Vector< double> vs(1);

  return top_cell.potential_at_point( info, contents, rs, fderivs, ds,vs, r);
}


template< class Interface>
template< class L3, class F3>
void Calculator< Interface>::
get_field_at_point( const L3& r, F3& F) const{

  const unsigned int ni = info.number_of_indices();
  Vector< double> fderivs( info.multipole_order()+1);
  Vector< double> rs( ni);
  Vector< double> ds( ni);
  Vector< double> vs(4);

  top_cell.get_field_at_point( info, contents, rs, fderivs, ds,vs, r, F);
}


template< class Interface>
void Calculator< Interface>::setup_cells(){
  top_cell.create_top_cell( info, contents, padding);
  fn_info.set_up( info, contents, top_cell.depth(), top_cell.length);
  top_cell.even_bottoms( info, contents);
  top_cell.set_contained_type( contents);
}

template< class Interface>
void Calculator< Interface>::
reload_contents(){
  top_cell.reload_contents( contents);
}


template< class Interface>
void Calculator< Interface>::compute_interactions_with_potential(){
  top_cell.compute_interactions_with_potential( info, contents);
}

template< class Interface>
void Calculator< Interface>::compute_interactions_with_field(){
  top_cell.compute_interactions_with_field( info, contents);
}

template< class Interface>
void Calculator< Interface>::compute_expansion(){
  top_cell.compute_expansion( info, fn_info, contents);
}

}
