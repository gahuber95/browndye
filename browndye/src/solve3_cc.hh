/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
template< class Um, class Ub, class Ux>
void Solve3::solve( Mat3< Um>& m, Vec3< Ub>& b, Vec3< Ux>& x)

Solves a generic 3x3 system using LU decomposition and back-substitution.
*/

#ifndef __SOLVE3_HH__
#define __SOLVE3_HH__

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "units.hh"
#include "small_vector.hh"

namespace Solve3{

template< class U>
void swap( Vec3< U>& a, unsigned int i, Vec3< U>& b, unsigned int j){
  U temp = a[i];
  a[i] = b[j];
  b[j] = temp;
}

template< class U>
void swapm( Mat3< U>& a, unsigned int i, unsigned int j, 
            Mat3< U>& b, unsigned int m, unsigned int n){

  U temp = a[i][j];
  a[i][j] = b[m][n];
  b[m][n] = temp;
}

template< class Um, class Ub, class Ux>
void solve( Mat3< Um>& m, Vec3< Ub>& b, Vec3< Ux>& x){

  if 
    ((fabs( m[1][0]) > fabs( m[0][0])) && 
     (fabs( m[1][0]) >= fabs( m[2][0]))) 
    {
    for (unsigned int  k=0; k<=2; k++){ 
      swapm( m, 0, k, m, 1, k);
    }
    swap( b, 0, b, 1);
    }
  else if 
    ((fabs( m[2][0]) > fabs( m[0][0])) && 
     (fabs( m[2][0]) >= fabs( m[1][0])))
    {
      for (unsigned int  k = 0; k<=2; k++){ 
        swapm( m, 0, k, m, 2, k);
      }
      swap( b, 0, b, 2);
    }

  double a01 = m[1][0]/m[0][0] ;
  
  for (unsigned int k=0; k<=2; k++){ 
    m[1][k] = m[1][k] - a01*m[0][k];
  }
  b[1] = b[1] -  a01*b[0];
  
  double a02 = m[2][0]/m[0][0] ;

  for (unsigned int k=0; k<=2; k++){ 
    m[2][k] = m[2][k] - a02*m[0][k];
  }
  
  b[2] = b[2] - a02*b[0];
  
  if 
    (fabs( m[2][1]) > fabs( m[1][1])) 
    {
      for (unsigned int  k=1; k<=2; k++){ 
        swapm( m, 2, k, m, 1, k);
      }
      swap( b, 2, b, 1);
    }

  double a12 = m[2][1]/m[1][1] ;
  
  for (unsigned int k=1; k<=2; k++){
    m[2][k] = m[2][k] - a12*m[1][k];
  }
  b[2] = b[2] - a12*b[1];
  
  // backsubstitution 
  x[2] = b[2]/m[2][2];
  x[1] = (b[1] - m[1][2]*x[2])/m[1][1];
  x[0] = (b[0] - m[0][1]*x[1] - m[0][2]*x[2])/m[0][0];
}

}

#endif

//********************************************************************
// test code
/*
int main(){

  double sq2 = sqrt( 0.5);
  double q = 0.353553;
  
  double m0[3][3] = {{0.0, sq2, 1.0 },
                     {sq2, 0.0, sq2 },
                     {2*sq2, sq2, 0.0 }
  };
  
  double m[3][3] = {
    {0.0, sq2, 1.0 },
    {sq2, 0.0, sq2 },
    {2*sq2, sq2, 0.0 },
    
  };
  
  double b0[3] = {q,q,q};
  double b[3] =  {q,q,q};
  
  double x[3] = { 0.0, 0.0, 0.0 };
  
  solve3( m, b, x);
  
  printf( "%f %f %f\n", x[0], x[1], x[2]);
  printf( "\n");
  printf( "%f %f %f\n", b0[0], b0[1], b0[2]);
  
  printf( "%f %f %f\n", 
          m0[0][0]*x[0] + m0[0][1]*x[1] + m0[0][2]*x[2],
          m0[1][0]*x[0] + m0[1][1]*x[1] + m0[1][2]*x[2],
          m0[2][0]*x[0] + m0[2][1]*x[1] + m0[2][2]*x[2]);

  return 0;
}
  
*/
  

