/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
WE_Simulator reads in the data, carries out a weighted-ensemble
simulation, and outputs data.  Also takes care of the multithreading. 
*/


#ifndef __WE_SIMULATOR_HH__
#define __WE_SIMULATOR_HH__

#include "we_simulator_pre.hh"  

class WE_Simulator: public Simulator{
public:
  WE_Simulator();

  void initialize( const char* file);
  void run();
  void generate_bins();

  bool building_bins;

private:
  friend class WE_Interface;

  unsigned int n_copies, n_bin_copies;
  unsigned long int n_steps;
  unsigned int n_steps_per_output;
  unsigned int n_final_rxns;

  std::ofstream output;
  std::ofstream bin_ofstream;

  Vector< double> bin_partitions; // make sure 0 has 0.0

  unsigned long int istep;
  std::streampos stream_position;
  Vector< double> next_fluxes;

  unsigned long int state_number;

  Weighted_Ensemble_Dynamics::Multithreaded::Sampler< WE_Interface> sampler;
  Browndye_RNG rng;

  void get_bins( JAM_XML_Pull_Parser::Node_Ptr);
};

//**********************************************************
template< class Function> 
void WE_Interface::apply_to_bin_neighbors( WE_Simulator* sim, 
                                           unsigned int bin, 
                                           Function& f){
  const unsigned int n = sim->bin_partitions.size();
  
  if (bin == 0)
    f( 1);
  else if (bin == (n-1))
    f( n-2);
  else{
    f( bin-1);
    f( bin+1);
  } 
}

/*

Input:

information for Molecule_Pair

Bins
  # copies per bin
  number
  locations 

total number of steps
number of steps per output
output file

Output:

probability fluxes
*/



#endif
