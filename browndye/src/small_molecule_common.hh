#ifndef __SMALL_MOLECULE_COMMON_HH__
#define __SMALL_MOLECULE_COMMON_HH__

#include <string>
#include "units.hh"
#include "small_vector.hh"
#include "jam_xml_pull_parser.hh"
#include "molecule_common.hh"
#include "charged_blob_simple.hh"
#include "blob_interface.hh"
#include "col_interface_large.hh"
#include "v_field_interface.hh"
#include "born_field_interface.hh"

class Small_Molecule_Common: public Molecule_Common{
public:
  typedef std::string String;

  void initialize( JAM_XML_Pull_Parser::Node_Ptr); 
  void set_electric_chebybox( String file, const Vec3< Length>& offset);

  Charge charge() const;

private:
  friend class Back_Door;

  typedef Blob_Interface< Col_Interface_Large, 
                          V_Field_Interface> Q_Blob_Interface;

  typedef Blob_Interface< Col_Interface_Large,
                          Born_Field_Interface> Q2_Blob_Interface;


  std::unique_ptr< Charged_Blob::Blob< 4, Q_Blob_Interface> > q_blob; 
  std::unique_ptr< Charged_Blob::Blob< 4, Q2_Blob_Interface> > q2_blob;

  friend
  void add_forces_and_torques(
                              const Large_Molecule_Common& molc0,
                              Large_Molecule_State& mol0,
                              const Small_Molecule_Common& molc1,
                              Small_Molecule_Thread& molt1,
                              Small_Molecule_State& mol1,

                              bool& has_collision,
                              Length& violation,
                              Atom_Large_Ref& aref0,
                              Atom_Small_Ref& aref1
                              );

  friend
  void get_potential_energy(
                            const Large_Molecule_Common& molc0,
                            Large_Molecule_State& mol0,
                            const Small_Molecule_Common& molc1,
                            Small_Molecule_Thread& molt1,
                            Small_Molecule_State& mol1,
                            Energy& vnear, Energy& vcoul, Energy& vdesolv
                             );

  template< class Mol>
  friend
  void set_desolvation_chebybox( Mol& mol, std::string file, const Vec3< Length>& offset);
};




#endif
