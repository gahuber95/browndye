/*
 * mover_interface.hh
 *
 *  Created on: Sep 8, 2015
 *      Author: root
 */

#ifndef MOVER_INTERFACE_HH_
#define MOVER_INTERFACE_HH_

#include "small_vector.hh"

class Molecule_Pair;

class Mover_Interface{
public:
  typedef Molecule_Pair Spheres;

  static
  void put_positions_and_rotations( const Vec3< double>& rsc0,
                                    const Mat3< double>& rot0,
                                    const Vec3< double>& rsc1,
                                    const Mat3< double>& rot1,
                                    Spheres& spheres);


  static
  void get_positions_and_rotations( const Spheres& spheres,
                                    Vec3< double>& r0,
                                    Mat3< double>& rot0,
                                    Vec3< double>& r1,
                                    Mat3< double>& rot1
                                    );


  static
  void get_forces_and_torques( const Spheres& spheres,
                               Vec3< double>& f0,
                               Vec3< double>& t0,
                               Vec3< double>& f1,
                               Vec3< double>& t1);


  static
  double gap( const Spheres& spheres);
};

#endif /* MOVER_INTERFACE_HH_ */
