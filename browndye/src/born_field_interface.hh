#ifndef __BORN_FIELD_INTERFACE_HH__
#define __BORN_FIELD_INTERFACE_HH__

#include <string>
#include "units.hh"

class Born_Field_Interface{
public:
  typedef UProd< ::Charge, ::Charge>::Res Charge2;
  typedef UQuot< Energy, Charge2>::Res Potential;
  typedef Charge2 Charge;
  typedef UProd< Charge2, Charge2>::Res Charge4;
  typedef UQuot< Charge4, UProd< Length, Energy>::Res >::Res Permittivity;
  typedef UQuot< Potential, Length>::Res Potential_Gradient;
  typedef std::string String;
};

#endif
