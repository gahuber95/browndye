/*
 * stepper_interface.hh
 *
 *  Created on: Sep 8, 2015
 *      Author: root
 */

#ifndef STEPPER_INTERFACE_HH_
#define STEPPER_INTERFACE_HH_

#include "vector.hh"
#include "small_vector.hh"
#include "units.hh"
#include "browndye_rng.hh"

class Molecule_Pair;

class Stepper_Interface{
public:

  typedef Molecule_Pair System;
  typedef ::Length Length;
  typedef ::Time Time;
  typedef ::Energy Energy;
  typedef ::Charge Charge;
  typedef Vector< int>::size_type size_type;
  typedef size_type Reaction;
  typedef size_type Reaction_State;
  typedef Browndye_RNG Random_Number_Generator;
  typedef SVec< Sqrt_Time, 12> Sqrt_Time_Vector;

  static
  void set_fate_in_action( Molecule_Pair& pair);

  static
  void set_fate_final_rxn( Molecule_Pair& pair);

  static
  void set_fate_escaped( Molecule_Pair& pair);

  static
  bool are_building_bins( const Molecule_Pair& pair);

  static
  void get_next_completed_rxn( const Molecule_Pair& pair, bool& completed, Reaction& next);

  static
  Reaction_State state_after_rxn( const Molecule_Pair& pair, Reaction irxn);

  static
  void set_rxn_state( Molecule_Pair& pair, Reaction_State istate);

  static
  void set_last_rxn( Molecule_Pair& pair, Reaction irxn);

  static
  void set_had_rxn( Molecule_Pair& pair);

  static
  int n_rxns_from( const Molecule_Pair& pair, Reaction_State istate);

  static
  Length q_radius( const Molecule_Pair& pair);

  static
  Length b_radius( const Molecule_Pair& pair);

  static
  Length mol_mol_distance( const Molecule_Pair& pair);

  template< class U3, class V3>
  static
  void v3_copy_to( const U3& a, V3& b);

  template< class M3, class N3>
  static
  void m3_copy_to( const M3& a, N3& b);

  template< class Position>
  static
  void get_positions( const Molecule_Pair& pair,
                      Position& pos0, Position& pos1);

  template< class Position, class Rotation>
  static
  void get_positions_and_rotations( const Molecule_Pair& pair,
                                    Position& pos0, Rotation& rot0,
                                    Position& pos1, Rotation& rot1);

  template< class Position, class Rotation>
  static
  void set_positions_and_rotations( Molecule_Pair& pair,
                                    const Position& pos0, const Rotation& rot0,
                                    const Position& pos1, const Rotation& rot1);

  static
  bool has_collision( Molecule_Pair& pair);

  static
  void rescale_separation( Molecule_Pair& pair, double scale);

  static
  Energy kT( Molecule_Pair& pair);

  static
  Time natural_dt( const Molecule_Pair& pair);

  static
  void incr_time( Molecule_Pair& pair, Time dt);

  static
  Time min_dt( const Molecule_Pair& pair);

  static
  Diffusivity separation_diffusivity( const Molecule_Pair& pair);

  template< class Force_Vec>
  static
  void get_mol1_force( Molecule_Pair& pair, Force_Vec& force);

  static
  Random_Number_Generator& random_number_generator( Molecule_Pair& pair);

  static
  int dimension( const Molecule_Pair& pair);

  static
  void save( Molecule_Pair& pair);

  static
  void restore( Molecule_Pair& pair);

  static
  void step_forward( Molecule_Pair& pair, Time dt, const Sqrt_Time_Vector& dw, bool& must_backstep, bool& has_collision);

  static
  void inc_num_of_successive_collisions( Molecule_Pair& pair);

  static
  void zero_num_of_successive_collisions( Molecule_Pair& pair);

  static
  int num_of_successive_collisions( Molecule_Pair& pair);

  static
  int max_collision_rejects( Molecule_Pair& pair);

  static
  void back_molecules_away( Molecule_Pair& pair, bool& still_has_collision);

  static
  double uniform( Browndye_RNG& rng);

  static
  double gaussian( Browndye_RNG& rng);

  static
  Time last_dt( const Molecule_Pair& pair);

  static
  void set_last_dt( Molecule_Pair& pair, Time dt);

};


#endif /* STEPPER_INTERFACE_HH_ */
