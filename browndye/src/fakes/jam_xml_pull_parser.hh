#ifndef __JAM_XML_PULL_PARSER_HH__
#define __JAM_XML_PULL_PARSER_HH__

#include <cstddef>
#include <string>
#include <list>
#include "../vector.hh"

namespace JAM_XML_Pull_Parser{

  typedef std::string String;

  class Str_Stream{
  public:

    template< class S>
    void operator>>( const S&){}

  };


  class Node{
  public:
    //Str_Stream& stream(){
    //return strm;
    //}

    const Vector< String>& data() const{
      return ndata;
    }

    //template< class A, class B>
    //void get_children( const A&, const B&) const{}

    template< class A>
    std::list< Node*> children_of_tag( const A&){
      return children;
    }

    template< class A>
    Node* child( const A&) const{
      return nullptr;
    }

    Vector< String> ndata;
    std::list< Node*> children;

    //Str_Stream strm;
  };

  typedef Node* Node_Ptr;

  class Parser{
  public:
    template< class A, class B>
    void apply_to_nodes_of_tag( const A&, const B&) const{}
    void complete_current_node() const{}
    Node_Ptr current_node() const{
      return Node_Ptr();
    }

    template< class A>
    Parser( const A&){}
  };
}

#endif
