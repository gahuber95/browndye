#ifndef __PARAMETER_INFO_HH__
#define __PARAMETER_INFO_HH__

#include "vector.hh"

namespace Near_Interactions{

  struct Parameter{
    Energy epsilon;

    Parameter( Energy eps){
      epsilon = eps;
    }
  };

  class Parameter_Info{
  public:
    template< class A>
    Parameter parameters( const A&) const{
      return Parameter( epsilon);
    }

    Parameter_Info( const char*){}
    
    Parameter_Info( Energy eps){
      epsilon = eps;
    }

    Energy epsilon;
  };

  template< class I, class A, class B, class C, class D, class E, class F, class G>
  void get_atoms_from_file( const A&, const B&, const C&, const D&, 
                            const E&, const F&, const G&){}

};

#endif
