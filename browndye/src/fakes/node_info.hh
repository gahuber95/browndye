#ifndef __NODE_INFO_HH__
#define __NODE_INFO_HH__

#include "jam_xml_pull_parser.hh"

template< class A>
std::string string_from_node( const A&){
  return "";
} 

template< class A, class B>
std::string string_from_node( const A&, const B&){
  return "";
} 

template< class A, class B>
double double_from_node( const A&, const B&){
  return 0.0;
} 

template< class A>
double double_from_node( const A&){
  return 0.0;
} 

template< class A, class B, class C>
void get_checked_double_from_node( const A&, const B&, const C&){}

template< class A, class B>
int int_from_node( const A&, const B&){
  return 0;
} 

template< class A>
int int_from_node( const A&){
  return 0;
} 

template< class A, class B>
JAM_XML_Pull_Parser::Node* checked_child( const A&, const B&){
  return nullptr;
}

#endif
