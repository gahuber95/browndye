#ifndef __CHARGED_BLOB_SIMPLE_HH__
#define __CHARGED_BLOB_SIMPLE_HH__

#include "linalg3.hh"
#include "random_atoms.hh"

namespace Charged_Blob{
  
  template< unsigned int n, class I>
  class Blob{
  public:

    template< class T>
    void initialize( const T&){}

    template< class T>
    void shift_position( const T&){}

    Charge total_charge(){
      return Charge( 0.0);
    }
    
    template< class A, class B>
    Energy potential_energy( const A&, const B&) const{
      return Energy( 0.0);
    }
    
    Vector< Atom> atoms;

    Blob(){}

    Blob( std::default_random_engine& gen, Charge max_q){
      const unsigned int na = 4;
      atoms.resize( na);
      get_random_atoms( gen, max_q, atoms);
    }

    template< class TForm, class EField>
    void get_force_and_torque( const TForm& tform, const EField& efieldb,
			       Vec3< Force>& force, Vec3< Torque>& torque) const{

      namespace L = Linalg3;
 
      auto& efield = *(efieldb.field);

      Vector< Atom> batoms;
      for( auto& atom: atoms){
        Atom batom;
        tform.get_transformed( atom.pos, batom.pos);
        batom.q = atom.q;
        batoms.push_back( batom);
      }

      Vec3< Length> bcenter;
      tform.get_translation( bcenter);

      L::zero( force);
      L::zero( torque);

      for( auto& batom: batoms){
	auto eps = Permittivity(1.0);
        auto qb = batom.q;
        auto& bpos = batom.pos;

        Vec3< Force> f;
        L::zero( f);
        for( auto& fatom: efield.atoms){
          auto& fpos = fatom.pos;
          auto qf = fatom.q;
          Vec3< Length> d;
          L::get_diff( bpos, fpos, d);

          auto r = L::norm( d);
          auto factor = qf*qb/( eps*r*r*r);
          Vec3< Force> ff;
          L::get_scaled( factor, d, ff);
          L::get_sum( ff, f, f);
        }

        Vec3< Torque> t;
        Vec3< Length> larm;
        L::get_diff( bpos, bcenter, larm);
        L::get_cross( larm, f, t);
        
        L::get_sum( force, f, force);
        L::get_sum( torque, t, torque);
      }	
    }

  };
}

#endif
