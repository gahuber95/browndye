#ifndef __FIELD_HH__
#define __FIELD_HH__

#include "linalg3.hh"
#include "random_atoms.hh"

namespace Field{

  template< class TT>
  class Field{
  public:

    template< class T, class U>
    void get_corners( const T&, const U&) const{}
    
    Length debye_length() const{
      return Length(1.0e20);
    }

    template< class T>
    void shift_position( const T&){}

    class Hint{
    public:
    };

    Hint first_hint() const{
      return Hint();
    }

    Field( std::default_random_engine& gen, Charge max_q){
      const int n = 4;
      atoms.resize( n);
      get_random_atoms( gen, max_q, atoms);
    }

    Field(){}

    Vector< Atom> atoms;

    template< class A>
    void initialize( const A&) const{}

    template< class A, class B>
    void initialize( const A&, const B&) const{}


  };

}

#endif
