#ifndef __GET_ATOMS_FROM_FILE_HH__
#define __GET_ATOMS_FROM_FILE_HH__

template< class A, class B, class C, class D, class E, class F>
void get_atoms_from_file( const A&, const B&, const C&, const D&, const E&, const F&){}

template< class Atom>
const Atom* atom_of_number( const Vector< Atom>& atoms, unsigned int number){
  return nullptr;
}

template< class A, class B, class C>
void check_for_param_file( const A&, const B&, const C&){}

#endif
