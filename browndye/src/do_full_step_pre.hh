#include "jam_xml_pull_parser.hh"
#include "wiener.hh"
#include "lmz_propagator.hh"
#include "step_near_absorbing_sphere.hh"
#include "small_vector.hh"
#include "linalg3.hh"
#include "rotations.hh"

namespace Full_Stepper{

  template< class I>
  class Wiener_Interface{
  public:
    typedef typename I::Time Time;
    typedef decltype( sqrt( Time())) Sqrt_Time;

    typedef typename I::Sqrt_Time_Vector Vector;
    typedef typename I::Random_Number_Generator* Gaussian;

    static
    void initialize( Vector&, unsigned int n){}

    static 
    void copy( const Vector& a, Vector& b){
      for( unsigned int i=0; i < a.size(); i++)
        b[i] = a[i];
    }

    static 
    void set_difference( const Vector& a, const Vector& b, Vector& c){
      for( unsigned int i=0; i < c.size(); i++)
        c[i] = a[i] - b[i];
    }

    static 
    void set_half( const Vector& a, Vector& b){
      for( unsigned int i=0; i < b.size(); i++)
        b[i] = 0.5*a[i];
    }

    static 
    void set_mean( const Vector& a, const Vector& b, Vector& c){
      for( unsigned int i=0; i < c.size(); i++)
        c[i] = 0.5*( a[i] + b[i]);
    }

    static 
    void set_gaussian( Gaussian& gen, Sqrt_Time sdev, Vector& w){
      for( unsigned int i=0; i < w.size(); i++)
        w[i] = sdev*I::gaussian( *gen);
    }

    static 
    void add_gaussian( Gaussian& gen, Sqrt_Time sdev, Vector& w){
      for( unsigned int i=0; i<12; i++)
        w[i] += sdev*I::gaussian( *gen);
    }

    static
    void set_zero( Vector& w){
      for( unsigned int i=0; i<12; i++)
        w[i] = Sqrt_Time( 0.0);
    }

    static
    void set_null( Gaussian& gptr){
      gptr = NULL;
    }
  };
  
  template< class I>
  class Boundary_Step_Interface{
  public:
    typedef typename I::Random_Number_Generator RNG;
    typedef typename I::Random_Number_Generator Random_Number_Generator;
    
    typedef typename I::Length Length;
    typedef typename I::Time Time;
    typedef typename I::Energy Energy;
    typedef typename I::Charge Charge;

    static
    double uniform( RNG& rng){
      return I::uniform( rng);
    }
    
    static
    double gaussian( RNG& rng){
      return I::gaussian( rng);
    }
    
  };

}
