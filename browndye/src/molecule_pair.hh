/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/


#ifndef __MOLECULE_PAIR_HH__
#define __MOLECULE_PAIR_HH__

#include "units.hh"
#include "vector.hh"
#include "molecule_pair_common.hh"
#include "molecule_pair_thread.hh"
#include "molecule_pair_state.hh"

/*
The Molecule_Pair classes contain the functions that carry out
the Brownian dynamics moves of the molecule pair.

The following classes are defined:

Molecule_Pair_Common - contains commmon data; one copy
for the whole simulation

Molecular_Pair_Thread - contains data that is specific to
different threads; one copy per thread

Molecule_Pair_State - contains data for a particular copy of
the bimolecular system; one copy per system copy.

Molecule_Pair - lightweight object temporarily created whenever
the system is stepped forward in time.


 */

class Molecule_Pair_Common;
class Molecule_Pair_Thread;
class Molecule_Pair_State;

class Molecule_Pair{
public:
  typedef SVec< Sqrt_Time, 12> Sqrt_Time_Vector;

  Molecule_Pair( Molecule_Pair_Common& _common, Molecule_Pair_Thread& _thread,
                 Molecule_Pair_State& _state, unsigned int i):
    common(_common), thread(_thread), state(_state), number(i){}

  Molecule_Pair( Molecule_Pair_Common& _common, Molecule_Pair_Thread& _thread,
                   Molecule_Pair_State& _state):
      common(_common), thread(_thread), state(_state), number(0){}

  void get_potential_energy( Energy&, Energy&, Energy&) const;
  Time time() const;
  void get_translation1( Vec3< Length>& trans) const;
  void get_rotation1( Mat3< double>& rot) const;
  unsigned int thread_number() const;
  Vec3< Length> hydro_center0() const;
  Vec3< Length> hydro_center1() const;
  void set_initial_state();
  void set_initial_physical_state();
  void set_initial_reaction_state( const Molecule_Pair_Common&);
  size_t current_reaction_state() const;
  std::string state_name( size_t) const;
  std::string reaction_name( size_t) const;
  void do_step( unsigned long int&);
  unsigned long int n_full_steps() const;
  size_t last_reaction() const;
  Length minimum_reaction_coordinate() const;
  size_t n_reactions() const;
  Length reaction_coordinate();
  void set_initial_state_for_bb();
  Molecule_Pair_Fate fate() const;

private:
  friend Stepper_Interface;
  friend Mover_Interface;
  friend Rxn_Interface;

  void save();
  void restore();
  void back_away( bool&);
  void normalize();

  Molecule_Pair_Common& common;
  Molecule_Pair_Thread& thread;
  Molecule_Pair_State& state;
  unsigned int number;

  void do_real_step( unsigned long int&);
  void rescale_separation( double scale);
  void outer_propagate( Molecule_Pair_Fate& fate);
  void set_initial_physical_state( Length radius);
  void get_mol_mol_diff( Vec3< Length>& diff) const;
  Length mol_mol_distance() const;
  Length minimum_rxn_coord() const;
  Diffusivity separation_diffusivity() const;
  Time time_step_guess() const;
  bool has_collision( const Mat3< double>& rot, const Vec3< Length>& trans);
  void step_forward( Time dt, const Sqrt_Time_Vector& dw, bool& must_backstep, bool& has_collision);

  
  typedef Vector< int>::size_type size_type;
};

inline
Molecule_Pair_Fate Molecule_Pair::fate() const{
  return state.ffate;
}

inline
Time Molecule_Pair::time() const{
	return state.time;
}

inline
void Molecule_Pair::get_translation1( Vec3< Length>& trans) const{
	state.mol1.get_position( trans);
}

inline
void Molecule_Pair::get_rotation1( Mat3< double>& rot) const{
	state.mol1.get_rotation( rot);
}

inline
size_t Molecule_Pair::current_reaction_state() const{
	return state.current_reaction_state();
}

inline
unsigned long int Molecule_Pair::n_full_steps() const{
	return state.n_full_steps;
}

inline
size_t Molecule_Pair::last_reaction() const{
	return state.last_rxn;
}

inline
Length Molecule_Pair::minimum_reaction_coordinate() const{
	return state.min_rxn_coord;
}

inline
size_t Molecule_Pair::n_reactions() const{
	return common.n_reactions();
}

/* 
Input:

files for both molecules

b radius

Time step size parameters
  inner distance
  inner value
  outer distance
  outer value

file for reaction criteria
file for HI interactions

viscosity
kT

*/
 

#endif
