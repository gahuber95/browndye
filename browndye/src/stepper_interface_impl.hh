/*
 * stepper_interface_impl.hh
 *
 *  Created on: Sep 9, 2015
 *      Author: root
 */

#ifndef STEPPER_INTERFACE_IMPL_HH_
#define STEPPER_INTERFACE_IMPL_HH_

#include "stepper_interface.hh"
#include "molecule.hh"

void Stepper_Interface::set_fate_in_action( Molecule_Pair& pair){
  pair.state.ffate = In_Action;
}

void Stepper_Interface::set_fate_final_rxn( Molecule_Pair& pair){
  pair.state.ffate = Final_Rxn;
}

void Stepper_Interface::set_fate_escaped( Molecule_Pair& pair){
  pair.state.ffate = Escaped;
}

bool Stepper_Interface::are_building_bins( const Molecule_Pair& pair){
  return pair.common.building_bins;
}

void Stepper_Interface::
get_next_completed_rxn( const Molecule_Pair& pair, bool& completed,
      Reaction& next){
  auto& state = pair.state;
  auto& common = pair.common;
  common.pathway->get_next_completed_reaction( state.current_rxn_state, pair,
                 completed, next);
}

Stepper_Interface::Reaction_State Stepper_Interface::
state_after_rxn( const Molecule_Pair& pair, Reaction irxn){
  return pair.common.pathway->state_after_reaction( irxn);
}

void Stepper_Interface::
set_rxn_state( Molecule_Pair& pair, Reaction_State istate){
  pair.state.current_rxn_state = istate;
}

void Stepper_Interface::set_last_rxn( Molecule_Pair& pair, Reaction irxn){
  pair.state.last_rxn = irxn;
}

void Stepper_Interface::set_had_rxn( Molecule_Pair& pair){
  pair.state.had_rxn = true;
  pair.state.min_rxn_coord = Length( INFINITY);
}

int Stepper_Interface::
n_rxns_from( const Molecule_Pair& pair, Reaction_State istate){
  return pair.common.pathway->n_reactions_from( istate);
}

Length Stepper_Interface::q_radius( const Molecule_Pair& pair){
  return pair.common.q_rad;
}

Length Stepper_Interface::b_radius( const Molecule_Pair& pair){
  return pair.common.b_rad;
}

Length Stepper_Interface::mol_mol_distance( const Molecule_Pair& pair){
  return pair.mol_mol_distance();
}

template< class U3, class V3>
void Stepper_Interface::v3_copy_to( const U3& a, V3& b){
  b[0] = a[0];
  b[1] = a[1];
  b[2] = a[2];
}

template< class M3, class N3>
void Stepper_Interface::m3_copy_to( const M3& a, N3& b){
  for( int i = 0; i < 3; i++){
    for( int j = 0; j < 3; j++)
      b[i][j] = a[i][j];
  }
}

template< class Position>
void Stepper_Interface::get_positions( const Molecule_Pair& pair,
               Position& pos0, Position& pos1){

  auto& mol0 = pair.state.mol0;
  auto& mol1 = pair.state.mol1;
  mol0.get_position( pos0);
  mol1.get_position( pos1);
}

template< class Position, class Rotation>
void Stepper_Interface::
get_positions_and_rotations( const Molecule_Pair& pair,
           Position& pos0, Rotation& rot0,
           Position& pos1, Rotation& rot1){

  auto& mol0 = pair.state.mol0;
  auto& mol1 = pair.state.mol1;

  mol0.get_position( pos0);
  mol0.get_rotation( rot0);

  mol1.get_position( pos1);
  mol1.get_rotation( rot1);
}

void Stepper_Interface::rescale_separation( Molecule_Pair& pair, double scale){
  pair.rescale_separation( scale);
}

Energy Stepper_Interface::kT( Molecule_Pair& pair){
  return pair.common.solvent.kT;
}

Time Stepper_Interface::natural_dt( const Molecule_Pair& pair){
  auto dt = pair.time_step_guess();
  return dt;
}

void Stepper_Interface::incr_time( Molecule_Pair& pair, Time dt){
  pair.state.time += dt;
}

Time Stepper_Interface::min_dt( const Molecule_Pair& pair){
  return pair.common.minimum_dt();
}

Diffusivity Stepper_Interface::
separation_diffusivity( const Molecule_Pair& pair){
  return pair.separation_diffusivity();
}

template< class Force_Vec>
void Stepper_Interface::get_mol1_force( Molecule_Pair& pair, Force_Vec& force){
  v3_copy_to( pair.state.mol1.force, force);
}

Stepper_Interface::Random_Number_Generator& Stepper_Interface::
random_number_generator( Molecule_Pair& pair){
  return pair.thread.rng;
}

int Stepper_Interface::dimension( const Molecule_Pair& pair){
  return 12;
}

void Stepper_Interface::save( Molecule_Pair& pair){
  pair.save();
}

void Stepper_Interface::restore( Molecule_Pair& pair){
  pair.restore();
}

void Stepper_Interface::
step_forward( Molecule_Pair& pair, Time dt, const Sqrt_Time_Vector& dw,
        bool& must_backstep, bool& has_collision){
  pair.step_forward( dt, dw, must_backstep, has_collision);
}

void Stepper_Interface::
inc_num_of_successive_collisions( Molecule_Pair& pair){
  ++(pair.state.n_succ_collisions);
}

void Stepper_Interface::
zero_num_of_successive_collisions( Molecule_Pair& pair){
  pair.state.n_succ_collisions = 0;
}

int Stepper_Interface::num_of_successive_collisions( Molecule_Pair& pair){
  return pair.state.n_succ_collisions;
}

int Stepper_Interface::max_collision_rejects( Molecule_Pair& pair){
  return 100;
}

void Stepper_Interface::
back_molecules_away( Molecule_Pair& pair, bool& still_has_collision){
  pair.back_away( still_has_collision);
}

double Stepper_Interface::uniform( Browndye_RNG& rng){
  return rng.uniform();
}

double Stepper_Interface::gaussian( Browndye_RNG& rng){
  return rng.gaussian();
}

Time Stepper_Interface::last_dt( const Molecule_Pair& pair){
  return pair.state.last_dt;
}

void Stepper_Interface::set_last_dt( Molecule_Pair& pair, Time dt){
  pair.state.last_dt = dt;
}

template< class Position, class Rotation>
void Stepper_Interface::set_positions_and_rotations( Molecule_Pair& pair,
           const Position& pos0, const Rotation& rot0,
           const Position& pos1, const Rotation& rot1){
  auto& mol0 = pair.state.mol0;
  auto& mol1 = pair.state.mol1;

  mol0.set_position( pos0);
  mol0.set_rotation( rot0);

  mol1.set_position( pos1);
  mol1.set_rotation( rot1);

  auto& common = pair.common;
  auto& thread = pair.thread;
  auto& state = pair.state;
  bool has_collision;
  compute_forces_and_torques( common.mol0, state.mol0,
            common.mol1, thread.mol1, state.mol1,
            has_collision);

  pair.state.diffu = pair.separation_diffusivity();
}

inline
bool Stepper_Interface::has_collision( Molecule_Pair& pair){
  auto& common = pair.common;
  auto& thread = pair.thread;
  auto& state = pair.state;
  bool result;
  compute_forces_and_torques( common.mol0, state.mol0,
            common.mol1, thread.mol1, state.mol1, result);
  return result;
}


#endif /* STEPPER_INTERFACE_IMPL_HH_ */
