/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
Various utility functions for extracting information from an
XML node. "Checked" implies that if a value is not found, an
exception is thrown. Those functions with a "tag" argument extract
information from a child node.

*/


#ifndef __NODE_INFO_HH__
#define __NODE_INFO_HH__

#include "jam_xml_pull_parser.hh"
#include "units.hh"

JAM_XML_Pull_Parser::Node_Ptr 
checked_child( JAM_XML_Pull_Parser::Node_Ptr node, const std::string& tag);

std::string 
string_from_node( JAM_XML_Pull_Parser::Node_Ptr node, const std::string& tag);

std::string string_from_node( JAM_XML_Pull_Parser::Node_Ptr node);

double double_from_node( JAM_XML_Pull_Parser::Node_Ptr node);
double double_from_node( JAM_XML_Pull_Parser::Node_Ptr node, 
			 const std::string& tag);

int int_from_node( JAM_XML_Pull_Parser::Node_Ptr node);
int int_from_node( JAM_XML_Pull_Parser::Node_Ptr node, const std::string& tag);

template< class Value>
void get_value_from_node( JAM_XML_Pull_Parser::Node_Ptr node, 
                          const std::string& tag, Value& value, bool& found){

  JAM_XML_Pull_Parser::Node_Ptr cnode = node->child( tag);  
  if (cnode == NULL)
    found = false;
  else{
    found = true;

    std::stringstream ss( cnode->data()[0]);
    ss >> value;
  }
}

#ifdef UNITS

template< class M, class L, class T, class Q, class Value>
void get_value_from_node( JAM_XML_Pull_Parser::Node_Ptr node, 
                          const std::string& tag, Unit< M,L,T,Q,Value>& value,
                          bool& found){
  
  double fvalue;
  get_value_from_node( node, tag, fvalue, found);
  if (found)
    value.value = fvalue;
}

#endif


template< class Value>
void get_value_from_node( JAM_XML_Pull_Parser::Node_Ptr node, 
                          const std::string& tag, Value& value){
  bool found;
  get_value_from_node( node, tag, value, found);
}


template< class Value>
void get_double_from_node( JAM_XML_Pull_Parser::Node_Ptr node, 
                           const std::string& tag, Value& value, bool& found){

  JAM_XML_Pull_Parser::Node_Ptr cnode = node->child( tag);  
  if (cnode == NULL)
    found = false;
  else{
    found = true;
    double fval = stod( cnode->data()[0]);
    set_fvalue( value, fval);
  }
}

template< class Value>
void get_value_from_node( JAM_XML_Pull_Parser::Node_Ptr node, 
                          Value& value){

  std::stringstream ss( node->data()[0]);
  ss >> value;
}

inline
void get_double_from_node( JAM_XML_Pull_Parser::Node_Ptr node, 
                           double& value){

  value = stod( node->data()[0]);
}

template< class U>
void get_double_from_node( JAM_XML_Pull_Parser::Node_Ptr node, 
			   const std::string& tag, U& unit){

  double value;
  bool found;
  get_value_from_node( node, tag, value, found);
  if (found)
    set_fvalue( unit, value);
}

template< class U>
void get_checked_double_from_node( JAM_XML_Pull_Parser::Node_Ptr node, 
                                   const std::string& tag, U& unit){

  double value;
  bool found;
  get_value_from_node( node, tag, value, found);
  if (found)
    set_fvalue( unit, value);
  else
    error( "tag \"", tag, "\" not found");
}

#endif
