/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

#ifndef __FOUND_HH__
#define __FOUND_HH__

/*
Uses a binary search to find the position of a item in an ordered array.
*/

#include "vector.hh"

// returns index of lower value
template< class U>
typename Vector<U>::size_type found( const Vector< U>& a, U x){
  typedef typename Vector< U>::size_type size_type;
  const size_type n = a.size();

  if (x < a[0])
    error( "found: x too low");
  
  if (x == a[n-1])
    return n-1;
  
  else {
    size_type il = 0, ih = n-1;
    while (ih - il > 1){
      const size_type im = (il + ih)/2;
      if (a[im] <= x)
        il = im;
      else
        ih = im;
    }
    return il;
  }
}

#endif
