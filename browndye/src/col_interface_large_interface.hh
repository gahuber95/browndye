#ifndef __COL_INTERFACE_LARGE_INTERFACE_HH__
#define __COL_INTERFACE_LARGE_INTERFACE_HH__

#include "vector.hh"
#include "blank_transform.hh"
#include "atom_large.hh"

class Col_Interface_Large_Interface{
public:
  typedef ::Atom_Large Atom;
  typedef Vector< Atom_Large> Atoms;
  typedef Atoms::iterator Ref;
  typedef Vector< Ref> Refs;
  typedef Atoms::size_type size_type;
  typedef Blank_Transform Transform;
};

#endif
