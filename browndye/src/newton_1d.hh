/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

#ifndef __NEWTON_RAPHSON_HH__
#define  __NEWTON_RAPHSON_HH__

/*
Finds the zero of a 1-D function using the Newton-Raphson method.

The Interface class has the following types and static functions:

Info - type of object carrying the function information
X - type of function argument
F - type of function result
DFDX - type of function derivative

F f( Info&, X x) - return function at "x"
void get_f_and_dfdx( Info&, X x, F& result, DFDX& dresult_dx) - 
  get function value and derivative at "x"

*/

#include <math.h>
#include "error_msg.hh"

namespace Newton_Raphson{

  /*
    "low" and "high" are bounds, "mid" is a first guess, "tol" is 
    allowed deviation from zero.
  */

template< class Interface> 
typename Interface::X
solution( typename Interface::Info& finfo, 
          typename Interface::X low, 
          typename Interface::X mid, 
          typename Interface::X high,
          typename Interface::F tol){

  typedef typename Interface::X X;
  typedef typename Interface::F F;
  typedef typename Interface::DFDX DFDX;
  
  X x = mid;
  X xlow = low;
  X xhigh = high;

  F flow, fhigh;

  flow = Interface::f( finfo, xlow);
  fhigh = Interface::f( finfo, xhigh);

  typedef typename UProd< F, F>::Res F2;
  if (flow*fhigh > F2(0.0)){
    error( "Newton_Raphson solution: zero is not bounded");
  }

  while (true){
    F f;
    DFDX dfdx;

    Interface::get_f_and_dfdx( finfo, x, f, dfdx);

    if (fabs(f) < tol)
      break;

    x = x - f/dfdx;
    
    // do a bisection step if x goes out of bounds
    if (x < xlow || xhigh < x){
      x = 0.5*( xlow + xhigh);
    }
    Interface::get_f_and_dfdx( finfo, x, f, dfdx);
    
    if (flow*f < F2( 0.0)){
      xhigh = x;
      fhigh = f;
    }
    else{
      xlow = x;
      flow = f;
    }
  }  
  return x;
}
 
}

#endif
