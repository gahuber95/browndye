#ifndef __TFORM_PRODUCT_HH__
#define __TFORM_PRODUCT_HH__

#include "transform.hh"
#include "blank_transform.hh"


template< class TForm0, class TForm1>
class TForm_Product{
public:
  typedef Transform Result;
};

template<>
class TForm_Product< Blank_Transform, Blank_Transform>{
public:
  typedef Blank_Transform Result;
};


inline
void get_product( const Blank_Transform&, const Transform& trans1,
                  Transform& trans01){
	trans01 = trans1;
}

inline
void get_product( const Transform& trans0, const Blank_Transform&,
                  Transform& trans01){
	trans01 = trans0;
}

#endif
