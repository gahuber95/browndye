#ifndef __DO_FULL_STEP_HH__
#define __DO_FULL_STEP_HH__

#include "do_full_step_pre.hh"

/*

typedefs:
System
Random_Number_Generator
Length
Time
Energy
Charge
Reaction
Reaction_State
Sqrt_Time_Vector

void set_fate_in_action( System&);
void set_fate_final_rxn( System&);
void set_fate_escaped( System&);
bool are_building_bins( System&);

void get_next_completed_rxn( System&, bool& completed, Reaction& next);
Reaction_State state_after_rxn( System&, Reaction);
void set_rxn_state( System&, Reaction_State);
void set_last_rxn( System&, Reaction);
void set_had_rxn( System&);
int n_rxns_from( System&, Reaction);

Length q_radius( System&);
Length b_radius( System&);

template< class Position, class Rot_Matrix>
void set_positions_and_rotations( System&, const Position&, const Rotation&,
                                      const Position&, const Rotation&); // leaves consistent state

template< class Position, class Rot_Matrix>
void get_positions_and_rotations( System&, Position&, Rotation&, Position&, Rotation&);

void rescale_separation( System&, double); // leaves consistent state

Energy kT( System&);
Time natural_dt( System&);
void incr_time( System&, Time);
Time min_dt( System&);

Diffusivity separation_diffusivity( System&);
  (Diffusivity unit need not be defined, but must be consistent)

template< class Force_Vec>
void get_mol1_force( System&, Force_Vec&);

Random_Number_Generator& random_number_generator( System&);

int dimension( System&);
void save( System&);
void restore( System&);

void step_forward( System&, Time dt, Sqrt_Time_Vector& dw, bool& must_backstep, 
                   bool& has_collision); // leaves consistent state

void inc_num_of_successive_collisions( System&);
void zero_num_of_successive_collisions( System&);
int num_of_successive_collisions( System&);
int max_collision_rejects( System&);
void back_molecules_away( System&, bool& still_has_collision);
bool has_collision( System&);

 */

namespace Full_Stepper{
 
  // Does a complete step as defined in "wiener.hh", and also takes
  // care of propagation beyond the b-sphere.
  // assumes that system is consistent before and after
  // enhanced LMZ
  // Adds number of substeps to n_steps
  template< class I>
  class Doer{
  public:
    typedef typename I::System System;
    typedef typename I::Length Length;
    typedef typename I::Time Time;
    typedef typename I::Reaction Reaction;
    
    void f( System& system, unsigned long int& n_steps);

    void initialize( const std::string& lmz_file);
    Doer();

    bool has_hemisphere() const;
    
  private:
    typedef Wiener::Process< Wiener_Interface<I> > WProcess;
    typedef Boundary_Step_Interface<I> BSI;
    
    void do_bd_step( System& system, unsigned long int& n_steps, bool& reacted);
    void outer_propagate( System& system);
    void inner_surface_propagate( System& system, bool& touches);  
    Length m_radius( System&) const;
    Length mol_mol_distance( System&) const;
    void test_for_reaction( System& system, bool& reacted) const;
    
    LMZ::Propagator< BSI> outer_propagator; 
    bool initialized;
    const double dt_growth_factor;
  };

  namespace L = Linalg3;

  template< class I>
  auto Doer<I>::m_radius( System& system) const -> Length{
    return 0.95*I::q_radius( system);
  }

  template< class I>
  auto Doer<I>::mol_mol_distance( System& system) const -> Length{
    Vec3< Length> pos0, pos1;
    I::get_positions( system, pos0, pos1);

    return L::distance( pos0, pos1);
  }

  template< class I>
  void Doer<I>::initialize( const std::string& lmz_file){
    std::ifstream input( lmz_file);
    JAM_XML_Pull_Parser::Parser parser( input);
    parser.complete_current_node();
    outer_propagator.initialize( parser.current_node());
    initialized = true;
  }

  // constructor
  template< class I>
  Doer<I>::Doer(): dt_growth_factor( 1.1){
    initialized = false;
  }
  
  // n_steps: number of elementary BD steps
  template< class I>
  void Doer<I>::f( System& system, unsigned long int& n_steps){
    if (!initialized)
      error( "full stepper not initialized");
    
    I::set_fate_in_action( system);
    
    Length q_rad = I::q_radius( system);
    //Length b_rad = I::b_radius( system);
    Length m_rad = m_radius( system);
    
    bool reacted;
    if (I::are_building_bins( system)){
      do_bd_step( system, n_steps, reacted);
      
      Length new_r = mol_mol_distance( system);
      if ((new_r > q_rad)){ 
        I::rescale_separation( system, q_rad/new_r);
      }    
    }
    
    else{  
      Length r = mol_mol_distance( system);
      if (r > m_rad){
        bool touches;
        inner_surface_propagate( system, touches);
        if (touches)
          outer_propagate( system);
      }
      else 
        do_bd_step( system, n_steps, reacted);
    }
  }  

  template< class I>
  void Doer<I>::inner_surface_propagate( System& system, bool& touches){
    auto& rng = I::random_number_generator( system);    
    Length r = mol_mol_distance( system);
    auto kT = I::kT( system);
    auto D_rad = kT*outer_propagator.radial_mobility( r);
    auto D_perp = kT*outer_propagator.perpendicular_mobility( r);
    auto F = outer_propagator.radial_force( r)/kT;
    Length q_rad = I::q_radius( system);
    Length new_r;
    Time time;
    bool survives;
    typedef Boundary_Step_Interface<I> BSI;
    step_near_absorbing_sphere< BSI>( rng, r, q_rad, F, D_rad, D_perp, survives, new_r, time);
    touches = !survives;

    Vec3< Length> pos0, pos1;
    Mat3< double> rot0, rot1;
    I::get_positions_and_rotations( system, pos0, rot0, pos1, rot1);
    
    Vec3< double> ux,uy,uz;
    Vec3< Length> dpos;
    L::get_diff( pos1, pos0, dpos);
    L::get_scaled( 1.0/L::norm( dpos), dpos, uz);
    L::get_perp_axes( uz, ux, uy);
    
    auto sqdt = sqrt( 2.0*D_perp*time);
    Length dx = sqdt*I::gaussian( rng);
    Length dy = sqdt*I::gaussian( rng);
    for( int k = 0; k < 3; k++)
      dpos[k] += dx*ux[k] + dy*uy[k];

    Length old_r = L::norm( dpos);
    L::get_scaled( new_r/old_r, dpos, dpos);

    Vec3< Length> new_pos1;
    L::get_sum( pos0, dpos, new_pos1);

    Mat3< double> new_rot0, new_rot1;
    auto& adr = add_diffusional_rotation< BSI>;
    auto rD0 = outer_propagator.rotational_diffusivity0();
    auto rD1 = outer_propagator.rotational_diffusivity1();
    if (!outer_propagator.has_hemisphere())
      adr( rng, time*rD0, rot0, new_rot0);
    else
      new_rot0 = rot0;

    adr( rng, time*rD1, rot1, new_rot1); 

    I::set_positions_and_rotations( system, pos0, new_rot0, new_pos1, new_rot1);
  }


  template< class I>
  void Doer<I>::outer_propagate( System& system){
    Vec3< Length> r0b, r1b;
    Vec3< Length> r1;
    Time time = Time( 0.0);
    Mat3< double> rot0b, rot1b;
    Mat3< double> rot0, rot1;
    bool escaped;

    I::get_positions_and_rotations( system, r0b, rot0b, r1b, rot1b);

    auto& rng = I::random_number_generator( system);

    outer_propagator.propagate( rng, r0b, rot0b, r1b, rot1b,
                                escaped, rot0, r1, rot1, time);
                           
    if (escaped){
      I::set_fate_escaped( system);
    }
    else{
      I::incr_time( system, time);
      I::set_fate_in_action( system);
      I::set_positions_and_rotations( system, r0b, rot0, r1, rot1); 
    } 
  }

  template< class I>
  void Doer<I>::test_for_reaction( System& system, bool& reacted) const{
    Reaction next_rxn;
    I::get_next_completed_rxn( system, reacted, next_rxn);
    if (reacted && (!I::are_building_bins( system))){
      auto current_rxn_state = I::state_after_rxn( system, next_rxn);
      I::set_rxn_state( system, current_rxn_state);
      I::set_last_rxn( system, next_rxn);
      I::set_had_rxn( system);
      if (I::n_rxns_from( system, current_rxn_state) == 0)
        I::set_fate_final_rxn( system);
    }
  }

  // n_steps: number of elementary BD steps
  template< class I>
  void Doer<I>::do_bd_step( System& system, unsigned long int& n_steps, bool& reacted){

    Wiener::Process< Wiener_Interface<I> > wprocess;
    wprocess.set_dim( I::dimension( system));
    wprocess.set_gaussian_generator( &I::random_number_generator( system));
    
    bool just_backstepped = false;
    reacted = false;
    bool out_of_bounds = false;

    // assume that state is consistent at start of loop
    Time last_dt = I::last_dt( system);
    last_dt *= dt_growth_factor;
    
    const Length q_rad = I::q_radius( system);
    const Length m_rad = m_radius( system);

    Time boundary_dt;
    if (I::are_building_bins( system))
      boundary_dt = Time( INFINITY);
    else {
      Length r = mol_mol_distance( system);
      Length dx = q_rad - r;
      auto D = outer_propagator.radial_mobility( r)*I::kT( system);
      boundary_dt = 0.333*dx*dx/D;
    }
    const Time start_dt = min( min( I::natural_dt( system), last_dt), boundary_dt);
    do {

      if (just_backstepped)
        just_backstepped = false;
      else{
        wprocess.step_forward( start_dt);
        /*
        while (wprocess.time_step() > I::natural_dt( system)){
          wprocess.backstep();       
          just_backstepped = true;
        }
        */
      }

      Time dt = wprocess.time_step();
      I::set_last_dt( system, dt);
      
      I::save( system);
      bool must_backstep, has_collision;
      Vec3< Length> old_r0, old_r1;
      Mat3< double> old_rot0, old_rot1;
      bool hemisphere = outer_propagator.has_hemisphere();
      if (hemisphere)
        I::get_positions_and_rotations( system, old_r0,old_rot0, old_r1,old_rot1);

      I::step_forward( system, dt, wprocess.dW(), must_backstep, has_collision);

      if (hemisphere){
        Vec3< Length> new_r0, new_r1;
        Mat3< double> new_rot0, new_rot1;
        I::get_positions_and_rotations( system, new_r0,new_rot0, new_r1,new_rot1);
        Transform old_tform0;
        old_tform0.set_translation( old_r0);
        old_tform0.set_rotation( old_rot0);

        Transform new_tform0;
        new_tform0.set_translation( new_r0);
        new_tform0.set_rotation( new_rot0);

        Transform inv_new_tform0;
        new_tform0.get_inverse( inv_new_tform0);

        Transform dtform0;
        get_product( old_tform0, inv_new_tform0, dtform0);

        Transform new_tform1;
        new_tform1.set_translation( new_r1);
        new_tform1.set_rotation( new_rot1);

        Transform tform1;
        get_product( dtform0, new_tform1, tform1);

	Mat3< double> rot1;
	Vec3< Length> r1;
	tform1.get_translation( r1);
	tform1.get_rotation( rot1);
	I::set_positions_and_rotations( system, old_r0, old_rot0, r1, rot1); 
      }

      ++n_steps;

      if (has_collision){
        I::restore( system);
        if (dt > I::min_dt( system)){
          wprocess.backstep();
          just_backstepped = true;
        }
        else{
          I::inc_num_of_successive_collisions( system);
          if (I::num_of_successive_collisions( system) > I::max_collision_rejects( system)){
            bool still_has_collision;
            do{
              I::back_molecules_away( system, still_has_collision);
            }
            while (I::has_collision( system));
            I::zero_num_of_successive_collisions( system);
            test_for_reaction( system, reacted);
          }
        }
      }
      else{
        I::zero_num_of_successive_collisions( system);

        if (must_backstep){
          I::restore( system);
          wprocess.backstep();
          just_backstepped = true;
        }
        else{
          test_for_reaction( system, reacted);

          if (!reacted && (mol_mol_distance( system) > q_rad)){
            I::restore( system);
            if (mol_mol_distance( system) < m_rad){
              wprocess.backstep();
              just_backstepped = true;
            }
            else
              out_of_bounds = true;
          }
        }
      }  
    }
    while (!(wprocess.at_chain_end() || reacted || out_of_bounds));
  }

  template< class I>
  bool Doer<I>::has_hemisphere() const{
    return outer_propagator.has_hemisphere();
  }
  
}

 

#endif
