
/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
Generic code for finding the smallest bounding sphere of a group of points.
*/

#include "circumspheres_cc.hh"

namespace Circumspheres{


//********************************************************************
void get_circumcircle( const Vec3< Length>& p0, const Vec3< Length>& p1, 
                       const Vec3< Length>& p2,
                       Vec3< Length>& center, Length& radius){

  Vec3< Length> d1, d2;
  Vec3< Length2> prp;
  L::get_diff( p1, p0, d1);
  L::get_diff( p2, p0, d2);
  L::get_cross( d1, d2, prp);
  Length2 nrm = L::norm( prp);

  if (nrm == Length2( 0.0)){ // collinear
    center[0] = Length( INFINITY);
    center[1] = Length( INFINITY);
    center[2] = Length( INFINITY);
    radius = Length( INFINITY);
  }
  else{
    Vec3< Length> perp, h1, h2;

    L::get_scaled( 1.0/sqrt( nrm), prp, perp);

    L::get_scaled( 0.5, d1, h1);
    L::get_scaled( 0.5, d2, h2);

    Mat3< Length> mat;
    mat[0][0] = d1[0];
    mat[0][1] = d1[1];
    mat[0][2] = d1[2];

    mat[1][0] = d2[0];
    mat[1][1] = d2[1];
    mat[1][2] = d2[2];

    mat[2][0] = perp[0];
    mat[2][1] = perp[1];
    mat[2][2] = perp[2];


    Vec3< Length2> b;
    Vec3< Length> relpos;
    b[0] = L::dot( h1, d1);
    b[1] = L::dot( h2, d2);
    b[2] = Length2( 0.0);

    Solve3::solve( mat, b, relpos);

    L::get_sum( p0, relpos, center);
    radius = L::distance( center, p0); 
  }
  
}



//********************************************************************
void get_circumsphere( const Vec3< Length>& p0, const Vec3< Length>& p1, 
                       const Vec3< Length>& p2, const Vec3< Length>& p3,
                       Vec3< Length>& center, Length& radius){
  
  Vec3< Length> d1, d2, d3;
  L::get_diff( p1, p0, d1);
  L::get_diff( p2, p0, d2);
  L::get_diff( p3, p0, d3);

  Vec3< Length> h1, h2, h3;
  L::get_scaled( 0.5, d1, h1);
  L::get_scaled( 0.5, d2, h2);
  L::get_scaled( 0.5, d3, h3);

  Vec3< Length2> h2ch3;
  L::get_cross( h2, h3, h2ch3);
  if (L::dot( h1, h2ch3) == Length3( 0.0)){ // coplanar
    center[0] = Length( INFINITY);
    center[1] = Length( INFINITY);
    center[2] = Length( INFINITY);
    radius = Length( INFINITY);
  }
  else{
    Mat3< Length> mat;
    mat[0][0] = d1[0];
    mat[0][1] = d1[1];
    mat[0][2] = d1[2];

    mat[1][0] = d2[0];
    mat[1][1] = d2[1];
    mat[1][2] = d2[2];

    mat[2][0] = d3[0];
    mat[2][1] = d3[1];
    mat[2][2] = d3[2];

    Vec3< Length2> b; 
    Vec3< Length> relpos;
    b[0] = L::dot( h1, d1);
    b[1] = L::dot( h2, d2);
    b[2] = L::dot( h3, d3);

    Solve3::solve( mat, b, relpos);
    
    L::get_sum( p0, relpos, center);
    radius = L::distance( center, p0);
  }
}    

//********************************************************************
void get_smallest_triangle_sphere( const Vec3< Length>& p0, 
                                   const Vec3< Length>& p1, 
                                   const Vec3< Length>& p2,
                                   Vec3< Length>& center, Length& radius){

  //  printf( "get triangle sphere\n");
  Vec3< Length> d01, d12, d20;
  L::get_diff( p1, p0, d01);
  L::get_diff( p2, p1, d12);
  L::get_diff( p0, p2, d20);

  Length r01 = L::norm( d01);
  Length r12 = L::norm( d12);
  Length r20 = L::norm( d20);

  Length maxr = max( max( r01, r12), r20);
  
  Vec3< Length> trial_center;
  const Vec3< Length> *rem_pt_ptr;
  if (r01 == maxr){
    get_avg( p0,p1, trial_center);
    rem_pt_ptr = &p2;
  }
  else if (r12 == maxr){
    get_avg( p1,p2, trial_center);
    rem_pt_ptr = &p0;
  }
  else{
    get_avg( p2,p0, trial_center);
    rem_pt_ptr = &p1;
  }
  const Vec3< Length>& rem_pt = *rem_pt_ptr;

  Length trial_radius = 0.5*maxr;
  
  if (L::distance( rem_pt, trial_center) <= trial_radius){
    L::copy( trial_center, center);
    radius = trial_radius;
  }
  else{
    get_circumcircle( p0, p1, p2, center, radius);
  }

  {
    // adjust in case round-off error keeps points outside 
    Length d0 = L::distance( center, p0);
    Length d1 = L::distance( center, p1);
    Length d2 = L::distance( center, p2);
    radius = max( max( d0, d1), d2); 
  }
}

//********************************************************************
void get_smallest_tetrahedron_sphere( 
                                     const Vec3< Length>& p0, 
                                     const Vec3< Length>& p1, 
                                     const Vec3< Length>& p2, 
                                     const Vec3< Length>& p3,
                                     Vec3< Length>& center, Length& radius){
  
  SVec< Vec3< Length>, 4> ps;
  ps[0] = p0;
  ps[1] = p1;
  ps[2] = p2;
  ps[3] = p3;

  int pair_ij[2] = {1,0};
  Length max_pd = L::distance( p0, p1);
  for( int i = 0; i<=3; i++){
    const Vec3< Length>& pi = ps[i];
    for( int j = 0; j <= i-1; j++){
      const Vec3< Length>& pj = ps[j];
      Length d = L::distance( pi, pj);
      if (d > max_pd){
        max_pd = d;
        pair_ij[0] = i;
        pair_ij[1] = j;
      }
    }
  }
  int i = pair_ij[0];
  int j = pair_ij[1];
  
  Vec3< Length> trial_center;
  get_avg( ps[i], ps[j], trial_center);
  Length trial_radius = max( L::distance( ps[i], trial_center),
                             L::distance( ps[j], trial_center));

  int rem_i, rem_j;
  if      ((i == 1) && (j == 0))
    {rem_i = 2; rem_j = 3;}
  else if ((i == 2) && (j == 0))
    {rem_i = 1; rem_j = 3;}
  else if ((i == 3) && (j == 0))
    {rem_i = 1; rem_j = 2;}
  else if ((i == 2) && (j == 1))
    {rem_i = 0; rem_j = 3;}
  else if ((i == 3) && (j == 1))
    {rem_i = 0; rem_j = 2;}
  else if ((i == 3) && (j == 2))
    {rem_i = 0; rem_j = 1;}
  else{
    rem_i = -1;
    rem_j = -1;
    error( "get_smallest_tetrahedron_sphere: internal error");
  }  

  if (L::distance( ps[ rem_i], trial_center) <= trial_radius &&
      L::distance( ps[ rem_j], trial_center) <= trial_radius){
    L::copy( trial_center, center);
    radius = trial_radius;
  }
  else{
    // try triangles now
    Vec3< Length> c0, c1, c2, c3;
    Length r0, r1, r2, r3;

    get_smallest_triangle_sphere( p1,p2,p3, c0,r0);
    get_smallest_triangle_sphere( p0,p2,p3, c1,r1);
    get_smallest_triangle_sphere( p0,p1,p3, c2,r2);
    get_smallest_triangle_sphere( p0,p1,p2, c3,r3);

    Length maxr = max (max (max( r0,r1), r2), r3);

    const Vec3< Length> *rem_pt_ptr = NULL;
    Vec3< Length> *trial_center_ptr = NULL;

    if (maxr == r0){
      trial_center_ptr = &c0;
      rem_pt_ptr = &p0;
    }
    else if (maxr == r1){
      trial_center_ptr = &c1;
      rem_pt_ptr = &p1;
    }
    else if (maxr == r2){
      trial_center_ptr = &c2;
      rem_pt_ptr = &p2;
    }
    else if (maxr == r3){
      trial_center_ptr = &c3;
      rem_pt_ptr = &p3;
    }
    else
      error( "get_smallest_tetrahedron_sphere: internal error 2"); 

    const Vec3< Length>& rem_pt = *rem_pt_ptr;
    const Vec3< Length>& trial_center = *trial_center_ptr;

    if (L::distance( rem_pt, trial_center) <= maxr){
      L::copy( trial_center, center);
      radius = maxr;
    }
    else{
      get_circumsphere( p0,p1,p2,p3, center, radius);
    }
  }

  {
    // adjust in case round-off error keeps points outside 
    Length d0,d1,d2,d3;
    d0 = L::distance( center, p0);
    d1 = L::distance( center, p1);
    d2 = L::distance( center, p2);
    d3 = L::distance( center, p3);
    radius = max( max( max( d0,d1),d2),d3);
  }
                
}

  bool stest( const Vec3< Length>& origin, const Vec3< Length2>& perp, 
              const Vec3< Length>& vert, const Vec3< Length>& p){
  
  Vec3< Length> pmorigin, vertmorigin;
  L::get_diff( p, origin, pmorigin);
  L::get_diff( vert, origin, vertmorigin);
  return L::dot( perp, pmorigin)*L::dot( perp, vertmorigin) > Length6( 0.0);
}

//********************************************************************
bool inside_tetrahedron( const Vec3< Length>& p0, const Vec3< Length>& p1, 
                         const Vec3< Length>& p2, const Vec3< Length>& p3,
                         const Vec3< Length>& p){

  Vec3< Length> d01, d02, d03, d12, d13;
  
  L::get_diff( p1,p0, d01);
  L::get_diff( p2,p0, d02);
  L::get_diff( p3,p0, d03);
  L::get_diff( p2,p1, d12);
  L::get_diff( p3,p1, d13);

  Vec3< Length2> perp0, perp1, perp2, perp3;
  L::get_cross( d01,d02, perp3);
  L::get_cross( d01,d03, perp2);
  L::get_cross( d02,d03, perp1);
  L::get_cross( d12,d13, perp0);
  
  return 
    stest( p1, perp0, p0, p) && stest( p0, perp1, p1, p) && 
    stest( p0, perp2, p2, p) && stest( p0, perp3, p3, p);
}


}


//*******************************************************************
// Test code
/* 
struct Point{
  double x[3];
};

class Interface{
public:
  typedef Vector< Point> Points;
  typedef Vector< Points::const_iterator> Point_Refs;
  
  static
  void initialize( const Points& pts, Point_Refs& refs){
    unsigned int n = pts.size();
    refs.resize( n);
    for( unsigned int i = 0; i<n; i++)
      refs[i] = pts.begin() + i;
  }

  static
  unsigned int size( const Points& refs){
    return refs.size();
  }

  static
  void get_position( const Points& pts, const Point_Refs& refs,
                     unsigned int i, double* pos){    
    const Point& pt = *(refs[i]);
    pos[0] = pt.x[0];
    pos[1] = pt.x[1];
    pos[2] = pt.x[2];
  }

  static
  void swap( Points& pts, Point_Refs& refs, unsigned int i, unsigned int j){
    Point_Refs::value_type temp = refs[i];
    refs[i] = refs[j];
    refs[j] = temp;
  }

};

int main(){
  srand( 111116);

  unsigned int n = 100;
  Vector< Point> pts( n);
  
  for( unsigned int i = 0; i<n; i++){
    Point& pt = pts[i];
    pt.x[0] = (float)(rand())/RAND_MAX;
    pt.x[1] = (float)(rand())/RAND_MAX;
    pt.x[2] = 2.0*(float)(rand())/RAND_MAX;
    //printf( "pt %f %f %f\n", pt.x[0], pt.x[1], pt.x[2]);
  }

  double center[3];
  double r;
  
  Circumspheres::get_smallest_enclosing_sphere< Interface>( pts, center, r);

  printf( "center %f %f %f\n", center[0], center[1], center[2]);
  printf( "radius %f\n", r);

  
  Circumspheres::do_brute_force< Interface>( pts, center, r);

  printf( "center %f %f %f\n", center[0], center[1], center[2]);
  printf( "radius %f\n", r);
  

  double max_r = 0.0;
  for( unsigned int i = 0; i<n; i++){
    double r = Circumspheres::distance( pts[i].x, center);
    if (r > max_r)
      max_r = r;
  }
  //printf( "max r %f\n", max_r);

}

*/
