/*
 * molecule_pair_thread.hh
 *
 *  Created on: Sep 8, 2015
 *      Author: root
 */

#ifndef MOLECULE_PAIR_THREAD_HH_
#define MOLECULE_PAIR_THREAD_HH_

#include "small_molecule_thread.hh"
#include "molecule_pair_state.hh"
#include "molecule_pair_common.hh"

class Molecule_Pair_Thread{
public:
  void initialize( Molecule_Pair_Common& common,
                   JAM_XML_Pull_Parser::Node_Ptr node, unsigned int n_in
                   );

  void set_seed( const Vector< uint32_t>& seed);


private:
  friend class Molecule_Pair;
  friend class Molecule_Pair_Common;
  friend class Stepper_Interface;
  friend class Mover_Interface;
  friend class Rxn_Interface;

  Small_Molecule_Thread mol1;
  unsigned int n;
  Browndye_RNG rng;

  Molecule_Pair_State saved;
  Length violation;
};

#endif /* MOLECULE_PAIR_THREAD_HH_ */
