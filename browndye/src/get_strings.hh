#ifndef __GET_STRINGS_HH__
#define __GET_STRINGS_HH__

#include <string>
#include "vector.hh"
#include "jam_xml_pull_parser.hh"

void get_strings( JAM_XML_Pull_Parser::Node_Ptr node, std::string tag, 
                  Vector< std::string>& carrays);

#endif
