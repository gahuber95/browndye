#ifndef __PARTITION_CONTENTS_EXT_HH__
#define __PARTITION_CONTENTS_EXT_HH__

#include "units.hh"
#include "small_vector.hh"

template< class F, class Interface>
void partition_contents_ext( 
                         const typename Interface::Refs& refs, const F& f, 
                         typename Interface::Refs& refs0, 
                         typename Interface::Refs& refs1){
  
  typedef typename Interface::size_type size_type;

  const size_type n = refs.size();
  size_type n1 = 0;
  for( size_type i = 0; i<n; i++){
    Vec3< Length> pos;
    refs[i]->get_position( pos);
    if (f( pos))
      ++n1;
  }
  refs1.resize( n1);
  refs0.resize( n-n1);

  size_type i0 = 0;
  size_type i1 = 0;
  for( size_type i = 0; i<n; i++){
    Vec3< Length> pos;
    refs[i]->get_position( pos);
     if (f( pos)){
       refs1[i1] = refs[i];
      ++i1;
     }
     else{
       refs0[i0] = refs[i];
      ++i0;
     }
  }
}

#endif
