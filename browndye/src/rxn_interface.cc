/*
 * rxn_interface.cc
 *
 *  Created on: Sep 8, 2015
 *      Author: root
 */

#include "rxn_interface.hh"
#include "units.hh"
#include "vector.hh"
#include "atom_small.hh"
#include "atom_large.hh"
#include "molecule_pair.hh"

namespace L = Linalg3;

Length Rxn_Interface::distance( const Molecule_Pair& mpair, size_type i0, size_type i1){
  const Atom_Large& atom0 = mpair.common.mol0.atom( i0);
  Atom_Small& atom1 = mpair.thread.mol1.atom( i1);

  Vec3< Length> tpos0, tpos1;
  Transform tform0, tform1;
  auto& state = mpair.state;
  state.mol0.get_transform( tform0);
  state.mol1.get_transform( tform1);
  atom0.get_transformed_position( tform0, tpos0);
  atom1.get_transformed_position( tform1, tpos1);
  atom1.clear_transformed();

  return L::distance( tpos0, tpos1);
}



