/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
Defines the classes describing the two molecules.  There is a distinction
made between the "Large" molecule (or Molecule 0) and the "Small" molecule
(or Molecule 1).  Also, the molecules are split further into several
classes, since certain data are duplicated across threads.
The following classes are of note:

Molecule_Common - base class of information held in common by molecule
classes on all threads. One object for the whole simulation.

Small_Molecule_Common - Small Molecule information held in common by all 
threads

Large_Molecule_Common - Large Molecule information held in common by all 
threads

Small_Molecule_Thread - information held separately by each thread. One
object per thread

Molecule_State - base class of information held by each molecule individually.
One per molecule.

Small_Molecule_State - Small Molecule information held by each molecule.
Large_Molecule_State - Large Molecule information held by each molecule.

In general, the "Common" classes contain unchangeable, large things 
like electrostatic and desolvation grids.  The "Thread" classes contain
the data structures used to test for collisions, since they cannot be
shared across threads.  The "State" classes contain state information
such as position, rotation, force, and torque.
 
*/

#ifndef __MOLECULE_HH__
#define __MOLECULE_HH__

#include "units.hh"
#include "large_molecule_common.hh"
#include "large_molecule_state.hh"
#include "small_molecule_common.hh"
#include "small_molecule_thread.hh"
#include "small_molecule_state.hh"

typedef UQuot< Energy, Volume>::Res Vol_Potential;

void add_forces_and_torques( 
                            const Large_Molecule_Common& molc0,
                            Large_Molecule_State& mols0,
                            const Small_Molecule_Common& molc1,
                            Small_Molecule_Thread& molt1,
                            Small_Molecule_State& mols1,
                            bool& has_collision);

void compute_forces_and_torques(
                                const Large_Molecule_Common& molc0,
                                Large_Molecule_State& mols0,
                                const Small_Molecule_Common& molc1,
                                Small_Molecule_Thread& molt1,
                                Small_Molecule_State& mols1,               
                                bool& has_collision,
                                Length& violation,
                                Atom_Large_Ref&, Atom_Small_Ref&
                                );

void compute_forces_and_torques(
                                const Large_Molecule_Common& molc0,
                                Large_Molecule_State& mols0,
                                const Small_Molecule_Common& molc1,
                                Small_Molecule_Thread& molt1,
                                Small_Molecule_State& mols1,               
                                bool& has_collision
                                );

void get_potential_energy( 
                          const Large_Molecule_Common& molc0,
                          Large_Molecule_State& mols0,
                          const Small_Molecule_Common& molc1,
                          Small_Molecule_Thread& molt1,
                          Small_Molecule_State& mols1,
                          Energy& near, Energy& coul, Energy& des
                           );


Length molecule_gap( bool is_hydro,                  
                     const Large_Molecule_Common& molc0,
                     const Large_Molecule_State& mols0,
                     const Small_Molecule_Common& molc1,
                     const Small_Molecule_Thread& molt1,
                     const Small_Molecule_State& mols1                 
                     );


Length hydro_gap( 
                 const Large_Molecule_Common& molc0,
                 const Large_Molecule_State& mols0,
                 const Small_Molecule_Common& molc1,
                 const Small_Molecule_Thread& molt1,
                 const Small_Molecule_State& mols1               
                 );

inline
void reset_hints( const Large_Molecule_Common& molc, Large_Molecule_State& mol){
  mol.born_hint = molc.born_hint0;
  mol.v_hint = molc.v_hint0;
}

inline
void reset_hints( const Small_Molecule_Common& molc, Small_Molecule_State& mol){
  mol.born_hint = molc.born_hint0;
}

void normalize( Large_Molecule_State& mol0, Small_Molecule_State& mol1);


#endif
