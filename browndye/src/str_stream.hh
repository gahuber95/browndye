#ifndef __STR_STREAM_HH__
#define __STR_STREAM_HH__

/*
Class for containing the contents of an XML node. It behaves as a
C++ input stream.

*/

#include <memory>
#include <sstream>
#include "str.hh"
#include "error_msg.hh"

namespace JAM_XML_Parser{

class Str_Stream{
public:
  typedef std::basic_stringstream< XML_Char> Super;
  typedef std::basic_string< XML_Char>  string_type;
  typedef string_type Str;

  Str_Stream( const XML_Parser& _parser, const Str& _tag, const Str& str);

  // new copy is reset
  Str_Stream( const Str_Stream& stm);

  Str_Stream& operator>>( Str& str);

  template< class Input>
  Str_Stream& operator>>( Input& x);

  void reset();
  
  bool fail() const;

  bool eof() const;

private:
  Str_Stream& operator=( const Str_Stream&);

  std::unique_ptr< Super> super;

  const XML_Parser& parser;
  const Str tag;
  Str sdata;
  bool is_reset;
};


template< class Input>
Str_Stream& Str_Stream::operator>>( Input& x){
  is_reset = false;
  if (super){
    (*super) >> x;    
    
    if (super->fail()){
      int line = XML_GetCurrentLineNumber( parser);
      error( "CDATA input failure at line ", line, "; tag ", tag.c_str());
    }      
  }      
  
  return *this;
}

inline
bool Str_Stream::fail() const{
  return super->fail();
}

inline
bool Str_Stream::eof() const{
  return super->eof();
}

}

#endif
