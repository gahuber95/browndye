/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

#ifndef __WIENER_HH__
#define __WIENER_HH__


/*
The Wiener::Process class implements the back-stepping Wiener process
as described in the paper.

The Interface class has the following types and static functions:

Vector
Gaussian - must have = operator defined
Time
Sqrt_Time

void copy( const Vector&, Vector&);
void set_mean( const Vector&, const Vector&, Vector&);
void set_half( const Vector&, Vector&);
void set_difference( const Vector&, const Vector&, Vector&);
void set_gaussian( Gaussian&, const Sqrt_Time& scale, Vector&);
void add_gaussian( Gaussian&, const Sqrt_Time& scale, Vector&);
void initialize( Vector&, unsigned int n);
void set_null( Gaussian&); 

 */

#include "wiener_pre.hh"

namespace Wiener{

template< class I>
class Process{
public:
  typedef typename I::Vector Vector;
  typedef typename I::Gaussian Gaussian;
  typedef typename I::Time Time;
  typedef typename I::Sqrt_Time Sqrt_Time;

  void backstep();
  void step_forward( Time dt);
  
  Time time_step() const;
  Time time() const;
  
  bool at_chain_end() const;
  
  const Vector& dW() const;
  
  void set_dim( unsigned int);
  void set_gaussian_generator( Gaussian);

  Process();
  Process( const Process< I>&);
  Process< I>& operator=( const Process< I>&);

  //**********************************
private: 

  static
  void set_mean( const Vector& w0, const Vector& w1, Vector& wh){
    I::set_mean( w0, w1, wh);
  }
  
  static
  void set_difference( const Vector& w0, const Vector& w1, 
                       Vector& dw){
    I::set_difference( w0, w1, dw);
  }
  
  static
  void add_gaussian( Gaussian& gaussian, Sqrt_Time a, Vector& w){
    I::add_gaussian( gaussian, a, w);
  }

  static
  void set_gaussian( Gaussian& gaussian, Sqrt_Time a, Vector& w){
    I::set_gaussian( gaussian, a, w);
  }
  
  static
  void initialize( Vector& w, unsigned int n){
    I::initialize( w, n);
  }

  static
  void copy( const Vector& w0, Vector& w1){
    I::copy( w0, w1);
  }

  static
  void set_half( const Vector& w0, Vector& w1){
    I::set_half( w0, w1);
  }

  static
  void set_zero( Vector& w){
    I::set_zero( w);
  }

  //**************************
  void normal_advance( Time dt);
  void copy_from( const Process& other);

  Gaussian gaussian;
  std::forward_list< W_Info< I> > ws;
  W_Info<I> *current, *next;
  Vector* dw;
  Vector dw0;
  unsigned int dim;
};

  // constructor
template< class I>
Process< I>::Process(){ 
  ws.emplace_front();
  current = &(ws.front());
  current->t = Time( 0.0);
  dw = &(current->w);
  dim = 0;
  I::set_null( gaussian);
  next = nullptr;
}

template< class I>
void Process< I>::copy_from( const Process& other){
  dim = other.dim;
  gaussian = other.gaussian;
  initialize( dw0, dim);
  I::copy( other.dw0, dw0);
  ws = other.ws;
  current = *(ws.front());
  
  if (other.next != nullptr){
    dw = &dw0;
    auto b = ws.begin();
    ++b;
    next = &(*b);
  }
  else{
    dw = &(current->w);
    next = nullptr;
  }
}

  // copy constructor
template< class I>
Process< I>::Process( const Process& other){
  copy_from( other);
}

template< class I>
Process< I>& Process< I>::operator=( const Process& other){
  if (&other != this){
    copy_from( other);
  }
  return *this;
}
  
template< class I>
void Process< I>::backstep(){

  if (next == nullptr){
    next = current;
    ws.emplace_front();
    current = &(ws.front());
    initialize( current->w, dim);
    set_zero( current->w);
    current->dt = next->dt;
    current->t = next->t - next->dt;
    dw = &dw0;
  }

  auto* mid = &(*(ws.emplace_after( ws.begin())));
  initialize( mid->w, dim);

  Sqrt_Time s = sqrt( 0.5*current->dt);
  current->dt /= 2.0;
  mid->dt = current->dt;
  mid->t = current->t + current->dt;
  set_mean( current->w, next->w, mid->w);

  add_gaussian( gaussian, s, mid->w);
  set_difference( mid->w, current->w, dw0);

  next = mid;
}

template< class I>
void Process<I>::normal_advance( Time dt){
  current->dt = dt;
  current->t += dt;
  set_gaussian( gaussian, sqrt( 2.0*dt), current->w); 
}

template< class I>
void Process< I>::step_forward( Time dt){

  if (next == nullptr)
    normal_advance( dt);

  else{ // in chain
    ws.pop_front();
    auto itr0 = ws.begin();
    current = &(*itr0);
    auto itr1 = itr0;
    ++itr1;
    if (itr1 == ws.end()){
      normal_advance( dt);
      next = nullptr;
      dw = &(current->w);
    }
    else{
      next = &(*itr1);
      set_difference( next->w, current->w, *dw);
    }
  }
}

template< class I>
void Process< I>::set_dim( unsigned int _dim){
  dim = _dim;
  initialize( current->w, dim);
  initialize( dw0, dim);
}

template< class I>
void Process< I>::
set_gaussian_generator( typename I::Gaussian g){
  gaussian = g;
}

template< class I>
typename I::Time Process< I>::time_step() const{
  return current->dt;
}

template< class I>
typename I::Time Process< I>::time() const{
  return current->t;
}

template< class I>
bool Process< I>::at_chain_end() const{
  if (next == nullptr)
    return true;
  else {
    auto b = ws.begin();
    ++b;
    ++b;
    return (b == ws.end());   
  }
}

template< class I>
const typename I::Vector& 
Process< I>::dW() const{
  return *dw;
}

}

#endif
