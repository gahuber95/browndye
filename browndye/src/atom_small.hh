#ifndef __ATOM_SMALL_HH__
#define __ATOM_SMALL_HH__

#include "atom_large.hh"
#include "transform.hh"

struct Atom_Small: public Atom_Large{
public:
  Atom_Small();

  void get_transformed_position( const Transform&, Vec3< Length>&) const;

  bool transform_is_cleared() const{
    return !is_transformed;
  }

  void clear_transformed();

private: 
  mutable Vec3< Length> tpos;
  mutable bool is_transformed;
};

typedef typename Vector< Atom_Small>::iterator Atom_Small_Ref;


inline
void Atom_Small::clear_transformed(){
  is_transformed = false;
}

inline
void Atom_Small::get_transformed_position( const Transform& tform, Vec3< Length>& _tpos) const{
  if (!is_transformed){
    tform.get_transformed( pos, tpos);
    is_transformed = true; 
  }  
  Linalg3::copy( tpos, _tpos);
}

// constructor
inline
Atom_Small::Atom_Small(){
  tpos[0] = tpos[1] = tpos[2] = Length( NAN);
  is_transformed = false;
}

#endif
