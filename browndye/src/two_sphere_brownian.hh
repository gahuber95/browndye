
/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
The Two_Sphere_Brownian::Mover class moves the molecules by a 
Brownian step, given forces, torques, and driving Wiener process.
Molecules are treated like spheres. 
Includes Rotne-Prager hydrodynamics.

Assume dimensionless varibables such that kT and the radius of
the first sphere are 1.

Interface class has following types and static functions:

Spheres

void get_positions( const Spheres& sph, Vec3< double>& r1, Vec3< double>& r2);
void get_rotations( const Spheres& sph, Mat3< double>& rot1, 
                                        Mat3< double>& rot2);

void get_forces( const Spheres& sph, Vec3< double>& f1, Vec3< double>& f2);
void get_torques( const Spheres& sph, Vec3< double>& t1, Vec3< double>& t2);

double gap( Spheres& sph);

void put_positions( const Vec3< double>& r1, const Vec3< double>& r2, 
                    Spheres& sph);
void put_rotations( const Mat3< double>& rot1, const Mat3< double>& rot2, 
                    Spheres& sph);

*/

#ifndef __TWO_SPHERE_BROWNIAN_HH__
#define __TWO_SPHERE_BROWNIAN_HH__

#include "two_sphere_brownian_pre.hh"

namespace Two_Sphere_Brownian{


template< class Interface>
class Mover{
public:
  typedef typename Interface::Spheres Spheres;

  Mover();
  
  void read_mobility_info( const char[]);

  void move_spheres( double dt, const SVec< double, 12>& w, Spheres&) const;

  // radius of second sphere
  void set_radius( double);

  double separation_mobility( double r) const;

  void set_no_hi(){
    no_hi = true;
  }

private:
  bool no_hi;
  double a2;
};

//****************************************************************
template< class I>
double Mover<I>::separation_mobility( double r) const{
  if (no_hi) 
    return //1.0/pi6 + 1.0/(pi6*a2);
      Rotne_Prager::no_hi_mobility( 1.0, 1.0, a2);
  else 
    return // Rotne-Prager
      Rotne_Prager::parallel_mobility( 1.0, 1.0, a2, r);
      //      1.0/pi6 + 1.0/(pi6*a2) - 
      //(1.0/(pi4*r))*(2.0 - (2.0/3.0)*(1.0 + a2*a2)/(r*r));
}

  // constructor
template< class I>
Mover< I>::Mover(){
  no_hi = false;
  a2 = 1.0;
}

  /*
template< class I>
void Mover< I>::read_mobility_info( const char name[]){
  
  std::ifstream input( name);

  if (!input.is_open())
    error( "file ", name, " could not be opened");

  JP::Parser parser( input);
  parser.complete_current_node();
  JP::Node_Ptr node = parser.current_node();
  
  a2 = Two_Sphere_Brownian::read_radius( node);

  no_hi = false;
}
  */

template< class I>
void Mover< I>::set_radius( double a){
  a2 = a;
}


template< class I>
void Mover< I>::
move_spheres( double dt, const SVec< double, 12>& w, 
              typename I::Spheres& sph) const{
  
  Vec3< double> r1, r2;
  Mat3< double> rot1, rot2;

  Vec3< double> f1, f2, t1, t2;
  I::get_positions_and_rotations( sph, r1, rot1, r2, rot2);
  I::get_forces_and_torques( sph, f1, t1, f2, t2);

  double gap = I::gap( sph);

  if (no_hi){          
    move_single_sphere( 1.0, r1, rot1, f1,t1, 
                        w.view<0,6>(), dt);
    move_single_sphere( a2, r2, rot2, f2,t2, 
                        w.view<6,12>(), dt);
  }
  else{
    move_rotne_prager_spheres( a2, 
                               r1, rot1, f1, t1,
                               r2, rot2, f2, t2,
                               w, dt, gap);   
  }    

  I::put_positions_and_rotations( r1, rot1, r2, rot2, sph);
}

}

#endif
