#ifndef __WE_MULTITHREAD_HH__
#define __WE_MULTITHREAD_HH__

/*
The Weighted_Ensemble_Dynamics::Multithreaded::Sampler combines
the Weighted_Ensemble_Dynamics::Sampler class and the
Multithread::Mover class to create a multithreaded version
of the Sampler class.  It has the same functionality and almost
the same interface.
*/

#include "multithread_mover.hh"
#include "we.hh"

namespace Weighted_Ensemble_Dynamics{

namespace Multithreaded{

template< class Interface>
class Sampler;

template< class Interface>
class MT_Interface{
public:
  typedef std::string Exception;
  typedef typename Interface::Info Objects_Ref;
  typedef typename Interface::Object Object_Ref;

  static void do_move( Objects_Ref info, unsigned int ithread, Object_Ref obj){
    Interface::step_forward( info, ithread, obj);
  }

  template< class Func>
  static void apply_to_object_refs( Func& f, Objects_Ref objects){
    Interface::apply_to_object_refs(  f, objects);    
  }

  static void set_null( Objects_Ref& objects){
    Interface::set_null( objects);
  }

  static bool is_null( Objects_Ref objects){
    return Interface::is_null( objects);
  }
};


//#######################################################
template< class Interface>
class WE_Interface{
public:

  typedef typename Interface::Object Object;
  typedef Sampler< Interface>* Info;

  static
  void step_objects_forward( Sampler< Interface>* sampler){
    sampler->mover.do_moves();
  }

  static
  Object new_object( Sampler< Interface>* sampler){
    Object res = Interface::new_object( sampler->info);
    return res;
  }

  static
  void initialize_object( Sampler< Interface>* sampler, Object obj){
    Interface::initialize_object( sampler->info, obj);
  }

  static
  void get_crossing( Sampler< Interface>* sampler, Object obj, 
                     bool& crossed, unsigned int& i){
    Interface::get_crossing( sampler->info, obj, crossed, i); 
  }

  static
  double uniform( Sampler< Interface>* sampler){
    return Interface::uniform( sampler->info);
  }

  static
  unsigned int bin( Sampler< Interface>* sampler, Object obj, bool& exists){
    return Interface::bin( sampler->info, obj, exists);
  }

  static
  unsigned int bin( Sampler< Interface>* sampler, Object obj){
    return Interface::bin( sampler->info, obj);
  }

  static
  void get_duplicated_objects( Sampler< Interface>* sampler, Object obj, 
                               unsigned int n_copies, std::list< Object>& objs){
   Interface::get_duplicated_objects( sampler->info, obj, n_copies, objs);
  }
                                                                                
  static
  unsigned int number_of_bins( Sampler< Interface>* sampler){
    return Interface::number_of_bins( sampler->info);
  }

  static
  Object new_object( Sampler< Interface>* sampler, Object obj){
    return Interface::new_object( sampler->info, obj);
  }

  static
  void remove( Sampler< Interface>* sampler, Object obj){
    Interface::remove( sampler->info, obj);
  }

  static
  bool is_null( Sampler< Interface>* sampler){
    return Interface::is_null( sampler->info);
  }

  static
  void set_null( Sampler< Interface>*& sampler){
    sampler = NULL;
  }

  static
  void output( Sampler< Interface>* sampler, unsigned int i, double& flux){
    Interface::output( sampler->info, i, flux);
  }

  static
  unsigned int number_of_bin_neighbors( Sampler< Interface>* sampler, unsigned int i){
    return Interface::number_of_bin_neighbors( sampler->info, i);
  }

  static
  bool have_population_reversal( Sampler< Interface>* sampler, unsigned int i0, double w0, 
                                 unsigned int i1, double w1){
    return Interface::have_population_reversal( sampler->info, i0,w0, i1,w1);
  }

  template< class Function>
  static
  void apply_to_bin_neighbors( Sampler< Interface>* sampler, unsigned int i, Function& f){
    Interface::apply_to_bin_neighbors( sampler->info, i, f);
  }

  static
  double reaction_coordinate( Sampler< Interface>* sampler, Object obj){
    return Interface::reaction_coordinate( sampler->info, obj);
  }

  static
  double weight( Sampler< Interface>* sampler, Object obj){
    return Interface::weight( sampler->info, obj);
  }

  static
  void set_weight( Sampler< Interface>* sampler, double wt, Object obj){
    Interface::set_weight( sampler->info, wt, obj);
  }

};


//##########################################################
template< class Interface>
class Sampler{
public:
  typedef typename Interface::Info Info;
  typedef typename Interface::Object Object;
 
  void step_objects_forward();
  void set_user_info( Info);
  void set_number_of_objects( unsigned int);
  void set_number_of_fluxes( unsigned int);
  void set_number_of_moves_per_output( unsigned int);
  void initialize_objects();
  void renormalize_objects();
  void renormalize_objects_1D();
  void update_fluxes();
  void set_number_of_threads( unsigned int);

  void generate_bins( std::list< double>& partitions){
    initialize_objects();
    we_sampler.generate_bins( partitions);
  }
 

private:
  friend class MT_Interface< Interface>;
  friend class WE_Interface< Interface>;

  Weighted_Ensemble_Dynamics::Sampler< WE_Interface< Interface> > we_sampler;
  Multithread::Mover< MT_Interface< Interface> > mover;
  Info info;

};

template< class Interface>
void Sampler< Interface>::step_objects_forward(){
  mover.do_moves();
}

template< class Interface>
void Sampler< Interface>::set_number_of_objects( unsigned int n){
  we_sampler.set_number_of_objects( n);
}
                                                                                
template< class Interface>
void Sampler< Interface>::set_number_of_fluxes( unsigned int n){
  we_sampler.set_number_of_fluxes( n);
}

template< class Interface>
void Sampler< Interface>::set_number_of_threads( unsigned int n){
  mover.set_number_of_threads( n);
}
                                                                                
template< class Interface>
void Sampler< Interface>::
set_user_info( typename Interface::Info _info){
  info = _info;
  we_sampler.set_user_info( this);
}
                                                                                
template< class Interface>
void Sampler< Interface>::set_number_of_moves_per_output( unsigned int n){
  we_sampler.set_number_of_moves_per_output( n);
}
                                                                                
template< class Interface>
void Sampler< Interface>::renormalize_objects(){
  we_sampler.renormalize_objects();
}

template< class Interface>
void Sampler< Interface>::renormalize_objects_1D(){
  we_sampler.renormalize_objects_1D();
}
                                             
template< class Interface>
void Sampler< Interface>::update_fluxes(){
  we_sampler.update_fluxes();
}
                                                                                

template< class Interface>
void Sampler< Interface>::initialize_objects(){
  mover.set_objects( info);
  we_sampler.initialize_objects();
}
}
}                                                                                
#endif
