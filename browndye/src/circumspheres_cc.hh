#ifndef __CIRCUMSPHERES_HH__ 
#define __CIRCUMSPHERES_HH__ 


/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/* 
Generic code for finding the smallest bounding sphere of a group
of points.  This implements the algorithm of E. Welzl
("Smallest Enclosing Disks (Balls And Ellipsoids)", 
Lect. Notes Comput. Sci. 555 (1991), 359-370.)

Also used to find the smallest bounding sphere of a collection of
smaller spheres.
 
Equivalent to the Ocaml code circumspheres.ml.

The Interface class has the following types and functions:

typedef Points;
typedef Point_Refs;
typedef size_type
typedef difference_type

static
void initialize( const Points& pts, Point_Refs& refs);

static
size_type size( const Points&);

static
void get_position( const Points& pts, const Point_Refs& refs,
                   size_type i, Length* pos);

static
void swap( Points& pts, Point_Refs& refs, size_type i, size_type j);

// needed by get_smallest_enclosing_sphere_of_spheres
static
Length radius( const Points& pts, const Point_Refs* refs, size_type i);

*/

#include <stdlib.h>
#include <math.h>
#include "error_msg.hh"
#include "units.hh"
#include "solve3_cc.hh"
#include "linalg3.hh"

namespace Circumspheres{

template< class Interface>
void get_smallest_enclosing_sphere( typename Interface::Points& pts_in,
                                    Vec3< Length>& center, Length& radius);


template< class Interface>
void 
get_smallest_enclosing_sphere_of_spheres( typename Interface::Points& pts_in,
                                          Vec3< Length>& center, Length& radius);





//**************************************************************
//**************************************************************
  void get_smallest_triangle_sphere( const Vec3< Length>& p0, 
                                     const Vec3< Length>& p1, 
                                     const Vec3< Length>& p2,
                                     Vec3< Length>& center, 
                                     Length& radius);

void get_smallest_tetrahedron_sphere( 
                                     const Vec3< Length>& p0, 
                                     const Vec3< Length>& p1, 
                                     const Vec3< Length>& p2, 
                                     const Vec3< Length>& p3,
                                     Vec3< Length>& center, 
                                     Length& radius);
template< class I>
class Rmoder{
public:
  typedef typename I::size_type size_type;

  size_type operator()( size_type n, size_type i){
    return (i + n) % n;
  }
};;


namespace L = Linalg3;


template< class U>
inline
void get_avg( const Vec3< U>& p0, const Vec3< U>& p1, Vec3< U>& avg){
  avg[0] = 0.5*( p0[0] + p1[0]);
  avg[1] = 0.5*( p0[1] + p1[1]);
  avg[2] = 0.5*( p0[2] + p1[2]);
}

template< class U>
inline
U max( U a, U b){
  return fvalue(a) > fvalue(b) ? a : b;
}

//********************************************************************

template< class Interface>
void 
get_smallest_enclosing_sphere_inner( 
                                    typename Interface::Points& pts_in,
                                    typename Interface::Point_Refs& pts,
                                    Vec3< Length>& center, Length& radius){

  typedef typename Interface::size_type size_type;
  const size_type n = Interface::size( pts_in);


  Rmoder< Interface> rmod;

  if (n == 0)
    error( "get_smallest_enclosing_sphere: no points");
  else if (n == 1){
    Interface::get_position( pts_in, pts, 0, center);
    radius = Length( 0.0);
  }
  else if (n == 2){
    Vec3< Length> p0, p1;
    Interface::get_position( pts_in, pts, 0, p0);
    Interface::get_position( pts_in, pts, 1, p1);
    get_avg( p0, p1, center);
    radius = max( L::distance( p0, center), L::distance( p1, center));
  }
  else if (n == 3){
    Vec3< Length> p0, p1, p2;
    Interface::get_position( pts_in, pts, 0, p0);
    Interface::get_position( pts_in, pts, 1, p1);
    Interface::get_position( pts_in, pts, 2, p2);
    get_smallest_triangle_sphere( p0, p1, p2, center, radius); 
  }
  else if (n == 4){
    Vec3< Length> p0, p1, p2, p3;
    Interface::get_position( pts_in, pts, 0, p0);
    Interface::get_position( pts_in, pts, 1, p1);
    Interface::get_position( pts_in, pts, 2, p2);
    Interface::get_position( pts_in, pts, 3, p3);
    get_smallest_tetrahedron_sphere( p0, p1, p2, p3, center, radius); 

  }
  else{
    size_type i0 = 0;
    size_type i;

  outer:
    i = rmod( n, i0+4);

    Vec3< Length> p0, p1, p2, p3;
    
    Interface::get_position( pts_in, pts, rmod( n, i0+0), p0); 
    Interface::get_position( pts_in, pts, rmod( n, i0+1), p1); 
    Interface::get_position( pts_in, pts, rmod( n, i0+2), p2); 
    Interface::get_position( pts_in, pts, rmod( n, i0+3), p3); 
    
    get_smallest_tetrahedron_sphere( p0,p1,p2,p3, center, radius);

  inner:    
    Vec3< Length> p;
    Interface::get_position( pts_in, pts, i, p);
    if (L::distance( center, p) > radius){
      i0 = rmod( n, i0-1);
      Interface::swap( pts_in, pts, i0, i);
      goto outer;
    }
    else{
      i = rmod( n, i+1);
      if (!(i == i0))
        goto inner;
    }

  }

  // consistency check
  for (size_type i = 0; i < n; i++){
    Vec3< Length> pos;
    Interface::get_position( pts_in, pts, i, pos);
    Length r = L::distance( pos, center);
    if (r > 1.0001*radius){
      fprintf( stderr, "outsider %g %g\n", fvalue( radius), fvalue( r));
      error( "outsider");
    }
  }
}

//********************************************************************
template< class Interface>
void get_smallest_enclosing_sphere( typename Interface::Points& pts_in,
                                    Vec3< Length>& center, Length& radius){

  typename Interface::Point_Refs pts;
  Interface::initialize( pts_in, pts);
  get_smallest_enclosing_sphere_inner< Interface>( pts_in, 
                                                   pts, center, radius);
}



//********************************************************************
template< class Interface>
void 
get_smallest_enclosing_sphere_of_spheres( typename Interface::Points& pts_in,
                                          Vec3< Length>& center, Length& radius){

  typedef typename Interface::size_type size_type;

  typename Interface::Point_Refs pts;
  Interface::initialize( pts_in, pts);
  get_smallest_enclosing_sphere_inner< Interface>( pts_in, 
                                                   pts, center, radius);

  size_type n = Interface::size( pts_in);
  
  Length max_d = Length( 0.0);
  for( size_type i = 0; i<n; i++){
    Length r = Interface::radius( pts_in, pts, i);
    Vec3< Length> p;
    Interface::get_position( pts_in, pts, i, p);
    Length d = r + L::distance( center, p);
    if (d > max_d)
      max_d = d;
  }
  radius = max_d;
}
}

#endif
