/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/


#ifndef __COLLISION_DETECTOR_HH__
#define __COLLISION_DETECTOR_HH__

/*
Generic code for finding collisions between two rigid collections of spheres,
and for computing pairwise interactions among spheres within a certain
cut-off.


Interface class has following types and static functions:

typedef Container; // of spheres
typedef Refs; // references to spheres in Container
typedef Transform; // translation and rotation
typedef size_type; // integer index to spheres
typedef Ref; // single reference to sphere

void copy_references( const Container&, Refs&);
void copy_references( const Container&, const Refs&, Refs&);
size_type size( const Container&);
size_type size( const Container&, const Refs&);
void get_position( const Container&, const Refs&, size_type i, 
                   Vec3< Length>& position);
void get_transformed_position( const Vec3< Length>&, const Transform&, 
                               Vec3< Length>&);
Length radius( const Container&, const Refs&, size_type i);
void swap( const Container&, const Refs&, size_type i, size_type j);

f has signature bool f( const Vec3< Length>&). The references of all 
spheres whose position gives "false" are placed in "refs0", and those
who give "true" are placed in "refs1". 

template< class F>
void partition_contents( const Container&, const Refs& refs, const F& f, 
                         Refs& refs0, Refs& refs1); 

void empty_container( Container&, Refs&);

Ref reference( const Container&, const Refs&, size_type i);

// called to tell all spheres that previous transforms are now forgotten.
void clear_transform( Container&, Refs&);

End of Interface description

// contains references to all spheres
template< class Interface>
class Structure{
public:
  typedef typename Interface::Container Container;
  typedef typename Interface::Ref Ref;
  
  Structure( Container& conr);

  void set_max_number_per_cell( size_type n);

  void load_objects();

  Ref nearest_object( const Vec3< Length>& r);

  Distance_Finder object has overloaded 
  bool ()( const Container& container, const Refs& refs, Length r, 
           const Vec3< Length>& pos) operator;
  returns "true" if any of the spheres in "refs" is within "r" of
  position "pos".

  template< class Distance_Finder>
  bool is_within( const Distance_Finder&, Length r, const Vec3< Length>& pos);

  makes no attempt to resolve distances > max_distance; will
  simply return infinity
  Distance_Fun object has overloaded 
  double operator()( const Vec3< Length>& pos), which returns
  the distance of position "pos" from the field.

  template< class Distance_Fun>
  Length distance_from_field( const Distance_Fun&, Length max_distance);

  void clear_transform();
  
  bool is_changeable;
};

class I has types Interface0 and Interface1 defined, which are 
the interface types, as described above, for Molecule 0 and Molecule 1.
The user-defined Local_Collision object has overloaded operator
bool operator()( const Interface0::Transform&, 
                 const Interface0::Container&,
                 const Interface0::Refs&,
                 Interface0::Ref&,

                 const Interface1::Transform&, 
                 const Interface1::Container&,
                 const Interface1::Refs&,
                 Interface1::Refs&,

                 Length& violation)
which returns "true" if any of the spheres in the inputs collide,
and also returns a reference to each of the two colliding spheres
and the extent of violation.

template< class I, class Local_Collision_Detector>
bool has_collision( 
                   const Local_Collision_Detector& cd,
                   
                   Structure< typename I::Interface0>& str0, 
                   const typename I::Interface0::Transform& trans0,
                   typename I::Interface0::Ref& ref0,
                   
                   Structure< typename I::Interface1>& str1,
                   const typename I::Interface1::Transform& trans1,
                   typename I::Interface1::Ref& ref1,
                   Length& violation
                   );

Uses user-defined class Interaction_Computer to compute local 
interactions that are within the radius as defined by the interface
function.  Interaction_Computer has the operator

bool operator()( const Interface0::Transform&, 
                 const Interface0::Container&,
                 const Interface0::Refs&,
                 const Interface0::Ref&,

                 const Interface1::Transform&, 
                 const Interface1::Container&,
                 const Interface1::Refs&,
                 const Interface1::Ref&,

                 bool& has_collision,
                 Interface0:Ref& ref0, Interface1:Ref& ref1,
                 Length& violation)
and works very much like the Local_Collision_Detector above,
except that it can accumulate information from the pair-wise
interactions. 

template< class I, class Interaction_Computer>
void compute_interaction( 
                         Interaction_Computer& ic,
                         Structure< typename I::Interface0>& str0, 
                         const typename I::Interface0::Transform& trans0,
                         Structure< typename I::Interface1>& str1,
                         const typename I::Interface1::Transform& trans1,
                         bool& has_collision,
                         typename I::Interface0::Ref& ref0,
                         typename I::Interface1::Ref& ref1,
                         Length& violation
                         );

*/

#include <stdlib.h>
#include <cfloat>
#include <math.h>
#include "vector.hh"
#include <algorithm>
#include <list>
#include <queue>
#include "circumspheres_cc.hh"
#include "linalg3.hh"


//***********************************************************************
namespace Collision_Detector{
  static const unsigned int X = 0;
  static const unsigned int Y = 1;
  static const unsigned int Z = 2;

  namespace L = Linalg3;


//*************************************************************
class Partitioner{
public:
  Partitioner( const Vec3< Length>& _com, const Vec3< double>& _axis, 
               Length _split){
    for( unsigned int k=0; k<3; k++){
      com[k] = _com[k];
      axis[k] = _axis[k];
    }
    split = _split;
  }

  bool operator()( const Vec3< Length>& r) const{
    Length proj = Length( 0.0);
    for( unsigned int k=0; k<3; k++){
      proj += (r[k] - com[k])*axis[k];
    }
    return proj > split;    
  }

  Vec3< Length> com;
  Vec3< double> axis;
  Length split;
};

//*************************************************************
template< class Interface, bool is_changeable>
class Changeable_Part;

template< class Interface>
class Changeable_Part< Interface, true>{
public:
  
  Changeable_Part(){
    is_transformed = false;
    tcenter[0] = tcenter[1] = tcenter[2] = Length( NAN);
  }

  void clear_transform_super(){
    if (is_transformed)
      is_transformed = false;
  }

  const Vec3< Length>& transformed_center( const Vec3< Length>& center){
    return tcenter;
  }

  void transform_super( const typename Interface::Transform& tform,
                        const Vec3< Length>& center){
    if (!is_transformed){
      Interface::get_transformed_position( center, tform, tcenter);
      is_transformed = true;
    }
  }

  Vec3< Length> tcenter;
  bool is_transformed;
};

template< class Interface>
class Changeable_Part< Interface, false>{
public:

  void clear_transform_super(){}
  
  void transform_super( const typename Interface::Transform& tform,
                        const Vec3< Length>& center){}

  const Vec3< Length>& transformed_center( const Vec3< Length>& center){
    return center;
  }
};

template< class Interface>
class Cell: public Changeable_Part< Interface, Interface::is_changeable>{
public:
  typedef typename Interface::Container Container;
  typedef typename Interface::Refs Refs;
  typedef typename Interface::Ref Ref;
  typedef typename Interface::size_type size_type;
  static const bool is_changeable = Interface::is_changeable;
  typedef Changeable_Part< Interface, is_changeable> Super;

  Cell(){
    child0 = NULL;
    child1 = NULL;
  }

  ~Cell();
  
  void clear_transform( Container&);

  void load_objects( Container& conr, size_type max_n){
    Interface::copy_references( conr, contents);
    split( conr, max_n);
  }

  void split( Container&, size_type max_n);

  bool is_bottom() const{
    return (child0 == NULL) & (child1 == NULL); 
  }

  void transform( const typename Interface::Transform&);

  Vec3< Length> center;
  Length radius;

  Refs contents;
  
  Cell< Interface>* child0;
  Cell< Interface>* child1;
};

template< class Interface>
void Cell< Interface>::transform( const typename Interface::Transform& tform){
  if (is_changeable)
    Super::transform_super( tform, center);
}


template< class Interface>
void Cell< Interface>::clear_transform( typename Interface::Container& conr){

  if (is_changeable){
    Super::clear_transform_super();
    
    if (child0 != NULL)
      child0->clear_transform( conr);

    if (child1 != NULL)
      child1->clear_transform( conr);

    if (child0 == NULL && child1 == NULL)
      Interface::clear_transform( conr, contents);      
  }
  
}

template< class Interface>
Cell< Interface>::~Cell(){
  if (!(child0 == NULL))
    delete child0;
  if (!(child1 == NULL))
    delete child1;
}
  
//*************************************************************
template< class I, class CD>
bool has_collision(
                   const CD& cd,
                   
                   Cell< typename I::Interface0>& cell0, 
                   const typename I::Interface0::Transform& trans0,
                   typename I::Interface0::Container& conr0,
                   typename I::Interface0::Ref& ref0,
                   
                   Cell< typename I::Interface1>& cell1,
                   const typename I::Interface1::Transform& trans1,
                   typename I::Interface1::Container& conr1,
                   typename I::Interface1::Ref& ref1,
                   Length& viol
                   ){

  cell0.transform( trans0);
  cell1.transform( trans1);
  const Length dist = L::distance( 
                                  cell0.transformed_center( cell0.center), 
                                  cell1.transformed_center( cell1.center));

  if (dist != dist)
    error( "nan encountered");

  bool res;
  if (dist > (cell0.radius + cell1.radius))
    res = false;
  else {
    if (!cell0.is_bottom() && !cell1.is_bottom()){
           if (has_collision<I,CD>( cd, 
                                    *(cell0.child0), trans0, conr0, ref0, 
                                    *(cell1.child0), trans1, conr1, ref1, 
                                    viol))
        res = true;
      else if (has_collision<I,CD>( cd, 
                                    *(cell0.child0), trans0, conr0, ref0,
                                    *(cell1.child1), trans1, conr1, ref1, 
                                    viol))
        res = true;
      else if (has_collision<I,CD>( cd, 
                                    *(cell0.child1), trans0, conr0, ref0,
                                    *(cell1.child1), trans1, conr1, ref1, 
                                    viol))
        res = true;
      else if (has_collision<I,CD>( cd, 
                                    *(cell0.child1), trans0, conr0, ref0,
                                    *(cell1.child0), trans1, conr1, ref1, 
                                    viol))
        res = true;
      else
        res = false;
    }
    else if (cell0.is_bottom() && !cell1.is_bottom()){
      if (has_collision<I,CD>( cd, 
                               cell0, trans0, conr0, ref0, 
                               *(cell1.child0), trans1, conr1, ref1, 
                               viol))
        res = true;
      else if (has_collision<I,CD>( cd, 
                                    cell0, trans0, conr0, ref0, 
                                    *(cell1.child1), trans1, conr1, ref1, 
                                    viol))
        res = true;
      else
        res = false;
    }
    else if (cell1.is_bottom() && !cell0.is_bottom()){
      if (has_collision<I,CD>( cd, 
                               *(cell0.child1), trans0, conr0, ref0, 
                               cell1, trans1, conr1, ref1, 
                               viol))
        res = true;
      else if (has_collision<I,CD>( cd, 
                                    *(cell0.child0), trans0, conr0, ref0, 
                                    cell1, trans1, conr1, ref1, 
                                    viol))
        res = true;
      else
        res = false;
    }
    else { // both bottom

      res = cd( trans0, conr0, cell0.contents, ref0,   
                 trans1, conr1, cell1.contents, ref1, 
                 viol);

    }
  }
  return res;
}


//*************************************************************
template< class I, class Interaction_Computer>
void compute_interaction( 
                         Interaction_Computer& ic,
                         Cell< typename I::Interface0>& cell0, 
                         const typename I::Interface0::Transform& trans0,
                         typename I::Interface0::Container& conr0,
                         Cell< typename I::Interface1>& cell1,
                         const typename I::Interface1::Transform& trans1,
                         typename I::Interface1::Container& conr1,
                         bool& has_collision,
                         typename I::Interface0::Ref& ref0,
                         typename I::Interface1::Ref& ref1,
                         Length& viol
                         ){

  cell0.transform( trans0);
  cell1.transform( trans1);

  const Length dist = L::distance( cell0.transformed_center( cell0.center), 
                                   cell1.transformed_center( cell1.center));

  has_collision = false;
  if (dist <= (cell0.radius + cell1.radius)){
    if (!cell0.is_bottom() && !cell1.is_bottom()){
      compute_interaction<I>( ic, 
                              *(cell0.child0), trans0, conr0, 
                              *(cell1.child0), trans1, conr1, 
                              has_collision, ref0, ref1, viol);
      
      if (!has_collision)
        compute_interaction<I>( ic, 
                                *(cell0.child0), trans0, conr0, 
                                *(cell1.child1), trans1, conr1, 
                                has_collision, ref0, ref1, viol);
      else
        return;

      if (!has_collision)
        compute_interaction<I>( ic, 
                                *(cell0.child1), trans0, conr0, 
                                *(cell1.child1), trans1, conr1, 
                                has_collision, ref0, ref1, viol);
      else
        return;
      
      if (!has_collision)
        compute_interaction<I>( ic, 
                                *(cell0.child1), trans0, conr0, 
                                *(cell1.child0), trans1, conr1, 
                                has_collision, ref0, ref1, viol);
    }

    else if (cell0.is_bottom() && !cell1.is_bottom()){

      compute_interaction<I>( ic, 
                              cell0, trans0, conr0, 
                              *(cell1.child0), trans1, conr1, 
                              has_collision, ref0, ref1, viol);
      
      if (!has_collision)
        compute_interaction<I>( ic, 
                                cell0, trans0, conr0, 
                                *(cell1.child1), trans1, conr1, 
                                has_collision, ref0, ref1, viol);
    }
    else if (cell1.is_bottom() && !cell0.is_bottom()){
      compute_interaction<I>( ic, 
                              *(cell0.child1), trans0, conr0, 
                              cell1, trans1, conr1, 
                              has_collision, ref0, ref1, viol);

      if (!has_collision)
        compute_interaction<I>( ic,                             
                                *(cell0.child0), trans0, conr0,
                                cell1, trans1, conr1, 
                                has_collision, ref0, ref1, viol);
    }
    else { // both bottom
      ic( trans0, conr0, cell0.contents,   
          trans1, conr1, cell1.contents, has_collision, ref0, ref1, viol);
    }
  }
}

//*************************************************************
template< class Container, class Refs>
struct CPoints{
  Container& container;
  Refs& refs;
  
  CPoints( Container& _container, Refs& _refs):
    container( _container), refs( _refs){}
};

//*************************************************************
template< class Interface>
class Circumspheres_Interface{
public:
  typedef typename Interface::Container Container;
  typedef typename Interface::size_type size_type;
  typedef typename Interface::difference_type difference_type;

  typedef typename Interface::Refs Point_Refs;
  typedef CPoints< Container, Point_Refs> Points;
  
  static
  void initialize( const Points& points, Point_Refs& refs){
    Interface::copy_references( points.container, 
                                points.refs, refs);
  }

  static
  size_type size( const Points& points){
    return Interface::size( points.container, points.refs);
  }

  static
  void get_position( const Points& pts, const Point_Refs& refs,
                     size_type i, Vec3< Length>& pos){
    Interface::get_position( pts.container, refs, i, pos);
  }

  static
  Length radius( const Points& pts, const Point_Refs& refs, size_type i){
    return Interface::radius( pts.container, refs, i);
  }

 
  static
  void swap( Points& pts, Point_Refs& refs, size_type i, size_type j){
    Interface::swap( pts.container, refs, i, j);
  }
  

};


//*************************************************************
template< class Interface> 
void Cell< Interface>::split( typename Interface::Container& conr, 
                              typename Interface::size_type max_n){

  typedef Circumspheres_Interface< Interface> CInterface;
  CPoints< 
    typename Interface::Container, 
    typename Interface::Refs> cpoints( conr, contents);


  Length r;
  Circumspheres::
    get_smallest_enclosing_sphere_of_spheres< CInterface>( cpoints, 
                                                           center, r);

  radius = 1.1*r;

  size_type n = Interface::size( conr, contents);
  
  if (n > max_n){

    Vec3< Length> com;
    com[0] = com[1] = com[2] = Length( 0.0);

    for( size_type i=0; i<n; i++){
      Vec3< Length> pos;
      Interface::get_position( conr, contents, i, pos);
      com[0] += pos[0];
      com[1] += pos[1];
      com[2] += pos[2];
    }  
    com[0] /= n;
    com[1] /= n;
    com[2] /= n;
    
    const Length lnan = Length( NAN);
    Vec3< Length> diff( lnan, lnan, lnan);
    Vec3< double> axis;
    {
      Length dmax = Length( 0.0);
      for( size_type i=0; i<n; i++){
        Vec3< Length> pos;
        Interface::get_position( conr, contents, i, pos);
        Length r = Interface::radius( conr, contents, i);
        Length d = L::distance( com, pos) + r;
        if (d > dmax){
          dmax = d;
          L::get_diff( pos, com, diff);
        }
      }  

      Length anorm = L::norm( diff);
      L::get_scaled( 1.0/anorm, diff, axis);
    }
   
    Vector< Length> projs( n);
    for( size_type i=0; i<n; i++){
      Vec3< Length> pos;
      Interface::get_position( conr, contents, i, pos);

      Length proj = Length( 0.0);
      for( unsigned int k=0; k<3; k++){
        proj += (pos[k] - com[k])*axis[k];
      }
      projs[i] = proj;
    }

    std::sort( projs.begin(), projs.end());
    
    size_type hn = n/2;
    Length split = 
      (n % 2 == 0) ? 0.5*( projs[ hn-1] + projs[ hn]) : projs[ hn];
    

    Partitioner pr( com, axis, split);
    
    child0 = new Cell< Interface>();
    child1 = new Cell< Interface>();

    Interface::partition_contents( conr, contents, pr, 
                                   child0->contents, child1->contents);
      
    child0->split( conr, max_n);
    child1->split( conr, max_n);

    Interface::empty_container( conr, contents);
  }  
}

//*************************************************************
template< class Interface>
class Structure{

public:
  typedef typename Interface::Container Container;
  typedef typename Interface::Ref Ref;
  typedef typename Interface::Transform Transform;
  typedef typename Interface::size_type size_type;
  
  Structure( Container& conr): container( conr), cell(){
    max_n = 2; 
  }

  void set_max_number_per_cell( size_type n);

  void load_objects();

  Ref nearest_object( const Vec3< Length>& r);

  template< class Distance_Finder>
  bool is_within( const Distance_Finder&,  
                  Length r, const Vec3< Length>& pos) const;

  template< class Distance_Fun>
  Length distance_from_field( const Distance_Fun&, Length max_distance);


private: 

  template< class I, class CD>
  friend
  bool has_collision( 
                     const CD& cd,
                     
                     Structure< typename I::Interface0>& str0, 
                     const typename I::Interface0::Transform& trans0,
                     typename I::Interface0::Ref& ref0,
                     
                     Structure< typename I::Interface1>& str1,
                     const typename I::Interface1::Transform& trans1,
                     typename I::Interface1::Ref& ref1,
                     Length& violation             
                     );
  

  template< class I, class Interaction_Computer>
  friend
  void compute_interaction( 
                           Interaction_Computer& ic,
                           Structure< typename I::Interface0>& str0, 
                           const typename I::Interface0::Transform& trans0,
                           Structure< typename I::Interface1>& str1,
                           const typename I::Interface1::Transform& trans1,
                           bool& has_collision,
                           typename I::Interface0::Ref& ref0,
                           typename I::Interface1::Ref& ref1,
                           Length& viol
                            );
  
  size_type max_n;
  Container& container;
  Cell< Interface> cell;

};


template< class Interface>
inline
void Structure< Interface>::load_objects(){
  cell.load_objects( container, max_n);
}
  
template< class Interface>
inline
void Structure< Interface>::set_max_number_per_cell( size_type n){
  max_n = n;
}

//*************************************************************
template< class I, class CD>
bool has_collision( 
                   const CD& cd,
                   
                   Structure< typename I::Interface0>& str0, 
                   const typename I::Interface0::Transform& trans0,
                   typename I::Interface0::Ref& ref0,
                   
                   Structure< typename I::Interface1>& str1,
                   const typename I::Interface1::Transform& trans1,
                   typename I::Interface1::Ref& ref1,
                   Length& violation               
                   ){

  bool res =  has_collision< I, CD>( 
                                 cd, 
                                 str0.cell, trans0, str0.container, ref0, 
                                 str1.cell, trans1, str1.container, ref1,
                                 violation);

  str0.cell.clear_transform( str0.container);
  str1.cell.clear_transform( str1.container);
  
  return res;
}

//*************************************************************
template< class I, class Interaction_Computer>
void compute_interaction( 
                         Interaction_Computer& ic,
                         Structure< typename I::Interface0>& str0, 
                         const typename I::Interface0::Transform& trans0,
                         Structure< typename I::Interface1>& str1,
                         const typename I::Interface1::Transform& trans1,
                         bool& has_collision,
                         typename I::Interface0::Ref& ref0,
                         typename I::Interface1::Ref& ref1,
                         Length& viol
                         ){
  
  compute_interaction<I>( ic, 
                          str0.cell, trans0, str0.container, 
                          str1.cell, trans1, str1.container, 
                          has_collision, ref0, ref1, viol);

  str0.cell.clear_transform( str0.container);
  str1.cell.clear_transform( str1.container);
}

//*************************************************************
template< class Interface>
struct Queue_Info{
  Queue_Info( const Cell< Interface>* _cell, const Vec3< Length>& pos){
    cell = _cell;
    Length dist = L::distance( pos, cell->center);
    high_bound = dist + cell->radius;
    low_bound = dist - cell->radius;
  }

  Length low_bound, high_bound;
  const Cell< Interface>* cell;

  Queue_Info(){
    cell = NULL;
  }
};

template< class Interface>
struct Queue_Comparer{
  bool operator()( const Queue_Info< Interface>& q0, 
                   const Queue_Info< Interface>& q1){
    return q0.high_bound > q1.high_bound;
  }
};


//*************************************************************
// pos must be in the frame of the Structure

template< class I>
template< class Distance_Finder>
bool Structure< I>::is_within( const Distance_Finder& df,
                               Length r, 
                               const Vec3< Length>& pos) const{

  typedef Cell< I> Cell;
  typedef Queue_Info< I> Queue_Info;

  bool result = false;  

  if (L::distance( this->cell.center, pos) < r + this->cell.radius){

    std::priority_queue< Queue_Info, Vector< Queue_Info>, 
      Queue_Comparer< I> > cells;

    cells.push( Queue_Info( &(this->cell), pos));
    
    while( !cells.empty()){
      Queue_Info qi = cells.top();
      cells.pop();
      
      const Cell* lcell = qi.cell;
      
      if (qi.high_bound < r){
        result = true;
        break;
      }
      else if (qi.low_bound < r){

        if (lcell->is_bottom()){
          bool within = df( container, lcell->contents, r, pos);
          if (within){
            result = true;
            break;
          }       
        }
        else { // not bottom
          const Cell* child0 = lcell->child0;
          const Cell* child1 = lcell->child1;

          cells.push( Queue_Info( child0, pos));
          cells.push( Queue_Info( child1, pos));
        }
      }
    }
  }

  return result;
}
  
//*************************************************************
template< class Interface>
typename Interface::Ref 
Structure< Interface>::nearest_object( const Vec3< Length>& r){

  typedef typename Interface::Ref Ref;
  typedef Cell< Interface> Cell;
  typedef Queue_Info< Interface> Queue_Info;

  std::priority_queue< Queue_Info, Vector< Queue_Info>, 
    Queue_Comparer< Interface> > cells;

  cells.push( Queue_Info( &(this->cell), r));
  

  Length closest = Length( DBL_MAX);
  Length high_bound = Length( DBL_MAX);

  Ref result;

  while( !cells.empty()){
    Queue_Info qi = cells.top();
    cells.pop();

    Cell* lcell = qi.cell;

    Length llow_bound = qi.low_bound;
    Length lhigh_bound = qi.high_bound;

    if (llow_bound < high_bound){

      if (lhigh_bound < high_bound)
        high_bound = lhigh_bound;
    
      if (lcell->is_bottom()){
        Length dist = Length( INFINITY);
        Ref lref;

        size_type n = Interface::size( container, lcell->contents);
        for( size_type i=0; i<n; i++){
          Vec3< Length> pos;
          Interface::get_position( container, lcell->contents, i, pos);
          Length radius = Interface::radius( container, lcell->contents, i);
          Length d = L::distance( r, pos) + radius;
          if (d < dist){
            dist = d;
            lref = Interface::reference( container, lcell->contents, i);
          }
        }

        if (dist < closest){
          closest = dist;
          result = lref;
        }
      }
      else{
   
        cells.push( Queue_Info( lcell->child0, r));
        cells.push( Queue_Info( lcell->child1, r));

      }
    } 
  }
  return result;

}

//*************************************************************
template< class Interface>
class Small_Queue_Info{
  Small_Queue_Info( Cell< Interface>* _cell, const Length dist){
    cell = _cell;
    low_bound = dist - cell->radius;
    high_bound = dist + cell->radius;
  }

  Length low_bound, high_bound;
  Cell< Interface>* cell;
};

template< class Interface>
struct Small_Queue_Comparer{
  bool operator()( const Small_Queue_Info< Interface>& q0, 
                   const Small_Queue_Info< Interface>& q1){
    return q0.low_bound > q1.low_bound;
  }
};


//*************************************************************
// makes no attempt to resolve distances > max_distance; will
// simply return infinity
template< class Interface> template< class Distance_Fun>
Length Structure< Interface>::
distance_from_field( const Distance_Fun& distance_fun, Length max_distance){

  typedef Cell< Interface> Cell;
  typedef Small_Queue_Info< Interface> Small_Queue_Info;

  std::priority_queue< Small_Queue_Info, Vector< Small_Queue_Info>, 
    Small_Queue_Comparer< Interface> > cells;

  cells.push( Small_Queue_Info( &(this->cell), 
                                distance_fun( this->cell->center)));

  Length high_bound = Length( DBL_MAX);
  Length min_distance = Length( DBL_MAX);

  while( !cells.empty()){
    Small_Queue_Info qi = cells.top();
    cells.pop();

    Cell* lcell = qi.cell;

    if (lcell->high_bound < high_bound)
      high_bound = lcell->high_bound;
    
    if (lcell->low_bound < high_bound && lcell->low_bound < max_distance){

      if (lcell->is_bottom()){
        Length dist = 
          Interface::minimum_distance( container, 
                                       lcell->contents, distance_fun);
        if (dist < min_distance)
          min_distance = dist;

        if (dist < high_bound)
          high_bound = dist;

      }
      else { // not bottom
        Cell* child0 = lcell->child0;
        Length r0 = distance_fun( child0->center);
        cells.push( Small_Queue_Info( child0, r0));

        Cell* child1 = lcell->child1;
        Length r1 = distance_fun( child1->center);
        cells.push( Small_Queue_Info( child1, r1));
      }
    }
  }
  return min_distance;
}

}

#endif
