/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
Implementation of NAM_Simulator
*/

#include <limits>
#include <time.h>
#include "nam_simulator.hh"
#include "node_info.hh"
#include "trajectory_outputter.hh"
#include "molecule.hh"

namespace L = Linalg3;

// constructor
NAM_Simulator::NAM_Simulator(){
  n_trajs = 0;
  n_trajs_completed = 0;
  n_trajs_started = 0;
  n_stuck = 0;
  n_escaped = 0;
  max_n_steps = std::numeric_limits< unsigned int>::max();
  n_steps_per_output = std::numeric_limits< unsigned int>::max();
  n_trajs_per_output = 1;
  do_output_rng_state = false;
}

//**************************************************************
// Outputting code

typedef std::string String;

std::string uitoa( unsigned int i){
  std::ostringstream stm;
  stm << i;
  std::string res( stm.str().c_str());
  return res;
}

void output_state( const Molecule_Pair& mpair, 
                   Trajectory_Outputter& outputter){

          
  Energy vnear, vcoul, vdes;
  mpair.get_potential_energy( vnear, vcoul, vdes);
  //get_potential_energy( mpair.common.mol0, mpair.state.mol0,
    //                    mpair.common.mol1, mpair.thread.mol1,
      //                  mpair.state.mol1,
        //                vnear, vcoul, vdes
          //              );
  
  Time time = mpair.time();

  Vec3< Length> trans;
  Mat3< double> rot;
  mpair.get_translation1( trans);
  mpair.get_rotation1( rot);
  
  outputter.output_state( time, trans, rot, vnear, vcoul, vdes);
}

//************************************************************************
// Movement code

void NAM_Simulator::run(){

  const size_type nrxns = common.n_reactions();

  completed_rxns.resize( nrxns+1);
  std::fill( completed_rxns.begin(), completed_rxns.end(), 0);
  n_trajs_completed = 0;
  n_trajs_started = 0;

  multi_mover.do_moves();

}

void NAM_Simulator::run_individual( Molecule_Pair& mpair){

  unsigned long int istep = 0;

  Trajectory_Outputter outputter;

  std::ofstream traj_file, index_file;
  bool output_trajectories = !(traj_file_name.empty());
  bool output_min_rxn_coord = !(min_rxn_coord_name.empty());

  if (output_trajectories){

    std::string number( uitoa( mpair.thread_number()));
    std::string suffix( ".xml");
    std::string itext( ".index");

    traj_file.open( (traj_file_name + number + suffix).c_str());
    index_file.open( (traj_file_name + number + itext + suffix).c_str());

    if (!(traj_file.is_open()))
      error( "trajectory file ", traj_file_name.c_str(), " not opened\n");

    //outputter.initialize( traj_file, mpair.common.mol0.hydro_ellipsoid.center,
      //                    mpair.common.mol1.hydro_ellipsoid.center,
        //                  index_file);
    outputter.initialize( traj_file, mpair.hydro_center0(), mpair.hydro_center1(), index_file);
  }

  // total number of elementary BD steps
  unsigned long int n_steps_tot = 0;
  while (true){

    pthread_mutex_lock( &mutex);
    if (n_trajs_started >= n_trajs){
      pthread_mutex_unlock( &mutex);
      break;
    }
    else
      ++n_trajs_started;

    pthread_mutex_unlock( &mutex);

    mpair.set_initial_state();
    size_type istate = mpair.current_reaction_state();

    if (output_trajectories){
      outputter.start_trajectory();
      //outputter.start_subtrajectory( mpair.common.pathway->state_name( istate));
      outputter.start_subtrajectory( mpair.state_name( istate));
    }
    
    unsigned long int n_steps = 0;
    Molecule_Pair_Fate fate;
    // stays in loop for one trajectory
    while (true){

      if (output_trajectories && 
          (mpair.n_full_steps() % n_steps_per_output == 0)){
        output_state( mpair, outputter);
      }

      mpair.do_step( n_steps);
      fate = mpair.fate();
      ++istep;
      
      size_type new_istate = mpair.current_reaction_state();

      if (new_istate != istate){
        if (output_trajectories && 
            (fate == In_Action) && (mpair.n_full_steps() <= max_n_steps)){
          outputter.end_subtrajectory( fate, 
                                       mpair.reaction_name( mpair.last_reaction()),
                                       mpair.state_name( new_istate));

          outputter.start_subtrajectory( mpair.state_name( new_istate));
        }

        if (output_trajectories)
          output_state( mpair, outputter);
        
        istate = new_istate;       

        pthread_mutex_lock( &mutex);    
        ++completed_rxns[ mpair.last_reaction()];
        pthread_mutex_unlock( &mutex);
      }

      if (fate != In_Action){
        pthread_mutex_lock( &mutex);

        ++n_trajs_completed;

        if (fate == Escaped)
          ++n_escaped;

        pthread_mutex_unlock( &mutex);
        break;
      }

      if (mpair.n_full_steps() > max_n_steps){
        fate = Stuck;
        pthread_mutex_lock( &mutex);

        ++n_stuck;
        ++n_trajs_completed;

        pthread_mutex_unlock( &mutex);
        break;
      }

    } // end trajectory

    n_steps_tot += n_steps;
    
    if (output_trajectories){
      const String reaction_name = fate == Final_Rxn ?  
        mpair.reaction_name( mpair.last_reaction()) :
        String( " ");

      outputter.end_subtrajectory( fate, 
                                   reaction_name,
                                   mpair.state_name( mpair.current_reaction_state()));

      outputter.end_trajectory( fate, 
                                reaction_name,
                                mpair.state_name( mpair.current_reaction_state()),
                                mpair.minimum_reaction_coordinate());

      traj_file.flush();
    }
    
    pthread_mutex_lock( &mutex);
    if ((n_trajs_completed+0) % n_trajs_per_output == 0){

      std::ofstream outp( output_name.c_str());
      begin_output( outp);

      outp << "  <reactions>\n";
      outp << "    <n-trajectories> " << n_trajs_completed << " </n-trajectories>\n";
      outp << "    <stuck> " << n_stuck << " </stuck>\n";
      outp << "    <escaped> " << n_escaped << " </escaped>\n";

      const size_type nrxns = mpair.n_reactions();
      for( size_type irxn = 0; irxn < nrxns; ++irxn){
        outp << "    <completed>\n";
        outp << "      <name> " << 
          mpair.reaction_name( irxn).c_str() << " </name>\n";
        outp << "      <n> " << completed_rxns[ irxn] << " </n>\n"; 
        outp << "    </completed>\n";
      }

      outp << "  </reactions>\n";
      outp << "</rates>\n";
      outp.flush();
      outp.close();
    }
    if (output_min_rxn_coord){
      bool first_one = n_trajs_completed == 1;
      std::ios_base::openmode omode = first_one ? std::ios_base::out : std::ios_base::app;
      std::ofstream outp( min_rxn_coord_name.c_str(), omode);

      if (first_one)
        outp << "<min-dists>\n";

      outp << "  <min-dist> " << fvalue( mpair.minimum_reaction_coordinate()) <<
        " </min-dist>\n";

      if (n_trajs_completed == n_trajs)
        outp << "</min-dists>\n";

      outp.close();
    }
    pthread_mutex_unlock( &mutex);
  }
  
  if (output_trajectories){
    outputter.end_trajectories();
  }
}

void NAM_Multi_Interface::do_move( NAM_Simulator* sim, 
                                   unsigned int ithread, Molecule_Pair* mpair){
  sim->run_individual( *mpair);
}

//**************************************************************
// Initialization code

namespace JP = JAM_XML_Pull_Parser;

void NAM_Simulator::initialize( const std::string& file){
  typedef std::string Str;

  std::ifstream input( file);
  if (!input.is_open())
    error( "file ", file, " could not be opened");

  JP::Parser parser( input);
  JP::Node_Ptr node = initialized_node( parser);

  n_trajs = int_from_node( node, "n-trajectories");

  bool found;
  get_value_from_node( node, "n-steps-per-output", n_steps_per_output, found);
 
  get_value_from_node( node, "n-trajectories-per-output", 
                       n_trajs_per_output, found); 
  get_value_from_node( node, "max-n-steps", max_n_steps, found);

  get_value_from_node( node, "trajectory-file", traj_file_name);
  get_value_from_node( node, "min-rxn-dist-file", min_rxn_coord_name); 

  Str dors;
  get_value_from_node( node, "print-rng-state", dors);
  do_output_rng_state = (dors == Str("yes"));

  for( unsigned long int i = 0; i < mthreads.size(); i++){
    Molecule_Pair_State* new_state = new Molecule_Pair_State( i);
    states.insert( new_state);
  }
    
  typedef std::set< Molecule_Pair_State*>::iterator Itr;
  size_type i = 0;
  for (Itr itr = states.begin(); itr != states.end(); ++itr){
    Molecule_Pair pair( common, mthreads[i], **itr, i);
    pairs.push_back( pair);
    ++i;
  }

  multi_mover.set_number_of_threads( mthreads.size());

  multi_mover.set_objects( this);

  pthread_mutex_init( &mutex, NULL);
}

NAM_Simulator::~NAM_Simulator(){
  pthread_mutex_destroy( &mutex);
}
