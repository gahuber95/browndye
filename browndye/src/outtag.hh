/*
 * outtag.hh
 *
 *  Created on: Sep 12, 2015
 *      Author: ghuber
 */

#ifndef OUTTAG_HH_
#define OUTTAG_HH_

template< class T>
T output_value( const T& t){
  return t;
}

#ifdef UNITS
template< class M, class L, class T, class Q, class VType>
VType output_value( const Unit< M,L,T,Q,VType>& u){
  return fvalue( u);
}
#endif

//template< class U>
//inline
//void outtag( std::ofstream& output, const char* tag, U value){
//  output << "    <" << tag << "> " << output_value( value)
//         << " </" << tag << ">\n";
//}

template< class U>
inline
void outtag( std::ofstream& output, const std::string& tag, U value, int ident){
  for( int i = 0; i < ident; i++)
    output << " ";

  output << "<" << tag << "> " << output_value( value) << " </" << tag << ">\n";
}

#endif /* OUTTAG_HH_ */
