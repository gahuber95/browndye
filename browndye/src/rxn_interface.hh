/*
 * rxn_interface.hh
 *
 *  Created on: Sep 8, 2015
 *      Author: root
 */

#ifndef RXN_INTERFACE_HH_
#define RXN_INTERFACE_HH_

#include "units.hh"
#include "vector.hh"

// Interface for "pathways.hh"

class Molecule_Pair;

class Rxn_Interface{
public:
  typedef const ::Molecule_Pair Molecule_Pair;
  typedef Vector< int>::size_type size_type;

  static
  Length distance( Molecule_Pair& mpair, size_type i0, size_type i1);

};

#endif /* RXN_INTERFACE_HH_ */
