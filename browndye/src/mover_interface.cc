/*
 * mover_interface.cc
 *
 *  Created on: Sep 8, 2015
 *      Author: root
 */

#include "mover_interface.hh"
#include "linalg3.hh"
#include "molecule.hh"
#include "molecule_pair.hh"

namespace L = Linalg3;

void Mover_Interface::put_positions_and_rotations( const Vec3< double>& rsc0,
          const Mat3< double>& rot0,
          const Vec3< double>& rsc1,
          const Mat3< double>& rot1,
          Spheres& spheres){

  auto& mol0 = spheres.state.mol0;
  auto& mol1 = spheres.state.mol1;

  const Length R = spheres.common.mol0.hydro_radius();
  Vec3< Length> r0, r1;

  L::get_scaled( R, rsc0, r0);
  L::get_scaled( R, rsc1, r1);

  mol0.set_position( r0);
  mol0.set_rotation( rot0);

  mol1.set_position( r1);
  mol1.set_rotation( rot1);
}

void Mover_Interface::
get_positions_and_rotations( const Spheres& spheres,
           Vec3< double>& r0,
           Mat3< double>& rot0,
           Vec3< double>& r1,
           Mat3< double>& rot1
           ){

  const Length R = spheres.common.mol0.hydro_radius();
  Vec3< Length> r;

  spheres.state.mol0.get_position( r);
  L::get_scaled( 1.0/R, r, r0);

  spheres.state.mol1.get_position( r);
  L::get_scaled( 1.0/R, r, r1);

  spheres.state.mol0.get_rotation( rot0);
  spheres.state.mol1.get_rotation( rot1);
}

void Mover_Interface::get_forces_and_torques( const Spheres& spheres,
                Vec3< double>& f0,
                Vec3< double>& t0,
                Vec3< double>& f1,
                Vec3< double>& t1){

  auto kT = spheres.common.solvent.kT;

  const URecip< Force>::Res factor =
    spheres.common.mol0.hydro_radius()/kT;

  const URecip< Torque>::Res tfactor = 1.0/kT;

  Vec3< Force> force0, force1;
  Vec3< Torque> torque0, torque1;

  auto& state = spheres.state;
  state.mol0.get_force( force0);
  state.mol0.get_torque( torque0);
  state.mol1.get_force( force1);
  state.mol1.get_torque( torque1);

  L::get_scaled( factor, force0, f0);
  L::get_scaled( factor, force1, f1);
  L::get_scaled( tfactor, torque0, t0);
  L::get_scaled( tfactor, torque1, t1);
}

double Mover_Interface::gap( const Spheres& spheres){

  if (!spheres.common.no_hi){
    const Length R = spheres.common.mol0.hydro_radius();

    return hydro_gap( spheres.common.mol0, spheres.state.mol0,
          spheres.common.mol1, spheres.thread.mol1,
          spheres.state.mol1)/R;
  }
  else
    return 0.0;
}


