/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/


#include <stdlib.h>
#include <math.h>
#include "linalg3.hh"

namespace Ellipsoid{



Length surface_distance(const SVec< Vec3<double>, 3>& axes,  
                         const Vec3< Length>& distances,
                         const Vec3< double>& u){

  Vec3< double> v;
  for (size_t k = 0; k < 3; k++)
    v[k] = Linalg3::dot( u, axes[k]);

  return 1.0/sqrt( sq( v[0]/distances[0]) + 
                   sq( v[1]/distances[1]) + 
                   sq( v[2]/distances[2]));
  
}


}

/*
Estimates the distance by connecting the ellipsoids by
a line segment and using the distance between intersections.
Overestimates the distance.  There is an iterative
method that approaches the exact answer, 
but it converges slowly and is inefficient.
*/

Length ellipsoid_distance( const SVec< Vec3<double>, 3>& axes0,  
                           const Vec3< Length>& distances0, 
                           const Vec3< Length>& center0,
                           const SVec< Vec3<double>, 3>& axes1,  
                           const Vec3< Length>& distances1,  
                           const Vec3< Length>& center1){


  Vec3< double> u;
  
  Vec3< Length> rd;
  Linalg3::get_diff( center1, center0, rd);  
  Linalg3::get_normed( rd, u);
  

  Length r0 = Ellipsoid::surface_distance( axes0, distances0, u);
  Length r1 = Ellipsoid::surface_distance( axes1, distances1, u);

  const Length r = Linalg3::norm( rd);
 

  return max( r - (r0 + r1), Length( 0.0));
}


