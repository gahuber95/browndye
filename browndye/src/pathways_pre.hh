/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
Defines various helper classes for the Pathway class.
Included by "pathways.hh".
*/

#include <iostream>
#include <stdlib.h>
#include "vector.hh"
#include <algorithm>
#include "units.hh"
#include "jam_xml_pull_parser.hh"
#include "node_info.hh"

namespace Pathways{
  typedef std::string String;
typedef Vector< int>::size_type size_type;

namespace JP = JAM_XML_Pull_Parser;

class Rxn_Pair{
public:
  size_type atom0, atom1;
  Length distance;

  Rxn_Pair( size_type, size_type, Length);
  Rxn_Pair();
};

class Criterion{
public:
  size_type n_needed;
  Vector< Rxn_Pair> pairs;
  Vector< Criterion*> criteria;

  Criterion(){}
  ~Criterion();

private:
  Criterion( const Criterion&);
  Criterion& operator=( const Criterion&);
};

class State{
public:
  size_type n;
  String name;
  Vector< size_type> rxns_to, rxns_from;

  State(){
    n = std::numeric_limits< size_type>::max();
  }
};

class Reaction{
public:
  size_type n;
  String name;
  Criterion* criterion;
  size_type state_before, state_after;
  String state_before_name, state_after_name;

  Reaction(){
    criterion = NULL;
    n = state_before = state_after = 
      std::numeric_limits< size_type>::max();
  }
  ~Reaction();

private:
  Reaction( const Reaction&);
  Reaction& operator=( const Reaction&);  
};

class Pathway_Base{
public:
  Pathway_Base();
  ~Pathway_Base();

  size_type n_states() const;
  size_type n_reactions() const;

  size_type state_before_reaction( size_type irxn) const;
  size_type state_after_reaction( size_type irxn) const;

  size_type n_reactions_from( size_type istate) const;
  size_type reaction_from( size_type istate, size_type i) const;

  void remap_molecule0( const std::vector< size_type>&);
  void remap_molecule1( const std::vector< size_type>&);

  size_type first_state() const;
  const String& reaction_name( size_type) const;
  const String& state_name( size_type) const;

  void initialize( const std::string& file);

  void initialize_from_node( JP::Node_Ptr node);


protected:
  Vector< State> states;
  Vector< Reaction*> reactions;

  std::map< String, size_type> reaction_str2int;
  std::map< String, size_type> state_str2int;

  String first_state_name;
  size_type first_state_number_data;

  void remap_molecule( const std::vector< size_type>& numbers, bool zero);

  void determine_states();

};


}
