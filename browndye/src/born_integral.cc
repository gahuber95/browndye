/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
Generates the desolvation energy grid as described in the paper. Uses
the generic Cartesian multipole code to keep the time within 
reasonable limits.

 */

#include <string.h>
#include <iostream>
#include "units.hh"
#include "vector.hh"
#include "linalg3.hh"
#include "cartesian_multipole.hh"
#include "jam_xml_pull_parser.hh"
#include "node_info.hh"
#include "get_args.hh"

struct Sphere{
  Vec3< double> pos;
  double radius;
};

enum Coord {X,Y,Z};

namespace L = Linalg3;

typedef Vector< Sphere> Spheres;

namespace JP = JAM_XML_Pull_Parser;

typedef Vector< unsigned int>::size_type size_type;

class Grid{
public:
  Grid( JP::Parser&);
  
  unsigned int input( size_type, size_type, size_type) const;
  unsigned int& input( size_type, size_type, size_type);

  double position( size_type ix, size_type iy, size_type iz, Coord dir) const;
  double position( size_type i, Coord dir) const;
  void get_position( size_type ix, size_type iy, size_type iz, 
                     Vec3< double>&) const;

  template< class F>
  void initialize( size_type, size_type, size_type, 
                   const Vec3< double>& low, 
                   const Vec3< double>& spacing, F& f);

  void initialize( size_type, size_type, size_type, const Vec3< double>& low, 
                   const Vec3< double>& spacing);

  double result( size_type, size_type, size_type) const;
  double& result( size_type, size_type, size_type);

  void get_live_bounds( const Vec3< size_type>& bounds, Vec3< size_type>& live_bounds) const;

  size_type nx,ny,nz, nyz;
  size_type max_n;
  Vec3< double> low_corner;
  double hx,hy,hz;

  Vector< unsigned int> data;
  Vector< double> results;

  double debye_length;
  double volume;

private:
  Grid();
  Grid& operator=( const Grid&);
};

void Grid::get_live_bounds( const Vec3< size_type>& bounds, 
                            Vec3< size_type>& live_bounds) const{
  live_bounds[X] = min( bounds[X], nx);
  live_bounds[Y] = min( bounds[Y], ny);
  live_bounds[Z] = min( bounds[Z], nz);
}

class Plane_Getter{
public:
  void operator()( JP::Node_Ptr node){
    std::list< JP::Node_Ptr> rnodes = node->children_of_tag( "row");
    size_type iy = 0;
    for( auto& rnode: rnodes){
      auto& ndata = rnode->data();

      for (size_type iz = 0; iz < nz; iz++)
        grid.input( ix,iy,iz) = stoul( ndata[iz]);
      
      ++iy;
    }
    ++ix;
  }
  
  Plane_Getter( JP::Parser& _parser, Grid& _grid): 
    parser( _parser), grid( _grid){

    ix = 0;
    nx = grid.nx;
    ny = grid.ny;
    nz = grid.nz;
  }

  //Row_Getter rg;
  JP::Parser& parser;
  Grid& grid;
  size_type nx,ny,nz;
  size_type ix;
};
 
// constructor
Grid::Grid( JP::Parser& parser){

  debye_length = INFINITY;

  parser.find_next_tag( "grid");

  parser.find_next_tag( "corner");
  parser.complete_current_node();
  JP::Node_Ptr cnode = parser.current_node();
  auto& cdata = cnode->data();
  low_corner[X] = double( stod( cdata[X]));
  low_corner[Y] = double( stod( cdata[Y]));
  low_corner[Z] = double( stod( cdata[Z]));

  parser.find_next_tag( "npts");
  parser.complete_current_node();
  JP::Node_Ptr npnode = parser.current_node();
  auto& npdata = npnode->data();

  nx = stoul( npdata[X]);
  ny = stoul( npdata[Y]);
  nz = stoul( npdata[Z]);

  nyz = ny*nz;
  max_n = max( max( nx, ny), nz);

  parser.find_next_tag( "spacing");
  parser.complete_current_node();
  JP::Node_Ptr snode = parser.current_node();
  auto& sdata = snode->data();

  hx = stod( sdata[X]);
  hy = stod( sdata[Y]);
  hz = stod( sdata[Z]);

  volume = hx*hy*hz;

  size_type nxyz = nx*ny*nz;
  data.resize( nxyz);
  results.resize( nxyz);

  parser.find_next_tag( "data");
  
  Plane_Getter pg( parser, *this);
  parser.apply_to_nodes_of_tag( "plane", pg);

  for( size_type i = 0; i < nxyz; i++)
    results[i] = 0.0;
}

double Grid::position( size_type i, Coord dir) const{
  if (dir == X){
    return low_corner[X] + ((double)i)*hx;
  }
  else if (dir == Y){
    return low_corner[Y] + ((double)i)*hy;
  }
  else
    return low_corner[Z] + ((double)i)*hz;
}

double Grid::position( size_type ix, size_type iy, size_type iz, Coord dir) const{
  if (dir == X){
    return low_corner[X] + ((double)ix)*hx;
  }
  else if (dir == Y){
    return low_corner[Y] + ((double)iy)*hy;
  }
  else
    return low_corner[Z] + ((double)iz)*hz;
}
  
void Grid::get_position( size_type ix, size_type iy, size_type iz, 
                         Vec3< double>& pos) const{

  pos[X] = low_corner[X] + ((double)ix)*hx;
  pos[Y] = low_corner[Y] + ((double)iy)*hy;
  pos[Z] = low_corner[Z] + ((double)iz)*hz;
}

inline
unsigned int Grid::input( size_type ix, size_type iy, size_type iz) const{
  return data[ iz + nz*iy + nyz*ix];
}

inline
unsigned int& Grid::input( size_type ix, size_type iy, size_type iz){
  return data[ iz + nz*iy + nyz*ix];
}

inline
double& Grid::result( size_type ix, size_type iy, size_type iz){
  return results[ iz + nz*iy + nyz*ix];
}

inline
double Grid::result( size_type ix, size_type iy, size_type iz) const{
  return results[ iz + nz*iy + nyz*ix];
}

Grid::Grid(){
  nx = ny = nz = nyz = max_n = (size_t)(-1);
  low_corner[X] = low_corner[Y] = low_corner[Z] = double( NAN);
  hx = hy = hz = double( NAN);
}

//********************************************************************
struct Grid_Refs{
public:
  Vec3< size_type> lowi, highi;
};

size_type size( const Grid& cont, const Grid_Refs& refs){

  Vec3< size_type> bounds;
  cont.get_live_bounds( refs.highi, bounds); 

  size_type iwx = 
    bounds[X] > refs.lowi[X] ? bounds[X] - refs.lowi[X] : 0;
  size_type iwy = 
    bounds[Y] > refs.lowi[Y] ? bounds[Y] - refs.lowi[Y] : 0;
  size_type iwz = 
    bounds[Z] > refs.lowi[Z] ? bounds[Z] - refs.lowi[Z] : 0;


  size_type res = iwx*iwy*iwz;
  return res;
}

void copy_references( const Grid& cont, Grid_Refs& refs){  
  refs.lowi[X] = refs.lowi[Y] = refs.lowi[Z] = 0;

  refs.highi[X] = cont.nx; 
  refs.highi[Y] = cont.ny; 
  refs.highi[Z] = cont.nz;  
}

void copy_references( const Grid& cont, 
                      const Grid_Refs& refs0,
                      Grid_Refs& refs1
                      ){
    
  L::copy( refs0.lowi, refs1.lowi);
  L::copy( refs0.highi, refs1.highi);
}


void empty_references( const Grid& cont, Grid_Refs& refs){
  L::copy( refs.lowi, refs.highi);
}


double grid_spacing( const Grid& cont, Coord dir){

  Vec3< double> hs;
  hs[X] = cont.hx;
  hs[Y] = cont.hy;
  hs[Z] = cont.hz;

  return hs[dir];
}

template< class Refs_Cube, class V3>
void split_container( const Grid& cont, 
                      const Grid_Refs& refs, 
                      //SVec< double,3>::Const_View center,
                      const V3& center,
                      const Refs_Cube& refs_cube
                      //Grid_Refs* const refs_cube[][2][2]
                      ){
  
  size_type midx = (refs.highi[X] + refs.lowi[X])/2;
  size_type midy = (refs.highi[Y] + refs.lowi[Y])/2;
  size_type midz = (refs.highi[Z] + refs.lowi[Z])/2;

  for( unsigned int ix = 0; ix < 2; ix++)
    for( unsigned int iy = 0; iy < 2; iy++)
      for( unsigned int iz = 0; iz < 2; iz++){
        Grid_Refs& srefs = *(refs_cube(ix,iy,iz));

        srefs.lowi[X] = (1-ix)*refs.lowi[X] + ix*midx;
        srefs.lowi[Y] = (1-iy)*refs.lowi[Y] + iy*midy;
        srefs.lowi[Z] = (1-iz)*refs.lowi[Z] + iz*midz;

        srefs.highi[X] = (1-ix)*midx + ix*refs.highi[X];
        srefs.highi[Y] = (1-iy)*midy + iy*refs.highi[Y];
        srefs.highi[Z] = (1-iz)*midz + iz*refs.highi[Z];        
      }
}

template< class V3>
void get_bounds( const Grid& cont, V3& low, V3& high){

  low[X] = cont.low_corner[X];
  low[Y] = cont.low_corner[Y];
  low[Z] = cont.low_corner[Z];

  high[X] = low[X] + cont.max_n*cont.hx;
  high[Y] = low[Y] + cont.max_n*cont.hy;
  high[Z] = low[Z] + cont.max_n*cont.hz;
}

template< class Function> 
void apply_multipole_function( const Grid& cont, 
                               const Grid_Refs& refs, Function& f){
  
  Vec3< double> pos;
  SVec< double, 1> mpole;

  Vec3< size_type> bounds;
  cont.get_live_bounds( refs.highi, bounds); 

  for( size_type ix = refs.lowi[X]; ix < bounds[X]; ++ix)
    for( size_type iy = refs.lowi[Y]; iy < bounds[Y]; ++iy)
      for( size_type iz = refs.lowi[Z]; iz < bounds[Z]; ++iz){    
        if (cont.input( ix, iy, iz) == 1){
          cont.get_position( ix,iy,iz, pos);
          mpole[0] = cont.volume;
          f( 0, pos, mpole); 
        }
      }
}

double greens_fun( const Grid& cont, double r){
  double L = cont.debye_length;
  double r2 = r*r;
  return sq( 1 + r/L)*exp( -2*r/L)/(r2*r2);
}

double greens_fun0( const Grid& cont, double r){
  double r2 = r*r;
  return 1.0/(r2*r2);
}

size_type n_sources( const Grid& cont, const Grid_Refs& refs){

  Vec3< size_type> bounds;
  cont.get_live_bounds( refs.highi, bounds); 

  size_type sum = 0;
  for( size_type ix = refs.lowi[X]; ix < bounds[X]-1; ++ix)
    for( size_type iy = refs.lowi[Y]; iy < bounds[Y]-1; ++iy)
      for( size_type iz = refs.lowi[Z]; iz < bounds[Z]-1; ++iz){
        if (cont.input( ix,iy,iz) == 1)
          ++sum;
      }
  return sum;
}

size_type n_receivers( const Grid& cont, const Grid_Refs& refs){
  Vec3< size_type> bounds;
  cont.get_live_bounds( refs.highi, bounds); 

  size_type sum = 0;
  for( size_type ix = refs.lowi[X]; ix < bounds[X]; ++ix)
    for( size_type iy = refs.lowi[Y]; iy < bounds[Y]; ++iy)
      for( size_type iz = refs.lowi[Z]; iz < bounds[Z]; ++iz){
        if (cont.input( ix,iy,iz) == 0)
          ++sum;
      }
  return sum;
}

double near_potential( const Grid& cont, 
                       const Grid_Refs& refs, 
                       SVec< double,3>::Const_View pos_in){

  Vec3< double> pos( pos_in);

  Vec3< size_type> bounds;
  cont.get_live_bounds( refs.highi, bounds); 

  const double volume = cont.volume;
  double sum = 0.0;
  for( size_type ix = refs.lowi[X]; ix < bounds[X]-1; ++ix)
    for( size_type iy = refs.lowi[Y]; iy < bounds[Y]-1; ++iy)
      for( size_type iz = refs.lowi[Z]; iz < bounds[Z]-1; ++iz){
        if (cont.input( ix,iy,iz) == 1){
          Vec3< double> gpos;
          cont.get_position( ix, iy, iz, gpos);
          double r = L::distance( pos, gpos);
          sum += volume*greens_fun( cont, r);
        }
      }
  return sum;
}


// r^(-4)
template< class V>
void get_r_derivatives0( const Grid& grid, unsigned int order, double r, 
                         V& derivs){
  derivs[0] = 1.0/(r*r*r*r);
  for (unsigned int i = 1; i <= order; i++){
    derivs[i] = -((3+i)*derivs[i-1]/r);
  }

}


// (1 + r/L)^2 exp(-2r/L) r^(-4)
template< class Vec>
void get_r_derivatives( const Grid& grid, 
                        unsigned int order, double r, Vec& derivs){

  double L = grid.debye_length;

  unsigned int order1 = order + 1;
  Vector< double> terms2( order1), terms1( order1), terms0( order1);
  Vector< double> new_terms2( order1), new_terms1( order1), new_terms0( order1);

  for( unsigned int i = 0; i <= order; i++){
    terms2[i] = 0.0;
    terms1[i] = 0.0;
    terms0[i] = 0.0;
  }

  const double exp_term = exp(-2*r/L);
  const double sq_term = 1 + r/L;
  const double r4 = r*r*r*r;
  double V = sq_term*sq_term*exp_term/r4;
 
  terms2[0] = V;

  for( unsigned int io = 0; io <= order; io++){

    double sum = 0.0;
    for( unsigned int i = 0; i <= order; i++){
      sum += terms0[i] + terms1[i] + terms2[i];
    }
    derivs[io] = sum;

    for( unsigned int i = 0; i <= order; i++){
      new_terms2[i] = 0.0;
      new_terms1[i] = 0.0;
      new_terms0[i] = 0.0;
    }
    
    for( unsigned int i = 0; i < order; i++){
      new_terms1[i] += 2*terms2[i]/(L*sq_term);
      new_terms0[i] += terms1[i]/(L*sq_term);

      new_terms0[i] -= 2*terms0[i]/L;
      new_terms1[i] -= 2*terms1[i]/L;
      new_terms2[i] -= 2*terms2[i]/L;

      new_terms2[i+1] -= terms2[i]*(i+4)/r;
      new_terms1[i+1] -= terms1[i]*(i+4)/r;
      new_terms0[i+1] -= terms0[i]*(i+4)/r;
    }

    for( unsigned int i = 0; i <= order; i++){
      terms0[i] = new_terms0[i];
      terms1[i] = new_terms1[i];
      terms2[i] = new_terms2[i];
    }
  }
}

template< class F>
void apply_potential( Grid& cont, Grid_Refs& refs, F& f){

  Vec3< size_type> bounds;
  cont.get_live_bounds( refs.highi, bounds); 

  size_type nxh = bounds[X];
  size_type nyh = bounds[Y];
  size_type nzh = bounds[Z];

  size_type nxl = refs.lowi[X];
  size_type nyl = refs.lowi[Y];
  size_type nzl = refs.lowi[Z];

  for (size_type ix = nxl; ix < nxh; ix++)
    for (size_type iy = nyl; iy < nyh; iy++)
      for (size_type iz = nzl; iz < nzh; iz++)
        if (cont.input( ix,iy,iz) == 0){
          Vec3< double> pos;
          cont.get_position( ix,iy,iz,pos);       
          double V = f( pos);
          cont.result( ix,iy,iz) += V;
        } 
}

void process_bodies( Grid& cont, Grid_Refs& refs){

  Vec3< size_type> bounds;
  cont.get_live_bounds( refs.highi, bounds); 

  size_type nxh = bounds[X];
  size_type nyh = bounds[Y];
  size_type nzh = bounds[Z];
  size_type nxl = refs.lowi[X];
  size_type nyl = refs.lowi[Y];
  size_type nzl = refs.lowi[Z];
  const double volume = cont.volume;

  for (size_type ix = nxl; ix < nxh; ix++)
    for (size_type iy = nyl; iy < nyh; iy++)
      for (size_type iz = nzl; iz < nzh; iz++)
        if (cont.input( ix,iy,iz) == 0){
          Vec3< double> posi;
          cont.get_position( ix,iy,iz, posi);

          double sum = 0.0;
          for (size_type jx = nxl; jx < nxh; jx++)
            for (size_type jy = nyl; jy < nyh; jy++)
              for (size_type jz = nzl; jz < nzh; jz++)
                if (cont.input( jx,jy,jz) == 1){
                  Vec3< double> posj;
                  cont.get_position( jx,jy,jz, posj);
                  double r = fvalue( L::distance( posi, posj));           
                  sum += volume*greens_fun( cont, r);
                }
          cont.result( ix, iy, iz) += sum;
        }
}

 
void process_bodies( Grid& cont, Grid_Refs& refs0, Grid_Refs& refs1){

  Vec3< size_type> bounds0;
  cont.get_live_bounds( refs0.highi, bounds0); 

  size_type nxh0 = bounds0[X];
  size_type nyh0 = bounds0[Y];
  size_type nzh0 = bounds0[Z];
  size_type nxl0 = refs0.lowi[X];
  size_type nyl0 = refs0.lowi[Y];
  size_type nzl0 = refs0.lowi[Z];

  Vec3< size_type> bounds1;
  cont.get_live_bounds( refs1.highi, bounds1); 

  size_type nxh1 = bounds1[X];
  size_type nyh1 = bounds1[Y];
  size_type nzh1 = bounds1[Z];
  size_type nxl1 = refs1.lowi[X];
  size_type nyl1 = refs1.lowi[Y];
  size_type nzl1 = refs1.lowi[Z];

  const double volume = cont.volume;

  for (size_type ix0 = nxl0; ix0 < nxh0; ix0++)
    for (size_type iy0 = nyl0; iy0 < nyh0; iy0++)
      for (size_type iz0 = nzl0; iz0 < nzh0; iz0++)
        if (cont.input( ix0,iy0,iz0) == 0){
          Vec3< double> pos0;
          cont.get_position( ix0,iy0,iz0, pos0);

          double sum = 0.0;
          for (size_type ix1 = nxl1; ix1 < nxh1; ix1++)
            for (size_type iy1 = nyl1; iy1 < nyh1; iy1++)
              for (size_type iz1 = nzl1; iz1 < nzh1; iz1++)
                if (cont.input( ix1,iy1,iz1) == 1){
                  Vec3< double> pos1;
                  cont.get_position( ix1,iy1,iz1, pos1);
                  double r = L::distance( pos0, pos1);
                  sum += volume*greens_fun( cont, r);
                }
          cont.result( ix0, iy0, iz0) += sum;             
        }
  
  for (size_type ix1 = nxl1; ix1 < nxh1; ix1++)
    for (size_type iy1 = nyl1; iy1 < nyh1; iy1++)
      for (size_type iz1 = nzl1; iz1 < nzh1; iz1++)
        if (cont.input( ix1,iy1,iz1) == 0){
          Vec3< double> pos1;
          cont.get_position( ix1,iy1,iz1, pos1);

          double sum = 0.0;
          for (size_type ix0 = nxl0; ix0 < nxh0; ix0++)
            for (size_type iy0 = nyl0; iy0 < nyh0; iy0++)
              for (size_type iz0 = nzl0; iz0 < nzh0; iz0++)
                if (cont.input( ix0,iy0,iz0) == 1){
                  Vec3< double> pos0;
                  cont.get_position( ix0,iy0,iz0, pos0);
                  double r = fvalue( L::distance( pos1, pos0));
                  sum += volume*greens_fun( cont, r);
                }
          cont.result( ix1, iy1, iz1) += sum;
        }
}

class Interface{
public:
  
  typedef Grid Container;
  typedef Grid_Refs Refs;
  typedef Vector< unsigned int>::size_type size_type;

  template< class F>
  static
  void apply_potential( Grid& cont, Grid_Refs& refs, F& f){
    ::apply_potential( cont, refs, f);
  }

  static
  void process_bodies( Grid& cont, Grid_Refs& refs){
    ::process_bodies( cont, refs);
  }

  static
  void process_bodies( Grid& cont, Grid_Refs& refs0, Grid_Refs& refs1){
    ::process_bodies( cont, refs0, refs1);
  }

  template< class V>
  static
  void get_r_derivatives( Grid& cont, unsigned int order, double r, 
                          V& derivs){
    ::get_r_derivatives( cont, order, r, derivs);
  }

  template< class V3>
  static
  double near_potential( const Grid& cont, 
                         const Grid_Refs& refs, const V3& pos){
    return ::near_potential( cont, refs, SVec< double,3>::Const_View( pos));
  }

  template< class Function> 
  static
  void apply_multipole_function( const Grid& cont, 
                                 const Grid_Refs& refs, Function& f){
    ::apply_multipole_function( cont, refs, f);
  }

  template< class V3>
  static
  void get_bounds( const Grid& cont, 
                   V3& low, V3& high){
    ::get_bounds( cont, low, high);
  }

  template< class Refs_Cube, class V3>
  static
  void split_container( const Grid& cont, 
                        const Grid_Refs& refs, 
                        const V3& center,
                        const Refs_Cube& refs_cube){
    
    ::split_container( cont, refs, center, refs_cube);
  }  

  static
  void empty_references( const Grid& cont, Grid_Refs& refs){
    ::empty_references( cont, refs);
  }

  static
  void copy_references( const Grid& cont, 
                        const Grid_Refs& refs0,
                        Grid_Refs& refs1
                        ){
    ::copy_references( cont, refs0, refs1);
  }

  static
  void copy_references( const Grid& cont, Grid_Refs& refs){
    ::copy_references( cont, refs);
  }

  static
  size_type size( const Grid& cont, const Grid_Refs& refs){
    return ::size( cont, refs);
  }

  static
  size_type n_receivers( const Grid& cont, const Grid_Refs& refs){
    return ::n_receivers( cont, refs);
  }

  static
  size_type n_sources( const Grid& cont, const Grid_Refs& refs){
    return ::n_sources( cont, refs);
  }


};

#include "pi.hh"

int main( int argc, char* argv[]){

  if (argc > 1 && strcmp( argv[1], "-help") == 0){
    printf( "computes desolvation grid\n");
    printf( "-in: name of input file\n");
    printf( "-vperm: vacuum permittivity (default in units of A, ps, kT at 298)\n");
    printf( "-ieps: dielectric of solute (default 4)\n");
    printf( "-oeps: dielectric of solvent (default 78)\n");
    printf( "-debye: Debye length (default infinity)\n");
    exit(0);
  }

  double vac_perm = 0.000142;
  double oeps = 78.0;
  double ieps = 4.0;
  double debye_length = INFINITY;
  char iname[100];

  get_double_arg( argc, argv, "-debye", debye_length);
  get_double_arg( argc, argv, "-vperm", vac_perm);
  get_double_arg( argc, argv, "-ieps", ieps);
  get_double_arg( argc, argv, "-oeps", oeps);
  get_string_arg( argc, argv, "-in", iname);

  const double ieps_inv = 1.0/ieps;
  const double oeps_inv = 1.0/oeps;
  double factor = (ieps_inv - oeps_inv)/(32*pi*pi*vac_perm);

  std::ifstream input( iname);

  JP::Parser parser( input);
  Grid grid( parser);
  grid.debye_length = debye_length;

  size_t order = 6;
  Cartesian_Multipole::Calculator< Interface> calc( order, grid);
  
  calc.set_max_number_per_cell( 64);
  calc.setup_cells();
  calc.compute_expansion();
  calc.compute_interactions_with_potential();
  
  const size_type nx = grid.nx;
  const size_type ny = grid.ny;
  const size_type nz = grid.nz;

  printf(
"object 1 class gridpositions counts %d %d %d\norigin %g %g %g\ndelta %g 0.0 0.0\ndelta 0.0 %g 0.0\ndelta 0.0 0.0 %g\nobject 2 class gridconnections counts %d %d %d\nobject 3 class array type double rank 0 items %d data follows\n",
(int)nx, (int)ny, (int)nz,
grid.low_corner[X], grid.low_corner[Y], grid.low_corner[Z], 
grid.hx, grid.hy, grid.hz, 
(int)nx, (int)ny, (int)nz, (int)(nx*ny*nz));

  size_type ip = 0;
  for (size_type ix = 0; ix < nx; ++ix)
    for (size_type iy = 0; iy < ny; ++iy)
      for (size_type iz = 0; iz < nz; ++iz){
        printf( "%g ", factor*grid.result( ix,iy,iz));
        ++ip;
        if (ip == 3){
          printf( "\n");
          ip = 0;
        }
      }
        
  printf( "attribute \"dep\" string \"positions\"\nobject \"regular positions regular connections\" class field\ncomponent \"positions\" value 1\ncomponent \"connections\" value 2\ncomponent \"data\" value 3\n");


}
