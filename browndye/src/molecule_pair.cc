/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
  Contains code for molecular movement, initialization, and
  implementations of interfaces.

*/

#include "molecule_pair.hh"
#include "step_near_absorbing_sphere.hh"
#include "linalg3.hh"
#include "node_info.hh"
#include "rotations.hh"
#include "stepper_interface_impl.hh"
#include "atom_small.hh"
#include "atom_large.hh"
#include "molecule.hh"

namespace L = Linalg3;


//**************************************************************
/*
  De-dimensionalizing constants for BD step with HI

  R - radius of larger sphere
  mu - viscosity
  kT

  dx - R
  dt - mu R^3/kT
  F - kT/R
  T - kT 
  M - 1/R (translational mobility coef)
  Mt - 1/R^3 (rotational mobility coef)

*/

#include "pi.hh"

// kludge
void get_3x3_inverse( const Mat3< double>& rot, Mat3< double>& inv_rot){
  Transform trans, inv_trans;
  trans.set_rotation( rot);
  trans.get_inverse( inv_trans);
  inv_trans.get_rotation( inv_rot);
} 

//********************************************************************
// Interface for Mover class



//************************************************************
// Interface for function in "step_near_absorbing_sphere.hh"

/*
  De-dimensionalizing constants for boundary step

  R - outer NAM radius
  D - diffusivity
  kT

  x - R
  F - kT/R
  dt - R R/D

*/

/*
class Boundary_Step_Interface{
public:
  typedef Browndye_RNG RNG;

  static
  double uniform( RNG& rng){
    return rng.uniform();
  }

  static
  double gaussian( RNG& rng){
    return rng.gaussian();
  }

};
*/

//**************************************************************
// Interface for BD stepper

template< class MS>
void get_molecule_info(const MS& state,
                       Vec3< Length>& pos, Mat3< double>& rot, 
                       Vec3< Force>& force,  Vec3< Torque>& torque){

  state.get_position( pos); 
  state.get_rotation( rot);
  state.get_force( force);
  state.get_torque( torque);
  //L::copy( state.force, force);
  //L::copy( state.torque, torque);
}

void get_vector_from_succ_rots( const Mat3< double>& rot0, 
                                const Mat3< double>& rot1,
                                Vec3< double>& phi);

void Molecule_Pair::
step_forward( Time dt, const Sqrt_Time_Vector& dw, bool& must_backstep, 
	      bool& has_collision){
    
  const Length R0 = common.mol0.hydro_radius();
  const Energy kT = common.solvent.kT;
  const auto mu = water_viscosity*common.solvent.relative_viscosity;
  const double dtau = dt*kT/(R0*R0*R0*mu);
    
  SVec< double, 12> w;
  auto sqdt = sqrt( 2.0*dt);
  for( int i = 0; i < 12; i++)
    w[i] = dw[i]/sqdt;
  
  common.mover->move_spheres( dtau, w, *this);
  state.time += dt;

  Atom_Large_Ref aref0;
  Atom_Small_Ref aref1;
  compute_forces_and_torques( common.mol0, state.mol0, 
			      common.mol1, thread.mol1, state.mol1, 
			      has_collision, thread.violation, aref0, aref1); 
  state.diffu = separation_diffusivity();

  const auto& ts_params = common.ts_params;

  if (dt < ts_params.dt_min)
    must_backstep = false;

  else {
    if (has_collision){
      must_backstep = true;
    }

    else{
      const auto& old_pair = thread.saved;
      Vec3< Length> old_pos0, old_pos1, pos0, pos1;
      Mat3< double> old_rot0, old_rot1, rot0, rot1;
      Vec3< Force> old_F0, old_F1, F0, F1;
      Vec3< Torque> old_T0, old_T1, T0, T1;
      get_molecule_info( old_pair.mol0, old_pos0, old_rot0, old_F0, old_T0);
      get_molecule_info( old_pair.mol1, old_pos1, old_rot1, old_F1, old_T1);
      get_molecule_info( state.mol0, pos0, rot0, F0, T0);
      get_molecule_info( state.mol1, pos1, rot1, F1, T1);

      auto gz = [&]( const Vec3< Force>& F, const Vec3< Torque>& T) -> bool{
        return 
        (L::norm2( F) > sq( Force( 0.0))) || 
        (L::norm2( T) > sq( Torque( 0.0))); 
      }; 

      if (gz( old_F0, old_T0) || gz( old_F1, old_T1) || 
          gz( F0, T0) || gz( F1, T1)){

        Vec3< Length> dx0, dx1;
        L::get_diff( pos0, old_pos0, dx0);
        L::get_diff( pos1, old_pos1, dx1);
        Vec3< double> phi0, phi1;
        get_vector_from_succ_rots( old_rot0, rot0, phi0);
        get_vector_from_succ_rots( old_rot1, rot1, phi1);
        
        namespace RP = Rotne_Prager;
        Length R0 = common.mol0.hydro_radius();
        Length R1 = common.mol1.hydro_radius();
        Diffusivity D0 = kT*RP::single_mobility( mu, R0); 
        Diffusivity D1 = kT*RP::single_mobility( mu, R1); 
        Inv_Time Dr0 = kT*RP::single_rotational_mobility( mu, R0); 
        Inv_Time Dr1 = kT*RP::single_rotational_mobility( mu, R1); 
        
        Vec3< decltype( Diffusivity()*Force())> num_vec;
        L::zero( num_vec);
        L::add_scaled( D0, old_F0, num_vec); 
        L::add_scaled( D1, old_F1, num_vec); 
        L::add_scaled( R0*Dr0, old_T0, num_vec); 
        L::add_scaled( R1*Dr1, old_T1, num_vec); 
        
        auto numc = 
          L::norm2( dx0) + L::norm2( dx1) + 
          R0*R0*L::norm2( phi0) + R1*R1*L::norm2( phi1);
        
        Vec3< Force> dF0, dF1;
        L::get_diff( F0, old_F0, dF0);
        L::get_diff( F1, old_F1, dF1);
        
        Vec3< Torque> dT0, dT1;
        L::get_diff( T0, old_T0, dT0);
        L::get_diff( T1, old_T1, dT1);
        
        Vec3< decltype( Diffusivity()*Force())> den_vec;
        L::zero( den_vec);
        L::add_scaled( D0, dF0, den_vec);
        L::add_scaled( D1, dF1, den_vec);
        L::add_scaled( R0*Dr0, dT0, den_vec);
        L::add_scaled( R1*Dr1, dT1, den_vec);
        
        auto denc = D0*L::dot( dx0, F0) + D1*L::dot( dx1, F1) +
          R0*R0*Dr0*L::dot( phi0, T0) + R1*R1*Dr1*L::dot( phi1, T1);
        
        Time ratio = kT*L::norm( num_vec)*numc/(L::norm( den_vec)*fabs( denc));
        must_backstep = (dt > ts_params.ftol*ratio);
      }
      else
        must_backstep = false;
    }
  }  
}

void Molecule_Pair::back_away( bool& still_has_collision){
  Vec3< Length> new_pos0, new_pos1;
  state.mol0.get_position( new_pos0);
  state.mol1.get_position( new_pos1);
  
  Vec3< Length> laxis;
  L::get_diff( new_pos1, new_pos0, laxis);
  Vec3< double> axis;
  L::get_normed( laxis, axis);
  Length new_gap =  thread.violation;
  Length old_distance = L::distance( new_pos0, new_pos1);
  Length new_distance = new_gap + old_distance;
          
  Vec3< Length> corr_pos1;
  Vec3< Length> diff;
  L::get_scaled( new_distance, axis, diff);
  L::get_sum( new_pos0, diff, corr_pos1);
  state.mol1.set_position( corr_pos1);
          
  compute_forces_and_torques( common.mol0, state.mol0, 
			      common.mol1, thread.mol1, state.mol1, 
			      still_has_collision); 
}

//************************************************************
// part of interface for "pathways.hh"



// returns minimum of successor reaction coordinates
Length Molecule_Pair::reaction_coordinate(){
  auto pathway = common.pathway.get();

  size_type n = pathway->n_reactions_from( state.current_rxn_state);
  Length coord = Length( INFINITY);
  for( size_type i=0; i<n; i++){
    size_type irxn = pathway->reaction_from( state.current_rxn_state, i);
    Length rc = pathway->reaction_coordinate( *this, irxn);
    coord = min( coord, rc);
  }  

  return coord;
}

//************************************************************
// Following code carries out movement of the molecules, given
// the forces and torques.

void Molecule_Pair::save(){
  thread.saved = state;
}

void Molecule_Pair::restore(){
  state = thread.saved;
}


void get_vector_from_succ_rots( const Mat3< double>& rot0, 
                                const Mat3< double>& rot1,
                                Vec3< double>& phi){
  SVec< double, 4> quat0, quat1;
  mat_to_quat( rot0, quat0);
  mat_to_quat( rot1, quat1);
  SVec< double,4> inv_quat0;
  invert_quat( quat0, inv_quat0);
  SVec< double, 4> dquat;
  multiply_quats( quat1, inv_quat0, dquat);
  double angle =  0.5*acos( dquat[0]);
  if (angle > 0.0){
    Vec3< double> q;
    q[0] = dquat[1];
    q[1] = dquat[2];
    q[2] = dquat[3];
    L::get_scaled( angle/L::norm( q), q, phi);
  }
  else
    L::zero( phi);
}


Length Molecule_Pair::minimum_rxn_coord() const{
  Length mc( DBL_MAX);
  
  auto pathway = common.pathway.get();
  size_type cur_irxn = state.current_rxn_state;
  size_type nrxns = pathway->n_reactions_from( cur_irxn);
  for( size_type i = 0; i < nrxns; i++){
    size_type irxn = pathway->reaction_from( cur_irxn, i);
    Length coord = pathway->reaction_coordinate( *this, irxn);
    if (coord < mc)
      mc = coord;
  }    
  return mc;
}

//const double otol = 1.0/(2*3*3);

Time Molecule_Pair::time_step_guess() const{

  Length min_rxn_coord = minimum_rxn_coord();
  
  state.min_rxn_coord = min( state.min_rxn_coord, min_rxn_coord);

  //const Length gap = hydro_gap( common.mol0, state.mol0, 
  //                            common.mol1, thread.mol1, state.mol1);


  const auto& ts_params = common.ts_params;

  const Diffusivity diffu = ts_params.diffu_inf;

  //Time dtg = common.gtol*gap*gap/diffu;
  Time dtr = sq( min_rxn_coord)*ts_params.rtol/diffu;

  Time dtmax = sq(10.0*pi/180.0)/(2.0*ts_params.max_rdiffu);

  /*
  return min( dtmax, min( max( common.dt_min, dtg),
                          max( common.dtr_min, dtr)));  
  */
  return min( dtmax, max( ts_params.dtr_min, dtr));
}


void Molecule_Pair::get_mol_mol_diff( Vec3< Length>& diff) const{
  Vec3< Length> pos0, pos1;
  state.mol0.get_position( pos0);
  state.mol1.get_position( pos1);
  L::get_diff( pos1, pos0, diff);
}

Length Molecule_Pair::mol_mol_distance() const{ 
  Vec3< Length> diff;
  get_mol_mol_diff( diff);
  return L::norm( diff);
}

void Molecule_Pair::rescale_separation( double scale){
  Vec3< Length> pos0, pos1;
  Vec3< Length> diff, new_diff;

  state.mol0.get_position( pos0);
  state.mol1.get_position( pos1);
  L::get_diff( pos1, pos0, diff);

  L::get_scaled( scale, diff, new_diff);
  L::get_sum( pos0, new_diff, pos1);

  state.mol1.set_position( pos1);
  
  bool has_collision;
  compute_forces_and_torques( common.mol0, state.mol0,  
                              common.mol1, thread.mol1, state.mol1,  
                              has_collision);
  if (has_collision){
    printf( "separation: diff %g new_diff %g\n", fvalue( L::norm( diff)), 
            fvalue( L::norm( new_diff)));
    error( "Molecule_Pair::do_step: should not have collision here");
  }
  state.diffu = separation_diffusivity();
}

/*
bool Molecule_Pair::has_collision( const Mat3< double>& rot, 
                                   const Vec3< Length>& trans){
  
  state.mol1.set_position( trans);
  state.mol1.set_rotation( rot);
  
  bool has_collision;
  compute_forces_and_torques( common.mol0, state.mol0, 
                              common.mol1, thread.mol1,
                              state.mol1, has_collision);
  return has_collision;
}
*/

Diffusivity Molecule_Pair::separation_diffusivity() const{
  const Length r = mol_mol_distance();
  if (r != r)
    error( "Molecule_Pair::do_step: NaN encountered");

  const Length R = common.mol0.hydro_radius();
  auto mu = water_viscosity*common.solvent.relative_viscosity;
  auto kT = common.solvent.kT;
  return common.mover->separation_mobility( r/R)*kT/(R*mu);
}

void normalize( Large_Molecule_State& mol0, Small_Molecule_State& mol1);

void Molecule_Pair::normalize(){
  ::normalize( state.mol0, state.mol1);  
}

// n_steps: number of elementary BD steps
void Molecule_Pair::do_step( unsigned long int& n_steps){
  state.had_rxn = false;
  common.full_stepper->f( *this, n_steps);
  ++(state.n_full_steps);
  normalize();
}

//************************************************************
// Initialization code

namespace JP = JAM_XML_Pull_Parser;
typedef std::string String;

// local interface class
class Rotation_Interface{
public:
  typedef Browndye_RNG Random_Number_Generator;
  typedef Random_Number_Generator RNG;

  static
  double uniform( RNG& rng){
    return rng.uniform();
  }

  static
  double gaussian( RNG& rng){
    return rng.gaussian();
  }
};

typedef Rotation_Interface RI;

// puts mol 0 in its orginal spot, and mol 1 relative to it
void Molecule_Pair::set_initial_physical_state( Length radius){

  Vec3< Length> pos1;
  Mat3< double> rot1;
  Browndye_RNG& rng = thread.rng;

  bool has_collision = false;

  do{
    state.mol0.set_zero_transform();
    bool hemisphere = false;
    
    if (common.start_at_site){
      state.mol1.set_zero_transform();
      state.mol1.set_position( common.starting_site_offset);
    }
    
    else{      
      Vec3< double> udisp;
      get_random_unit_vector< RI>( rng, udisp);

      hemisphere = common.full_stepper->has_hemisphere();
      if( hemisphere){
	while( udisp[2] < 0.0)
	  get_random_unit_vector< RI>( rng, udisp);
      }
      
      L::get_scaled( radius, udisp, pos1);
      
      get_random_rotation< RI>( rng, rot1);
      
      state.mol1.set_position( pos1);
      state.mol1.set_rotation( rot1);
    }
    
    reset_hints( common.mol0, state.mol0);
    reset_hints( common.mol1, state.mol1);
    
    bool has_collision;
    compute_forces_and_torques( common.mol0, state.mol0,
				common.mol1, thread.mol1, state.mol1,
				has_collision);
    if (has_collision){
      if (!hemisphere)
	error( "Molecule_Pair::set_initial_state: collision");
    }
      
    state.n_full_steps = 0;
    state.time = Time( 0.0);
    state.diffu = separation_diffusivity();
    state.last_dt = Time( INFINITY);
  }
  while( has_collision);
}
  
void Molecule_Pair::set_initial_physical_state(){
  set_initial_physical_state( common.b_rad);
}


void Molecule_Pair::set_initial_state(){
  set_initial_physical_state();
  state.set_initial_reaction_state( common);
}

void Molecule_Pair::set_initial_state_for_bb(){
  set_initial_physical_state( common.qb_factor()*common.b_rad);
  state.set_initial_reaction_state( common);
}

void Molecule_Pair::get_potential_energy( Energy& vnear, Energy& vcoul, Energy& vdes) const{

  ::get_potential_energy( common.mol0, state.mol0,
      common.mol1, thread.mol1, state.mol1,
      vnear, vcoul, vdes);
}

Vec3< Length> Molecule_Pair::hydro_center0() const{
	Vec3< Length> c;
	common.mol0.get_hydro_center( c);
	return c;
}

Vec3< Length> Molecule_Pair::hydro_center1() const{
	Vec3< Length> c;
	common.mol1.get_hydro_center( c);
	return c;
}

std::string Molecule_Pair::state_name( size_t istate) const{
  return common.pathway->state_name( istate);
}

std::string Molecule_Pair::reaction_name( size_t irxn) const{
  return common.pathway->reaction_name( irxn);
}

unsigned int Molecule_Pair::thread_number() const{
    return thread.n;
 }


