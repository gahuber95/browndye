#ifndef __ATTR_HH__
#define __ATTR_HH__

/*
Defines class Attr for containing the attributes of an XML element.
*/

#include "str.hh"

namespace JAM_XML_Parser{

  class Attrs{  
  public:
    
    std::pair< String, bool> value( const String& key) const{
      auto itr = dict.find( key);
      if (!(itr == dict.end()))
        return std::make_pair( itr->second, true);
      else
        return std::make_pair( "", false);      
    }
    
    void insert( const String& key, const String& res){
      dict.insert( std::make_pair( key, res));
    }

    std::map< String, String> dict;
  };
}

#endif
