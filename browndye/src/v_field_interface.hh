#ifndef __V_FIELD_INTERFACE_HH__
#define __V_FIELD_INTERFACE_HH__

#include <string>
#include "units.hh"

class V_Field_Interface{
public:
  typedef EPotential Potential;
  typedef ::Charge Charge;
  typedef ::Permittivity Permittivity;
  typedef ::EPotential_Gradient Potential_Gradient;
  typedef std::string String;
};

#endif
