/*
 * time_step_parameters.hh
 *
 *  Created on: Sep 11, 2015
 *      Author: ghuber
 */

#ifndef TIME_STEP_PARAMETERS_HH_
#define TIME_STEP_PARAMETERS_HH_

#include "units.hh"
#include "jam_xml_pull_parser.hh"
#include "node_info.hh"

class Molecule_Pair_Common;

struct Time_Step_Parameters{
  typedef JAM_XML_Pull_Parser::Node_Ptr Node_Ptr;

  Time dt_min, dtr_min;
  double ftol, gtol, rtol;
  Length dx_min, dxr_min;

  Diffusivity diffu_inf;
  Inv_Time max_rdiffu;

  void initialize( const Molecule_Pair_Common& common, Node_Ptr& node);

  Time_Step_Parameters();

};



#endif /* TIME_STEP_PARAMETERS_HH_ */
