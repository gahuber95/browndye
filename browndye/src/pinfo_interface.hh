#ifndef __PINFO_INTERFACE_HH__
#define __PINFO_INTERFACE_HH__

#include "units.hh"
#include "small_vector.hh"

// Interface for reading in atoms and parameter information
template< class Atom>
class Pinfo_Interface{
public:
  typedef Vector< Atom> Spheres;
  typedef typename Spheres::size_type size_type;

  static 
  void put_position( Spheres& atoms, size_type i, const Vec3< Length>& pos){
    atoms[i].set_position( pos);
  }

  static
  void put_hard_radius( Spheres& atoms, size_type i, Length r){
    atoms[i].hard_radius = r;
  }

  static
  void put_soft_radius( Spheres& atoms, size_type i, Length r){
    atoms[i].soft_radius = r;
  }

  static
  void put_interaction_radius( Spheres& atoms, size_type i, Length r){
    atoms[i].interac_radius = r;
  }

  static
  void put_charge( Spheres& atoms, size_type i, Charge q){
    // atom charge not used this time
  }

  static
  void put_type( Spheres& atoms, size_type i, size_type type){
    atoms[i].type = type;
  }

  static
  void put_number( Spheres& atoms, size_type i, size_type number){
    atoms[i].number = number;
  } 

  static
  void resize( Spheres& atoms, size_type n){
    atoms.resize( n);
  }

  static
  void make_soft( Spheres& atoms, size_type i){
    atoms[i].soft = true;
  }  
};

#endif
