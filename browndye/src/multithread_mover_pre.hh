/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
Defines interface class to Mover_Base in "multithread_mover_base.hh".

*/

template< class Interface>
class Mover;

template< class Interface>
class MT_Interface{
public:
  
  typedef typename Interface::Exception Exception;
  typedef Mover< Interface>* Objects_Ref;
  typedef unsigned int Object_Ref;

  static void do_move( Objects_Ref, Object_Ref);

  template< class Func>
  static void apply_to_object_refs( Func& f, Objects_Ref objects);

  static unsigned int size( Objects_Ref objects);

  static void set_null( Objects_Ref& objects);

  static bool is_null( Objects_Ref objects);

};

template< class Interface>
template< class Func>
void MT_Interface< Interface>::
apply_to_object_refs( Func& f, 
                      Mover< Interface>* mmover){

  const unsigned int n = mmover->mover.n_threads();
  for( unsigned int i=0; i<n; i++)
    f( i);
}

template< class Interface>
unsigned int MT_Interface< Interface>::size( Mover< Interface>* mmover){
  return mmover->n_threads;
}

template< class Interface>
void MT_Interface< Interface>::set_null( Mover< Interface>*& mmover){
  mmover = NULL;
}

template< class Interface>
bool MT_Interface< Interface>::is_null( Mover< Interface>* mmover){
  return (mmover == NULL);
}

template< class Interface>
void MT_Interface< Interface>::do_move( Mover< Interface>* mmover,
                                        unsigned int ithread){
  mmover->thread_move( ithread);
}
 
