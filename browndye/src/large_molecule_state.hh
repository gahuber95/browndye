#ifndef __LARGE_MOLECULE_STATE_HH__
#define __LARGE_MOLECULE_STATE_HH__

#include "small_molecule_state.hh"
#include "v_field_interface.hh"
#include "field.hh"

class Large_Molecule_State: public Small_Molecule_State{

private:
  Field::Field< V_Field_Interface>::Hint v_hint;

  friend
    void reset_hints( const Large_Molecule_Common& molc, Large_Molecule_State& mol);

    friend
    void add_forces_and_torques(
                                const Large_Molecule_Common& molc0,
                                Large_Molecule_State& mol0,
                                const Small_Molecule_Common& molc1,
                                Small_Molecule_Thread& molt1,
                                Small_Molecule_State& mol1,

                                bool& has_collision,
                                Length& violation,
                                Atom_Large_Ref& aref0,
                                Atom_Small_Ref& aref1
                                );

    friend
    void get_potential_energy(
                              const Large_Molecule_Common& molc0,
                              Large_Molecule_State& mol0,
                              const Small_Molecule_Common& molc1,
                              Small_Molecule_Thread& molt1,
                              Small_Molecule_State& mol1,
                              Energy& vnear, Energy& vcoul, Energy& vdesolv
                               );
};

#endif


