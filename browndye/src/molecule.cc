/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
These functions, involving the classes defined in "molecule.hh", 
include those concerned with initializing, 
computing forces, and estimating the hydrodynamic distance.
It implements the interface classes for the electrostatic and
desolvation forces and the collision tester.  It also implements
the short-ranged L-J forces.
*/

#include "molecule.hh"
#include "ellipsoid_distances.hh"
#include "node_info.hh"
#include "tform_product.hh"

//static const double collision_pad = 1.2; // for 1/r^12 interaction

typedef std::string String;
typedef Vector< Atom_Large> Atoms_Large;
typedef Vector< Atom_Large_Ref> Atom_Large_Refs;
typedef Vector< Atom_Small> Atoms_Small;
typedef Vector< Atom_Small_Ref> Atom_Small_Refs;



//***************************************************************************
// Ellipsoid functions

template< class Tform>
void rotate_ellipsoid( const Ellipsoid& ellipsoid,
                       const Tform& transform,
                       SVec< Vec3< double>, 3>& axes){

  for( int i=0; i<3; i++)
    transform.get_rotated( ellipsoid.axes[i], axes[i]);
}

Length hydro_gap(      
                 const Large_Molecule_Common& molc0,
                 const Large_Molecule_State& mols0,
                 const Small_Molecule_Common& molc1,
                 const Small_Molecule_Thread& molt1,
                 const Small_Molecule_State& mols1                     
                 ){
  
  Vec3< Length> center0, center1;
  SVec< Vec3< double>, 3> axes0, axes1;
  
  const Ellipsoid& ell0 = molc0.hydro_ellipsoid;  
  const Ellipsoid& ell1 = molc1.hydro_ellipsoid;
  
  mols0.transform.get_translation( center0);
  mols1.transform.get_translation( center1);

  rotate_ellipsoid( ell0, mols0.transform, axes0);
  rotate_ellipsoid( ell1, mols1.transform, axes1);
  
  return
    ellipsoid_distance( axes0, ell0.distances, center0,
                        axes1, ell1.distances, center1);
  
}

namespace L = Linalg3;

//*****************************************************************
// Interface classes for the collision tester


class Col_Interface{
public:
  typedef Col_Interface_Large Interface0;
  typedef Col_Interface_Small Interface1;
};

//******************************************************************
// Class member functions




//******************************************************************
// Interface for charge blob

template< class C0, class FI, class Mol0, class TForm0, class Mol1, class TForm1>
Energy cheby_and_grid_energy( 
             const Field_For_Blob< C0, FI>& field0, 
             const Charged_Blob::Blob< 4, Blob_Interface< C0, FI> >& blob1,
             const Mol0& mol0, const TForm0& tform0, 
             const Mol1& mol1, const TForm1& tform1){

  
  TForm0 inv_tform0;
  tform0.get_inverse( inv_tform0);
  typename TForm_Product< TForm0, TForm1>::Result rel_tform;
  get_product( inv_tform0, tform1, rel_tform);
  
  return blob1.potential_energy( rel_tform, field0);
}

// remember hydro center of molecule 0 sits at origin, cheby torques are about origin,
// and rotation is of blob relative to field
template< class C0, class FI, class Mol0, class TForm0, class Mol1, class TForm1>
void add_cheby_and_grid_forces( 
             const Field_For_Blob< C0, FI>& field0, 
             const Charged_Blob::Blob< 4, Blob_Interface< C0, FI> >& blob1,
             Mol0& mol0, const TForm0& tform0, 
             Mol1& mol1, const TForm1& tform1, double factor){
  
  TForm0 inv_tform0;
  tform0.get_inverse( inv_tform0);
  typename TForm_Product< TForm0, TForm1>::Result rel_tform;
  get_product( inv_tform0, tform1, rel_tform);

  Vec3< Force> force1_fm0;
  Vec3< Torque> torque1_ori_fm0;
  blob1.get_force_and_torque( rel_tform, field0, force1_fm0, torque1_ori_fm0);

  Vec3< Force> force1;
  tform0.get_rotated( force1_fm0, force1);

  Vec3< Torque> torque1;
  tform0.get_rotated( torque1_ori_fm0, torque1);

  Vec3< Force> force0;
  L::get_scaled( -1.0, force1, force0);

  Vec3< Torque> torque0;
  {  
    Vec3< Length> r0, r1, d;
    tform0.get_translation( r0);
    tform1.get_translation( r1);
    L::get_diff( r1, r0, d);

    Vec3< Torque> torque_corr;
    L::get_cross( force1, d, torque_corr);
    L::get_scaled( -1.0, torque1, torque0);
    L::get_sum( torque0, torque_corr, torque0);
  }

  L::get_scaled( factor, force0, force0);
  L::get_scaled( factor, force1, force1);
  L::get_scaled( factor, torque0, torque0);
  L::get_scaled( factor, torque1, torque1);

  L::get_sum( force0, mol0.force, mol0.force);
  L::get_sum( force1, mol1.force, mol1.force);
  
  L::get_sum( torque0, mol0.torque, mol0.torque);
  L::get_sum( torque1, mol1.torque, mol1.torque);  
}
 
typedef Vec3< Length> Length_Vec3;

typedef Near_Interactions::Parameter_Info PInfo;


//********************************************************************
// Short-ranged forces

class Local_Force_Computer_Base{
public:
  Local_Force_Computer_Base( const Length_Vec3& _center1, const Molecule_Common& _mol0, const Molecule_Common& _mol1):
    center1(_center1), mol0(_mol0), mol1(_mol1)
  {
    L::zero( force1);
    L::zero( torque1);
  }

  Vec3< Force> force1;
  Vec3< Torque> torque1;
  const Length_Vec3& center1;
  const Molecule_Common &mol0, &mol1;
};

template< class Pair_Computer>
class Local_Force_Computer: public Local_Force_Computer_Base{
public:

  Local_Force_Computer( const Length_Vec3& center1, const Molecule_Common& mol0, const Molecule_Common& mol1):
    Local_Force_Computer_Base( center1, mol0, mol1){}

  void operator()( const Blank_Transform& tform0, Atoms_Large& atoms0, Atom_Large_Refs& refs0,
                   const Transform& tform1, Atoms_Small& atoms1, Atom_Small_Refs& refs1,
                   bool& has_collision) const;    

  void operator()( 
                  const Blank_Transform& tform0, Atoms_Large& atoms0, Atom_Large_Refs& refs0,
                  const Transform& tform1, Atoms_Small& atoms1, Atom_Small_Refs& refs1,
                  bool& has_collision, 
                  Atom_Large_Ref& aref0, Atom_Small_Ref& aref1, Length& viol);

  typedef Atoms_Large::size_type size_type;
  Pair_Computer pc;
};

template< class Pair_Computer>
void Local_Force_Computer< Pair_Computer>::operator()( 
           const Blank_Transform& tform0, Atoms_Large& atoms0, Atom_Large_Refs& refs0,
           const Transform& tform1, Atoms_Small& atoms1, Atom_Small_Refs& refs1,
           bool& has_collision, 
           Atom_Large_Ref& aref0, Atom_Small_Ref& aref1, Length& viol){
  
  has_collision = false;
  for( Atom_Large_Refs::iterator it0 = refs0.begin(); it0 != refs0.end(); it0++){
    Atom_Large& atom0 = *(*it0);

    Vec3< Length> tpos0;
    atom0.get_transformed_position( tform0, tpos0);

    size_type itype0 = atom0.type;

    Energy eps0( NAN);
    if (atom0.soft){
      #ifdef DEBUG
      if (mol0.pinfo == NULL)
        error( "Local_Force_Computer: mol0.pinfo is null");
      #endif
      eps0 = mol0.pinfo->parameters( itype0).epsilon;
    }

    for( Atom_Small_Refs::iterator it1 = refs1.begin(); it1 != refs1.end(); it1++){
      Atom_Small& atom1 = *(*it1);

      Vec3< Length> tpos1;
      atom1.get_transformed_position( tform1, tpos1);

      size_type itype1 = atom1.type;
      
      Vec3< Length> diff;
      L::get_diff( tpos1, tpos0, diff);

      Length d = L::norm( diff);

      if (d != d)
        error( "nan encountered");
      
      if (atom0.soft && atom1.soft){
        #ifdef DEBUG
        if (mol1.pinfo == NULL)
          error( "Local_Force_Computer: mol1.pinfo is null");
        #endif

        Length sigma = atom0.soft_radius + atom1.soft_radius;
        Energy eps1 = mol1.pinfo->parameters( itype1).epsilon;
        Energy eps = sqrt( eps0*eps1);
        Vec3< Force> f;
        
        pc( sigma, eps, diff, d, f);
  
        L::get_sum( f, force1, force1);
        Vec3< Length> rpos1;
        L::get_diff( tpos1, center1, rpos1);
        Vec3< Torque> tq;
        L::get_cross( rpos1, f, tq);
        L::get_sum( tq, torque1, torque1);
      }
      else{ 
        Length sigma = atom0.hard_radius + atom1.hard_radius;
        if (d < sigma){
          viol = sigma - d;
          has_collision = true;
  
          goto loop_end;
        }
      }      
    }
  }
  loop_end:
  return;
}

class Pair_Force_Computer_6_12{
public:
  void operator()( Length sigma, Energy eps, 
                   Vec3< Length>& diff, Length d, Vec3< Force>& f) const{    
    double r = d/sigma;
    double r2 = r*r;
    double r4 = r2*r2;
    double r6 = r2*r4;
    double r12 = r6*r6;
    L::get_scaled( 12.0*eps*(1.0/r12 - 1.0/r6)/(d*d), diff, f);
  }
};

class Pair_Force_Computer_6_8{
public:
  void operator()( Length sigma, Energy eps, 
                   Vec3< Length>& diff, Length d, Vec3< Force>& f) const{    
    double r = d/sigma;
    double r2 = r*r;
    double r4 = r2*r2;
    double r6 = r2*r4;
    double r8 = r6*r2;
    L::get_scaled( 24.0*eps*(1.0/r8 - 1.0/r6)/(d*d), diff, f);
  }
};

class Local_Potential_Computer_Base{
public:
  Local_Potential_Computer_Base( const Molecule_Common& _mol0, const Molecule_Common& _mol1):
    mol0(_mol0), mol1(_mol1), potential( 0.0){}

  typedef Atoms_Large::size_type size_type;  
  const Molecule_Common &mol0, &mol1;
  Energy potential;
};

template< class Pair_Computer>
class Local_Potential_Computer: public Local_Potential_Computer_Base{
public:

  Local_Potential_Computer( const Molecule_Common& mol0, const Molecule_Common& mol1):
    Local_Potential_Computer_Base( mol0, mol1){}

  void operator()( const Blank_Transform& tform0, Atoms_Large& atoms0, Atom_Large_Refs& refs0,
                     const Transform& tform1, Atoms_Small& atoms1, Atom_Small_Refs& refs1) const;    

  
  void operator()( 
                  const Blank_Transform& tform0, Atoms_Large& atoms0, Atom_Large_Refs& refs0,
                  const Transform& tform1, Atoms_Small& atoms1, Atom_Small_Refs& refs1,
                  bool& has_collision, 
                  Atom_Large_Ref& aref0, Atom_Small_Ref& aref1, Length& viol);
  
  Pair_Computer pc;
};

template< class Pair_Computer>
void Local_Potential_Computer< Pair_Computer>::
operator()( 
           const Blank_Transform& tform0, Atoms_Large& atoms0, Atom_Large_Refs& refs0,
           const Transform& tform1, Atoms_Small& atoms1, Atom_Small_Refs& refs1,
           bool& has_collision, 
           Atom_Large_Ref& aref0, Atom_Small_Ref& aref1, Length& viol){
  

  has_collision = false;

  for( Atom_Large_Refs::iterator it0 = refs0.begin(); it0 != refs0.end(); it0++){
    Atom_Large& atom0 = *(*it0);

    Vec3< Length> tpos0;
    atom0.get_transformed_position( tform0, tpos0);

    size_type itype0 = atom0.type;

    Energy eps0( NAN);
    if (atom0.soft){

#ifdef DEBUG
      if (mol0.pinfo == NULL)
        error( "Local_Potential_Computer: mol0.pinfo is null");
#endif

      eps0 = mol0.pinfo->parameters( itype0).epsilon;
    }


    for( Atom_Small_Refs::iterator it1 = refs1.begin(); it1 != refs1.end(); it1++){
      Atom_Small& atom1 = *(*it1);

      Vec3< Length> tpos1;
      atom1.get_transformed_position( tform1, tpos1);

      size_type itype1 = atom1.type;
      
      Vec3< Length> diff;
      L::get_diff( tpos1, tpos0, diff);

      Length d = L::norm( diff);

      if (d != d)
        error( "nan encountered");

      if (atom0.soft && atom1.soft){

#ifdef DEBUG
      if (mol1.pinfo == NULL)
        error( "Local_Potential_Computer: mol1.pinfo is null");
#endif

        Length sigma = atom0.soft_radius + atom1.soft_radius;
        Energy eps1 = mol1.pinfo->parameters( itype1).epsilon;
        Energy eps = sqrt( eps0*eps1);
        potential += pc( sigma, eps, d);        
      }

      else{ 
        Length sigma = atom0.hard_radius + atom1.hard_radius;
        if (d < sigma){
          viol = sigma - d;
          has_collision = true;
          potential = Energy( INFINITY);
          goto loop_end;
        }
      }      
    }
  }
  loop_end:

  return;
}

class Pair_Potential_Computer_6_12{
public:
  Energy operator()( Length sigma, Energy eps, Length d) const{    
    double r = d/sigma;
    double r2 = r*r;
    double r4 = r2*r2;
    double r6 = r2*r4;
    double r12 = r6*r6;
    return eps*(1.0/r12 - 2.0/r6);
  }
};

class Pair_Potential_Computer_6_8{
public:
  Energy operator()( Length sigma, Energy eps, Length d) const{    
    double r = d/sigma;
    double r2 = r*r;
    double r4 = r2*r2;
    double r6 = r2*r4;
    double r8 = r6*r2;
    return eps*(3.0/r8 - 4.0/r6);
  }
};

//********************************************************************************
// All forces

// mol0 has vgrid; forces and torques about hydro centers
void add_forces_and_torques( 
                            const Large_Molecule_Common& molc0,
                            Large_Molecule_State& mol0,
                            const Small_Molecule_Common& molc1,
                            Small_Molecule_Thread& molt1,
                            Small_Molecule_State& mol1,
                            
                            bool& has_collision,
                            Length& violation,
                            Atom_Large_Ref& aref0,
                            Atom_Small_Ref& aref1
                            ){

  Blank_Transform tform0;
  Transform tform1;
  {
    Transform inv_tform0;
    mol0.transform.get_inverse( inv_tform0);
    get_product( inv_tform0, mol1.transform, tform1);
  }
    
  Vec3< Length> center0, center1;
  L::zero( center0);
  tform1.get_translation( center1);

  Local_Force_Computer< Pair_Force_Computer_6_12> lfc6_12( center1, molc0, molc1);
  Local_Force_Computer< Pair_Force_Computer_6_8>  lfc6_8( center1, molc0, molc1);

  Local_Force_Computer_Base* lfc;

  if (molc0.use_68){
    lfc = &lfc6_8;
    Collision_Detector::
      compute_interaction< Col_Interface>( lfc6_8, 
                                           *(molc0.collision_structure), tform0,
                                           *(molt1.collision_structure), tform1, 
                                           has_collision,
                                           aref0, aref1, violation);
  }
  else{
    lfc = &lfc6_12;
    Collision_Detector::
      compute_interaction< Col_Interface>( lfc6_12, 
                                           *(molc0.collision_structure), tform0,
                                           *(molt1.collision_structure), tform1, 
                                           has_collision,
                                           aref0, aref1, violation);
  }

  if (has_collision){
    return;
  }
  else{
    {
      Vec3< Force> force0;
      L::get_scaled( -1.0, lfc->force1, force0);
      Vec3< Torque> torque0_abt_1;
      L::get_scaled( -1.0, lfc->torque1, torque0_abt_1);
      Vec3< Length> r01;
      L::get_diff( center1, center0, r01);
      Vec3< Torque> torque_shift;
      L::get_cross( r01, force0, torque_shift);
      Vec3< Torque> torque0;
      L::get_sum( torque0_abt_1, torque_shift, torque0);
            
      L::get_sum( force0, mol0.force, mol0.force);
      L::get_sum( torque0, mol0.torque, mol0.torque);
      L::get_sum( lfc->force1, mol1.force, mol1.force);
      L::get_sum( lfc->torque1, mol1.torque, mol1.torque);      
    }

    Field_For_Blob< Col_Interface_Large, V_Field_Interface> v_field_for_blob;
    v_field_for_blob.field = molc0.v_field.get();
    v_field_for_blob.collision_structure = molc0.collision_structure.get();
    v_field_for_blob.hint_ptr = &(mol0.v_hint);

    add_cheby_and_grid_forces( v_field_for_blob, *(molc1.q_blob), mol0, tform0, mol1, tform1, 1.0);

    if (molc0.born_field && molc1.born_field){

    // desolvation forces
      Field_For_Blob< Col_Interface_Small, Born_Field_Interface> born_field_for_blob1;
      born_field_for_blob1.field = molc1.born_field.get();
      born_field_for_blob1.collision_structure = 
	molt1.collision_structure.get();
      born_field_for_blob1.hint_ptr = &(mol1.born_hint);

      Field_For_Blob< Col_Interface_Large, Born_Field_Interface> born_field_for_blob0;
      born_field_for_blob0.field = molc0.born_field.get();
      born_field_for_blob0.collision_structure = 
	molc0.collision_structure.get();
      born_field_for_blob0.hint_ptr = &(mol0.born_hint);

      add_cheby_and_grid_forces( born_field_for_blob0, *(molc1.q2_blob), mol0, tform0, mol1, tform1, molc1.desolve_fudge);
      add_cheby_and_grid_forces( born_field_for_blob1, *(molc0.q2_blob), mol1, tform1, mol0, tform0, molc0.desolve_fudge);
    }     
  }
  // tether force
  if ((molc0.spring_atom0 != NULL) && (molt1.spring_atom1 != NULL)){
    Vec3< Length> tpos0, tpos1;
    molc0.spring_atom0->get_transformed_position( tform0, tpos0);
    molt1.spring_atom1->get_transformed_position( tform1, tpos1);

    Vec3< Length> diff;
    L::get_diff( tpos1, tpos0, diff);
    Length d = L::norm( diff);
    Force f = molc0.ks*( molc0.equilib_len - d);
    Vec3< Force> F1, F0;
    L::get_scaled( (f/d), diff, F1);
    L::get_scaled( -1.0, F1, F0);

    Vec3< Length> larm0, larm1;
    L::get_diff( tpos0, center0, larm0);
    L::get_diff( tpos1, center1, larm1);
    Vec3< Torque> T0, T1;
    L::get_cross( larm0, F0, T0);
    L::get_cross( larm1, F1, T1);

    L::get_sum( F0, mol0.force, mol0.force);
    L::get_sum( F1, mol1.force, mol1.force);
    L::get_sum( T0, mol0.torque, mol0.torque);
    L::get_sum( T1, mol1.torque, mol1.torque);
  }
  
  Vec3< Force> F0, F1;
  Vec3< Torque> T0, T1;
  const Transform& mol0tform = mol0.transform;
  
  mol0tform.get_rotated( mol0.force, F0);
  mol0.force = F0;
  
  mol0tform.get_rotated( mol1.force, F1);
  mol1.force = F1;
  
  mol0tform.get_rotated( mol0.torque, T0);
  mol0.torque = T0;
  
  mol0tform.get_rotated( mol1.torque, T1);
  mol1.torque = T1;
}

void add_forces_and_torques( 
                            const Large_Molecule_Common& molc0,
                            Large_Molecule_State& mol0,
                            const Small_Molecule_Common& molc1,
                            Small_Molecule_Thread& molt1,
                            Small_Molecule_State& mol1,                     
                            bool& has_collision
                            ){

  Length violation;
  Atom_Large_Ref aref0;
  Atom_Small_Ref aref1;
  add_forces_and_torques( molc0, mol0, molc1, molt1, mol1, has_collision, violation, aref0, aref1);
}

void get_potential_energy( 
                          const Large_Molecule_Common& molc0,
                          Large_Molecule_State& mol0,
                          const Small_Molecule_Common& molc1,
                          Small_Molecule_Thread& molt1,
                          Small_Molecule_State& mol1,
                          Energy& vnear, Energy& vcoul, Energy& vdesolv
                           ){
 
  Blank_Transform tform0;
  //const auto& tform1 = mol1.transform;
  
  Transform tform1;
  {
    Transform inv_tform0;
    mol0.transform.get_inverse( inv_tform0);
    get_product( inv_tform0, mol1.transform, tform1);
  }
  
  Local_Potential_Computer< Pair_Potential_Computer_6_12> lpc6_12( molc0, molc1);
  Local_Potential_Computer< Pair_Potential_Computer_6_8> lpc6_8( molc0, molc1);
  Local_Potential_Computer_Base* lpc;

  bool has_collision;

  {
    Length violation;
    Atom_Large_Ref aref0;
    Atom_Small_Ref aref1;

    if (molc0.use_68){
      lpc = &lpc6_8;
      Collision_Detector::
        compute_interaction< Col_Interface>( lpc6_8, 
                                             *(molc0.collision_structure), tform0,
                                             *(molt1.collision_structure), tform1, 
                                             has_collision, aref0, aref1, violation);    
    }
    else{
      lpc = &lpc6_12;
      Collision_Detector::
        compute_interaction< Col_Interface>( lpc6_12, 
                                             *(molc0.collision_structure), tform0,
                                             *(molt1.collision_structure), tform1, 
                                             has_collision, aref0, aref1, violation);    
    }
  }
 
  if (has_collision){
    vnear = Energy( INFINITY);
  }
  else {
    vnear = lpc->potential;
    Field_For_Blob< Col_Interface_Large, V_Field_Interface> v_field_for_blob;
    v_field_for_blob.field = molc0.v_field.get();
    v_field_for_blob.collision_structure = molc0.collision_structure.get();
    v_field_for_blob.hint_ptr = &(mol0.v_hint);

    vcoul = cheby_and_grid_energy( v_field_for_blob, *(molc1.q_blob), mol0, tform0, mol1, tform1);
    
    vdesolv = Energy( 0.0);
    if (molc0.born_field && molc1.born_field){
      // desolvation forces

      Field_For_Blob< Col_Interface_Small, Born_Field_Interface> born_field_for_blob1;
      born_field_for_blob1.field = molc1.born_field.get();
      born_field_for_blob1.collision_structure = 
	molt1.collision_structure.get();
      born_field_for_blob1.hint_ptr = &(mol1.born_hint);

      Field_For_Blob< Col_Interface_Large, Born_Field_Interface> born_field_for_blob0;
      born_field_for_blob0.field = molc0.born_field.get();
      born_field_for_blob0.collision_structure = 
	molc0.collision_structure.get();
      born_field_for_blob0.hint_ptr = &(mol0.born_hint);

      vdesolv = 
        molc1.desolve_fudge*cheby_and_grid_energy( born_field_for_blob0, *(molc1.q2_blob), mol0, tform0, mol1, tform1) 
        +
        molc0.desolve_fudge*cheby_and_grid_energy( born_field_for_blob1, *(molc0.q2_blob), mol1,tform1, mol0,tform0);
    }
  }
}

void compute_forces_and_torques(
                                const Large_Molecule_Common& molc0,
                                Large_Molecule_State& mol0,
                                const Small_Molecule_Common& molc1,
                                Small_Molecule_Thread& molt1,
                                Small_Molecule_State& mol1,                
                                bool& has_collision){
	mol0.zero_forces();
	mol1.zero_forces();
  //L::zero( mol0.force);
  //L::zero( mol0.torque);
  //L::zero( mol1.force);
  //L::zero( mol1.torque);
  add_forces_and_torques( molc0, mol0, 
                          molc1, molt1, mol1, 
                          has_collision);
}

void compute_forces_and_torques(
                                const Large_Molecule_Common& molc0,
                                Large_Molecule_State& mol0,
                                const Small_Molecule_Common& molc1,
                                Small_Molecule_Thread& molt1,
                                Small_Molecule_State& mol1,                
                                bool& has_collision,
                                Length& violation,
                                Atom_Large_Ref& aref0, Atom_Small_Ref& aref1
                                ){
	mol0.zero_forces();
	mol1.zero_forces();
  //L::zero( mol0.force);
  //L::zero( mol0.torque);
  //L::zero( mol1.force);
  //L::zero( mol1.torque);
  add_forces_and_torques( molc0, mol0, 
                          molc1, molt1, mol1, 
                          has_collision, violation, aref0, aref1);
}

void normalize( Large_Molecule_State& mol0, Small_Molecule_State& mol1){
  Transform& tform0 = mol0.transform;
  Transform& tform1 = mol1.transform;
  Transform tform1_new, inv_tform0;

  tform0.get_inverse( inv_tform0);
  get_product( inv_tform0, tform1, tform1_new);
  tform1 = tform1_new;
  tform0.set_zero();

  inv_tform0.rotate( mol0.force);
  inv_tform0.rotate( mol1.force);
  inv_tform0.rotate( mol0.torque);
  inv_tform0.rotate( mol1.torque);
}

