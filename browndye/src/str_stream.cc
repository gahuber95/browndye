#include <string.h>
#include "str_stream.hh"

// Implements input stream for XML node contents.

namespace JAM_XML_Parser{

  Str_Stream::Str_Stream( const XML_Parser& _parser, 
                          const String& _tag, const String& str): 
    parser(_parser), tag( _tag), sdata( str){
    if (str.size() > 0)
      super.reset( new Super( str.c_str()));

    is_reset = true;
  }
  
  // new copy is reset
  Str_Stream::Str_Stream( const Str_Stream& stm): 
    parser( stm.parser), tag( stm.tag), sdata( stm.sdata){
    if (sdata.size() > 0)
      super.reset( new Super( stm.sdata.c_str()));
    
    is_reset = true;
  }

  Str_Stream& Str_Stream::operator>>( Str& str){
    is_reset = false;

    if (super){
      //std::stringstream ss; need to fix potential danger      
      //char data[100];      
      (*super) >> str;
      //str = data;     
    }    

    return *this;
  }

  void Str_Stream::reset(){
    if (!is_reset){
      if (sdata.size() > 0)
        super.reset( new Super( sdata.c_str()));
      else
        super.release();

      is_reset = true;
    }
  }
}
