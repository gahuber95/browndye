#ifndef __MOLECULE_COMMON_HH__
#define __MOLECULE_COMMON_HH__

#include <memory>
#include <string>
#include "units.hh"
#include "linalg3.hh"
#include "jam_xml_pull_parser.hh"
#include "parameter_info.hh"
#include "ellipsoid.hh"
#include "field.hh"
#include "born_field_interface.hh"
#include "atom_large.hh"
#include "atom_small.hh"

class Large_Molecule_Common;
class Large_Molecule_State;
class Small_Molecule_Common;
class Small_Molecule_State;
class Small_Molecule_Thread;

template< class Pair_Computer>
class Local_Force_Computer;

template< class Pair_Computer>
class Local_Potential_Computer;

class Molecule_Common{
public:
  typedef Vector< int>::size_type size_type;
  typedef std::string String;

  void initialize_super( JAM_XML_Pull_Parser::Node_Ptr, Vec3< Length>& offset); 

  Molecule_Common();

  void set_h_radius( String file);
  void set_ellipsoids( String file);
  void get_hydro_center( Vec3< Length>& c) const;
  bool charge_available() const;
  Charge charge() const;

  Length hydro_radius() const;
  void set_desolve_fudge( double);
  void set_use_68();

protected:
  friend class Back_Door;

  std::unique_ptr< const Near_Interactions::Parameter_Info> pinfo;
  bool soft_radii_from_param_file, hard_radii_from_param_file;

  Ellipsoid hydro_ellipsoid;
  Length h_radius;

  std::unique_ptr< Field::Field< Born_Field_Interface> > born_field;
  Field::Field< Born_Field_Interface>::Hint born_hint0;

  double desolve_fudge;
  bool use_68;

  friend void reset_hints( const Large_Molecule_Common& molc, Large_Molecule_State& mol);
  friend void reset_hints( const Small_Molecule_Common& molc, Small_Molecule_State& mol);
  friend Length hydro_gap(
                   const Large_Molecule_Common& molc0,
                   const Large_Molecule_State& mols0,
                   const Small_Molecule_Common& molc1,
                   const Small_Molecule_Thread& molt1,
                   const Small_Molecule_State& mols1
                   );
  friend void add_forces_and_torques(
                              const Large_Molecule_Common& molc0,
                              Large_Molecule_State& mol0,
                              const Small_Molecule_Common& molc1,
                              Small_Molecule_Thread& molt1,
                              Small_Molecule_State& mol1,
                              bool& has_collision,
                              Length& violation,
                              Atom_Large_Ref& aref0,
                              Atom_Small_Ref& aref1
                              );

  friend void get_potential_energy(
                            const Large_Molecule_Common& molc0,
                            Large_Molecule_State& mol0,
                            const Small_Molecule_Common& molc1,
                            Small_Molecule_Thread& molt1,
                            Small_Molecule_State& mol1,
                            Energy& vnear, Energy& vcoul, Energy& vdesolv
                             );

  template< class Pair_Computer>
  friend
  class Local_Force_Computer;

  template< class Pair_Computer>
  friend
  class Local_Potential_Computer;

  friend class Small_Molecule_Thread;
};

inline
void Molecule_Common::get_hydro_center( Vec3< Length>& c) const{
	c = hydro_ellipsoid.center;
}

inline
Length Molecule_Common::hydro_radius() const{
  return h_radius;
}

inline
void Molecule_Common::set_desolve_fudge( double f){
	desolve_fudge = f;
}

inline
void Molecule_Common::set_use_68(){
	use_68 = true;
}

template< class Atom>
const Vector<  size_t> gen_atom_indices( const Vector< Atom>& atoms){

  auto n = atoms.size();
  Vector< size_t> indices( n);
  for( size_t i = 0; i < n; i++)
    indices[i] = atoms[i].number;
  return indices;
}

#endif
