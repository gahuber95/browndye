/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
Implements a multithreaded barrier.  The object is initialized
with argument "n". When a process calls "wait", it is held up
until "n" processes have called wait. Then, they are all released.
*/

#ifndef __We_Barrier_hh__
#define __We_Barrier_hh__

#include <pthread.h>

class Barrier{
public:
  void wait();
  void initialize( unsigned int n);
  void release();

private:
  unsigned int count;
  bool cycle;
  unsigned int threshhold;
  pthread_mutex_t mutex;
  pthread_cond_t cond;

};


#endif
