template< class T, class Res>
using Input_Variable = Expression< Input_Variable_Info< T, Res> >;

template< class T>
using Constant = Expression< Constant_Info< T> >;

template< class V>
Variable< Expression< V> > var( Expression< V> expr){
  return Variable< Expression< V> >( expr);
}

template< class T>
T var( T t){
  return t;
}

template< class T>
void copy( const T& a, T& b){
  b = a;
}

template< class Expr, class T, class R>
void copy( const Variable< Expr>& a, Expression< Input_Variable_Info< T,R> >& b){
  b.set_value( a.value());
}

template< class U, class T>
T constant( U dum, T a){
  return a;
}

template< class T0, class T1, class Obj>
Constant< T1> constant( const Input_Variable< T0, Obj>& dum, T1 a){
  return Constant< T1>(a);
}

template< class T>
Constant< T> constant( T a){
  return Constant< T>( a);
}

template< class T, class TAdj>
Input_Variable< T, decltype( TAdj()*T())> 
input( T& t, TAdj& tadj){
  typedef decltype( TAdj()*T()) Obj;
  return Input_Variable< T, Obj>( t, tadj);
} 

template< class Expr>
void back_prop( Variable< Expr>& var){
  var.back_prop();
} 

template< class Expr, class T, class Res>
void back_prop( Variable< Expr>& var, const Input_Variable<T, Res>& iv){
  var.back_prop( iv.adjoint()); 
} 

template< class Expr, class T>
void back_prop( Variable< Expr>& var, T t){
  var.back_prop( t);
}

template< class T>
void back_prop( T){}

template< class T0, class T1>
void back_prop( T0, T1){}

template< class Expr>
void back_prop_top( Variable< Expr>& var){
  var.back_prop_top(); 
} 

template< class T>
void back_prop_top( T){}

template< class T>
typename T::Value value( const Variable<T>& var){
  return var.value();
}

template< class T>
T value( T t){
  return t;
}


