/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

// Implements Spline class. Included by "spline.hh".

#include <stdlib.h>
#include "error_msg.hh"
#include "spline.hh"
#include "found.hh"


/*******************************************************************/

template< class X, class Y> 
void Spline< X, Y>::tri_diag_solve( const Vector< X>& a, 
                             const Vector< X>& b, 
                             Vector< X>& c, 
                             Vector< typename Spline< X,Y>::dYdX>& d, 
                             Vector< typename Spline< X,Y>::d2YdX2>& x){

  const unsigned int n = x.size();

  c[0] = (X)(c[0]/b[0]);
  d[0] = (dYdX)(d[0]/b[0]);
  double id;
  for(unsigned int i = 1; i != n; i++){
    id = fvalue( 1.0/(b[i] - (X)(c[i-1]*a[i])));
    c[i] = c[i]*id;                 
    d[i] = (d[i] - (dYdX)(a[i]*d[i-1]))*id;
  }
  
  x[n-1] = (d2YdX2)(d[n-1]);
  for(int i = n-2; i != -1; i--)
    x[i] = (d2YdX2)(d[i] - c[i]*x[i+1]);
}

/*******************************************************************/
// constructor
template< class X, class Y>
Spline< X,Y>::Spline(){
  odata = false;
  xs_ptr = NULL;
  ys_ptr = NULL;
}

template< class X, class Y>
void Spline< X,Y>::initialize( const Vector< X>& xs, const Vector< Y>& ys){
  clear();

  xs_ptr = &xs;
  ys_ptr = &ys;
  odata = false;

  const unsigned int n = xs.size() < ys.size() ? xs.size() : ys.size(); 
  y2s.resize( n);


  Vector< X> a( n);
  for (unsigned int i = 0; i<n; i++){
    if ((i == 0) || (i == n-1)) 
      a[i] = X(0.0);
    else
      a[i] = (xs[i] - xs[i-1])/6.0; 
  }

  Vector< X> b( n);
  for (unsigned int i = 0; i<n; i++){
    if ((i == 0) || (i == n-1)) 
      b[i] = X(1.0);
    else
      b[i] = (xs[i+1] - xs[i-1])/3.0; 
  }

  Vector< X> c( n);
  for (unsigned int i = 0; i<n; i++){
    if ((i == 0) || (i == n-1)) 
      c[i] = X( 0.0);
    else
      c[i] = (xs[i+1] - xs[i])/6.0; 
  }

  Vector< dYdX> d( n);
  for (unsigned int i = 0; i<n; i++){
    if ((i == 0) || (i == n-1)) 
      d[i] = dYdX( 0.0);
    else
      d[i] = 
        (ys[i+1] - ys[i])/(xs[i+1] - xs[i]) - 
        (ys[i] - ys[i-1])/(xs[i] - xs[i-1]);
  }

  tri_diag_solve( a,b,c,d,y2s);
}

template< class X, class Y>
Spline< X,Y>::Spline( const Vector< X>& xs, const Vector< Y>& ys){
  initialize( xs, ys);
}

/**************************************************************/
template< class X, class Y>
Y Spline< X,Y>::value( const X& x) const{

#ifdef DEBUG
  if (xs_ptr == NULL || ys_ptr == NULL)
    error( "spline not initialized");

#endif


  const Vector< X>& xs = *xs_ptr;
  const Vector< Y>& ys = *ys_ptr;
 
  const unsigned int n = xs.size();
  const unsigned int i = found( xs, x);

  if (i == (n-1))
    return ys[n-1];
  else{
    const X xp = xs[i+1];
    const X xm = xs[i];
    const X delx = xp - xm;
    const double a = (xp - x)/delx;
    const double b = 1.0 - a;
    const X2 c = (a*a*a - a)*delx*delx/6.0;
    const X2 d = (b*b*b - b)*delx*delx/6.0;
    return a*ys[i] + b*ys[i+1] + c*y2s[i] + d*y2s[i+1];
  }
}

/**************************************************************/
template< class X, class Y>
typename UQuot< Y, X>::Res 
Spline< X,Y>::first_deriv( const X& x) const{ 

#ifdef DEBUG
  if (xs_ptr == NULL || ys_ptr == NULL)
    error( "spline not initialized");

#endif


  const Vector< X>& xs = *xs_ptr;
  const Vector< Y>& ys = *ys_ptr;
  const unsigned int n = xs.size();
 
  const unsigned int ii = found( xs, x); 
  const unsigned int i = (ii == n-1) ? n-2 : ii;

  if (i == (n-1))
    return ys[n-1];
  else{
    const X xp = xs[i+1];
    const X xm = xs[i];
    const X delx = xp - xm;
    const double a = (xp - x)/delx;
    const double b = 1.0 - a;
    return
      (ys[i+1] - ys[i])/delx - 
      (3.0*a*a - 1.0)*delx*y2s[i]/6.0 + 
      (3.0*b*b - 1.0)*delx*y2s[i+1]/6.0;
  }
}

/**************************************************************/
// destructor
template< class X, class Y>
void Spline< X,Y>::clear(){
  if (odata){
    if (xs_ptr != NULL)
      delete xs_ptr;
    if (ys_ptr != NULL)
      delete ys_ptr;
  }
}

template< class X, class Y>
Spline< X,Y>::~Spline(){
  clear();
}

template< class X, class Y>
void Spline< X,Y>::make_to_own_data(){
  odata = true;
}

template< class X, class Y>
bool Spline< X,Y>::owns_data() const{
  return odata;
}
