#ifndef __INITIALIZE_FIELD_HH__
#define __INITIALIZE_FIELD_HH__

#include <memory>
#include <string>
#include "vector.hh"
#include "jam_xml_pull_parser.hh"
#include "node_info.hh"
#include "field.hh"
#include "get_strings.hh"

template< class Interface>
void initialize_field( JAM_XML_Pull_Parser::Node_Ptr enode, 
                       std::unique_ptr< Field::Field< Interface> >& field){
  if (enode != NULL){

    auto mpnode = enode->child( "multipole-field");
    
    typedef std::string String;

    // fields listed in no particular order
    Vector< String> grid_files;

    get_strings( enode, "grid", grid_files);

    field.reset( new Field::Field< Interface>());
    
    if (mpnode != NULL){
      String mpfile = string_from_node( mpnode);
      field->initialize( mpfile.c_str(), grid_files);
    }
    else { // mpnode == NULL
      field->initialize( grid_files);
    }
  }
}

#endif
