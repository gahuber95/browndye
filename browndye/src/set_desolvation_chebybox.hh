#ifndef __SET_DESOLVATION_CHEBYBOX_HH__
#define __SET_DESOLVATION_CHEBYBOX_HH__

#include <string>
#include <iostream>
#include "units.hh"
#include "small_vector.hh"
#include "charged_blob_simple.hh"
#include "node_info.hh"

template< class Mol>
void set_desolvation_chebybox( Mol& mol, std::string file, const Vec3< Length>& offset){
  std::cout << "set desolvation chebybox " << file;
  std::cout.flush();
  mol.q2_blob.reset( 
    new Charged_Blob::Blob< 4, typename Mol::Q2_Blob_Interface>());
  mol.q2_blob->initialize( file);
  mol.q2_blob->shift_position( offset);
}

template< class Mol>
void set_desolvation_chebybox_from_node( Mol& mol, JAM_XML_Pull_Parser::Node_Ptr node, const Vec3< Length>& offset){

  bool found;
  std::string cbox_file;
  get_value_from_node( node, "charge-squared", cbox_file, found);
  if (found){
    std::cout <<  "get eff-volumes\n";
    set_desolvation_chebybox( mol, cbox_file.c_str(), offset);
  }    
}

#endif
