#ifndef __PII_HH__
#define __PII_HH__

constexpr double pi = 3.1415926;
constexpr double pi6 = 6*pi;
constexpr double pi4 = 4*pi;
constexpr double pi8 = 8*pi;

#endif
