
  class SField_Interface{
  public:
    typedef SGrid Contained;
    typedef Vec3< Length> Point;

    static 
    bool ioverlap( Length a, Length b, Length x, Length y){
      bool res =  (b > x) && (a < y);
      return res;
    }
    
    static 
    bool overlap( const SGrid& grida, const SGrid& gridb){
      Point lowa, higha;
      grida.get_low_corner( lowa);
      grida.get_high_corner( higha);
      Point lowb, highb;
      gridb.get_low_corner( lowb);
      gridb.get_high_corner( highb);

      return
        ioverlap( lowa[0], higha[0], lowb[0], highb[0]) && 
        ioverlap( lowa[1], higha[1], lowb[1], highb[1]) && 
        ioverlap( lowa[2], higha[2], lowb[2], highb[2]);
    }

    static 
    bool iencloses( Length a, Length b, Length x, Length y){
      return (a <= x) && (y <= b);
    }

    static
    bool encloses( const SGrid& grid0, const SGrid& grid1){
      Point low1, high1;
      grid1.get_low_corner( low1);
      grid1.get_high_corner( high1);

      Point low0, high0;
      grid0.get_low_corner( low0);
      grid0.get_high_corner( high0);

      bool in0 =  iencloses( low0[0], high0[0], low1[0], high1[0]);
      bool in1 =  iencloses( low0[1], high0[1], low1[1], high1[1]);
      bool in2 =  iencloses( low0[2], high0[2], low1[2], high1[2]);
      
      return in0 && in1 && in2;
    } 

    static
    bool between( Length a, Length x, Length b){
      return a <= x && x < b;
    }

    static
    bool is_inside( Vec3< Potential_Gradient>& grad, 
                    const Point& pos, const SGrid& grid){
      bool in_range;
      grid.get_gradient( pos, grad, in_range);
      return in_range;
    }

    static
    bool is_inside( Potential& v, 
                    const Point& pos, const SGrid& grid){

      bool in_range;
      grid.get_potential( pos, v, in_range);
      return in_range;
    }

    static 
    void error( const std::string& msg1){
      const std::string msg0 = "Field: ";
      ::error( msg0, msg1);
    }

  };

private:
  typedef Multipole_Field::Field MField;

  std::unique_ptr< MField> mfield;
  SField_Ptr::Tree< SField_Interface> sfields;
};
