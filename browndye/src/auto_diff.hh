#include <math.h>
#include <type_traits>

template< class Info>
class Expression;

class None{};

template< class T>
class Constant_Info{
public:
  typedef T Value;
  typedef None Objective;
};


template< class T>
class Expression< Constant_Info< T> >{
public:
  typedef T Value;
  typedef None Objective;

  Expression( Value _val): val(_val){}

  T value() const{
    return val;
  }

  static
  bool is_variable(){
    return false;
  }

  template< class Adjoint>
  void back_prop( Adjoint) const{
    // dummy
  }

private:
  T val;
};

template< class T, class Obj>
class Input_Variable_Info{
public:
  typedef T Value;
  typedef Obj Objective;
};

template< class T, class Obj>
class Expression< Input_Variable_Info< T, Obj> >{
public:
  typedef T Value;
  typedef decltype( Obj()/T()) Adjoint;
  typedef T Input_Value;
  typedef Obj Objective;

  Expression( T& t, Adjoint& tadj): val(&t), adj( &tadj){
    *adj = Adjoint( 0.0);
  }
  
  Expression(){
    #ifdef DEBUG
    val = NULL;
    adj = NULL;
    #endif
  }

  T value() const{
    return *val;
  }

  void set_value( Value a){
    (*val) = a;
  }

  Adjoint adjoint() const{
    return *adj;
  }

  static
  bool is_variable(){
    return true;
  }

  void back_prop( Adjoint dadj){
    (*adj) += dadj;
  }

private: 
  T* val;
  Adjoint* adj;
};

template< class Expr>
class Variable{
public:
  typedef typename Expr::Value Value;
  typedef typename Expr::Adjoint Adjoint;

  Variable( Expr _expr): adj_sum( Adjoint(0.0)), expr(_expr){}

  void back_prop(){
    expr.back_prop( adj_sum);
  }

  void back_prop( Adjoint adj){
    adj_sum += adj;
    expr.back_prop( adj_sum);
  }

  void back_prop_inc( Adjoint adj){
    adj_sum += adj;
  }

  void back_prop_top(){
    expr.back_prop( Adjoint(1.0));
  }

  Value value() const{
    return expr.value();
  }

  Adjoint adjoint() const{
    return adj_sum;
  }

private:
  Adjoint adj_sum;
  Expr expr;
};

template< class Expr>
class Variable_Wrapper_Info{
public:
  typedef typename Expr::Value Value;
  typedef typename Expr::Objective Objective;
};

template< class Expr>
class Expression< Variable_Wrapper_Info< Expr> >{
public:
  typedef typename Expr::Value Value;
  typedef typename Expr::Adjoint Adjoint;

  Expression( Variable< Expr>& _var): var(_var){}

  static
  bool is_variable(){
    return true;
  }
  
  void back_prop( Adjoint adj){
    var.back_prop_inc( adj);
  }

  Value value() const{
    return var.value();
  }

  Adjoint adjoint() const{
    return var.adjoint();
  }
  
private:
  Variable< Expr>& var;
};

template< class Input, class F, class Obj>
class Fun1_Info{
public:
  typedef typename F::Value Value;
  typedef Obj Objective;
};

template< class Input, class F, class Obj>
class Expression< Fun1_Info< Input, F, Obj> >{
public:
  typedef typename F::Value Value;

  typedef decltype( Obj()/Value()) Adjoint;
  typedef typename Input::Value Input_Value;
  typedef decltype( Value()/Input_Value()) Derivative;
  typedef Obj Objective;

  void back_prop( Adjoint adj){
    input.back_prop( adj*deriv);
  }

  Expression( Input inp): input(inp){
    F::get_info( inp.value(), _value, deriv);
  }

  static
  bool is_variable(){
    return false;
  }

  Value value() const{
    return _value;
  }

private:
  Input input;
  Value _value;
  Derivative deriv;                       
};

template< class Input0, class Input1, class F, class Obj>
class Fun2_Info{
public:
  typedef typename F::Value Value;
  typedef Obj Objective;

  static_assert( !(std::is_same< None, Obj>::value), "Fun2_Info: Obj should not be None");
};

template< class Input0, class Input1, class F, class Obj>
class Expression< Fun2_Info< Input0, Input1, F, Obj> >{
public:
  typedef typename F::Value Value;

  static_assert( !(std::is_same< None, Obj>::value), "Expression Fun2_Info: Obj should not be None");

  typedef decltype( Obj()/Value()) Adjoint;
  typedef typename Input0::Value Input_Value0;
  typedef typename Input1::Value Input_Value1;
  typedef decltype( Value()/Input_Value0()) Derivative0;
  typedef decltype( Value()/Input_Value1()) Derivative1;
  typedef Obj Objective;

  void back_prop( Adjoint adj){
    input0.back_prop( adj*deriv0);
    input1.back_prop( adj*deriv1);
  }

  Expression( Input0 inp0, Input1 inp1): input0(inp0), input1(inp1){
    F::get_info( inp0.value(), inp1.value(), _value, deriv0, deriv1);
  }

  static
  bool is_variable(){
    return false;
  }

  Value value() const{
    return _value;
  }

private:
  Input0 input0;
  Input1 input1;
  Value _value;
  Derivative0 deriv0;
  Derivative1 deriv1;
};

template< class T>
class Sqrt{
public:
  typedef T Input_Value;
  typedef decltype( sqrt(T())) Value;
  typedef decltype( 1.0/sqrt(T())) Deriv;

  static
  void get_info( Input_Value x, Value& value, Deriv& deriv){
    value = sqrt(x);
    deriv = 0.5/value;
  }  
};

template< class T>
class Abs{
public:
  typedef T Input_Value;
  typedef T Value;
  typedef double Deriv;

  static
  void get_info( Input_Value x, Value& value, Deriv& deriv){
    value = fabs(x);
    deriv = x > T(0.0) ? 1.0 : -1.0;
  }  

  static
  Value value( Input_Value x){
    return fabs(x);
  }
};

template< class T>
class Log{
public:
  static_assert( std::is_same< T, double>::value, "Log: input must be double");
  typedef double Input_Value;
  typedef double Value;
  typedef double Deriv;

  static
  void get_info( Input_Value x, Value& value, Deriv& deriv){
    value = log(x);
    deriv = 1.0/x;
  }  
};

template< class T>
class Step{
public:
  typedef T Input_Value;
  typedef double Value;
  typedef decltype(double(1.0)/T()) Deriv;

  static
  void get_info( Input_Value x, Value& value, Deriv& deriv){
    value = Step::value(x);
    deriv = Deriv( 0.0);
  }  

  static
  Value value( Input_Value x){
    return x > T(0.0) ? 1.0 : 0.0;
  }
};

template< class T>
class Sq{
public:
  typedef T Input_Value;
  typedef decltype( T()*T()) Value;
  typedef T Deriv;

  static
  void get_info( Input_Value x, Value& value, Deriv& deriv){
    value = x*x;
    deriv = 2.0*x;
  }

  static
  Value value( Input_Value x){
    return x*x;
  }
};

template< class T>
class Cube{
public:
  typedef T Input_Value;
  typedef decltype( T()*T()*T()) Value;
  typedef decltype(T()*T()) Deriv;

  static
  void get_info( Input_Value x, Value& value, Deriv& deriv){
    auto x2 = x*x;
    value = x*x2;
    deriv = 3.0*x2;
  }  

  static
  Value value( Input_Value x){
    return x*x*x;
  }
};

template< class T>
class Exp{
public:
  static_assert( std::is_same< T, double>::value, "Exp: input must be double");

  typedef double Input_Value;
  typedef double Value;
  typedef double Deriv;

  static
  void get_info( Input_Value x, Value& value, Deriv& deriv){
    value = exp(x);
    deriv = value;
  }  

  static
  Value value( Input_Value x){
    return exp(x);
  }
};

template< class T>
class Neg{
public:
  typedef T Input_Value;
  typedef T Value;
  typedef double Deriv;

  static
  void get_info( Input_Value x, Value& value, Deriv& deriv){
    value = -x;
    deriv = -1.0;
  }

  static
  Value value( Input_Value x){
    return -x;
  }

};

template< class T0, class T1>
class Plus{
public:
  typedef T0 Input_Value0;
  typedef T1 Input_Value1;
  typedef T0 Value;
  typedef decltype(T0()/T0()) Deriv0;
  typedef decltype(T1()/T1()) Deriv1;

  static_assert( std::is_same< T0, T1>::value, 
                 "Plus: both args must be same type");

  static
  void get_info( Input_Value0 x0, Input_Value1 x1, Value& value, 
                 Deriv0& deriv0, Deriv1& deriv1){
    
    value = x0 + x1;
    deriv0 = Deriv0( 1.0);
    deriv1 = Deriv1( 1.0);
  }
};

template< class T0, class T1>
class Minus{
public:
  typedef T0 Input_Value0;
  typedef T1 Input_Value1;
  typedef T0 Value;
  typedef decltype(T0()/T0()) Deriv0;
  typedef decltype(T1()/T1()) Deriv1;

  static_assert( std::is_same< T0, T1>::value, 
                 "Minus: both args must be same type");

  static
  void get_info( Input_Value0 x0, Input_Value1 x1, Value& value, 
                 Deriv0& deriv0, Deriv1& deriv1){
    
    value = x0 - x1;
    deriv0 = Deriv0( 1.0);
    deriv1 = Deriv1( -1.0);
  }
};

template< class T0, class T1>
class Times{
public:
  typedef T0 Input_Value0;
  typedef T1 Input_Value1;
  typedef decltype(T0()*T1()) Value;
  typedef T1 Deriv0;
  typedef T0 Deriv1;

  static
  void get_info( Input_Value0 x0, Input_Value1 x1, Value& value, 
                 Deriv0& deriv0, Deriv1& deriv1){
    
    value = x0*x1;
    deriv0 = x1;
    deriv1 = x0;
  }  
};

template< class T0, class T1>
class Divided_By{
public:
  typedef T0 Input_Value0;
  typedef T1 Input_Value1;
  typedef decltype(T0()/T1()) Value;
  typedef decltype( Value()/T0()) Deriv0;
  typedef decltype( Value()/T1()) Deriv1;

  static
  void get_info( Input_Value0 x0, Input_Value1 x1, Value& value, 
                 Deriv0& deriv0, Deriv1& deriv1){
    
    value = x0/x1;
    deriv0 = 1.0/x1;
    deriv1 = -x0/(x1*x1);
  }  
};

template< class T0, class T1>
class Obj_Resolver{
public:
  static_assert( !(std::is_same<T0,None>::value || 
                   std::is_same<T1,None>::value), 
                 "Obj_Resolver: one is a None");
};

template< class T>
class Obj_Resolver<T,T>{
public:
  static_assert( !(std::is_same< T, None>::value), 
                 "Obj_Resolver: should not be a None"); 
  typedef T Res;
};

template< class T>
class Obj_Resolver<T,None>{
public:
  typedef T Res;
};

template< class T>
class Obj_Resolver<None,T>{
public:
  typedef T Res;
};

template<>
class Obj_Resolver< None, None>{};

#define DEF_OP1( fun, Fun)                                              \
template< class Info>                                                   \
inline                                                                  \
auto fun( Expression< Info> expr) ->                                    \
  Expression< Fun1_Info< Expression< Info>, Fun< typename Info::Value>, typename Info::Objective> >{ \
  typedef Expression< Info> Expr;                                       \
  typedef typename Expr::Objective Obj;                                 \
  typedef typename Expr::Value Value;                                   \
  return Expression< Fun1_Info< Expr, Fun< Value>, Obj > >( expr);      \
}                                                                       \
                                                                        \
template< class Expr>                                                   \
inline                                                                  \
auto fun( Variable< Expr>& var) ->                                      \
  Expression< Fun1_Info< Expression< Variable_Wrapper_Info< Expr> >, Fun< typename Expr::Value>, typename Expr::Objective> >{ \
                                                                        \
  typedef Expression< Variable_Wrapper_Info< Expr> > Wrap;              \
  typedef typename Expr::Objective Obj;                                 \
  typedef typename Expr::Value Value;                                   \
                                                                        \
  return Expression< Fun1_Info< Wrap, Fun< Value>, Obj> >( Wrap(var));  \
}                                                                       \
                                                                        \
template< class T>                                                      \
inline                                                                  \
auto fun( const Expression< Constant_Info< T> >& c) -> Expression< Constant_Info< typename Fun< T>::Value> >{ \
  return Expression< Constant_Info< typename Fun< T>::Value> >( Fun<T>::value( c.value())); \
}


#define DEF_OP2( op, Op)                                                 \
template< class Info0, class Info1>                                     \
inline                                                                  \
auto operator op( Expression< Info0> expr0, Expression< Info1> expr1) -> \
  Expression< Fun2_Info< Expression< Info0>, Expression< Info1>,        \
                         Op< typename Info0::Value, typename Info1::Value >, \
                         typename Obj_Resolver< typename Info0::Objective, typename Info1::Objective>::Res> > \
{                                                                      \
  typedef Expression< Info0> Expr0;                                     \
  typedef Expression< Info1> Expr1;                                     \
  typedef typename Obj_Resolver< typename Expr0::Objective, typename Expr1::Objective>::Res Obj; \
  static_assert( !(std::is_same< Obj, None>::value), "op2: should not be None"); \
  typedef typename Expr0::Value T0;                                     \
  typedef typename Expr1::Value T1;                                     \
  return Expression< Fun2_Info< Expr0, Expr1, Op< T0,T1>, Obj> >( expr0, expr1); \
}                                                                       \
                                                                        \
template< class Expr0, class Expr1>                                     \
inline                                                                  \
auto operator op( Variable< Expr0>& var0, Variable< Expr1>& var1) ->   \
  Expression< Fun2_Info< Expression< Variable_Wrapper_Info< Expr0> >,   \
                         Expression< Variable_Wrapper_Info< Expr1> >,   \
                         Op< typename Expr0::Value, typename Expr1::Value>, \
                         typename Obj_Resolver< typename Expr0::Objective, typename Expr1::Objective>::Res> > \
{                                                                       \
  typedef Expression< Variable_Wrapper_Info< Expr0> > Wrap0;            \
  typedef Expression< Variable_Wrapper_Info< Expr1> > Wrap1;            \
  typedef typename Obj_Resolver< typename Expr0::Objective, typename Expr1::Objective>::Res Obj; \
  typedef typename Expr0::Value T0;                                     \
  typedef typename Expr1::Value T1;                                     \
                                                                        \
  return Expression< Fun2_Info< Wrap0, Wrap1, Op< T0,T1>, Obj> >( Wrap0( var0), Wrap1( var1)); \
}                                                                       \
                                                                        \
template< class Info0, class Expr1>                                     \
inline                                                                  \
auto operator op( Expression< Info0> expr0, Variable< Expr1>& var1) -> \
  Expression< Fun2_Info< Expression< Info0>,                            \
                         Expression< Variable_Wrapper_Info< Expr1> >,   \
                         Op< typename Info0::Value, typename Expr1::Value>, \
                         typename Obj_Resolver< typename Info0::Objective, typename Expr1::Objective>::Res> > \
{                                                                       \
  typedef Expression< Info0> Expr0;                                     \
  typedef Expression< Variable_Wrapper_Info< Expr1> > Wrap1;            \
  typedef typename Obj_Resolver< typename Info0::Objective, typename Expr1::Objective>::Res Obj; \
  typedef typename Info0::Value T0;                                     \
  typedef typename Expr1::Value T1;                                     \
                                                                        \
  return Expression< Fun2_Info< Expr0, Wrap1, Op< T0,T1>, Obj> >( expr0, Wrap1( var1)); \
}                                                                       \
                                                                        \
template< class Expr0, class Info1>                                     \
inline                                                                  \
auto operator op( Variable< Expr0>& var0, Expression< Info1> expr1) -> \
  Expression< Fun2_Info< Expression< Variable_Wrapper_Info< Expr0> >, Expression< Info1>, \
                         Op< typename Expr0::Value, typename Info1::Value >, \
                         typename Obj_Resolver< typename Expr0::Objective, typename Info1::Objective>::Res> > \
{                                                                       \
  typedef Expression< Info1> Expr1;                                     \
  typedef Expression< Variable_Wrapper_Info< Expr0> > Wrap0;            \
  typedef typename Obj_Resolver< typename Expr0::Objective, typename Info1::Objective>::Res Obj; \
  typedef typename Expr0::Value T0;                                     \
  typedef typename Info1::Value T1;                                     \
                                                                        \
  return Expression< Fun2_Info< Wrap0, Expr1, Op< T0,T1>, Obj> >( Wrap0( var0), expr1); \
}                                                                       \
                                                                        \
template< class T0, class T1>                                           \
inline                                                                  \
auto operator op( const Expression< Constant_Info< T0> >& c0, const Expression< Constant_Info< T1> >& c1) -> \
  Expression< Constant_Info< decltype( T0() op T1())> > {               \
  return Expression< Constant_Info< decltype( T0() op T1())> >( c0.value() op c1.value()); \
} 


DEF_OP2( +, Plus)
DEF_OP2( *, Times)
DEF_OP2( -, Minus)
DEF_OP2( /, Divided_By)

DEF_OP1( sqrt, Sqrt)
DEF_OP1( log, Log)
DEF_OP1( fabs, Abs)
DEF_OP1( sq, Sq)
DEF_OP1( cube, Cube)
DEF_OP1( exp, Exp)
DEF_OP1( step, Step)
DEF_OP1( operator-, Neg)

#define DEF_COMP( op)                                                   \
template< class T0, class T1>                                           \
bool operator op( const Expression< T0>& ex0, const Expression< T1>& ex1){ \
  return ex0.value() op ex1.value();                                    \
}                                                                       \
                                                                        \
template< class T0, class T1>                                           \
bool operator op( const Expression< T0>& ex0, const Variable< T1>& ex1){ \
  return ex0.value() op ex1.value();                                    \
}                                                                       \
                                                                        \
template< class T0, class T1>                                           \
bool operator op( const Variable< T0>& ex0, const Variable< T1>& ex1){  \
  return ex0.value() op ex1.value();                                    \
}                                                                       \
                                                                        \
template< class T0, class T1>                                           \
bool operator op( const Variable< T0>& ex0, const Expression< T1>& ex1){ \
  return ex0.value() op ex1.value();                                    \
}

DEF_COMP( <)
DEF_COMP( <=)
DEF_COMP( >)
DEF_COMP( >=)
DEF_COMP( ==)
DEF_COMP( !=)

