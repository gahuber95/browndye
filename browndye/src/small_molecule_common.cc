#include "small_molecule_common.hh"
#include "set_desolvation_chebybox.hh"

void Small_Molecule_Common::set_electric_chebybox( String file, const Vec3< Length>& offset){

  typedef Blob_Interface< Col_Interface_Large,  
                          V_Field_Interface> Q_Blob_Interface;
  
  q_blob.reset( new Charged_Blob::Blob< 4, Q_Blob_Interface>());
  q_blob->initialize( file);
  q_blob->shift_position( offset);
}

void Small_Molecule_Common::initialize( JAM_XML_Pull_Parser::Node_Ptr node){
  Vec3< Length> offset; 
  initialize_super( node, offset);

  bool found;
  String cbox_file;
  get_value_from_node( node, "eff-charges", cbox_file, found);
  if (found){
    printf( "get eff-charges\n"); 
    set_electric_chebybox( cbox_file.c_str(), offset);
  }
  set_desolvation_chebybox_from_node( *this, node, offset); 
}

Charge Small_Molecule_Common::charge() const{
  if (q_blob == NULL)
    error( "Molecule::charge: info not available");

  return q_blob->total_charge();
}

