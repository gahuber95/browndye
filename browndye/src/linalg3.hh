/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/


#ifndef __LINALG3_HH__
#define __LINALG3_HH__

/*
Implements simple linear algebra functions of 3-D systems.

 */

#include <math.h>
#include <stdlib.h>
#include "units.hh"
#include "small_vector.hh"

namespace Linalg3{

template< class U>
inline
void fill_with_nan( Vec3< U>& a){
  set_fvalue( a[0], NAN);
  set_fvalue( a[1], NAN);
  set_fvalue( a[2], NAN);
}

template< class U>
inline
void fill( U x, Vec3< U>& a){
  a[0] = x;
  a[1] = x;
  a[2] = x;
}

template< class U>
inline
void zero( Vec3< U>& a){
  fill( U( 0.0), a);
}

template< class U0, class U1>
inline
void get_cross( const Vec3< U0>& a, const Vec3< U1>& b, 
                Vec3<  typename UProd<  U0, U1>::Res>& c){
  c[0] = a[1]*b[2] - a[2]*b[1];
  c[1] = a[2]*b[0] - a[0]*b[2];
  c[2] = a[0]*b[1] - a[1]*b[0];
}

template< class U>
inline
void copy( const Vec3< U>& from, Vec3< U>& to){
  to[0] = from[0];
  to[1] = from[1];
  to[2] = from[2];
}

template< class U>
inline
void get_diff( const Vec3< U>&a, const Vec3< U>& b, Vec3< U>& c){
  for( int i = 0; i<3; i++)
    c[i] = a[i] - b[i];
}

template< class U>
inline
void get_sum( const Vec3< U>&a, const Vec3< U>& b, Vec3< U>& c){
  for( int i = 0; i<3; i++)
    c[i] = a[i] + b[i];
}


template< class U0, class U1>
inline
void get_scaled( const U0& a, const Vec3< U1>& b, 
                 Vec3< typename UProd< U0, U1>::Res>& c){
  for( int i = 0; i<3; i++)
    c[i] = a*b[i];
}

template< class U0, class U1>
inline
void add_scaled( const U0& a, const Vec3< U1>& b, 
                 const Vec3< typename UProd< U0, U1>::Res>& c,
                 Vec3< typename UProd< U0, U1>::Res>& d){
  for( int i = 0; i<3; i++)
    d[i] = a*b[i] + c[i];
}



template< class U0, class U1>
inline
void add_scaled( const U0& a, const Vec3< U1>& b, 
                 Vec3< typename UProd< U0, U1>::Res>& c){
  for( int i = 0; i<3; i++)
    c[i] += a*b[i];
}

template< class U>
inline
typename UProd< U, U>::Res sq( const U& x){ return x*x;}

template< class U0, class U1>
void get_mv_prod( const Mat3< U0>& A, const Vec3< U1>& x, 
                  Vec3< typename UProd< U0, U1>::Res>& b){
  for( int i = 0; i<3; i++){
    //typename Mat3<U0>::Const_Row Ai = A[i];
    const Vec3< U0> Ai = A[i];
    b[i] = Ai[0]*x[0] + Ai[1]*x[1] + Ai[2]*x[2];
  }
}

  // kludge to for compiling with g++ 3.2.2.
inline
void get_mv_prod( const Mat3< double>& A, const double* x, 
                  double* b){
  for( int i = 0; i<3; i++){
    //Mat3<double>::Const_Row 
    const Vec3< double> Ai = A[i];
    b[i] = Ai[0]*x[0] + Ai[1]*x[1] + Ai[2]*x[2];
  }
}

inline
void get_id_mat( Mat3< double>& mat){
  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++)
      mat[i][j] = 0.0;

  for (int i = 0; i < 3; i++)
    mat[i][i] = 1.0;
}

template< class U0, class U1, class U01>
void get_mm_prod( const Mat3< U0>& A, const Mat3< U1>& B, 
                  Mat3<  U01>& C){

  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++){
      U01 sum( 0.0);
      for (int k = 0; k < 3; k++)
        sum += A[i][k]*B[k][j];
      C[i][j] = sum;
    }
}

  // kludge to for compiling with g++ 3.2.2
inline
void get_mm_prod( const Mat3< double>& A, const Mat3< double>& B, 
                  Mat3< double>& C){

  for (int i = 0; i < 3; i++)
    for (int j = 0; j < 3; j++){
      double sum( 0.0);
      for (int k = 0; k < 3; k++)
        sum += A[i][k]*B[k][j];
      C[i][j] = sum;
    }
}
  
template< class U>
void mat_mult( const Mat3< double>& rot, Vec3< U>& a){

  Vec3< U> b;
  get_mv_prod( rot, a, b);
  copy( b, a);
}

template< class U0, class U1>
inline
typename UProd< U0, U1>::Res dot( const Vec3< U0>& x, const Vec3< U1>& y){
  return x[0]*y[0] + x[1]*y[1] + x[2]*y[2];
}

template< class U>
inline
U norm( const Vec3< U>& x){
  return sqrt( dot( x,x));
}

template< class U>
inline
decltype(U()*U()) norm2( const Vec3< U>& x){
  return dot( x,x);
}

template< class U>
void get_normed( const Vec3< U>& x, Vec3< double>& y){
  U nrm = norm( x);
  get_scaled( 1.0/nrm, x, y);
}

template< class U0, class U, class U1>
inline
typename UProd< typename UProd< U0, U>::Res, U1>::Res
vmv_prod( const Vec3< U0>& x0, const Mat3< U>& A, const Vec3< U1>& x1){
  Vec3< typename UProd< U, U1>::Res> Ax1;
  get_mv_prod( A, x1, Ax1);
  return dot( x0, Ax1);
}

template< class U>
inline
U distance( const Vec3< U>& a, const Vec3< U>& b){
  Vec3< U> d;
  get_diff( a, b, d);
  return norm( d);
}

void get_perp_axes( const Vec3< double>& axis2, 
                    Vec3< double>& axis0, Vec3< double>& axis1);
 

}
#endif
