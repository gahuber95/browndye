/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
Implementation of Simulator.
*/

#include <time.h>
#include "physical_constants.hh"
#include "simulator.hh"
#include "node_info.hh"
#include "outtag.hh"

void Simulator::begin_output( std::ofstream& outp){
  
  outp << "<rates>\n";
  common.output( outp);
}

// destructor
Simulator::~Simulator(){
  for (auto itr = states.begin(); itr != states.end(); ++itr)
    delete (*itr);
}


// make sure mthreads are allocated first
Vector< uint32_t> Simulator::initialize_rngs( uint32_t seed){
  size_type n = mthreads.size();

  constexpr size_t ns = 624;

  std::seed_seq sseq = {seed};
  std::mt19937_64 gen( sseq);
  std::uniform_int_distribution< uint32_t> unif;

  std::set< Vector< uint32_t> > seeds;
  for( auto i = 0u; i < n; i++){
    Vector< uint32_t> seed( ns);
    do {
      for( auto& subseed: seed)
	subseed = unif( gen);
    }
    while( seeds.find( seed) != seeds.end());
    mthreads[i].set_seed( seed);
  }

  Vector< uint32_t> xseed( ns);
  do {
    for( auto& subseed: xseed)
      subseed = unif( gen);
  }
  while( seeds.find( xseed) != seeds.end());
  
  return xseed;
}

typedef std::string String;
namespace JP = JAM_XML_Pull_Parser;

template< class U>
void get_unit_from_node( JP::Node_Ptr node, const char* tag, U& value, bool& found){
  double fval;
  get_value_from_node( node, tag, fval, found);
  if (found)
    set_fvalue( value, fval);
}

JAM_XML_Pull_Parser::Node_Ptr Simulator::initialized_node( JP::Parser& parser){

  parser.complete_current_node();
  Node_Ptr node = parser.current_node();

  bool tfound;
  size_type nthreads;
  get_value_from_node( node, "n-threads", nthreads, tfound);
  if (!tfound)
    nthreads = 1;

  mthreads.resize( nthreads);

  bool sfound;
  uint32_t seed;
  get_value_from_node( node, "seed", seed, sfound);

  if (sfound){
    extra_seed = initialize_rngs( seed);
  }
  else{
    extra_seed = initialize_rngs( clock());
  }

  Node_Ptr mp_node = checked_child( node, "molecule-pair");

  String solvent_file; 
  get_value_from_node( node, "solvent-file", solvent_file);

  common.initialize( mp_node, solvent_file.c_str());

  for( size_type i = 0; i < nthreads; i++)
    mthreads[i].initialize( common, mp_node, i);

  output_name = string_from_node( node, "output");

  common.renormalize_reactions( mthreads[0]);
  return node;
}

void psp( std::ofstream& out, unsigned int n){
  for( unsigned int i = 0; i < n; i++)
    out << " ";
}

/*
template< class Atom>
const Vector<  Vector< int>::size_type > atom_indices( const Vector< Atom>& atoms){
  typedef Vector< int>::size_type size_type;
  size_type n = atoms.size();
  Vector< size_type> indices( n);
  for( size_type i = 0; i < n; i++)
    indices[i] = atoms[i].number;
  return indices;
}

// necessary to renumber atoms because only surface atoms are included.
void Simulator::renormalize_reactions(){
  const Vector< Atom_Large>& atoms0 = common.mol0.atoms;
  const Vector< size_type> indices0 = atom_indices( atoms0);

  const Vector< Atom_Small>& atoms1 = mthreads[0].mol1.atoms;
  const Vector< size_type> indices1 = atom_indices( atoms1);
  
  common.pathway->remap_molecule0( indices0);
  common.pathway->remap_molecule1( indices1);
}
*/
