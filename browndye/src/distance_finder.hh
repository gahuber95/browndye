#ifndef __DISTANCE_FINDER_HH__
#define __DISTANCE_FINDER_HH__

#include "units.hh"
#include "small_vector.hh"
#include "linalg3.hh"

template< class CInterface> 
class Distance_Finder{
public:
  typedef typename CInterface::Atom Atom;
  typedef typename CInterface::Atoms Atoms;
  typedef typename CInterface::Transform Transform;
  typedef typename CInterface::Ref  Ref;
  typedef typename CInterface::Refs Refs;
  typedef typename Atoms::size_type size_type;

  bool operator()( const Atoms&, const Refs& arefs, 
                   Length r, const Vec3< Length>& pos) const{

    const size_type n = arefs.size();
    for( size_type i = 0; i<n; i++){
      Ref aref = arefs[i];
      Vec3< Length> tpos;
      aref->get_position( tpos);

      if (Linalg3::distance( tpos, pos) < r + aref->hard_radius)
        return true;
    }
    return false;
  }
};

#endif
