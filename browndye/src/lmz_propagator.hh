/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/


#ifndef __LMZ_PROPAGATOR_HH__
#define __LMZ_PROPAGATOR_HH__

/*
  Used to propagate the molecules in the region outside the b-sphere.
  The Propagator object is initialized from the data output by the Ocaml program 
  "sphere_return_distribution" 
  (see notes in ../aux/sphere_return_distribution.ml)

  The arguments to the "propagate" function are as follows:
  Inputs:
  rng - random number generator
  r0b, rot0b - position and rotational state of Molecule 0 at beginning
  r1b, rot1b - position and rotational state of Molecule 1 at beginning

  Outputs:
  escaped - "true" if escaped
  rot0 - rotational state of Molecule 0 at end (position stays the same)
  r1, rot1 position and rotational state of Molecule 1 at end
  t - elapsed time
*/

#include "lmz_propagator_pre.hh"

namespace LMZ{

  template< class I>
  class Propagator{
  public:
    typedef typename I::Random_Number_Generator RNG;
    typedef typename I::Length Length;
    typedef typename I::Time Time;
    typedef typename I::Energy Energy;
    typedef typename I::Charge Charge;

    typedef decltype( Energy()/Length()) Force;
    typedef decltype( Force()*Time()/(Length()*Length())) Viscosity;
    typedef decltype( Charge()*Charge()/(Energy()*Length())) Permittivity;
    typedef decltype( 1.0/(Length()*Viscosity())) Mobility;
    typedef decltype( 1.0/Time()) Inv_Time;    

    void initialize( JP::Node_Ptr node);
  
    void propagate( RNG& rng, 
                    const Vec3< Length>& r0b, const Mat3< double>& rot0b,
                    const Vec3< Length>& r1b, const Mat3< double>& rot1b,
                    bool& escaped, Mat3< double>& rot0, 
                    Vec3< Length>& r1, Mat3< double>& rot1, 
                    Time& t) const; 

    Mobility radial_mobility( Length) const;
    Mobility perpendicular_mobility( Length) const;
    Force radial_force( Length) const;
    Inv_Time rotational_diffusivity0() const;
    Inv_Time rotational_diffusivity1() const;

    bool has_hemisphere() const;
  private:
    typedef typename Vector< Shell_Distribution<I> >::size_type size_type;

    Vector< Shell_Distribution<I> > shell_dists;
    double outer_prob_return;
    Inv_Time rdiff0, rdiff1;
    Length inner_radius;

    Charge q0, q1;
    Length dL;
    Length a0, a1;
    Permittivity vperm;
    double diel;
    Viscosity mu;
    bool hi;
    bool hemisphere = false;
  };

}

#include "lmz_propagator_impl.hh"

#endif
