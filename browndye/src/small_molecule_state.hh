#ifndef __SMALL_MOLECULE_STATE_HH__
#define __SMALL_MOLECULE_STATE_HH__

#include "units.hh"
#include "small_vector.hh"
#include "transform.hh"
#include "field.hh"
#include "born_field_interface.hh"
#include "atom_large.hh"
#include "atom_small.hh"
#include "charged_blob_simple.hh"
#include "field_for_blob.hh"
#include "blob_interface.hh"

class Small_Molecule_Common;
class Large_Molecule_Common;
class Large_Molecule_State;
class Small_Molecule_Thread;

class Small_Molecule_State{
public:
  Small_Molecule_State();

  void get_rotation( Mat3< double>& rot) const;
  void get_position( Vec3< Length>& ) const;
  void set_rotation( const Mat3< double>& rot);
  void set_position( const Vec3< Length>& ); 
  void set_postion( const Vec3< Length>& pos);
  void get_transform( Transform&) const;
  void set_zero_transform();
  void zero_forces();
  void get_force( Vec3< Force>&) const;
  void get_torque( Vec3< Torque>&) const;

protected: 
  friend class Back_Door;

  Transform transform;
  Vec3< Force> force;
  Vec3< Torque> torque;
  Field::Field< Born_Field_Interface>::Hint born_hint;

  friend class Stepper_Interface;

  friend
  void reset_hints( const Small_Molecule_Common& molc, Small_Molecule_State& mol);

  friend
  void add_forces_and_torques(
                              const Large_Molecule_Common& molc0,
                              Large_Molecule_State& mol0,
                              const Small_Molecule_Common& molc1,
                              Small_Molecule_Thread& molt1,
                              Small_Molecule_State& mol1,
                              
                              bool& has_collision,
                              Length& violation,
                              Atom_Large_Ref& aref0,
                              Atom_Small_Ref& aref1
                              );
  
  friend
  void get_potential_energy(
                            const Large_Molecule_Common& molc0,
                            Large_Molecule_State& mol0,
                            const Small_Molecule_Common& molc1,
                            Small_Molecule_Thread& molt1,
                            Small_Molecule_State& mol1,
                            Energy& vnear, Energy& vcoul, Energy& vdesolv
                            );
  
  friend
  Length hydro_gap(
                   const Large_Molecule_Common& molc0,
                   const Large_Molecule_State& mols0,
                   const Small_Molecule_Common& molc1,
                   const Small_Molecule_Thread& molt1,
                   const Small_Molecule_State& mols1
                   );
  
  template< class C0, class FI, class Mol0, class TForm0, class Mol1, class TForm1>
  friend
  void add_cheby_and_grid_forces(
                                 const Field_For_Blob< C0, FI>& field0,
                                 const Charged_Blob::Blob< 4, Blob_Interface< C0, FI> >& blob1,
                                 Mol0& mol0, const TForm0& tform0,
                                 Mol1& mol1, const TForm1& tform1, double factor);
  
  friend
  void normalize( Large_Molecule_State& mol0, Small_Molecule_State& mol1);
  
  
};

inline
void Small_Molecule_State::get_rotation( Mat3< double>& rot) const{
  transform.get_rotation( rot);
}

inline
void Small_Molecule_State::set_position( const Vec3< Length>& pos){
  transform.set_translation( pos);
}

inline
void Small_Molecule_State::set_rotation( const Mat3< double>& rot){
  transform.set_rotation( rot);  
}

inline
void Small_Molecule_State::get_position( Vec3< Length>& pos) const {
  transform.get_translation( pos);
}

inline
void Small_Molecule_State::set_zero_transform(){
	transform.set_zero();
}

inline
void Small_Molecule_State::zero_forces(){
	Linalg3::zero( force);
	Linalg3::zero( torque);
}

inline
void Small_Molecule_State::get_force( Vec3< Force>& f) const{
	f = force;
}

inline
void Small_Molecule_State::get_torque( Vec3< Torque>& t) const{
	t = torque;
}

inline
void Small_Molecule_State::get_transform( Transform& tform) const{
	tform = transform;
}

// constructor
inline
Small_Molecule_State::Small_Molecule_State(){
  Linalg3::fill( Force( 0.0), force);
  Linalg3::fill( Torque( 0.0), torque);
}

#endif
