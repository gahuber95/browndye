/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
Implementation of physical units. This is used to ensure, at compile time,
that one is not attempting inconsistent math operations with numbers that
have physical units.  The main class is 

template< class M, class L, class T, class Q, class VType = double>
class Unit;

where the first four template parameters denote mass, length, time, 
and charge.  The parameters are instances of the Rational class, 
which represents rational numbers:

template< int Num, unsigned int Den> class Rational;

The operators and many of the math functions are overloaded.

Two commonly used classes include

template< class U0, class U1> class UProd;
template< class U0, class U1> class UQuot;

which are used to get the product and quotient of two units.

These classes are used only when UNITS is #define'd; otherwise,
they are typedef'd to the bare numerical type.

There are several pre-defined units listed near the end of this file.

 */

#ifndef __UNITS_HH__
#define __UNITS_HH__

#include <math.h>
#include <stdlib.h>
#include <sstream>
#include <iostream>

#ifdef UNITS

template< int Num, unsigned int Den>
class Rational{
public:
  static const int num = Num;
  static const unsigned int den = Den;
};

typedef Rational< 0, 1> RZero;
typedef Rational< 1, 1> ROne;

template< class M, class L, class T, class Q, class VType>
class Unit;

// put in own namespace someday
template< class Num>
inline
void check_nan( Num x){
  #ifdef DEBUG
  if (isnan( x)){
    std::cout.flush();
    throw "Units: is nan";
  }
  #endif
}

template< class M, class L, class T, class Q, class VType = double>
class Unit{
public:
  explicit Unit( double _value): value( _value){};
  Unit(): value( NAN){};

  template< class VT>
  Unit( const Unit< M,L,T,Q, VT>& other){
    check_nan( other);
    value = other.value;
  } 

  template< class VT>
  Unit& operator=( const Unit< M,L,T,Q, VT>& other){
    check_nan( other);
    value = other.value;
    return *this;
  } 

  template< class VT1>
  void operator+=( const Unit< M,L,T,Q,VT1>& u){
    check_nan( u);
    check_nan( value);
    this->value += u.value;
  }

  template< class VT1>
  void operator-=( const Unit< M,L,T,Q,VT1>& u){
    check_nan( u);
    check_nan( value);
    this->value -= u.value;
  }


  template< class VT1>
  void operator*=( const VT1& u){
    check_nan( u);
    check_nan( value);
    this->value *= u;
  }

  template< class VT1>
  void operator/=( const VT1& u){
    check_nan( u);
    check_nan( value);
    this->value /= u;
  }

  typedef M Mass;
  typedef L Length;
  typedef T Time;
  typedef Q Charge;
  typedef VType Value_Type;

  VType value;
};

template< class M, class L, class T, class Q, class VType = double>
inline
void check_nan( const Unit< M,L,T,Q,VType>& u){
  check_nan( u.value);
}

typedef Rational< 1,1> ROne;
typedef Rational< 0,1> RZero;

template< class VType>
class Unit< RZero, RZero, RZero, RZero, VType>
{
public:
  Unit( VType _value): value( _value){};
  Unit(): value( NAN){};
  operator VType(){
    return value;
  }

  VType value;
};


template< unsigned int a, unsigned int b>
class GCD{
public:
  static const unsigned int res = GCD< b, a % b >::res; 
};

template< unsigned int a>
class GCD< a, 0>{
public:
  static const unsigned int res = a;
};

template< unsigned int a, unsigned int b>
class LCM{
public:
  static const unsigned int res = a*b/GCD< a, b>::res;
};

// Implements Euclid algorithm for LCM
template< int a, unsigned int b>
class Simplest_Fraction{
public:
  static const unsigned int absa = a > 0 ? a : -a;
  static const unsigned int gcd = GCD< absa, b>::res;
  static const unsigned int den = b/gcd;
  static const int num = a/(int)gcd;
};

template< class R0, class R1>
class RSum{
public:
  static const int n0 = R0::num;
  static const unsigned int d0 = R0::den;

  static const int n1 = R1::num;
  static const unsigned int d1 = R1::den;

  static const unsigned int pden = LCM< d0, d1>::res;
  static const int pnum = n0*(int)pden/(int)d0 + n1*(int)pden/(int)d1;

  typedef Simplest_Fraction< pnum, pden> SF;
  static const int num = SF::num; 
  static const unsigned int den = SF::den;

  typedef Rational< num, den> Res;
};

template< class R0, class R1>
class RDiff{
public:
  static const int n0 = R0::num;
  static const unsigned int d0 = R0::den;

  static const int n1 = R1::num;
  static const unsigned int d1 = R1::den;

  static const unsigned int pden = LCM< d0, d1>::res;
  static const int pnum = n0*(int)pden/(int)d0 - (int)n1*pden/(int)d1;

  typedef Simplest_Fraction< pnum, pden> SF;
  static const int num = SF::num; 
  static const unsigned int den = SF::den;

  typedef Rational< num, den> Res;
};


template< class R0, class R1>
class RProd{
public:
  static const int n0 = R0::num;
  static const unsigned int d0 = R0::den;

  static const int n1 = R1::num;
  static const unsigned int d1 = R1::den;

  static const int pnum = n0*n1;
  static const unsigned int pden = d0*d1;
  
  typedef Simplest_Fraction< pnum, pden> SF;
  static const int num = SF::num; 
  static const unsigned int den = SF::den;

  typedef Rational< num, den> Res;
};

template< class R0, class R1>
class RQuot{
public:
  static const int n0 = R0::num;
  static const unsigned int d0 = R0::den;

  static const int n1 = R1::num;
  static const unsigned int d1 = R1::den;

  static const int n0n1 = n0*n1;
  static const unsigned int absn0n1 = n0n1 > 0 ? n0n1 : -n0n1;
  static const int sgn = absn0n1/n0n1;

  static const unsigned int absn0 = n0 > 0 ? n0 : -n0;
  static const int pnum = absn0*d1;
  static const unsigned int absn1 = n1 > 0 ? n1 : -n1;
  static const unsigned int pden = absn1*d0;
  
  typedef Simplest_Fraction< pnum, pden> SF;
  static const int num = sgn*SF::num; 
  static const unsigned int den = SF::den;

  typedef Rational< num, den> Res;

};

template< class T0, class T1>
class BiType;

template<>
class BiType< double, double>{
public:
  typedef double Res;
};

template<>
class BiType< double, float>{
public:
  typedef double Res;
};

template<>
class BiType< float, double>{
public:
  typedef double Res;
};

template<>
class BiType< float, float>{
public:
  typedef float Res;
};

template< class M, class L, class T, class Q, class VT0, class VT1>
inline
Unit< M,L,T,Q, typename BiType< VT0, VT1>::Res > 
operator+( const Unit< M,L,T,Q,VT0>& u0, 
           const Unit< M,L,T,Q,VT1>& u1){
  check_nan( u0);
  check_nan( u1);
  typedef typename BiType< VT0, VT1>::Res VT;
  return Unit< M,L,T,Q,VT>( u0.value + u1.value);
} 

template< class M, class L, class T, class Q, class VT0, class VT1>
inline
Unit< M,L,T,Q, typename BiType< VT0, VT1>::Res > 
operator-( const Unit< M,L,T,Q,VT0>& u0, 
           const Unit< M,L,T,Q,VT1>& u1){
  check_nan( u0);
  check_nan( u1);  
  typedef typename BiType< VT0, VT1>::Res VT;
  return Unit< M,L,T,Q,VT>( u0.value - u1.value);
} 

template< class M, class L, class T, class Q, class VT>
Unit< M,L,T,Q, VT> operator-( const Unit< M,L,T,Q,VT>& u){
  check_nan( u);
  return Unit< M,L,T,Q,VT>( -u.value);
}

template< class R>
class RNeg{
public:
  static const int num = -R::num;
  static const unsigned int den = R::den;

  typedef Rational< num, den> Res;
};

template< class R>
class RHalf{
public:
  static const int pnum = R::num;
  static const unsigned int pden = R::den*2;

  typedef Simplest_Fraction< pnum, pden> SF;

  static const int num = SF::num; 
  static const unsigned int den = SF::den;

  typedef Rational< num, den> Res;
};

template< class R>
class RDouble{
public:
  static const int pnum = R::num*2;
  static const unsigned int pden = R::den;

  typedef Simplest_Fraction< pnum, pden> SF;

  static const int num = SF::num; 
  static const unsigned int den = SF::den;

  typedef Rational< num, den> Res;
};

template< class U>
class USqrt{
public:
  typedef typename U::Mass M;
  typedef typename U::Length L;
  typedef typename U::Time T;
  typedef typename U::Charge Q;
  typedef typename U::Value_Type VT;  

  typedef Unit< typename RHalf< M>::Res, typename RHalf< L>::Res, 
                typename RHalf< T>::Res, typename RHalf< Q>::Res, VT>
  Res;
};

template< class U>
class USq{
public:
  typedef typename U::Mass M;
  typedef typename U::Length L;
  typedef typename U::Time T;
  typedef typename U::Charge Q;
  typedef typename U::Value_Type VT;
  
  typedef Unit< typename RDouble< M>::Res, typename RDouble< L>::Res, 
                typename RDouble< T>::Res, typename RDouble< Q>::Res, VT>
  Res;
};

template< class M, class L, class T, class Q, class VT>
Unit< typename RHalf< M>::Res, typename RHalf< L>::Res, 
      typename RHalf< T>::Res, typename RHalf< Q>::Res, VT >
sqrt( const Unit< M,L,T,Q,VT>& u){
  check_nan( u);
  return 
    Unit< typename RHalf< M>::Res, typename RHalf< L>::Res, 
    typename RHalf< T>::Res, typename RHalf< Q>::Res, VT>( sqrt( u.value));
}

template< class M, class L, class T, class Q, class VT>
Unit< M, L, T, Q, VT>
fabs( const Unit< M,L,T,Q,VT>& u){
  check_nan( u);
  return Unit< M,L,T,Q, VT>( fabs( u.value));
}

template< class M, class L, class T, class Q, class VT>
inline
Unit< M, L, T, Q, VT>
max( const Unit< M,L,T,Q,VT>& x, const Unit< M,L,T,Q,VT>& y){
  check_nan( x);
  check_nan( y);
  return x > y ? x : y;
}

template< class M, class L, class T, class Q, class VT>
inline
Unit< M, L, T, Q, VT>
min( const Unit< M,L,T,Q,VT>& x, const Unit< M,L,T,Q,VT>& y){
  check_nan( x);
  check_nan( y);
  return x < y ? x : y;
}

template< class T>
inline
T max( const T& x, const T& y){
  check_nan( x);
  check_nan( y);
  return x > y ? x : y;
}

template< class T>
inline
T min( const T& x, const T& y){
  check_nan( x);
  check_nan( y);
  return x < y ? x : y;
}


template< class M, class L, class T, class Q, class VT>
class Dimless_Filter{
public:
  typedef Unit< M,L,T,Q,VT> Res;
};

template< class VT>
class Dimless_Filter< RZero, RZero, RZero, RZero, VT>{
public:
  typedef VT Res;
};

template< class U0, class U1>
class UProd{
public:
  typedef typename U0::Mass M0;
  typedef typename U0::Length L0;
  typedef typename U0::Time T0;
  typedef typename U0::Charge Q0;
  typedef typename U0::Value_Type VT0;

  typedef typename U1::Mass M1;
  typedef typename U1::Length L1;
  typedef typename U1::Time T1;
  typedef typename U1::Charge Q1;
  typedef typename U1::Value_Type VT1;

  typedef typename BiType< VT0, VT1>::Res VT;

  typedef typename Dimless_Filter< typename RSum< M0,M1>::Res, 
                                   typename RSum< L0,L1>::Res, 
                                   typename RSum< T0,T1>::Res, 
                                   typename RSum< Q0,Q1>::Res, VT >::Res Res; 
  
};

template< class U>
class UProd< U, double>{
public:
  typedef U Res;
};

template< class U>
class UProd< double, U>{
public:
  typedef U Res;
};

template< class M0, class L0, class T0, class Q0, class VT0,
          class M1, class L1, class T1, class Q1, class VT1>
inline
typename UProd< Unit< M0,L0,T0,Q0,VT0>, Unit< M1,L1,T1,Q1,VT1> >::Res
operator*( const Unit< M0,L0,T0,Q0,VT0>& u0, 
           const Unit< M1,L1,T1,Q1,VT1>& u1){
  check_nan( u0);
  check_nan( u1);
  typedef typename UProd< 
    Unit< M0,L0,T0,Q0,VT0>, 
    Unit< M1,L1,T1,Q1,VT1> >::Res Res;
  return Res( u0.value * u1.value);
}

template< class M, class L, class T, class Q, class VT0, class VT1>
inline
Unit< M,L,T,Q,typename BiType< VT0, VT1>::Res > 
operator*( const Unit< M,L,T,Q,VT0> u, const VT1& v){
  check_nan( u);
  check_nan( v);
  typedef typename BiType< VT0, VT1>::Res VT;
  return Unit< M,L,T,Q,VT>( u.value*v);
}

template< class M, class L, class T, class Q, class VT0, class VT1>
Unit< M,L,T,Q,typename BiType< VT0, VT1>::Res > 
operator*( const VT0& v, const Unit< M,L,T,Q,VT1> u){
  check_nan( u);
  check_nan( v);
  typedef typename BiType< VT0, VT1>::Res VT;
  return Unit< M,L,T,Q,VT>( u.value*v);
}

template< class U>
class URecip{
public:
  typedef typename U::Mass M;
  typedef typename U::Length L;
  typedef typename U::Time T;
  typedef typename U::Charge Q;
  typedef typename U::Value_Type VT;

  typedef Unit< typename RNeg< M>::Res, 
                typename RNeg< L>::Res, 
                typename RNeg< T>::Res, 
                typename RNeg< Q>::Res, VT> Res;
};

template< class U0, class U1>
class UQuot{
public:
  typedef typename UProd< U0, typename URecip< U1>::Res>::Res Res;
};

template< class U>
class UQuot< U, double>{
public:
  typedef U Res;
};

template< class U>
class UQuot< double, U>{
public:
  typedef typename URecip< U>::Res Res;
};

template< class M0, class L0, class T0, class Q0, class VT0,
          class M1, class L1, class T1, class Q1, class VT1>
inline
typename UQuot< Unit< M0,L0,T0,Q0>, Unit< M1,L1,T1,Q1> >::Res
operator/( const Unit< M0,L0,T0,Q0,VT0>& u0, 
           const Unit< M1,L1,T1,Q1,VT1>& u1){
  check_nan( u0);
  check_nan( u1);
  typedef typename UQuot< Unit< M0,L0,T0,Q0>, Unit< M1,L1,T1,Q1> >::Res Res;
  return Res( u0.value / u1.value);
}

template< class M, class L, class T, class Q, class VT0, class VT1>
inline
Unit< typename RNeg< M>::Res, typename RNeg< L>::Res, 
      typename RNeg< T>::Res, typename RNeg< Q>::Res, 
      typename BiType< VT0, VT1>::Res >
operator/( const VT1& vt, const Unit< M,L,T,Q,VT0>& u){
  check_nan( vt);
  check_nan( u);
  typedef typename BiType< VT0, VT1>::Res VT;
  return Unit< typename RNeg< M>::Res, typename RNeg< L>::Res, 
    typename RNeg< T>::Res, typename RNeg< Q>::Res, VT >( vt/u.value);    
}

template< class M, class L, class T, class Q, class VT0, class VT1>
inline
Unit< M, L, T, Q, typename BiType< VT0, VT1>::Res >
operator/( const Unit< M,L,T,Q,VT0>& u, const VT1& vt){
  check_nan( u);
  check_nan( vt);
  typedef typename BiType< VT0, VT1>::Res VT;
  return Unit< M, L, T, Q, VT>( u.value/vt);
}


template< class M, class L, class T, class Q, class VT>
inline
bool operator==( const Unit< M,L,T,Q,VT>& u0, 
                 const Unit< M,L,T,Q,VT>& u1){
  check_nan( u0);
  check_nan( u1);
  return u0.value == u1.value;
}

template< class M, class L, class T, class Q, class VT>
inline
bool operator>( const Unit< M,L,T,Q,VT>& u0, 
                const Unit< M,L,T,Q,VT>& u1){
  check_nan( u0);
  check_nan( u1);
  return u0.value > u1.value;
}

template< class M, class L, class T, class Q, class VT>
inline
bool operator<( const Unit< M,L,T,Q,VT>& u0, const Unit< M,L,T,Q,VT>& u1){
  check_nan( u0);
  check_nan( u1);
  return u0.value < u1.value;
}

template< class M, class L, class T, class Q, class VT>
inline
bool operator>=( const Unit< M,L,T,Q,VT>& u0, const Unit< M,L,T,Q,VT>& u1){
  check_nan( u0);
  check_nan( u1);
  return u0.value >= u1.value;
}

template< class M, class L, class T, class Q, class VT>
inline
bool operator<=( const Unit< M,L,T,Q,VT>& u0, const Unit< M,L,T,Q,VT>& u1){
  check_nan( u0);
  check_nan( u1);
  return u0.value <= u1.value;
}

template< class M, class L, class T, class Q, class VT>
inline
bool operator!=( const Unit< M,L,T,Q,VT>& u0, const Unit< M,L,T,Q,VT>& u1){
  check_nan( u0);
  check_nan( u1);
  return u0.value != u1.value;
}

template< class U>
class Short_Version{
public:
  typedef typename U::Mass M;
  typedef typename U::Length L;
  typedef typename U::Time T;
  typedef typename U::Charge Q;

  typedef Unit< M,L,T,Q,float> Res;
};

template< class M, class L, class T, class Q, class VT>
inline
VT fvalue( const Unit< M,L,T,Q,VT>& u){
  return u.value;
}

template< class M, class L, class T, class Q, class VT0, class VT1>
inline
void set_fvalue( Unit< M,L,T,Q,VT0>& u, const VT1& vt){
  u.value = vt;
}

template< class M, class L, class T, class Q, class VT>
inline
typename USq< Unit< M,L,T,Q,VT> >::Res
sq( const Unit< M,L,T,Q,VT>& u){
  typedef typename USq< Unit< M,L,T,Q,VT> >::Res Res;
  return Res( u.value * u.value);
}

typedef Unit< ROne, RZero, RZero, RZero> Mass;
typedef Unit< RZero, ROne, RZero, RZero> Length;
typedef Unit< RZero, RZero, ROne, RZero> Time;
typedef Unit< RZero, RZero, RZero, ROne> Charge;

template< class S, class M, class L, class T, class Q, class VT>
S& operator<<( S& s, const Unit< M,L,T,Q,VT>& u){
  s << u.value;
  return s;
}

#else
//###########################################
template< class T>
inline
T max( const T& x, const T& y){
  return x > y ? x : y;
}

template< class T>
inline
T min( const T& x, const T& y){
  return x < y ? x : y;
}

typedef double Mass;
typedef double Length;
typedef double Time;
typedef double Charge;

template< class U1, class U2>
class UProd{};
  
template< class U1, class U2>
class UQuot{};


template< class U>
class URecip{};

template< class U>
class USqrt{};

template< class U>
class Short_Version{};


#endif

//###########################################

template<>
class UProd< double, double>{
public:
  typedef double Res;
};


template<>
class UProd< double, float>{
public:
  typedef double Res;
};
  
template<>
class UProd< float, double>{
public:
  typedef double Res;
};
  
template<>
class UProd< float, float>{
public:
  typedef float Res;
};

template<>
class UQuot< double, double>{
public:
  typedef double Res;
};
  
template<>
class UQuot< double, float>{
public:
  typedef double Res;
};
  
template<>
class UQuot< float, double>{
public:
  typedef double Res;
};
  
template<>
class UQuot< float, float>{
public:
  typedef float Res;
};

template<>
class URecip< double>{
public:
  typedef double Res;
};

template<>
class URecip< float>{
public:
  typedef float Res;
};

template<>
class USqrt< double>{
public:
  typedef double Res;
};

template<>
class USqrt< float>{
public:
  typedef float Res;
};

template<>
class Short_Version< double>{
public:
  typedef float Res;
};


inline
double fvalue( const double a){
  return a;
}

inline
float fvalue( const float a){
  return a;
}

inline
void set_fvalue( double& a, double x){
  a = x;
}

inline
void set_fvalue( float& a, float x){
  a = x;
}

inline
double sq( double x){
  return x*x;
}

typedef UQuot< Length, Time>::Res Velocity;
typedef UQuot< Velocity, Time>::Res Acceleration;
typedef UProd< Mass, Acceleration>::Res Force;
typedef UProd< Force, Length>::Res Torque;
typedef UProd< Force, Length>::Res Energy;
typedef UQuot< Energy, Charge>::Res EPotential;
typedef UQuot< Force, Charge>::Res EPotential_Gradient;

typedef UQuot<  UProd< Charge, Charge>::Res, 
                UProd< Length, Energy>::Res>::Res Permittivity; 

typedef UProd< Length, Length>::Res Length2;
typedef UProd< Length, Length2>::Res Length3;
typedef UProd< Length, Length3>::Res Length4;
typedef UProd< Length, Length4>::Res Length5;
typedef UProd< Length, Length5>::Res Length6;
typedef Length3 Volume;

typedef UProd< Time, Time>::Res Time2;

typedef URecip< Length>::Res Inv_Length;
typedef URecip< Length2>::Res Inv_Length2;
typedef URecip< Length3>::Res Inv_Length3;
typedef URecip< Length4>::Res Inv_Length4;
typedef URecip< Length5>::Res Inv_Length5;
typedef URecip< Length6>::Res Inv_Length6;

typedef USqrt< Time>::Res Sqrt_Time;
typedef URecip< Time>::Res Inv_Time;
typedef UProd< Time, Time>::Res Time2;

typedef UQuot< Force, UProd< Velocity, Length>::Res>::Res Viscosity;
typedef UQuot< Length2, Time>::Res Diffusivity;

typedef UProd< Length, Charge>::Res Dipole;
typedef UProd< Length2, Charge>::Res Quadrupole;

#endif



