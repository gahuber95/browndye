/*
 * time_step_parameters.cc

 *
 *  Created on: Sep 11, 2015
 *      Author: ghuber
 */

#include "time_step_parameters.hh"
#include "molecule_pair_common.hh"

// constructor
Time_Step_Parameters::Time_Step_Parameters(){
  dt_min = Time( NAN);
  dtr_min = Time( NAN);

  ftol = 0.02;
  gtol = 0.05; // 1.0/(2*3*3);
  rtol = 1.0/(2*10*10);

  dx_min = Length( 1.0);
  dxr_min = Length( 1.0);
  diffu_inf = Diffusivity( NAN);
  max_rdiffu = Inv_Time( NAN);
}

template< class T>
decltype( T()*T()*T()) cube( T t){
  return t*t*t;
}


void Time_Step_Parameters::initialize( const Molecule_Pair_Common& common,
                                       Node_Ptr& node){
    auto tnode = node->child( "time-step-tolerances");

    if (tnode != NULL){
        bool found;
        get_double_from_node( tnode, "force", ftol, found);
        get_double_from_node( tnode, "reaction", rtol, found);
        get_double_from_node( tnode, "collision", gtol, found);
        get_double_from_node( tnode, "minimum-dx", dx_min, found);
        dxr_min = dx_min;
        get_double_from_node( tnode, "minimum-reaction-dx", dxr_min, found);
      }

    auto& mol0 = common.mol0;
    auto& mol1 = common.mol1;
    auto& solvent = common.solvent;
    auto kT = solvent.kT;
    auto mu = water_viscosity*solvent.relative_viscosity;
    {
      Diffusivity diffu0 = kT/(pi6*mu*mol0.hydro_radius());
      Diffusivity diffu1 = kT/(pi6*mu*mol1.hydro_radius());
      diffu_inf = diffu0 + diffu1;
      dt_min = dx_min*dx_min/(2.0*diffu_inf);
      dtr_min = dxr_min*dxr_min/(2.0*diffu_inf);
    }
    {
      Inv_Time rdiffu0 = kT/(pi8*mu*cube( mol0.hydro_radius()));
      Inv_Time rdiffu1 = kT/(pi8*mu*cube( mol1.hydro_radius()));
      max_rdiffu = max( rdiffu0, rdiffu1);
    }
  }



