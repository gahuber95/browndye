/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
Spline is a generic implementation of the cubic spline.
*/

#ifndef __SPLINE_HH__
#define __SPLINE_HH__

#include "units.hh"
#include "vector.hh"

template< class X, class Y>
class Spline{
public:
  typedef typename UQuot< Y, X>::Res dYdX;

  Spline();

  Spline( const Vector< X>& xs_in, 
          const Vector< Y>& ys_in);
  ~Spline();

  void initialize( const Vector< X>& xs_in, 
                   const Vector< Y>& ys_in);
  
  void make_to_own_data();
  bool owns_data() const;
  
  Y value( const X&) const;
  dYdX first_deriv( const X&) const;

  unsigned int size() const{
    return y2s.size();
  }

private:
  typedef typename UProd< X, X>::Res X2;
  typedef typename UQuot< Y, X2>::Res d2YdX2;

  void clear();
  static void tri_diag_solve( const Vector< X>& a, 
                              const Vector< X>& b, 
                              Vector< X>& c, 
                              Vector< dYdX>& d, 
                              Vector< d2YdX2>& x);

  bool odata;
  const Vector< X>* xs_ptr;
  const Vector< Y>* ys_ptr;
  Vector< d2YdX2> y2s;
};

#include "spline_impl.hh"

#endif


