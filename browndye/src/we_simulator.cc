/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

/*
Implementation of WE_Simulator
*/

#include <fstream>
#include <string.h>
#include <vector>
#include "we_simulator.hh"
#include "linalg3.hh"
#include "transform.hh"
#include "found.hh"
#include "node_info.hh"


namespace L = Linalg3;

template< class F>
void WE_Interface::apply_to_object_refs( F& f, WE_Simulator* sim){
  //std::set< Molecule_Pair_State*>& states = sim->states;
  for (auto itr = sim->states.begin();
       itr != sim->states.end(); ++itr){
    f( *itr);
  }
}

void WE_Interface::initialize_object( WE_Simulator* sim, 
                                      Molecule_Pair_State* state){

  Molecule_Pair mpair( sim->common, sim->mthreads[0], *state);

  if (sim->building_bins)
    mpair.set_initial_state_for_bb();
  else
    mpair.set_initial_state();
}
 
unsigned int WE_Interface::number_of_threads( WE_Simulator* sim){
  return sim->mthreads.size();
}

void WE_Interface::step_forward( WE_Simulator* sim, unsigned int ithread,
                                 Molecule_Pair_State* state){
  
  Molecule_Pair mpair( sim->common, sim->mthreads[ithread], *state);
 
  unsigned long int n_steps;
  mpair.do_step( n_steps);
  auto fate = mpair.fate();
  
  if (fate == Escaped){
    if (!sim->building_bins){
      mpair.set_initial_physical_state();
    }
  }
  else if (fate == Final_Rxn){
    if (!sim->building_bins){
      mpair.set_initial_physical_state();
    }       
  }
}

unsigned int WE_Interface::bin( WE_Simulator* sim, Molecule_Pair_State* state, 
                          bool& exists){
  Molecule_Pair mpair( sim->common, sim->mthreads[0], *state);

  if (sim->bin_partitions.size() > 0){
    exists = true;
    Length rxn_coord = mpair.reaction_coordinate();
    return found( sim->bin_partitions, 
                  rxn_coord/(Molecule_Pair_Common::qb_factor()*sim->common.b_radius()));
  }
  else {
    exists = false;
    return std::numeric_limits< unsigned int>::max();
  }
}

unsigned int WE_Interface::bin( WE_Simulator* sim, Molecule_Pair_State* state){
  bool exists;
  unsigned int res = bin( sim, state, exists);
  if (!exists)
    error( "bin: bin does not exist");
  return res;
}

// debug
/*
void WE_Interface::check_order( const WE_Simulator* sim){
  unsigned long int max_num = 0;
  for( auto& state: sim->states){
    if (state->number() < max_num){
      cout << "out of order\n";

      for( auto& s: sim->states)
        cout << s->number() << " " << s << endl;

      error( "set out of order");
    }
    max_num = state->number();
  }      
}
*/

Molecule_Pair_State* WE_Interface::new_object( WE_Simulator* sim){
  Molecule_Pair_State* new_state = new Molecule_Pair_State( sim->state_number);
  ++(sim->state_number);

  initialize_object( sim, new_state);

  auto psuccess = sim->states.insert( new_state);

  if (!psuccess.second)
    error( "new_object: should not get here");

  new_state->set_weight( 1.0);
  return new_state;
}

void WE_Interface::get_duplicated_objects( 
                                          WE_Simulator* sim,
                                          Molecule_Pair_State* state,
                                          unsigned int n_copies, 
                                          std::list< Molecule_Pair_State*>& copies){
  
  double wt = state->weight()/(1 + n_copies);
  for( unsigned int i = 0; i < n_copies; i++){
    Molecule_Pair_State* new_state = new_object( sim);
    new_state->copy_from( *state);
    new_state->set_weight( wt);
    copies.push_back( new_state);
  }
  state->set_weight( wt);
}
                                           
void WE_Interface::remove( WE_Simulator* sim, Molecule_Pair_State* state){
  
  unsigned int nerased = sim->states.erase( state);

  if (nerased == 0){
    error( "remove: nothing to be removed", state->number());
  }

  delete state;
}

void WE_Interface::get_crossing( WE_Simulator* sim, Molecule_Pair_State* state, 
                                 bool& crossed, unsigned int& crossing){
    
  auto fate = state->fate();
  bool had_reaction = state->had_reaction();
  crossed = had_reaction || fate == Escaped;

  if (had_reaction){
    crossing = state->last_reaction();
    if (fate == Final_Rxn)
      state->set_initial_reaction_state( sim->common);
  }
  else if (fate == Escaped){
    crossing = sim->n_final_rxns;
    state->set_initial_reaction_state( sim->common);
  }
}


unsigned int WE_Interface::number_of_bins( WE_Simulator* sim){
  return sim->bin_partitions.size();
}

unsigned int WE_Interface::number_of_bin_neighbors( WE_Simulator* sim, unsigned int bin){
  if ((bin == 0) || (bin == sim->bin_partitions.size() - 1))
    return 1;
  else
    return 2;
}

// low bins are closer to reaction
bool WE_Interface::have_population_reversal( WE_Simulator* sim, unsigned int bin0, double wt0, unsigned int bin1, double wt1){
  //printf( "pop rev %d %g %d %g ", bin0, wt0, bin1, wt1);
  bool res = (((int)bin0 - (int)bin1)*(wt0 - wt1) <= 0.0); 
  //printf( "%d\n", res);
  return res;
}

// rxn coordinate is normalized by q-radius
double WE_Interface::reaction_coordinate( WE_Simulator* sim, Molecule_Pair_State* state){

  Molecule_Pair mpair( sim->common, sim->mthreads[0], *state);

  Length rxn_coord = mpair.reaction_coordinate();
  double res = rxn_coord/(Molecule_Pair_Common::qb_factor()*sim->common.b_radius());
  return res;
}

//*****************************************************************
void WE_Simulator::get_bins( JAM_XML_Pull_Parser::Node_Ptr node){
  namespace JP = JAM_XML_Pull_Parser;
  std::string bin_file = string_from_node( node, "bin-file");
   
 
  std::ifstream input( bin_file.c_str());

  if (!input.good())
    error( "bins file ", bin_file.c_str(), " not present");

  JP::Parser parser( input);
  
  parser.complete_current_node();
  JP::Node_Ptr bnode = parser.current_node();
  
  std::list< JP::Node_Ptr> bnodes = bnode->children_of_tag( "bin-partition");
  unsigned int n_bins = bnodes.size();
  bin_partitions.resize( n_bins-1);

  auto itr = bnodes.begin();
  for( unsigned int i = 0; i < n_bins-1; i++){
    bin_partitions[i] = stod( (*itr)->data()[0]);
    ++itr;
  }
}

void WE_Simulator::initialize( const char* file){
  namespace JP = JAM_XML_Pull_Parser;

  std::ifstream input( file);
  if (!input.is_open())
    error( "file ", file, " could not be opened");

  JP::Parser parser( input);
  JP::Node_Ptr node = initialized_node( parser);
  rng.set_seed( extra_seed);

  n_final_rxns = common.n_reactions();
  next_fluxes.resize( n_final_rxns+1);
  n_copies = int_from_node( node, "n-copies");
  n_bin_copies = int_from_node( node, "n-bin-copies");

  bool found;
  get_value_from_node( node, "n-steps", n_steps, found);
  if (!(found || building_bins))
    error( "WE_Simulator::initialize: need n-steps");

  get_value_from_node( node, "n-we-steps-per-output", n_steps_per_output, found);

  sampler.set_number_of_threads( mthreads.size());
  
  if (building_bins){
    std::string bin_file_name = 
      string_from_node( node, "bin-file");
    bin_ofstream.open( bin_file_name.c_str());
  }
  else
    get_bins( node);
}

void WE_Interface::output( WE_Simulator* sim, unsigned int icrossing, double flux){
  std::ofstream& output = sim->output;
  unsigned int ncross = sim->n_final_rxns + 1;
  Vector< double>& next_fluxes = sim->next_fluxes;

  next_fluxes[ icrossing] = flux;
  if (icrossing == ncross-1){
    output.seekp( sim->stream_position);
    output << "    <d> ";
    for (unsigned int i = 0; i < ncross; i++)
      output << next_fluxes[i] << " ";
    output << "</d>\n";
    sim->stream_position = output.tellp();
    
    output << "  </data>\n";
    output << "</rates>\n";
    output.flush();
  }
}

double WE_Interface::uniform( WE_Simulator* sim){
  return sim->rng.uniform();
}

// constructor
WE_Simulator::WE_Simulator(){
	istep = 0;
  n_copies = 0;
  n_bin_copies = 0;
  n_steps = 0;
  n_steps_per_output = 1;
  building_bins = false;
  n_final_rxns = 1;
  state_number = 0;
}

void WE_Simulator::run(){
  building_bins = false;

  output.open( output_name.c_str());
  sampler.set_number_of_objects( n_copies);
  sampler.set_number_of_fluxes( n_final_rxns + 1);
  
  sampler.set_user_info( this);
  sampler.set_number_of_moves_per_output( n_steps_per_output);

  sampler.initialize_objects();
  
  begin_output( output);
  
  output << "  <n> " << n_final_rxns+1 << " </n>\n";
  output << "  <data>\n";
  stream_position = output.tellp();
  for( istep=0; istep < n_steps; ++istep){
    sampler.step_objects_forward();
    sampler.update_fluxes();
    sampler.renormalize_objects_1D();
  }
}

void WE_Simulator::generate_bins(){

  building_bins = true;
  common.set_building_bins();
  //common.building_bins = true;

  sampler.set_number_of_objects( n_bin_copies);
  sampler.set_number_of_fluxes( common.n_reactions());
  
  istep = 0;
  sampler.set_user_info( this);

  std::list< double> partitions;
  sampler.generate_bins( partitions);

  // should print from 0 to largest 
  bin_ofstream << "<bin-partitions>\n";
  for (auto itr = partitions.begin();
       itr != partitions.end(); ++itr){
    bin_ofstream << " <bin-partition> " << (*itr) << " </bin-partition>\n";
  }
  bin_ofstream << "</bin-partitions>\n";
}


