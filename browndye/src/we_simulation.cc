/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

// Actual program that is called for we_simulation

#include "release_info.hh"
#include "we_simulator.hh"

int main( int argc, char* argv[]){

  if (argc < 2){
    error( "we_simulation: need input file name");
  }
  const char* filename = argv[1];

  print_release_info();

  WE_Simulator wes;
  wes.initialize( filename);
  wes.run();
}
