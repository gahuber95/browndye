#ifndef __LARGE_MOLECULE_COMMON_HH__
#define __LARGE_MOLECULE_COMMON_HH__

#include <string>
#include "vector.hh"
#include "molecule_common.hh"
#include "jam_xml_pull_parser.hh"
#include "blob_interface.hh"
#include "field.hh"
#include "col_interface_large.hh"
#include "col_interface_small.hh"
#include "born_field_interface.hh"
#include "v_field_interface.hh"
#include "atom_large.hh"
#include "collision_detector.hh"
#include "charged_blob_simple.hh"

class Large_Molecule_Common: public Molecule_Common{
public:
  typedef std::string String;

  Large_Molecule_Common();
  void set_desolvation_chebybox( String file, const Vec3< Length>& offset);
  void set_up_collision_structure();
  void initialize( JAM_XML_Pull_Parser::Node_Ptr node, 
                   Energy kT);

  Charge charge() const;
  Atom_Large& atom( size_type i);
  const Atom_Large& atom( size_type i) const;

  void check_mpole_consistency() const;
  Vector< size_t> atom_indices() const;

private:
  friend class Back_Door;

  typedef Blob_Interface< Col_Interface_Small, 
                          Born_Field_Interface> Q2_Blob_Interface;

  std::unique_ptr< Field::Field< V_Field_Interface> > v_field;
  Field::Field< V_Field_Interface>::Hint v_hint0;//initialize!

  Vector< Atom_Large> atoms; 
  std::unique_ptr< Collision_Detector::Structure< Col_Interface_Large> > 
  collision_structure;
  std::unique_ptr< Charged_Blob::Blob< 4, Q2_Blob_Interface> > q2_blob;
  Charge total_charge;

  // tether spring
  typedef UQuot< Force, Length>::Res Spring_Constant;
  Spring_Constant ks;
  Length equilib_len;
  const Atom_Large* spring_atom0; 

  template< class Mol>
  friend
  void set_desolvation_chebybox( Mol& mol, std::string file, const Vec3< Length>& offset);

  friend
  void reset_hints( const Large_Molecule_Common& molc, Large_Molecule_State& mol);

  friend
  void add_forces_and_torques(
                              const Large_Molecule_Common& molc0,
                              Large_Molecule_State& mol0,
                              const Small_Molecule_Common& molc1,
                              Small_Molecule_Thread& molt1,
                              Small_Molecule_State& mol1,

                              bool& has_collision,
                              Length& violation,
                              Atom_Large_Ref& aref0,
                              Atom_Small_Ref& aref1
                              );

  friend
  void get_potential_energy(
                            const Large_Molecule_Common& molc0,
                            Large_Molecule_State& mol0,
                            const Small_Molecule_Common& molc1,
                            Small_Molecule_Thread& molt1,
                            Small_Molecule_State& mol1,
                            Energy& vnear, Energy& vcoul, Energy& vdesolv
                            );
};


inline
Atom_Large& Large_Molecule_Common::atom( Vector< int>::size_type i){
  return atoms[i];
}

inline
const Atom_Large& Large_Molecule_Common::atom( Vector< int>::size_type i) const{
  return atoms[i];
}

inline
Vector< size_t> Large_Molecule_Common::atom_indices() const{
	return gen_atom_indices( atoms);
}


#endif
