/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

// Actual program that gets called for nam_simulation

#include "release_info.hh"
#include "nam_simulator.hh"
#include "get_args.hh"

int main( int argc, char* argv[]){

  if (argc < 2)
    error( "nam_simulation: need input file name\n");  

  const char* input = argv[1];     
  print_release_info();
 
  NAM_Simulator nams;
  nams.initialize( input);
  nams.run();
}
