// Inline functions for multipole field expansion

namespace Multipole_Field{

inline
Length Field::debye_length() const{
  return debye;
}

  // do not use for much longer
inline
Charge Field::eff_charge() const{
  return pol1*exp( fit_radius/debye);
}

inline
Permittivity Field::vacuum_permittivity() const{
  return vperm;
}

inline
double Field::solvent_dielectric() const{
  return solvdi;
}

inline
void Field::get_center( Vec3< Length>& c) const{
  c[0] = cx;
  c[1] = cy;
  c[2] = cz;
}

inline
void Field::shift_position( const Vec3< Length>& offset){
  cx += offset[0];
  cy += offset[1];
  cz += offset[2];
}

}
