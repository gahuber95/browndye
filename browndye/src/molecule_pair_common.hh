/*
 * molecule_pair_common.hh
 *
 *  Created on: Sep 8, 2015
 *      Author: root
 */

#ifndef MOLECULE_PAIR_COMMON_HH_
#define MOLECULE_PAIR_COMMON_HH_

#include <memory>
#include "units.hh"
#include "vector.hh"
#include "jam_xml_pull_parser.hh"
#include "large_molecule_common.hh"
#include "small_molecule_common.hh"
#include "pathways.hh"
#include "two_sphere_brownian.hh"
#include "mover_interface.hh"
#include "rxn_interface.hh"
#include "do_full_step.hh"
#include "stepper_interface.hh"
#include "physical_constants.hh"
#include "solvent.hh"
#include "time_step_parameters.hh"

class Molecule_Pair_Thread;

class Molecule_Pair_Common{
public:
  typedef typename Vector< int>::size_type size_type;

  Molecule_Pair_Common();

  void initialize( JAM_XML_Pull_Parser::Node_Ptr node,
                   const std::string& solvent_file);

  void output( std::ofstream& outp);

  size_type n_reactions() const;

  Time maximum_dt() const;
  Time minimum_dt() const;

  void renormalize_reactions( const Molecule_Pair_Thread& mthread);

  static double qb_factor(){
    return 1.1;
  }

  Length b_radius() const{
    return b_rad;
  }

  void set_building_bins(){
    building_bins = true;
  }

private:
  friend class Molecule_Pair;
  friend class Molecule_Pair_Thread;
  friend class Molecule_Pair_State;
  friend class Stepper_Interface;
  friend class Mover_Interface;
  friend class Rxn_Interface;
  friend struct Time_Step_Parameters;

  Solvent solvent;

  Length b_rad, q_rad;
  bool building_bins;

  // assume mol0 radius > mol1 radius
  Large_Molecule_Common mol0;
  Small_Molecule_Common mol1;

  std::unique_ptr< Two_Sphere_Brownian::Mover< Mover_Interface> > mover;
  std::unique_ptr< Pathways::Pathway< Rxn_Interface> > pathway;
  std::unique_ptr< Full_Stepper::Doer< Stepper_Interface> > full_stepper;

  pthread_mutex_t mutex;

  Time_Step_Parameters ts_params;

  // starting place of Molecule 1 relative to 0
  bool start_at_site;
  Vec3< Length> starting_site_offset;

  bool no_hi;
};

inline
Time Molecule_Pair_Common::minimum_dt() const{
  return ts_params.dt_min;
}

inline
Molecule_Pair_Common::size_type
Molecule_Pair_Common::n_reactions() const{
  return pathway->n_reactions();
}

#endif /* MOLECULE_PAIR_COMMON_HH_ */
