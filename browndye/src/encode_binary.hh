#ifndef __ENCODE_BINARY_HH__
#define __ENCODE_BINARY_HH__

/*
Returns a MIME (Base64 format) string representation of the array of floats.
This is the inverse of the Ocaml code found in aux/decode_binary.ml
*/

#include <string>

std::string
string_of_floats( const Vector< double>& floats, unsigned int nf);

#endif
