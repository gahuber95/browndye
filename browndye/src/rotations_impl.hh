template< class I>
void get_random_quat( typename I::Random_Number_Generator& rng, 
                      SVec< double,4>& quat){
  double qnm = 0.0;
  for( unsigned int i=0; i<4; i++){
    quat[i] = I::gaussian( rng);
    qnm += quat[i]*quat[i];
  }  
  qnm = sqrt( qnm);
  
  for( unsigned int i=0; i<4; i++)
    quat[i] /= qnm;
}

template< class I>
void get_random_rotation( typename I::Random_Number_Generator& rng, 
                          Mat3< double>& rot){
  SVec< double,4> quat;
  get_random_quat<I>( rng, quat);
  quat_to_mat( quat, rot);
}

template< class I>
void get_random_unit_vector( typename I::Random_Number_Generator& rng, 
                             Vec3< double>& u){
  double unm = 0.0;
  for( unsigned int i=0; i<3; i++){
    u[i] = I::gaussian( rng);
    unm += u[i]*u[i];
  }  
  unm = sqrt( unm);

  for( unsigned int i=0; i<3; i++)
    u[i] /= unm;
}

namespace Rotations{
  const Spline< double, double>& rprob0p5(); 
  const Spline< double, double>& rprob1p0(); 
  const Spline< double, double>& rprob2p0(); 

  template< class I>
  void get_spline_rot( typename I::Random_Number_Generator& rng, 
                       const Spline< double, double>& rprob, 
                       SVec< double,4>& quat){
    
    double pc = I::uniform( rng);
    double phi = rprob.value( pc); 
    Vec3< double> u;
    get_random_unit_vector<I>( rng, u);
    Vec3< double> dom;
    Linalg3::get_scaled( phi, u, dom);
    dom_to_quat( dom, quat);
  }

}

template< class I>
void get_diffusional_rotation( typename I::Random_Number_Generator& rng,  
                               double t, SVec< double,4>& quat){
  
  using namespace Rotations;

  if (t < 1.0){
    
    if (t <= 0.25){
      // take infinitesimal step
      double sqdt = sqrt( 2*t);
      Vec3< double> dom;
      dom[0] = sqdt*I::gaussian( rng);
      dom[1] = sqdt*I::gaussian( rng);
      dom[2] = sqdt*I::gaussian( rng);
      dom_to_quat( dom, quat);
    }
    else if (t < 0.5){
      SVec< double,4> quat0, quat1;
      get_diffusional_rotation<I>( rng, 0.25, quat0);
      get_diffusional_rotation<I>( rng, t - 0.25, quat1);
      multiply_quats( quat1, quat0, quat);
    }
    else { // t < 1.0 
      SVec< double,4> quat0, quat1;
      get_spline_rot<I>( rng, rprob0p5(), quat0);    
      get_diffusional_rotation<I>( rng, t - 0.5, quat1);
      multiply_quats( quat1, quat0, quat);
    }
  }
  
  else{ // t >= 1.0
    
    if (t < 2.0){
      SVec< double,4> quat0, quat1;
      get_spline_rot<I>( rng, rprob1p0(), quat0);    
      get_diffusional_rotation<I>( rng, t - 1.0, quat1);
      multiply_quats( quat1, quat0, quat);
    }
    
    else if (t < 4.0){
      SVec< double,4> quat0, quat1;
      get_spline_rot<I>( rng, rprob2p0(), quat0);    
      get_diffusional_rotation<I>( rng, t - 2.0, quat1);
      multiply_quats( quat1, quat0, quat);
    }
    
    else
      // past 4 time constants, it is effectively random.
      get_random_quat<I>( rng, quat);
  }
}

template< class I>
void add_diffusional_rotation( typename I::Random_Number_Generator& rng, 
                               double t, 
                               const Mat3< double>& rot0, 
                               Mat3< double>& rot){
  
  SVec< double,4> quat0, quat1, quat;
  mat_to_quat( rot0, quat0);
  
  get_diffusional_rotation<I>( rng, t, quat1);
  multiply_quats( quat1, quat0, quat);
  
  quat_to_mat( quat, rot);
}


