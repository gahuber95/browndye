#ifndef __FLOAT32_T_HH__
#define __FLOAT32_T_HH__

/*
This is used to define a floating point number that is 32 bits.
It selects either "float" or "double", depending on the sizeof
function, which is used to select a template.  Needed for 
encode_binary.cc.
*/

namespace Float32_T{

template< bool, typename, typename>
class Cond_Type;

template< typename A, typename B>
class Cond_Type< true, A, B>{
public:
  typedef A Result;
};

template< typename A, typename B>
class Cond_Type< false, A, B>{
public:
  typedef B Result;
};

template< bool, typename>
class Filter;

class None{};

template< typename T>
class Filter< true, T>{
public:
  typedef T Result;
};

template< typename T>
class Filter< false, T>{
public:
  typedef None Result;
};

}

typedef  
Float32_T::Cond_Type< sizeof(float) == 4, 
                      float, 
                      Float32_T::Filter< sizeof(double) == 4, 
                                         double>::Result>::Result 
float32_t;

#endif
