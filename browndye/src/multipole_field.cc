/* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*/

// Implementation. Uses Cartesian formulation out to quadrupole level.

#include <math.h>
#include <fstream>
#include <limits>
#include "multipole_field.hh"
#include "node_info.hh"
#include "multipole_field.hh"
#include "pi.hh"
#include "auto_diff.hh"
#include "auto_diff_helper.hh"

namespace Multipole_Field{

  const double pi4 = 4.0*pi;
namespace JP = JAM_XML_Pull_Parser;

  /*
EPotential Field::potential( const Vec3< Length>& pos) const{
  const Length x = pos[0] - cx;
  const Length y = pos[1] - cy;
  const Length z = pos[2] - cz;

  const Length2 r2 = x*x + y*y + z*z;
  const Length r = sqrt( r2);
  const Length3 r3 = r*r2;

  const Inv_Length eff_1or = 1.0/r + 1.0/debye;
  const Inv_Length2 eff_1or2 = 1.0/r2 + 1.0/(debye*r) + 1.0/(3.0*debye*debye);
  double expf = exp(-r/debye);

  typedef UQuot< Charge, Length>::Res QOR;

  const QOR vc = q*expf/r;
  const QOR vm = (1.0/r2)*eff_1or*expf*( x*mx + y*my + z*mz);
  const QOR vq = (0.5/r3)*eff_1or2*expf*
    ( qxx*x*x + 2.0*qxy*x*y + 2.0*qxz*x*z + qyy*y*y + 
      2.0*qyz*y*z + qzz*z*z);

  return (vc + vm + vq)/(pi4*vperm*solvdi);
}
  */


  void Field::get_potential_n_grads( const Vec3< Length>& pos, EPotential& phi_out,
                                     Vec3< EPotential_Gradient>& grad,
                                     bool get_grad) const{

    auto ccx = constant( cx);
    auto ccy = constant( cy);
    auto ccz = constant( cz);
    auto cdebye = constant( debye);
    auto cfit_radius = constant( fit_radius);
    auto den = constant( pi4*vperm*solvdi);
    auto one = constant( 1.0);
    auto two = constant( 2.0);
    auto three = constant( 3.0);
    auto cpol1 = constant( pol1);
    auto cpolx = constant( polx);
    auto cpoly = constant( poly);
    auto cpolz = constant( polz);
    auto cpolqm2 = constant( polqm2);
    auto cpolqm1 = constant( polqm1);
    auto cpolq0 = constant( polq0);
    auto cpolq1 = constant( polq1);
    auto cpolq2 = constant( polq2);

    Length pposx = pos[0];
    Length pposy = pos[1];
    Length pposz = pos[2];

    auto posx = input( pposx, grad[0]);
    auto posy = input( pposy, grad[1]);
    auto posz = input( pposz, grad[2]);
    
    auto x = var( posx - ccx);
    auto y = var( posy - ccy);
    auto z = var( posz - ccz);
    
    auto r2 = var( x*x + y*y + z*z);
    auto r = var( sqrt( r2));

    auto f1 = constant( 1.0/sqrt( pi4));
    
    auto nrm1 = constant( sqrt( pi4/3.0));
    auto fx = var( x/(nrm1*r));
    auto fy = var( y/(nrm1*r));
    auto fz = var( z/(nrm1*r));
    
    auto nrm2 = constant( sqrt( pi4/15.0));
    auto tst = constant( 2.0*sqrt(3.0));

    auto fqm2 = var( x*y/(nrm2*r2));
    auto fqm1 = var( z*y/(nrm2*r2));
    auto fq0 = var( (three*z*z/r2 - one)/(tst*nrm2));
    auto fq1 = var( z*x/(nrm2*r2));
    auto fq2 = var( (x*x - y*y)/(two*r2*nrm2));
    
    auto k0 = var( one/r);
    auto k1 = var( k0*(one/cdebye + one/r));
    auto k2 = var( k0*(one/(cdebye*cdebye) + three/(r*cdebye) + three/r2));
    
    auto db_factor = var( exp(-(r - cfit_radius)/cdebye)/den);
    auto comp0 = var( f1*cpol1*k0);
    auto comp1 = var( (fx*cpolx + fx*cpoly + fx*cpolz)*k1);
    auto comp2 = var((fqm2*cpolqm2 + fqm1*cpolqm1 + fq0*cpolq0 + fq1*cpolq1 + fq2*cpolq2)*k2);
    
    auto phi = var( db_factor*( comp0 + comp1 + comp2));
    phi_out = value( phi);
    
    if (get_grad){
      back_prop_top( phi);
      back_prop( comp2);
      back_prop( comp1);
      back_prop( comp0);
      back_prop( db_factor);
      back_prop( k2);
      back_prop( k1);
      back_prop( k0);
      back_prop( fq2);
      back_prop( fq1);
      back_prop( fq0);
      back_prop( fqm1);
      back_prop( fqm2);
      back_prop( fz);
      back_prop( fy);
      back_prop( fx);
      back_prop( r);
      back_prop( r2);
      back_prop( x);
      back_prop( y);
      back_prop( z);
    }
  }

  EPotential Field::potential( const Vec3< Length>& pos) const{
    Vec3< EPotential_Gradient> grad;
    EPotential phi;
    get_potential_n_grads( pos, phi, grad, false);
    return phi;
  }

void Field::get_gradient( const Vec3< Length>& pos, 
                          Vec3< EPotential_Gradient>& grad) const{

    EPotential phi;
    get_potential_n_grads( pos, phi, grad, true);  
}


  /*
void Field::
get_gradient( const Vec3< Length>& pos, 
              Vec3< EPotential_Gradient>& grad) const{

  const Length x = pos[0] - cx;
  const Length y = pos[1] - cy;
  const Length z = pos[2] - cz;

  const Length2 r2 = x*x + y*y + z*z;
  const Length r = sqrt( r2);
  const Length3 r3 = r*r2;
  const Length4 r4 = r2*r2;
  const Length5 r5 = r*r4;
  const Length6 r6 = r*r5;
  const double expf = exp(-r/debye);

  const Inv_Length dexpf = -expf/debye;
 
  typedef UQuot< Charge, Length2>::Res QOR2;
 
  const QOR2 dceterm = q*(dexpf/r - expf/r2);
  
  const Inv_Length3 mterm = 1.0/r3 + 1.0/(debye*r2);
  const Inv_Length4 dmterm = -3.0/r4 - 2.0/(debye*r3);
  
  const Inv_Length3 meterm = expf*mterm;
  const Inv_Length4 dmeterm = dexpf*mterm + expf*dmterm;

  const Inv_Length5 qterm = 0.5/r5 + 0.5/(debye*r4) + 1.0/(6.0*debye*debye*r3);
  const Inv_Length6 dqterm = -2.5/r6 - 2.0/(debye*r5) - 0.5/(debye*debye*r4);

  const Inv_Length5 qeterm = expf*qterm;
  const Inv_Length6 dqeterm = expf*dqterm + dexpf*qterm;

  const double drdx = x/r;
  const double drdy = y/r;
  const double drdz = z/r;

  const EQuadrupole mv = mx*x + my*y + mz*z;
  const UProd< Length2, EQuadrupole>::Res qv = 
    qxx*x*x + 2.0*qxy*x*y + 2.0*qxz*z*z + qyy*y*y + 2.0*qyz*y*z + 
    qzz*z*z;

  Permittivity factor = pi4*vperm*solvdi;

  grad[0] = (drdx*dceterm + (drdx*dmeterm*mv + meterm*mx) +
    (drdx*dqeterm*qv + 2.0*qeterm*( qxx*x + qxy*y + qxz*z)))/factor;

  grad[1] = (drdy*dceterm + (drdy*dmeterm*mv + meterm*my) +
    (drdy*dqeterm*qv + 2.0*qeterm*( qxy*x + qyy*y + qyz*z)))/factor;

  grad[2] = (drdz*dceterm + (drdz*dmeterm*mv + meterm*mz) +
    (drdz*dqeterm*qv + 2.0*qeterm*( qxz*x + qyz*y + qzz*z)))/factor;
}

  */

  inline
  JP::Node_Ptr child( JP::Node_Ptr node, const String& tag){
    JP::Node_Ptr cnode = checked_child( node, tag.c_str());
    if (cnode == NULL)
      error( "child: no node of ", tag);
    
    return cnode;
  } 

  double fdata( JP::Node_Ptr node){
    return stod( node->data()[0]);
  }

  template< class Value>
  void get_v3_from_node( JP::Node_Ptr node, const String& tag, 
                         Value& x, Value& y, Value& z){
    
    JP::Node_Ptr cnode = child( node, tag);
    
    JP::Node_Ptr xnode = child( cnode, "x");
    JP::Node_Ptr ynode = child( cnode, "y");
    JP::Node_Ptr znode = child( cnode, "z");
    
    set_fvalue( x, fdata( xnode));
    set_fvalue( y, fdata( ynode));
    set_fvalue( z, fdata( znode));
  }
  
  void Field::get_m3_from_node( JP::Node_Ptr node, const String& tag){
    
    JP::Node_Ptr cnode = child( node, tag);
    
    JP::Node_Ptr qm2node = child( cnode, "qm2");
    JP::Node_Ptr qm1node = child( cnode, "qm1");
    JP::Node_Ptr q0node = child( cnode, "q0");
    JP::Node_Ptr q1node = child( cnode, "q1");
    JP::Node_Ptr q2node = child( cnode, "q2");
    
    set_fvalue( polqm2, fdata( qm2node));
    set_fvalue( polqm1, fdata( qm1node));
    set_fvalue( polq0, fdata( q0node));
    set_fvalue( polq1, fdata( q1node));
    set_fvalue( polq2, fdata( q2node));
  }
  
  // constructor
  Field::Field( const String& file){
    std::ifstream input( file);
    
    if (!input.is_open())
      error( "file ", file, " could not be opened");
    
    initialize( input);
  }
  
  // constructor
  Field::Field( std::ifstream& input){ 
    initialize( input);
  }
  
  // constructor
  Field::Field( JP::Parser& parser){
    initialize( parser);
  }
  
  // constructor
  Field::Field(){
    init_variables();
  }

  void Field::initialize( std::ifstream& input){
    JP::Parser parser( input);
    initialize( parser);
  }

  void Field::init_variables(){
    set_fvalue( cx, NAN);
    set_fvalue( cy, NAN);
    set_fvalue( cz, NAN);
    
    set_fvalue( pol1, NAN);
    set_fvalue( polx, NAN);
    set_fvalue( poly, NAN);
    set_fvalue( polz, NAN);
    set_fvalue( polqm2, NAN);
    set_fvalue( polqm1, NAN);
    set_fvalue( polq0, NAN);
    set_fvalue( polq1, NAN);
    set_fvalue( polq2, NAN);
    set_fvalue( debye, NAN);
    set_fvalue( fit_radius, NAN);
  }
  
  void Field::initialize( JP::Parser& parser){
    init_variables();
    
    parser.complete_current_node();
    JP::Node_Ptr node = parser.current_node();

    double val;
    val = double_from_node( node, "zeroth_order_pole");
    set_fvalue( pol1, val);
    val = double_from_node( node, "debye");
    set_fvalue( debye, val);
    val = double_from_node( node, "vacuum-permittivity");
    set_fvalue( vperm, val);
    solvdi = double_from_node( node, "solvent-dielectric");
    val = double_from_node( node, "fit-radius");
    set_fvalue( fit_radius, val);

    get_v3_from_node( node, "center", cx,cy,cz);
    get_v3_from_node( node, "first_order_poles", polx,poly,polz);
    get_m3_from_node( node, "second_order_poles");
  }
  
}


