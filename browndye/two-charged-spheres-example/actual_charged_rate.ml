let pi = 3.1415926;;
let a1 = 1.01823;;
let a2 = 1.01079;;
let r0 = 2.5;;
let mu = 0.243;;
let kT = 1.0;;
let epsi = 78.0*.0.000142;;
let qq = -1.0*.1.00798;;
let debye = 7.828;; 
(* let debye = 1.0e40;; *)

let diff0 = 1.0/.a1 +. 1.0/.a2;;
let factor = kT/.(6.0*.pi*.mu);;

let old_diff = 
  diff0*.factor
;;

let hydro = 1.0;;

let diff r = 
  let asq = (a1*.a1 +. a2*.a2)/.2.0 in
  factor*.( 1.0/.a1 +. 1.0/.a2 -. hydro*.(3.0/.r -. 2.0*.asq/.(r*.r*.r)))
;;

let potential r = 
  qq*.(exp (-.r/.debye))/.(4.0*.pi*.epsi*.r)
;;

let conv_factor = 602000000.0;;

let igrand s =
  let num = 
    if s = 0.0 then
      1.0
    else
      let r = 1.0/.s in 
      exp ((potential r)/.kT)
  in
  num/.(4.0*.pi*.(diff (1.0/.s)))
;;

let eps = 1.0e-7;;
let k = conv_factor/.(Romberg.integral igrand 0.0 (1.0/.r0) eps);;

Printf.printf "k %g\n" k;;
