<html><head></head><body>

<h2>Generic Cartesian-Based Adaptive Multipole Calculator</h2>


<p> Multipole methods allow the rapid computation of pairwise
interactions among a large number of particles.
If there
are N particles, then the time required is proportional to N,
whereas the simple method of doing a double loop and computing
the interaction of each pair requires time proportional to
N<sup>2</sup>. 

</p><p>
This particular code has the following features.  First of all,
it uses a Cartesian formulation of the multipole method, rather
than spherical harmonics.  Rather than using explicit formulas,
it uses a sparse matrix approach to automatically generate
expansion terms, using the method of Visscher and Apalkov 
(J. of Magnetism and Magnetic Materials 322(2)  275-281 (2010))
This allows us to specify <b>any order of expansion</b> at the time
of computation.  This approach also allows the code to use
<b>any spherically symmetric potential of interaction</b>. 
So, in addition to the usual Coulombic interaction, one can just
as easily use a screened Coulombic, or any other.
The partitioning of space in this code is <b>adaptive</b>;
the particles are bounded by a cubes which are recursively subdivided
into octrees.  Particles can also be divided into "sources" and
"receivers"; the sources provide the field and the field is evaluated
at the receivers.  (The sources and receivers can be the same particles.)
Providing this information can help the software optimize the 
division of space into cubic cells.
Finally, the code itself is <b>generic</b>;
it can be used with any container of particles; one just has to provide
the necessary types and functions to an Interface class, as discussed
in previous progress reports.

</p><p>
In tests I've run with Coulombic particles evenly distributed in a box, 
using order 5 gives
forces with relative errors on the average less than 0.01 percent.  At
this order, the break-even point, above which the multipole method
is faster than the simple N<sup>2</sup> double loop, is a bit
less than 1000 particles.

</p><p>
My motivation for writing this is the fact that multipole
methods are needed if current methods for doing protein-protein Brownian dynamics
are to scale to larger things like ribosomes and other organelles.
For example, boundary element methods would need to use Coulombic
and screened Coulombic fields.  Also, in computing desolvation 
forces in my Brownian dynamics code, I need to compute interactions
that scale as 1/r<sup>4</sup>.
So, it's nice that one piece of
code can handle all three types of interactions.

</p><p> This code is found in two files in browndye/src.  The header file
<code>cartesian_multipole.hh</code> is included in the user code,
and it is linked to the object code of <code>cartesian_multipole.cc</code>.


</p><h4>Class and Member Functions</h4>

<p>
The templated C++ class used for this is the <code>Calculator</code>
class, found in namespace <code>Cartesian_Multipole</code>.  
The <code>Calculator</code> takes an Interface class as its template
argument.  It has the following member functions and types:

</p><ul>
<li><code>Container</code> - type of particle container; pulled from
the <code>Interface</code> template argument

</li><li><code>Calculator( size_t order, Container&amp; particles)</code> - 
constructor;  <code>order</code> is the order of multipole expansion.

</li><li><code>void set_max_number_per_box( size_t n)</code> - sets
the maximum number of particles in a bottom-level box.  The
default is 20.

</li><li><code>size_t max_number_per_box()</code>

</li><li><code>void set_distance_ratio( double)</code> - Two boxes
are considered <i>near neighbors</i> if the ratio of the
maximum dimension of Box 1 or Box 2 (whichever is larger),
and the center-to-center distance, is larger than a certain
value.  This value can be changed by this function; the
default is 0.5.  

</li><li><code>double distance_ratio()</code>

</li><li><code>void set_padding( double)</code> - if there is no
padding, then the outermost box has its bounds set at the
extremes of particle positions.  The padding value increases
the box size by a certain amount in each direction.  The
default value is 0.0.  Adding padding might be a good idea
if you think your particles might drift outside the box.

</li><li><code>void setup_boxes()</code> - sets up the containing boxes and
precomputes many of the multipole coefficients.  Must be done
at the beginning of a simulation, and again only if the distribution
of the particles changes significantly.  You probably don't want
to call this at each time step of a simulation, since it can
take significantly more time than the other functions described below.
The three
setting functions above, <code>set_padding</code>, <code>set_max_number_per_box</code>,
and <code>set_distance_ratio</code>, must be called before this
function if the user doesn't want to use the default values.

</li><li><code>void compute_expansion()</code> - performs the computation
steps: the upward combining of multipoles, the translation to
far boxes, and the downward combining of Taylor expansions.
Must be done each time the particles change positions or 
interaction strength (e.g., charge).  Basically, this computes
the far-field stuff.

</li><li><code>void compute_interactions_with_potential()</code> - 
finishes the pairwise interactions using the <b>value</b> of 
the potential field.  Loops over the particles in each box
and near-neighbor boxes, and includes the far field computed
in <code>compute_expansion</code>.  Makes use of the engine function
<code>apply_potential</code> (see below). The 
<code>compute_expansion</code> function must be called beforehand
to compute the far-field stuff.

</li><li><code>void compute_interactions_with_field()</code> - 
like previous, but uses the <b>gradient</b> of the 
field.  Makes use of the engine function
<code>apply_field</code> (see below). 

</li><li><code>reload_contents()</code> - if the particles have
changed position enough that many of them are no longer in
their original boxes, then this function redistributes them.
It does not change the boxes themselves.  In an MD simulation,
it is probably a good idea to call this function frequently,
since it doesn't appear to consume much time.

</li><li><code>double potential_at_point( const double* r)</code> - 
given a point position (x,y,z) in r, returns the value of 
the field at the point, whether inside or outside the
outer box.  Only valid once <code>compute_expansion</code> has
been called.

</li><li><code>void get_field_at_point( const double* r, double* F)</code> - 
just like the previous function, except it put the gradient
of the field at that point into <code>F</code>.

</li></ul>

<h4>Interface Class</h4>
The Interface class must have the following types and functions defined:

<ul>
<li><code>typedef Container</code> - type of particle container. This
actually contains the particles. At no place does the multipole
code ever address either individual particles or their references,
so this gives lots of flexibility.

</li><li><code>typedef Refs</code> - Each bottom-level box maintains references
to the particles, and <code>Refs</code> is the type of the container.
For example, it could be an array of pointers, or it could be
an array of integers that address the <code>Container object</code>.

</li><li><code>size_t size( const Container&amp; container, const Refs&amp; refs)</code> - 
number of contained references. Note that in this function and
several others below, the reference container <code>refs</code> is 
always accompanied by the original particle container; some implementations
might require it (e.g., using integers as references would require
a starting memory address to address a particle). 

</li><li><code>void copy_references( Container&amp; container, Refs&amp; refs)</code> - 
copies references from <code>container</code> to <code>refs</code>

</li><li><code>void copy_references( Container&amp; container, Refs&amp; refs0, Refs&amp; refs1)</code> -
copies references from <code>refs0</code> to <code>refs1</code>.

</li><li><code>void empty_references( Container&amp; container, Refs&amp; refs)</code> - empties
<code>refs</code>

</li><li><code>void split_container( Container&amp; cont, Refs&amp; refs, 
Refs* const refs_cube[][2][2]
)</code> - splits <code>refs</code>
and distributes the particles into the eight subcubes of the current cube.
Pointers to eight <code>Refs</code> data structures are provided by the
multipole code and presented to the user in a 2x2x2 array.  If 0 and 1 
are indices describing coordinates of the eight cubes, then the 
first index of <code>refs_cube</code> is the x-coordinate.

</li><li><code>void get_bounds( const Container&amp; cont, 
double low[3], double high[3])</code> - 
places the lowest and
highest x,y, and z coordinate of <code>refs</code> into 
<code>low</code> and <code>high</code>.

</li><li><code>void process_bodies( Container&amp; cont, Refs&amp; refs)</code> - 
directly computes the interactions among all particles 
referenced in <code>refs</code>.  This and the following function are
used to compute the near-field interactions.

</li><li><code>void process_bodies( Container&amp; cont, Refs&amp; refs0, Refs&amp; refs1)</code> - 
directly computes the interactions between the particles in 
<code>refs0</code> and those in <code>refs1</code>.  Does not compute interactions
among particles in the same container.  

</li><li><code>template&lt; class Function&gt; void apply_potential( Container&amp; cont, 
Refs&amp; refs, Function&amp; f)</code> - the function object <code>f</code>, which
is provided by and
passed to this function by the multipole code,  has
overloaded operator <code>double operator()( const double* r)</code>.
This function object <code>f</code> returns the field value at point <code>r</code>.
The <code>apply_potential</code> function must loop through each particle in <code>refs</code>,
get its position, feed the position to <code>f</code>, get the 
field value, and do something with it (e.g., multiply it by the
particle charge and add to the total energy).  This is used to 
compute the far-field interactions on the individual particles.

</li><li><code>template&lt; class Function&gt; void apply_field( Container&amp; cont,
Refs&amp; refs, Function&amp; f)</code> - like the previous function, except
that <code>f</code> has <code>void operator( const double* r, double* F)</code>.
This takes particle position <code>r</code> and places the field
gradient at that point into <code>F</code>, which is provided by the
user.
For example, this function would be
used to compute forces in a Coulombic system by getting <code>F</code> for
each particle, multiplying it by the charge and -1 to get the far-field
contribution to the force on that particle.

</li><li><code>template&lt; class Function&gt; void apply_multipole_function(
const Container&amp; cont, const Refs&amp; refs, Function&amp; f
)</code> - this function is used to get multipoles from individual
particles in <code>refs</code>.  The function object <code>f</code> has
the () operator overloaded as 
<code>void operator()( size_t order, const double* r, const double* mpole)</code>.  The <code>apply_multipole_function</code>
must loop through each particle and call the function object <code>f</code> with
the particle's multipole order in <code>order</code>,
the position in <code>r</code>, and its multipole moments (more below) in <code>mpole</code>.
For example, if we were
doing a Coulombic calculation with point charges, <code>order</code>
would be 0, and the particle charge would be placed in <code>mpole[0]</code>.
If the particles were dipoles, <code>order</code> would be 1,
the charge would be placed
into <code>mpole[0]</code>, and the x,y and z components of the
dipole would be placed 
into <code>mpole[1]</code>,<code> mpole[2]</code>and <code>mpole[3]</code>.
See the note below on how the arguments to <code>mpole</code> are defined.

</li><li><code>void get_r_derivatives( size_t order, double r, double* derivs)</code> - 
This places the i<sup>th</sup> derivative of the spherically symmetric 
potential (or Green's function) at <code>r</code> into <code>derivs[i]</code>.
Derivatives up to and including <code>order</code> must be included.
 
</li><li><code>double near_potential( const Container&amp; cont, const Refs&amp; refs,
const double* r)</code> - given a position <code>r</code>, computes the
field value at that point from the particles in <code>refs</code>.
Required only if the <code>potential_at_point</code> function is
being used.

</li><li><code>double get_near_field( const Container&amp; cont, const Refs&amp; refs,
const double* r, double* F)</code> - like the previous, except 
computes the field gradient at <code></code> and puts it into
<code>F</code>.  
Required only if the <code>get_field_at_point</code> function is
being used.

</li><li><code>size_t n_sources( const Container&amp; cont, const Refs&amp; refs)
</code> - number of source particles in <code>refs</code>. 
If the user is not
distinguishing between sources and receivers, this should return the same number
as <code>size</code>.

</li><li><code>size_t n_receivers( const Container&amp; cont, const Refs&amp; refs)
</code> - number of receiver particles in <code>refs</code>.
If the user is not
distinguishing between sources and receivers, this should return the same number
as <code>size</code>.

</li></ul>
<h4>Further Comments</h4>
<p>
As can be seen, all individual particles are accessed only by the
user-supplied code.  This allows the use of containers with
more that one type of objects in each container.
For example, boundary element calculations
use both boundary elements and permanent charges, but the multipole
code does not need to know that it is dealing with two different kinds of
objects.

</p><p> 
Regarding the <code>mpole</code> argument to the function object given to
<code>apply_multipole_function</code>,
the multipoles are laid out in a one-dimensional array, even though
they represent higher-order tensors.  The
multipole (i,j,k) is defined simply as the integral over the
charge distribution of
the Green's function f times (x<sup>i</sup> y<sup>j</sup> z<sup>k</sup>).
This is different than the conventional definition of a quadrupole,
for example.  The three indices are arranged in order by the
following generation algorithm:  

<font color="maroon"><pre>i = previous_i; // order among indices 
for( int kz = 0; kz &lt;= order, kz++)
  for( int ky = kz; ky &lt;= order; ky++){
    indices[i][0] = order - ky; // x power
    indices[i][1] = ky - kz;    // y power
    indices[i][2] = kz;         // z power
    ++i;
}
</pre></font>
It can be seen that the 3 indices always sum to <code>order</code>.
This double loop is run for all orders, from 0 up to the
actual order of the computation, and indices of the next
higher order are placed right after the previous one.
For example, the six quadrupole indices would be
(2,0,0), (1,1,0), (0,2,0), (1,0,1), (0,1,1), (0,0,2), and
they would be placed in slots 4 through 9, because
the 0 slot has the 0<sup>th</sup>-order pole, and
slots 1 through 3 have the dipoles.

</p><p>
An actual simulation loop would look something like this:

<font color="maroon"><pre>size_t order = 5;
Particles particles;
size_t nsteps = ...;

// initialize the particles
...

Cartesian_Multipole::Calculator&lt; Interface&gt; ms( order, particles);
ms.set_max_number_per_cell( 10);
ms.setup_cells();

for( size_t istep = 0; istep &lt; nsteps; istep++){
  ms.compute_expansion();
  ms.compute_interactions_with_field(); // get forces

  // do other stuff for the simulation, including updating particle
  // positions
  ...
  ms.reload_contents();
}


</pre></font>
It is the loop in the code above that is the basis for my
determination of the breakeven point.

</p><h4>Future Work</h4>
I would like to include code to compute interactions between two
such multipole boxes that have been rotated and translated
relative to each other.  To do that, I need to figure out how
to efficiently apply rotation operators to these multipoles.
Fortunately, this isn't immediately necessary for what I'm
working on, but it would be nice further down the road.

<p>
It would also be nice to further genericize this code to
allow people to add their own methods for doing multipole
expansions.  There are some very fast and sophisticated
algorithms based on spherical harmonics and plane waves,
and allowing others to plug these into a generic framework could be 
very useful.




</p></body></html>
