(* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*)


let swap a i b j = 
  let temp = a.(i) in
    a.(i) <- b.(j);
    b.(j) <- temp
;;

let swapm a i j b m n = 
  let temp = a.(i).(j) in
    a.(i).(j) <- b.(m).(n);
    b.(m).(n) <- temp
;;

let solve3 ~matrix:m ~b_array:b ~x_array:x = 

  if 
    (((abs_float m.(1).(0)) > (abs_float m.(0).(0))) && 
     (abs_float (m.(1).(0)) >= (abs_float m.(2).(0)))) 
  then (
    for k=0 to 2 do 
      swapm m 0 k m 1 k;
    done;
    swap b 0 b 1;
  )
  else if 
    (((abs_float m.(2).(0)) > (abs_float m.(0).(0))) && 
     ((abs_float m.(2).(0)) >= (abs_float m.(1).(0))))
  then (
    for k = 0 to 2 do 
      swapm m 0 k m 2 k;
    done;
    swap b 0 b 2;
  )
  else ();

  let a01 = m.(1).(0)/.m.(0).(0) in
  
    for k=0 to 2 do 
      m.(1).(k) <- m.(1).(k) -. a01*.m.(0).(k);
    done;
    b.(1) <- b.(1) -.  a01*.b.(0);
    
    let a02 = m.(2).(0)/.m.(0).(0) in

      for k=0 to 2 do 
	m.(2).(k) <- m.(2).(k) -. a02*.m.(0).(k);
      done;

      b.(2) <- b.(2) -. a02*.b.(0);
  

  if 
    ((abs_float m.(2).(1)) > (abs_float m.(1).(1))) 
  then (
    for k=1 to 2 do 
      swapm m 2 k m 1 k;
    done;
    swap b 2 b 1;
  );

  let a12 = m.(2).(1)/.m.(1).(1) in
  
    for k=1 to 2 do
      m.(2).(k) <- m.(2).(k) -. a12*.m.(1).(k);
    done;

    b.(2) <- b.(2) -. a12*.b.(1);

    (* backsubstitution *)
    x.(2) <- b.(2)/.m.(2).(2);
    x.(1) <- (b.(1) -. m.(1).(2)*.x.(2))/.m.(1).(1);
    x.(0) <- (b.(0) -. m.(0).(1)*.x.(1) -. m.(0).(2)*.x.(2))/.m.(0).(0)

;;




let get_mv_prod mat x b = 
  b.(0) <- mat.(0).(0)*.x.(0) +. mat.(0).(1)*.x.(1) +. mat.(0).(2)*.x.(2);
  b.(1) <- mat.(1).(0)*.x.(0) +. mat.(1).(1)*.x.(1) +. mat.(1).(2)*.x.(2);
  b.(2) <- mat.(2).(0)*.x.(0) +. mat.(2).(1)*.x.(1) +. mat.(2).(2)*.x.(2)
;;

let dot x y = 
  x.(0)*.y.(0) +. x.(1)*.y.(1) +. x.(2)*.y.(2)
;;

let norm x = 
  sqrt (dot x x)
;;

let eps = 1.0e-5;;

let copy x y = 
  y.(0) <- x.(0);
  y.(1) <- x.(1);
  y.(2) <- x.(2)
;;


let reduce mat e x = 
  for i = 0 to 2 do
    for j = 0 to 2 do
      mat.(i).(j) <- mat.(i).(j) -. e*.x.(i)*.x.(j)
    done;
  done
;;

let get_cross a b c = 
  c.(0) <- a.(1)*.b.(2) -. a.(2)*.b.(1);
  c.(1) <- a.(2)*.b.(0) -. a.(0)*.b.(2);
  c.(2) <- a.(0)*.b.(1) -. a.(1)*.b.(0)
;;

let normalize x = 
  let nrm = norm x in
    x.(0) <- x.(0) /. nrm;
    x.(1) <- x.(1) /. nrm;
    x.(2) <- x.(2) /. nrm
;;

let evalue mat ev = 
  let new_ev = [| 0.0; 0.0; 0.0 |] in
    get_mv_prod mat ev new_ev;
    dot ev new_ev
;;

let iterate mat d old_x x = 
  copy x old_x;

  let finis = ref false in
    while not !finis do
      get_mv_prod mat old_x x;
      normalize x;
      d.(0) <- x.(0) -. old_x.(0);
      d.(1) <- x.(1) -. old_x.(1);
      d.(2) <- x.(2) -. old_x.(2);
      if (norm d) < eps then
	finis := true;

      copy x old_x;
    done
    
;;


let eigenvectors ~matrix: mat = 

  let old_x = [| 1.0; 0.0; 0.0; |] in
  let x0 = [| 1.0; 0.0; 0.0; |] in
  let d = [| 0.0; 0.0; 0.0; |] in
    iterate mat d old_x x0;
    let e0 = evalue mat x0 in
      reduce mat e0 x0;
    
    let x1 = 
      let y0 = [| 1.0; 0.0; 0.0; |] 
      and y1 = [| 0.0; 1.0; 0.0; |] 
      and y2 = [| 0.0; 0.0; 1.0; |] 
      in
      let d0 = abs_float (dot y0 x0)
      and d1 = abs_float (dot y1 x0)
      and d2 = abs_float (dot y2 x0)
      in
      let dd = min (min d0 d1) d2 in
      let y = 
	if dd = d0 then 
	  y0
	else if dd = d1 then
	  y1 
	else 
	  y2
      in
      let xx = [| 0.0; 0.0; 0.0; |] in
	get_cross y x0 xx;
	normalize xx;
	xx
    in
      iterate mat d old_x x1;

      let e1 = evalue mat x1 in
	
	reduce mat e1 x1;

      let x2 = 
	let xx = [| 0.0; 0.0; 0.0; |] in
	  get_cross x0 x1 xx;
	  normalize xx;
	  xx
      in
	iterate mat d old_x x2;

	let e2 = evalue mat x2 in

	  (x0,x1,x2),(e0,e1,e2)
;;


(****************************************************)
(* Test code *)
(*
let print_mat m = 
  Printf.printf "%f %f %f\n" m.(0).(0) m.(0).(1) m.(0).(2); 
  Printf.printf "%f %f %f\n" m.(1).(0) m.(1).(1) m.(1).(2); 
  Printf.printf "%f %f %f\n" m.(2).(0) m.(2).(1) m.(2).(2)
;; 

let print_vec b = 
  Printf.printf "%f %f %f\n" b.(0) b.(1) b.(2)
;;
*)

(*

let mat = [| [| 3.0; 0.2; 0.1; |]; 
	     [| 0.2; 2.0; 0.5; |]; 
	     [| 0.1; 0.5; 1.0; |]; 
|]
;;

let mat0 = Array.init 3 (fun i -> Array.init 3 (fun j -> mat.(i).(j)));;


let (x0,x1,x2),(e0,e1,e2) = f mat;;


Printf.printf "evalues %f %f %f\n" e0 e1 e2;;

let priev x = 
  let b = [| 0.0; 0.0; 0.0 |] in
    get_mv_prod mat0 x b;
    Printf.printf "%f %f %f\n" 
      (b.(0)/.x.(0)) (b.(1)/.x.(1)) (b.(2)/.x.(2))
;;

priev x0;;
priev x1;;
priev x2;;

*)  
	



    
(*
let sq2 = sqrt 0.5  in
let q = 0.353553 in
  
let m0 = [|[|0.0; sq2; sq2 |];
	   [|sq2; 0.0; sq2 |];
	   [|sq2; sq2; 0.0 |]
|]

and m = [|
  [|0.0; sq2; sq2 |];
  [|sq2; 0.0; sq2 |];
  [|sq2; sq2; 0.0 |];
  
|]


and b0 = [|q;q;q|] 
and b =  [|q;q;q|]

and x = [| 0.0; 0.0; 0.0 |] in
  solve3 m b x;

  Printf.printf "%f %f %f\n" x.(0) x.(1) x.(2);
  Printf.printf "\n";
  Printf.printf "%f %f %f\n" b0.(0) b0.(1) b0.(2);
  
  Printf.printf "%f %f %f\n" 
	  (m0.(0).(0)*.x.(0) +. m0.(0).(1)*.x.(1) +. m0.(0).(2)*.x.(2))
	  (m0.(1).(0)*.x.(0) +. m0.(1).(1)*.x.(1) +. m0.(1).(2)*.x.(2))
	  (m0.(2).(0)*.x.(0) +. m0.(2).(1)*.x.(1) +. m0.(2).(2)*.x.(2))
  ;
  
*)


