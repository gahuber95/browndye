(* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*)

(*
Used internally.
Solves the 1D 2nd-order boundary value problem using finite differences.

  cxx*pxx + cx*px + c1*p + c0 = 0
 
  on [0, 1]
 
with two Diriclet boundary conditions.

solve_dd cxx cx c1 c0 bc0 bc1 p 

The number of evenly-spaced points is determined from the size of "p",
and the two end points are included in the solution.  The boundary
values are given by "bc0" and "bc2", and the coefficients are given
by functions of x.
*)
 

(*
Finite difference formulation:

  cxx*( pi+1 + pi-1 - 2 pi)/h2 + cx*( pi+1 - pi-1)/(2*h) + c1*pi = -c0
 
*)

let solve_dd cxx cx c1 c0 bc0 bc1 p = 
  let n = Array.length p in
  let new_array () = 
    Array.init n (fun i -> 0.0)
  in

  let subdiag = new_array()
  and supdiag = new_array()
  and diag = new_array()
  and rhs = new_array()
  in
  let h = 1.0/.( float (n-1)) in
  let hh = h*.h in

    rhs.(0) <- bc0;
    diag.(0) <- 1.0;
    supdiag.(0) <- 0.0;

    for i = 1 to n-2 do
      let x = (float i)*.h in
      let cxx_x = (cxx x)/.hh 
      and cx_x = (cx x)/.(2.0*.h) 
      in
	rhs.(i) <-  -.(c0 x);
	diag.(i) <- (c1 x) -. 2.0*.cxx_x;
	subdiag.(i) <- -.cx_x +. cxx_x;
	supdiag.(i) <- cx_x +. cxx_x;
    done;
    
    rhs.(n-1) <- bc1;
    diag.(n-1) <- 1.0;
    subdiag.(n-1) <- 0.0;

    Tridiagonal_solver.solve subdiag diag supdiag rhs p
;;


(*
let n = 100;;
let p = Array.init n (fun i -> 0.0);;

let cxx x = 1.0;;
let cx x = 1.0;;
let c1 x = 1.0;;
let c0 x = 1.0;;

solve_dd cxx cx c1 c0 0.0 0.0 p;;


for i = 0 to n-1 do
  Printf.printf "%f %f\n" ((float i)/.(float (n-1))) p.(i)
done
;;
*)
 

