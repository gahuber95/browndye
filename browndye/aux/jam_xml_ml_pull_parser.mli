(* Used internally.  This is a hybrid-style parser.  It's like SAX in
that it reads in a bit at a time, but like DOM in that you can build
a tree on the spot.  It sits on top of jam_ml_xml_1f_parser.
*)

(* tree is made of nodes *)
type node

val node_tag: node -> string
(* node has buffer which contains contents of tag *)
val node_stream: node -> Scanf.Scanning.scanbuf
val node_attributes: node -> (string*string) list

(* child with given tag *)
val child: node->string -> node option
val gchild: node->string->string -> node option
val ggchild: node->string->string->string -> node option

(* children with given tag *)
val children: node->string -> node list
val all_children: node -> node list

val int_from_stream: Scanf.Scanning.scanbuf -> int
val int64_from_stream: Scanf.Scanning.scanbuf -> int64
val float_from_stream: Scanf.Scanning.scanbuf -> float
val float3_from_stream: Scanf.Scanning.scanbuf -> float*float*float
val string_from_stream: Scanf.Scanning.scanbuf -> string

type parser

val new_parser: Scanf.Scanning.scanbuf -> parser
val new_parser_of_channel: in_channel -> string -> parser

val is_current_node_tag: parser->string -> bool

(* parser goes from node to node in XML tree *)
val current_node: parser -> node

(* Build complete tree from current node *)
val complete_current_node: parser -> unit

(* moves parser to next tag *)
val next: parser -> unit

(* result is current node *)
val node_of_tag: parser->string -> node option
val node_of_tags: parser-> (string array) -> (node*string) option
val node_of_tag_with_parent: parser->string->string -> node option
val skip_current_node: parser -> unit
val skip_to_parent_end: parser -> unit
val finished: parser->bool
val go_up: parser -> unit

(* node argument to function is current node *)
val apply_to_nodes_of_tag: parser->string->(node->unit) -> unit

val fold_nodes_of_tag: parser->string->('a->node->'a)->'a -> 'a

