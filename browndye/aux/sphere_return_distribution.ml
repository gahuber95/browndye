(* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*)


(* 
Called by bd_top.

This generates data for an enhanced version of the Luty-McCammon-Zhou 
algorithm for computing the probability and angular distribution of a 
charged molecule returning to the inner sphere from the outer sphere
around the other molecule.

In addition to the LMZ algorithm, it computes time distributions 
of return as a function of polar angle, and this information is used
by the simulation program to update the rotational states of the
molecules (Unpublished work, contact author for rough draft).

This information is output in an XML file, and is used by the simulation
program.  It takes the following arguments, with flags:

-arad0 : radius of molecule 0;
-arad1 : radius of molecule 1;
-ch0 : charge of molecule 0 (default 0);
-ch1 : charge of molecule 1 (default 0);
-debye : debye length (default infinity);
-kT : kT (default 1);
-vperm : vacuum permittivity (default units of kT, Angstrom, and picosecond;
-diel : dielectric (default 78);
-h2ovisc : water viscosity at 298 K (default units of kT, Angstrom and picosecond;
-visc : viscosity relative to water at 298 K (default 1); 
-rbegin : beginning radius of trajectory;
-hi: no argument; include hydrodynamic interactions  
-hemi: system exists only in northern hemisphere (theta < pi/2)
*)

(****************************************************************)
(* inner abs. radius, eps, kT, and D are unity 

Characteristics:

Length: R = inner absorbing radius - outer absorbing radius
Time:   R^2/diffusivity
Energy:  kT
Charge:  sqrt( kT eps R) 

outer absorbing radius = 1 + 3*( rb - 1) 

q^2 = q0*q1

2/a = 1/a0 + 1/a1

*)


(****************************************************************)

let pi = 3.1415926;;
let pi4 = 4.0*.pi;;
let pi6 = 6.0*.pi;;
let pi8 = 8.0*.pi;;

let sq x = x*.x;;
let len = Array.length;;

let rounded x = 
  let low = floor x in
  let ilow = int_of_float x in
    if x >= low +. 0.5 then 
      ilow + 1
    else 
      ilow
;;

let a0_ref = ref nan;;
let a1_ref = ref nan;;
let q0_ref = ref 0.0;;
let q1_ref = ref 0.0;;
let rin_ref = ref nan;;
let kT_ref = ref 1.0;;
let dL_ref = ref infinity;;
let eps0_ref = ref 0.000142;;
let diel_ref = ref 78.0;;
let rel_mu_ref = ref 1.0;;
let water_mu_ref = ref 0.243;;
let has_hi_ref = ref false;;
let hemisphere_ref = ref false;;

Arg.parse
  [("-arad0", Arg.Set_float a0_ref, "radius of molecule 0");
   ("-arad1", Arg.Set_float a1_ref, "radius of molecule 1");
   ("-ch0", Arg.Set_float q0_ref, "charge of molecule 0 (default 0)");
   ("-ch1", Arg.Set_float q1_ref, "charge of molecule 1 (default 0)");
   ("-debye", Arg.Set_float dL_ref, "debye length (default infinity)");
   ("-kT", Arg.Set_float kT_ref, "kT (default 1)");
   ("-vperm", Arg.Set_float eps0_ref, 
   "vacuum permittivity (default units of kT, Angstrom, and picosecond)");
   ("-diel", Arg.Set_float diel_ref, "dielectric (default 78)");
   ("-h2ovisc", Arg.Set_float water_mu_ref, 
   "water viscosity at 298 K(default units of kT, Angstrom and picosecond)");
   ("-visc", Arg.Set_float rel_mu_ref, "viscosity relative to water at 298 K(default 1)"); 
   ("-rbegin", Arg.Set_float rin_ref, "beginning radius of trajectory");
   ("-hi", Arg.Unit (fun () -> has_hi_ref := true), 
    "include hydrodynamic interactions");
     ("-hemi", Arg.Unit (fun () -> hemisphere_ref := true),
      "system exists only in northern hemisphere")
  ]
  (fun arg -> raise (Failure "no anonymous arguments"))
  "Outputs time and probability distributions for simulation"
;;

let has_hi = !has_hi_ref;;
let hydro = if has_hi then 1.0 else 0.0;;
let hemisphere = !hemisphere_ref;;

let outer_abs_radius rin rb = 
  rin +. 3.0*.( rb -. rin)
;;

let force_n_diff a0 a1 rin q dL =

  let q2 = 
    let qq = q*.q in
    let sgn = if q > 0.0 then 1.0 else -1.0 in
      sgn*.qq 
  in

  (* rt ranges from 0 to 1 *)
  let f rt = 
    let r = rt +. rin in
    let rL = r/.dL in
    (q2*.(exp (-.rL))/.(pi4*.r*.r))*.(1.0 +. rL)
  in

  let a = 1.0/.(1.0/.a0 +. 1.0/.a1) in
  let asq = (a0*.a0 +. a1*.a1)/.2.0 in

  let diff_para rt =
    let r = rt +. rin in
      1.0 -. hydro*.(3.0/.r -. 2.0*.asq/.(r*.r*.r))*.a
  in
  let diff_perp rt =
    let r = rt +. rin in
      1.0 -. hydro*.(1.5/.r +. asq/.(r*.r*.r))*.a

  in  
  f, diff_para, diff_perp

let distribution_1d a0 a1 rin q dL pleg =
  
  let fpleg = float pleg in
 
  let f, diff_para, diff_perp = force_n_diff a0 a1 rin q dL in

  let cxx = diff_para
  and cx rt = 
    let r = rt +. rin in
      (diff_para rt)*.(f rt) +. (diff_perp rt)*.2.0/.r
  and c1 rt = 
    let r = rt +. rin in
      -.fpleg*.(fpleg +. 1.0)*.(diff_perp rt)/.(r*.r)
  in
  let n = 10000 in
  let new_arr () = Array.init n (fun i -> nan) in
  let p0, p1 = 
    let p0, p1 = new_arr(), new_arr() in

    let c0 rt = 0.0 in
      Ode_bc_2nd_order_solver.solve_dd cxx cx c1 c0 1.0 0.0 p0;
      Ode_bc_2nd_order_solver.solve_dd cxx cx c1 c0 0.0 1.0 p1;

      p0,p1
  in

  let pt0 = 
    let pt0 = new_arr() in
    let c0 rt = 
      let i = rounded (rt*.(float (n-1))) in
	p0.(i)
    in    
      Ode_bc_2nd_order_solver.solve_dd cxx cx c1 c0 0.0 0.0 pt0;
      pt0
    
  and pt1 = 
    let pt1 = new_arr() in
    let c0 rt = 
      let i = rounded (rt*.(float (n-1))) in
	p1.(i)
    in    
      Ode_bc_2nd_order_solver.solve_dd cxx cx c1 c0 0.0 0.0 pt1;
      pt1
    
  in

  let ptt0 = 
    let ptt0 = new_arr() in
    let c0 rt = 
      let i = rounded (rt*.(float (n-1))) in
	2.0*.pt0.(i)
    in    
      Ode_bc_2nd_order_solver.solve_dd cxx cx c1 c0 0.0 0.0 ptt0;
      ptt0
    
  and ptt1 = 
    let ptt1 = new_arr() in
    let c0 rt = 
      let i = rounded (rt*.(float (n-1))) in
	2.0*.pt1.(i)
    in    
      Ode_bc_2nd_order_solver.solve_dd cxx cx c1 c0 0.0 0.0 ptt1;
      ptt1
  in

  let nr = (n-1)/3 in
    (p0.(nr),pt0.(nr),ptt0.(nr)), (p1.(nr),pt1.(nr),ptt1.(nr))
;;


let map2 f xs ys = 
  let n = len xs in
    Array.init n
      (fun i -> f xs.(i) ys.(i))
;;    

type sub_inner_dist = {
  pcm0: float;
  t0: float;
  tt0: float;

  pcm1: float;
  t1: float;
  tt1: float;

  theta: float;
}
;;

type inner_dist = {
  sub_dists: sub_inner_dist array;
  rb: float;
  rin: float;
  rout: float;
  prob_back: float;
  mtime0: float;
  mtime1: float;
}
;;

(* input is dimensionless; output has dimensions *)
let distribution a0 a1 rin q dL clength ctime = 

  let nleg = 100 in
  let ntheta = 181 in
  let dists_1d = 
    Array.init nleg
      (fun pleg -> distribution_1d a0 a1 rin q dL pleg)
  in
       		   
  let fntheta = float (ntheta - 1) in
  let delta_coefs = Array.init nleg 
		      (fun i -> float (2*i + 1))
  in
  let prob_back, mtime0, mtime1 = 
    let (p0,pt0,ptt0),(p1,pt1,ptt1) = dists_1d.(0) in
      p0, pt0/.p0, pt1/.p1
  in

  let p0_coefs = 
    map2 
      (fun c dist -> 
	 let (p0,t0,tt0), info1 = dist in
	   p0*.c
      )
      delta_coefs dists_1d

  and pt0_coefs = 
    map2 
      (fun c dist -> 
	 let (p0,pt0,ptt0), info1 = dist in
	   pt0*.c
      )
      delta_coefs dists_1d

  and ptt0_coefs = 
    map2 
      (fun c dist -> 
	 let (p0,pt0,ptt0), info1 = dist in
	   ptt0*.c
      )
      delta_coefs dists_1d

  in
  let p1_coefs = 
    map2 
      (fun c dist -> 
	 let info0, (p1,pt1,ptt1) = dist in
	   p1*.c
      )
      delta_coefs dists_1d

  and pt1_coefs = 
    map2 
      (fun c dist -> 
	 let info0, (p1,pt1,ptt1) = dist in
	   pt1*.c
      )
      delta_coefs dists_1d

  and ptt1_coefs = 
    map2 
      (fun c dist -> 
	 let info0, (p1,pt1,ptt1) = dist in
	   ptt1*.c
      )
      delta_coefs dists_1d

  in  

  let ps = 
    Array.init ntheta
      (fun ith ->
	let theta = pi*.(float ith)/.fntheta in
	let cth = cos theta in
	let p0 = Legendre.sum p0_coefs cth 
	and p1 = Legendre.sum p1_coefs cth in
	  p0,p1
      )
  in
    
  let pcms = Array.init ntheta (fun i -> 0.0,0.0) in
    pcms.(0) <- 0.0, 0.0;
    for ith = 1 to ntheta-1 do
      let theta = pi*.(float ith)/.fntheta in
      let sth = sin theta in (* put sin(theta) term in at this point *)
      let pcm0,pcm1 = pcms.(ith-1) 
      and p0,p1 = ps.(ith) in
	pcms.(ith) <- pcm0 +. sth*.p0, pcm1 +. sth*.p1
    done;
    
    let ptot0, ptot1 = pcms.( ntheta-1) in
      for i = 0 to ntheta-1 do
	let pcm0,pcm1 = pcms.(i) in
	  pcms.(i) <- pcm0/.ptot0, pcm1/.ptot1
      done;
      
      let sub_dists =	
	Array.init ntheta
	  (fun ith ->
	    let theta = pi*.(float ith)/.fntheta in
	    let cth = cos theta in
	    let pcm0, pcm1 = pcms.(ith) in
	    let p0,p1 = ps.(ith) in 
	      
	    let pt0 = Legendre.sum pt0_coefs cth in
	    let ptt0= Legendre.sum ptt0_coefs cth in
	    let t0 = pt0/.p0 in
	    let tt0 = ptt0/.p0 in
      
	    let pt1 = Legendre.sum pt1_coefs cth in
	    let ptt1 = Legendre.sum ptt1_coefs cth in
	    let t1 = pt1/.p1 in
	    let tt1 = ptt1/.p1 in  	      
	      {
		pcm0 = pcm0;
		t0 = t0*.ctime;
		tt0 = tt0*.ctime*.ctime;
		pcm1 = pcm1;
		t1 = t1*.ctime;
		tt1 = tt1*.ctime*.ctime;
		theta = theta;
	      }
	  )
	  
      in 
	(* Because the Legendre series is truncated, the
	   actual mean time should be computed from the n=0 solution,
	   and the other times scaled by it
	*)

      let inner_t_adj = 
	let sum = ref 0.0 in
	  for ith = 0 to ntheta-2 do
	    let p = sub_dists.(ith+1).pcm0 -. sub_dists.(ith).pcm0 in
	      sum := !sum +. p*.sub_dists.(ith).t0;
	  done;
	  let tapprox = !sum/.sub_dists.(ntheta-1).pcm0 in
	    ctime*.mtime0 /. tapprox

      and outer_t_adj = 
	let sum = ref 0.0 in
	  for ith = 0 to ntheta-2 do
	    let p = sub_dists.(ith+1).pcm1 -. sub_dists.(ith).pcm1 in
	      sum := !sum +. p*.sub_dists.(ith).t1;
	  done;
	  let tapprox = !sum/.sub_dists.(ntheta-1).pcm1 in
	    ctime*.mtime1 /. tapprox
      in

      let adj_sub_dists = 
	Array.map
	  (fun sub_dist ->
	    {sub_dist with 
	      t0 = inner_t_adj*.sub_dist.t0;
	      tt0 = inner_t_adj*.inner_t_adj*.sub_dist.tt0;

	      t1 = outer_t_adj*.sub_dist.t1;
	      tt1 = outer_t_adj*.outer_t_adj*.sub_dist.tt1;		
	    }
	  )
	  sub_dists
      in
      let rb = rin +. 1.0/.3.0 in
	{
	  prob_back = prob_back;
	  sub_dists = adj_sub_dists;
	  rb = rb*.clength;
	  rin = rin*.clength;
	  rout = (outer_abs_radius rin rb)*.clength;
	  mtime0 = mtime0*.ctime;
	  mtime1 = mtime1*.ctime;
	}
	
;;


type outer_dist = {
  rb_o: float;
  rin_o: float;
  prob_back_o: float;
}
;;

type dist = 
    | Outer_Dist of outer_dist 
    | Inner_Dist of inner_dist
;;


(* let distribution a0 a1 rin q dL =  *)

(* nothing is dimensionless yet *)
let multi_distribution rin0 a0 a1 q dL mu kT eps =

  let diff = (kT/.(pi6*.mu))*.(1.0/.a0 +. 1.0/.a1) in

  let time_rot = 
    let trot a = 
      pi8*.mu*.a*.a*.a/.kT
    in
      max (trot a0) (trot a1) 
  in
    
  let rec loop rin rb mdist =

    if (* (rin > 4.0*.dL) || *) ((sq (rb -. rin))/.diff > 4.0*.time_rot) then
      let integral clength =
	let qt = q/.(sqrt (kT*.eps*.clength)) 
	and dLt = dL/.clength 
	and a0t, a1t = a0/.clength, a1/.clength
	in
	  (Escape_prob.integral has_hi a0t a1t qt dLt)/.clength	
      in
	
      let pret = 
	(integral rb)/.(integral rin)
      in
	
      let odist = 
	Outer_Dist {
	  rb_o = rb;
	  rin_o = rin;
	  prob_back_o = pret;
	}
	  
      in
	odist :: mdist
    else
      let rout = rin +. 3.0*.(rb -. rin) in
      let dist = 
	let clength = rout -. rin in
	let diff0 = kT*.(1.0/.a0 +. 1.0/.a1)/.(pi6*.mu) in
	let ctime = clength*.clength/.diff0 in
	let rint = rin/.clength
	and a0t, a1t = a0/.clength, a1/.clength 
	and qt = q/.(sqrt (kT*.eps*.clength)) 
	and dLt = dL/.clength 
	in
	  Inner_Dist (distribution a0t a1t rint qt dLt clength ctime) 
      in
      let new_mdist = dist :: mdist in
	loop rb rout new_mdist
	  
  in
  let dist_lst = loop rin0 (1.1*.rin0) [] in
    Array.of_list (List.rev dist_lst)
;;

(*
type outer_dist = {
  rb_o: float;
  rin_o: float;
  prob_back_o: float;
}
;;
*)

let pr = Printf.printf;;

let print_outer_distribution odist = 
  pr "  <outer-distr>\n";
  pr "    <begin-radius> %g </begin-radius>\n" (odist.rb_o);
  pr "    <absorbing-radius> %g </absorbing-radius>\n" (odist.rin_o);
  pr "    <prob-return> %g </prob-return>\n" odist.prob_back_o;
  pr "  </outer-distr>\n"
;;


let print_inner_distribution idist = 
  let pr_sub_dist i sdists = 
    let sdist = sdists.(i) in
      
    let sdev t tt = 
      sqrt (tt -. t*.t)
    in

    let thresh = 0.999 in

    let ipr0_prev = (i = 0) || sdists.(i-1).pcm0 < thresh
    and ipr1_prev = (i = 0) || sdists.(i-1).pcm1 < thresh
    in
    let ipr0, ipr1 = sdist.pcm0 < thresh, sdist.pcm1 < thresh in

      if ipr0_prev || ipr1_prev then (
	pr "    <sub-distr>\n";
	pr "      <theta> %g </theta>\n" sdist.theta;
	
	if ipr0_prev then (
	  pr "      <inner>\n";
	  pr "        <cumul-prob> %g </cumul-prob>\n" (if ipr0 then sdist.pcm0 else 1.0);
	  pr "        <mean-t> %g </mean-t>\n" sdist.t0;
	  pr "        <sdev-t> %g </sdev-t>\n" (sdev sdist.t0 sdist.tt0);
	  pr "      </inner>\n";
	);
	
	if ipr1_prev then (
	  pr "      <outer>\n";
	  pr "        <cumul-prob> %g </cumul-prob>\n" (if ipr1 then sdist.pcm1 else 1.0);
	  pr "        <mean-t> %g </mean-t>\n" sdist.t1;
	  pr "        <sdev-t> %g </sdev-t>\n" (sdev sdist.t1 sdist.tt1);
	  pr "      </outer>\n";
	);
	
	pr "    </sub-distr>\n";
      )
  in
    pr "  <inner-distr>\n";
    pr "    <begin-radius> %g </begin-radius>\n" idist.rb;
    pr "    <inner-absorbing-radius> %g </inner-absorbing-radius>\n" idist.rin;
    pr "    <outer-absorbing-radius> %g </outer-absorbing-radius>\n" idist.rout;
    pr "    <prob-inner> %g </prob-inner>\n" idist.prob_back;
    pr "    <mean-time-inner> %g </mean-time-inner>\n" idist.mtime0;
    pr "    <mean-time-outer> %g </mean-time-outer>\n" idist.mtime1;

    for i = 0 to (len idist.sub_dists) - 1 do
      pr_sub_dist i idist.sub_dists;
    done;

    (* Array.iter pr_sub_dist idist.sub_dists; *)
    pr "  </inner-distr>\n"
;;
  
let print_multi_distribution mdistr inner_r rdiff0 rdiff1 
    q0 q1 a0 a1 vperm diel dL visc hi = 
  pr "<distribution>\n";
  pr "  <inner-radius> %g </inner-radius>\n" inner_r;
  pr "  <rotational-diffusivity0> %g </rotational-diffusivity0>\n" rdiff0; 
  pr "  <rotational-diffusivity1> %g </rotational-diffusivity1>\n" rdiff1; 
  pr "  <charge0> %g </charge0>\n" q0;
  pr "  <charge1> %g </charge1>\n" q1;
  pr "  <hydro-radius0> %g </hydro-radius0>\n" a0; 
  pr "  <hydro-radius1> %g </hydro-radius1>\n" a1;
  pr "  <vacuum-permittivity> %g </vacuum-permittivity>\n" vperm;
  pr "  <dielectric> %g </dielectric>\n" diel;
  pr "  <debye-length> %g </debye-length>\n" dL;
  pr "  <viscosity> %g </viscosity>\n" visc;
  pr "  <hydrodynamic-interactions> %b </hydrodynamic-interactions>\n" hi;
  pr "  <hemisphere> %b </hemisphere>\n" hemisphere;

  Array.iter
    (fun distr ->
      match distr with
	| Inner_Dist idistr -> print_inner_distribution idistr;
	| Outer_Dist odistr -> print_outer_distribution odistr;
    )
    mdistr;

  pr "</distribution>\n"
;;

let a0 = !a0_ref;;
let a1 = !a1_ref;;
  
let q0 = !q0_ref;;
let q1 = !q1_ref;;

let kT = !kT_ref;;
let dL = !dL_ref;;
let eps0 = !eps0_ref;;
let diel = !diel_ref;;
let rel_mu = !rel_mu_ref;;
let water_mu = !water_mu_ref;;

let rin = !rin_ref;;

let pi6 = 6.0*.pi;;
let pi8 = 8.0*.pi;;

let mu = rel_mu*.water_mu;;
let diffusivity = (kT/.(pi6*.mu))*.(1.0/.a0 +. 1.0/.a1);;
let eps = eps0*.diel;;

let q = 
  let qq = q0*.q1 in
  let sgn = if qq > 0.0 then 1.0 else -1.0 in
    sgn*.sqrt( (abs_float qq))
;;

let mdistr = multi_distribution rin a0 a1 q dL mu kT eps;; 

let rdiff0 = kT/.(pi8*.a0*.a0*.a0*.mu);;
let rdiff1 = kT/.(pi8*.a1*.a1*.a1*.mu);;

print_multi_distribution mdistr rin rdiff0 rdiff1 q0 q1 a0 a1 eps0 diel dL mu has_hi;; 
