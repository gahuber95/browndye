(* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*)

(*
Used internally. Higher level XML parser than ocamlexpat, on which it sits

See header file for documentation.
*)

type 'a begin_tag_fun = 'a->string->((string*string) list) -> unit;;
type 'a end_tag_fun = 'a->string->Scanf.Scanning.scanbuf -> unit;;
type str_stack = string Stack.t;;

type 'a parser = {
  eparser: Expat.expat_parser;
  state: 'a;
  funs: ('a begin_tag_fun)*('a end_tag_fun);
  str_stacks: str_stack Stack.t;
}
;;

let get_int buf = 
  Scanf.bscanf buf " %d" (fun i -> i)
;;

let get_float buf = 
  Scanf.bscanf buf " %f" (fun x -> x)
;;

let get_string buf = 
  Scanf.bscanf buf " %s" (fun x -> x)
;;

let new_parser state sfun efun = 

  let parser = {
    str_stacks = Stack.create();
    state = state;
    eparser = Expat.parser_create None;
    funs = sfun,efun
  } 
  in
       
  let start_tag tag attrs = 
    let sfun, efun = parser.funs in
      Stack.push (Stack.create()) parser.str_stacks;
      sfun state tag attrs
  
  and end_tag tag =
    let sfun, efun = parser.funs in
      
    let stk = Stack.pop parser.str_stacks in
    let str = Str_stack.str_from_stack stk in
    let buf = Scanf.Scanning.from_string str	      
    in
      efun state tag buf
	
  and handle_chars str = 
    let str_stack = Stack.top parser.str_stacks in
      Str_stack.add_str_to_stack str str_stack;
      
  in
    Expat.set_start_element_handler parser.eparser start_tag;
    Expat.set_end_element_handler parser.eparser end_tag;
    Expat.set_character_data_handler parser.eparser handle_chars;
    parser
;;

let pre_parse parser s = 
  Expat.parse parser.eparser s
;;

let parse parser buf =
  try
    while not (Scanf.Scanning.end_of_input buf) do
      Scanf.bscanf buf "%s@\n"
	(fun s ->
           Expat.parse parser.eparser s;
	)
    done;
    Expat.final parser.eparser
      
  with Expat.Expat_error ex ->
    let msg = Expat.xml_error_to_string ex in
      Printf.fprintf stderr "%s\n" msg;
      raise (Expat.Expat_error ex)
;;


(* returns true if parsing is done *)
let parse_some parser buf =
  try
    let eof () = Scanf.Scanning.end_of_input buf in
      if not (eof()) then (
	Scanf.bscanf buf "%s@\n"
			     (fun s ->
			       Expat.parse parser.eparser s;
			     );
	
	if (eof()) then (
	  Expat.final parser.eparser;
	  true
	)
	else 
	  false
      )
      else
	raise (Failure "parse_some: end of buffer")

  with Expat.Expat_error ex ->
    let msg = Expat.xml_error_to_string ex in
      Printf.fprintf stderr "%s\n" msg;
      raise (Expat.Expat_error ex)	  
;;

(* returns true if parsing is done *)
let parse_some_of_channel parser channel =
  try
    let line = input_line channel in
      (* Printf.printf "%s\n" line; *)
      Expat.parse parser.eparser line;
      false

  with 
    | End_of_file ->
	Expat.final parser.eparser;
	true;
	
    | Expat.Expat_error ex ->
	let msg = Expat.xml_error_to_string ex in
	  Printf.fprintf stderr "%s\n" msg;
	  raise (Expat.Expat_error ex)	  
	    

;;



