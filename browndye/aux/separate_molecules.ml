(* 
Convenience program.

Reads in two pqrxml files and replaces those files with files in which
the two molecules have been separated a given distance along the line
which connects their centers of mass. It takes the following arguments 
with flags:

-mol0 : pqrxml file of molecule 0
-mol1 : pqrxml file of molecule 1
-dist : separation distance

*)

let file0_ref = ref "";;
let file1_ref = ref "";;
let dist_ref = ref 0.0;;

Arg.parse 
  [
    ("-mol0", Arg.Set_string file0_ref, "pqrxml of molecule 0");
    ("-mol1", Arg.Set_string file1_ref, "pqrxml of molecule 1");
    ("-dist", Arg.Set_float dist_ref, "separation distance");
  ]
  (fun arg -> raise (Failure "no anonymous arguments"))
  "Separates molecules"
;;

let file0 = !file0_ref;;
let file1 = !file1_ref;;
let dist = !dist_ref;;

type atom = {
  rname: string;
  rnumber: int;
  aname: string;
  anumber: int;
  x: float;
  y: float;
  z: float;
  r: float;
  q: float;
};;

let atoms file = 
  let buf = Scanf.Scanning.from_file file in
  let alist = ref [] in
    Atom_parser.apply_to_atoms buf
      (fun ~rname:rname ~rnumber:rnumber ~aname:aname 
	~anumber:anumber ~x:x ~y:y ~z:z ~radius:r ~charge:q ->
	let atom = {
	  rname = rname;
	  rnumber = rnumber;
	  aname = aname;
	  anumber = anumber;
	  x = x;
	  y = y;
	  z = z;
	  r = r;
	  q = q;
	}
	in
	  alist := atom :: (!alist);
      )
    ;
    Array.of_list (List.rev !alist)
;;

let atoms0 = atoms file0;;
let atoms1 = atoms file1;;


let center atoms = 
  let xm, ym, zm = ref 0.0, ref 0.0, ref 0.0 in
    Array.iter
      (fun atom ->
	xm := !xm +. atom.x;
	ym := !ym +. atom.y;
	zm := !zm +. atom.z;
      )
      atoms;
    let fn = float (Array.length atoms) in
      !xm/.fn, !ym/.fn, !zm/.fn
;;

let center0 = center atoms0;;
let center1 = center atoms1;;

let axis = 
  let diff = 
    let x0,y0,z0 = center0
    and x1,y1,z1 = center1
    in
      (x1 -. x0), (y1 -. y0), (z1 -. z0)
  in
  let dx,dy,dz = diff in
  let r = sqrt (dx*.dx +. dy*.dy +. dz*.dz) in
    dx/.r, dy/.r, dz/.r
;;

let pr = Printf.fprintf;;


let print_atoms atoms disp file =
  let buf = open_out file in
  let ax,ay,az = axis in
  let current_res = ref (-1) in
    pr buf "<roottag>\n";
    Array.iter
      (fun atom ->
	if not (atom.rnumber = !current_res) then (
	  if not (!current_res = (-1)) then
	    pr buf "  </residue>\n";
	  pr buf "  <residue>\n";
	  pr buf  "    <residue_name> %s </residue_name>\n" atom.rname;
	  pr buf  "    <residue_number> %d </residue_number>\n" atom.rnumber;
	  current_res := atom.rnumber;
	);
	pr buf  "      <atom>\n";
	pr buf  "        <atom_name> %s </atom_name>\n" atom.aname;
	pr buf  "        <atom_number> %d </atom_number>\n" atom.anumber;
	pr buf  "        <x> %g </x>\n" (atom.x +. disp*.ax);
	pr buf  "        <y> %g </y>\n" (atom.y +. disp*.ay);
	pr buf  "        <z> %g </z>\n" (atom.z +. disp*.az);
	pr buf  "        <charge> %g </charge>\n" atom.q;
	pr buf  "        <radius> %g </radius>\n" atom.r;
	pr buf  "      </atom>\n";
      )
      atoms;
    pr buf "  </residue>\n";
    pr buf "</roottag>\n";
;;

print_atoms atoms0 (-.0.5*.dist) file0;;
print_atoms atoms1 (  0.5*.dist) file1;;








