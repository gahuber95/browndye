(* Used internally. Computes rate constant of diffusion to start-radius by 
integrating the Smoluchowski equation.
Arguments are the solvent node and molecule node from results XML file.
*)

val f:
  Jam_xml_ml_pull_parser.node -> Jam_xml_ml_pull_parser.node -> float
