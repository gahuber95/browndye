(* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*)

(* 
Parses sphere (atom) objects in XML data.

Used internally.
Applies given function to each atom object in an XML file:
(f rname rnumber aname anumber x y z r q)

where 
  rname - contents of "residue_name" tag
  rnumber - contents of "residue_number" tag
  aname - contents of "atom_name" tag
  anumber - contents of "atom_number" tag
  x, y, z - coordinates found in "x","y","z" tags
  r - contents of "radius" tag
  q - contents of "charge" tag
*)

module JP = Jam_xml_ml_pull_parser
module NI = Node_info

type ftype = 
    rname:string -> rnumber:int -> 
  aname:string -> anumber:int -> 
  x:float -> y:float -> z:float -> 
  radius:float -> charge:float -> unit

(* applies function f to contents of input buffer buf *) 
let apply_to_atoms ~buffer:buf ~f:(f:ftype) = 
  let parser = JP.new_parser buf in
    JP.apply_to_nodes_of_tag parser "residue" 
	(fun rnode ->
	  JP.complete_current_node parser;

	  let rnumber = NI.int_of_child rnode "residue_number"
	  and rname = NI.string_of_child rnode "residue_name"
	  in
	  let anodes = JP.children rnode "atom" in
	    List.iter
	      (fun anode -> 
		let fn tag = NI.float_of_child anode tag in
		let x,y,z = fn "x", fn "y", fn "z"
		and q,r = fn "charge", fn "radius"
		and aname = NI.string_of_child anode "atom_name"
		and anumber = NI.int_of_child anode "atom_number"
		in
		  f rname rnumber aname anumber x y z r q
	      )
	      anodes;
	)

(* prints out atom information in PQR format to standard output *)
let print_pqr_atom ~rname ~rnumber:rnum ~aname ~anumber:anum 
    ~x ~y ~z ~radius:r ~charge:q =
  let apre,amid,apos = 
    match String.length aname with
      | 1 -> " ", aname, "  "
      | 2 -> " ", aname, " "
      | 3 -> 
	  let first = aname.[0] in
	    if 'A' <= first && first <= 'Z' then
	      " ", aname,""
	    else
	      "", aname, " "
      | 4 -> "", aname, ""
      | _ -> raise (Failure "should not get here")
  in
    Printf.printf "ATOM %6d %s%s%s %-3s%6d     %7.3f %7.3f %7.3f %7.4f %7.3f\n" 
      anum apre amid apos rname rnum x y z q r;

