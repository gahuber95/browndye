(* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*)


(* Used internally. Used to parse the output grid of "inside_points". *)

(*
let array_init2 nx ny f =
  Array.init nx
    (fun ix ->
      Array.init ny (f ix)
    )
;;

let array_init3 nx ny nz f =
  Array.init nx
    (fun ix -> 
      array_init2 ny nz (f ix)
    )
;;
 *)

open Bigarray;;

let array_init3 nx ny nz f = 
  let res = Array3.create Int8_unsigned C_layout nx ny nz in
  for ix = 0 to nx-1 do
    for iy = 0 to ny-1 do
      for iz = 0 to nz-1 do
        res.{ix,iy,iz} <- (f ix iy iz);
      done;
    done;
  done;
  res
;;

module JP = Jam_xml_ml_pull_parser;;

type pstatus = In_Grid | In_Data | In_Plane | Out;;

type data3d =
    (int, int8_unsigned_elt, c_layout)
    Array3.t

type t = {
  data: data3d;
  spacing: float*float*float;
  corner: float*float*float;
  exclusion_radius: float;
}


(*
type t = {
  data: int array array array;
  spacing: float*float*float;
  corner: float*float*float;
  exclusion_radius: float;
};;
 *)

let tag_node parser tag = 
  match JP.node_of_tag parser tag with
    | None -> raise (Failure ("igrid_parser: no tag of "^tag))
    | Some cnode -> cnode
;;


let float_of_node parser tag = 
  let inode = tag_node parser tag in
  let stm = JP.node_stream inode in
  let res = ref nan in
    Scanf.bscanf stm " %g" (fun r -> res := r);
    !res
;;

let float3_of_node parser tag = 
  let inode = tag_node parser tag in
  let stm = JP.node_stream inode in
  let resx,resy,resz = ref nan, ref nan, ref nan in
    Scanf.bscanf stm " %g %g %g" (fun rx ry rz -> resx := rx; resy := ry; resz := rz);
    !resx, !resy, !resz
;;

let grid xml_format buf = 
  if xml_format then

    let parser = JP.new_parser buf in

    let exclusion_radius = float_of_node parser "exclusion_radius" in

    ignore (tag_node parser "grid");

    let corner = 
      let npnode = tag_node parser "corner" in
      let stm = JP.node_stream npnode in
      let nx_ref, ny_ref, nz_ref = ref nan, ref nan, ref nan in
	Scanf.bscanf stm " %g %g %g" 
	  (fun nx ny nz ->
	     nx_ref := nx;
	     ny_ref := ny;
	     nz_ref := nz;
	  );
	
	!nx_ref, !ny_ref, !nz_ref
    in
    let nx,ny,nz = 
      let npnode = tag_node parser "npts" in
      let stm = JP.node_stream npnode in
      let nx_ref, ny_ref, nz_ref = ref 0, ref 0, ref 0 in
	Scanf.bscanf stm " %d %d %d" 
	  (fun nx ny nz ->
	     nx_ref := nx;
	     ny_ref := ny;
	     nz_ref := nz;
	  );
	
	!nx_ref, !ny_ref, !nz_ref
    in

    let spacing = float3_of_node parser "spacing" in
      ignore (tag_node parser "data");

      let grid = array_init3 nx ny nz (fun ix iy iz -> 2) in 

      let ix = ref 0 in
	JP.apply_to_nodes_of_tag parser "plane" 
	    (fun node ->
	       let iy = ref 0 in
		 JP.apply_to_nodes_of_tag parser "row"
		     (fun node ->
			JP.complete_current_node parser; 
			let stm = JP.node_stream node in
			  for iz = 0 to nz-1 do
			    Scanf.bscanf stm " %d"
			      (fun res -> grid.{!ix,!iy,iz} <- res)
			  done;
			  iy := !iy + 1
		     );
		 ix := !ix + 1
	    );
	
	{
	  data = grid;
	  corner = corner;
	  spacing = spacing;
	  exclusion_radius = exclusion_radius;
	}
	
  else (* not xml format *)
    let clear_blank_lines buf = 
      let finis = ref false in
	while not !finis do
	  Scanf.kscanf buf
	    (fun buf ex -> finis := true;)
	    " \n"
	    ()
	done
    in
      
    let clear_comments buf =
      clear_blank_lines buf;
      
      let finis = ref false in
	while not !finis do
	  Scanf.kscanf buf
	    (fun buf ex -> finis := true;)
	    "#%s@\n"
	    (fun s -> ())
	done;
	
	clear_blank_lines buf;
    in
     
      
    let nx = ref 0 
    and ny = ref 0 
    and nz = ref 0 
    in
      clear_comments buf;
      Scanf.bscanf buf "%d %d %d " 
	(fun ix iy iz -> nx := ix; ny := iy; nz := iz);
      
      clear_comments buf;

      let grid = array_init3 !nx !ny !nz (fun i j k -> 0) 
      in    
	for iz = 0 to !nz-1 do
	  for iy = 0 to !ny-1 do
	    for ix = 0 to !nx-1 do
	      Scanf.bscanf buf "%d " 
		(fun i -> 
		  grid.{ix,iy,iz} <- i 
		)		
	    done;
	  done;
	done;    
	{
	  data = grid;
	  spacing = 1.0,1.0,1.0;
	  corner = 0.0, 0.0, 0.0;
	  exclusion_radius = 0.0;
	}
;;  
