(* Generic code for finding the smallest bounding sphere of a group
of points.  This implements the algorithm of E. Welzl
("Smallest Enclosing Disks (Balls And Ellipsoids)", 
Lect. Notes Comput. Sci. 555 (1991), 359-370.) 
*)

(* defines container of points and position vectors *)
module type Vector3 =
  sig
    (* 3-D vector type *)
    type t
 
    (* reference to a point in the container *)
    type reff 

    (* container of points *)
    type container 

    (* vector from components *)
    val v3 : float -> float -> float -> t 

    (* components from vector *)
    val xyz : t -> float * float * float

    (* vector sum *)
    val sum : t -> t -> t

    (* vector difference *)
    val diff : t -> t -> t

    (* vector scaled by scalar *)
    val scaled : float -> t -> t

    (* dot product *)
    val dot : t -> t -> float

    (* cross product *)
    val cross : t -> t -> t

    (* distance between two positions *)
    val distance : t -> t -> float

    (* position of point defined by reff in container *)
    val position: container->reff -> t

    (* array of point references *)
    val to_array: container -> reff array
  end

(******************************************)
(* module of algorithms *)
module M :
  functor (V : Vector3) ->
    sig

      (* (p0 p1 p2) circle defined by points p0,p1,p2 *)
      val circumcircle : 
	V.t -> V.t -> V.t -> V.t * float

      (* (p0 p1 p2 p3)  sphere defined by points p0, p1, p2, p3 *)
      val circumsphere :
        V.t -> V.t -> V.t -> V.t -> V.t * float

      (* (p0 p1 p2) smallest sphere that contains triangle defined by 
	 p0,p1, and p2 *)
      val smallest_triangle_sphere :
        V.t -> V.t -> V.t -> V.t * float

      (* (p0 p1 p2 p3) smallest sphere containing tetrahedron defined 
	 by p0,p1,p2, and p3 *)
      val smallest_tetrahedron_sphere :
        V.t -> V.t -> V.t -> V.t -> V.t * float

      (* (p0 p1 p2 p3 p) returns true if p is inside tetrahedron defined 
	 by p0 thru p3 *)
      val inside_tetrahedron :
        V.t -> V.t -> V.t -> V.t -> V.t -> bool

      (* (ctr) returns center and radius of smallest containing sphere of 
	 points defined by container ctr *)
      val smallest_enclosing_sphere :
        V.container -> V.t * float
    end
