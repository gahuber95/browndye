(*
Used internally to output grids to dx format.
"data" (can be any type), "corner" (lower corner), 
and "spacing" (isotropic) describe the grid.
"processed" is a function that converts the data at the grid point to
a float.
*)

val f :
  data: ('a array array array) ->
  corner: (float * float * float) -> spacing: (float*float*float) -> 
  processed: ('a -> float) -> unit

