(* 
used interally for parsing XML files.  

apply_to_objects otag buffer int_tags float_tags string_tags f:

For every node in an XML tree with tag "otag", applies the function
"f" to the following arguments:

f int_info float_info string_info

"int_info" contains a list of string names paired with integers;
the string names are the tags of the child nodes of the "otag" node,
and the integers are the integer contents.

"float_info" and "string_info" work the same way for floats and strings.

"f" is called for every "otag" node.  

The tags that are in the arguments to "f" are listed in the arguments
"int_tags", "float_tags", and "string_tags" to "apply_to_objects".

*)

val apply_to_objects: string->Scanf.Scanning.scanbuf ->
  (string list)->(string list)->(string list)->
  (((string*int) list)->((string*float) list)->((string*string) list)->unit) 
  -> unit
