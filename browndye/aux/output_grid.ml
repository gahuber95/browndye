(* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*)

(*
Used internally to output grids to dx format.
"data" (can be any type), "corner" (lower corner), 
and "spacing" (isotropic) describe the grid.
"processed" is a function that converts the data at the grid point to
a float.
*)

let f ~data ~corner ~spacing ~processed = 
  let pr = Printf.printf in
  let len = Array.length in
  let nx,ny,nz = len data, len data.(0), len data.(0).(0) in

    pr "<roottag>\n";
    pr "  <n> %d %d %d </n>\n" nx ny nz;
    let hx,hy,hz = spacing in 
    pr "  <spacing> %g %g %g </spacing>\n" hx hy hz;
    let cx,cy,cz = corner in
      pr "  <corner> %g %g %g </corner>\n" cx cy cz;
      
      pr "  <data>\n";
      pr "    <d>";
      let n = nx*ny*nz in
      let count = ref 0 in
	
	for ix = 0 to nx-1 do
	  for iy = 0 to ny-1 do
	    for iz = 0 to nz-1 do
	      let d = processed data.(ix).(iy).(iz) in 
		pr "%g " d;
		count := !count + 1;
		if (!count mod 3) = 0 then (
		  pr "</d>\n";
		  let dn = n - !count in
		    if dn = 0 then
		      ()
		    else if dn = 1 then
		      pr "    <d n=\"1\">"
		    else if dn = 2 then
		      pr "    <d n=\"2\">"
		    else if dn > 2 then
		      pr "    <d>"
		    else
		      ()
		)
	    done
	  done
	done
	;
	
	let nm3 = n mod 3 in
	  
	  if nm3 = 1 || nm3 = 2 then
	    pr "</d>\n";
	  
	  pr "  </data>\n";
	  pr "</roottag>\n"

