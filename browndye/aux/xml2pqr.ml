(*
Convenience function.
Converts pqrxml file to pqr file (as described in APBS documentation).
Uses standard input and output.
*)

Arg.parse
  []
   (fun arg -> raise (Failure "no anonymous arguments"))
  "Converts pqrxml file to pqr file (as described in APBS documentation). \nReceive
s xml file through standard input and sends pqr file to standard output."
;;

Atom_parser.apply_to_atoms Scanf.Scanning.stdin Atom_parser.print_pqr_atom 
;;
