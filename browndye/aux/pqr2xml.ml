(* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*)


(* 
Convenience function.

Converts pqr file to xml file (as described in APBS documentation). Receives 
pqr file through standard input and sends xml file to standard output.
*)


let pr = Printf.printf;;
let slen = String.length;;

Arg.parse
  []
   (fun arg -> raise (Failure "no anonymous arguments"))
  "Converts pqr file to xml file (as described in APBS documentation). \nReceives pqr file through standard input and sends xml file to standard output."
;;

let buf = Scanf.Scanning.from_channel stdin;;

let test_eof line = 
  if
    (Scanf.sscanf line "%s" (fun s -> slen s)) > 0 
  then
    raise (Scanf.Scan_failure "incomplete line")
;;
  
let do_nothing () = ();;

type atom = {
  rnum: int;
  rname: string;
  anum: int;
  aname: string;
  charge: float;
  radius: float;
  position: Vec3.t;
}
;;

let n_tokens a = 
  let n = slen a in 
  let rec loop i itok last_sp =
    if i = n then
      itok
    else
      let c = a.[i] in 
      if c = ' ' then
	loop (i+1) itok true
      else
	if last_sp then
	  loop (i+1) (itok+1) false
	else
	  loop (i+1) itok false
  in
  loop 0 0 true
;;
      
      
let atom_from_line line =

  let line = Lines.stripped line in

  if not (slen line = 0) then
    let tokens = Lines.split line in
    let head = tokens.(0)
      (* let hend = min 6 (slen line) in
         String.sub line 0 hend *)
    in
      if not ((head = "REMARK") || (head = "END") || (head = "TER")) then (

	let do_error ex = 
	  match ex with
	  | End_of_file -> 
	     test_eof line;
	     None
	  | _ ->
	     Printf.fprintf stderr "%s\n" line;
	     raise ex
	in
        
        (* let tokens = Lines.split line in *)
        
        if (Array.length tokens) < 10 then
          failwith 
	    (Printf.sprintf "wrong number of tokens at %s" line)
        else (

          try
            let anum = int_of_string tokens.(1) in
            let anam = tokens.(2) in
            let rnam = tokens.(3) in
            let id_offset = 
              try 
                ignore (int_of_string tokens.(4));
                0
              with ex ->  
                1
            in
            let rnum = int_of_string tokens.(4+id_offset) in
            let fa i = 
              float_of_string tokens.(i + id_offset)
            in
            let x = fa 5 in
            let y = fa 6 in
            let z = fa 7 in
            let q = fa 8 in
            let  r = fa 9 in
	    let atom = 
	      {
	        aname = anam;
	        anum = anum;
	        rname = rnam;
	        rnum = rnum;
	        charge = q;
	        radius = r;
	        position = Vec3.v3 x y z;
	      }
	    in
	    Some atom

          with ex -> do_error ex
        )
      )
      else
        None
  else
    None
;;
               
       


let next_atom buf = 
  let rec loop () = 
    if not (Scanf.Scanning.end_of_input buf) then
      let atom_opt = Scanf.bscanf buf "%s@\n" atom_from_line in
	match atom_opt with
	  | None -> loop ()
	  | Some atom -> Some atom, 0
    else
      None, 0
  in
    loop ()
	
;;

module Atom = 
  struct
    type t = atom
    type container = Scanf.Scanning.scanbuf
    type index = int
    let atom_name atom = atom.aname
    let atom_number atom = atom.anum
    let residue_name atom = atom.rname
    let residue_number atom = atom.rnum
    let position atom = atom.position
    let charge atom = atom.charge
    let radius atom = atom.radius
    let first_index atom = 0
    let next buf index = next_atom buf
	
  end
;;

module Outputter = Output_pqrxml.M( Atom);;

Outputter.f buf stdout;;

