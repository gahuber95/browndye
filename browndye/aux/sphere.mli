(*   
Used internally to generate polyhedra and points on the surface
of spheres.
*)

(* information on size, position, and order of division of polyhedron.
If order is 0, the original polyhedron results. Each increment of order
means four times as many triangles.
*)

type ball_t = {
  order : int;
  center : Vec3.t;
  radius : float;
}

(* returns new "t" *)
val made : order: int -> radius: float -> center: Vec3.t -> ball_t

(* returns vertices at center of triangles, projected onto 
enclosing sphere. The triangles are of an icosohedron subdivided
"order" times. *)
val icoso_vertices: ball_t -> Vec3.t array

(* order, center, radius -> face center, area *)
val icoso_face_centers: int -> Vec3.t -> float -> (Vec3.t*float) array

