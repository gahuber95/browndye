(* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*)

(*  
Not called by bd_top.

Generates an XML file describing a reaction pathway from the output of 
make_rxn_pairs.  Take the following arguments with flags:

-pairs : file of reaction pairs (output from make_rxn_pairs)
-distance : distance required for reaction
-nneeded : number of pairs needed for reaction
*)


let pair_file_ref = ref "";;
let distance_ref = ref nan;;
let n_needed_ref = ref 1000000;;

Arg.parse [
  ("-pairs", Arg.Set_string pair_file_ref, "file of reaction pairs (output from make_rxn_pairs)");
  ("-distance", Arg.Set_float distance_ref, "distance required for reaction");
  ("-nneeded", Arg.Set_int n_needed_ref, "number of pairs needed for reaction");
]
  (fun a -> Printf.fprintf stderr "needs args\n";)
  "Generates XML file describing reaction pathway from the output of make_rxn_pairs"
;;

let pair_file = !pair_file_ref;;
let distance = !distance_ref;;
let n_needed = !n_needed_ref;;

let buf = Scanf.Scanning.from_file pair_file;;

module JP = Jam_xml_ml_pull_parser;;
let parser = JP.new_parser buf;;

let pr = Printf.printf;;

pr "<roottag>\n";;
pr "  <first-state> start </first-state>\n";
pr "  <reactions>\n";;
pr "    <reaction>\n";;
pr "      <name> association </name>\n";
pr "      <state-before> start </state-before>\n";
pr "      <state-after> end </state-after>\n";
pr "      <criterion>\n";;
pr "        <n-needed> %d </n-needed>\n" n_needed;;

JP.apply_to_nodes_of_tag parser "pair"
    (fun pnode ->
      JP.complete_current_node parser;
      let stm = JP.node_stream pnode in
	Scanf.bscanf stm " %d %d" 
	  (fun i0 i1 -> 
	    pr "        <pair>\n";
	    pr "          <atoms> %d %d </atoms>\n" i0 i1;
	    pr "          <distance> %f </distance>\n" distance;
	    pr "        </pair>\n";
	  )
    )
;;

pr "      </criterion>\n";;
pr "    </reaction>\n";;
pr "  </reactions>\n";;

(*
pr "  <states>\n";;

pr "    <state>\n";;
pr "      <number> 0 </number>\n";;
pr "      <rxn-from> 0 </rxn-from>\n";;
pr "    </state>\n";;

pr "    <state>\n";;
pr "      <number> 1 </number>\n";;
pr "      <rxn-to> 0 </rxn-to>\n";;
pr "    </state>\n";;


pr "  </states>\n";;
*)

pr "</roottag>\n";;
