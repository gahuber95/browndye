(* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*)

(*
Convenience program.
Converts XML grid file to DX file.
Uses standard input and output.
*)


module DG = Dgrid_parser;;

Arg.parse
[]
(fun arg -> raise (Failure "no anonymous arguments"))
"Converts XML grid file to DX file"
;;


let len = Array.length;;

let info = DG.grid Scanf.Scanning.stdin;;
let h = info.DG.spacing;;
let corner = info.DG.corner;;
let xmin = corner.Vec3.x;;
let ymin = corner.Vec3.y;;
let zmin = corner.Vec3.z;;
let data = info.DG.data;;
let nx,ny,nz = len data, len data.(0), len data.(0).(0);;

Printf.printf "object 1 class gridpositions counts %d %d %d\n" nx ny nz;;
Printf.printf "origin %g %g %g\n" xmin ymin zmin;;
Printf.printf "delta %g 0.0 0.0\n" h;;
Printf.printf "delta 0.0 %g 0.0\n" h;;
Printf.printf "delta 0.0 0.0 %g\n" h;;
Printf.printf "object 2 class gridconnections counts %d %d %d\n" nx ny nz;;
Printf.printf "object 3 class array type double rank 0 items %d data follows\n" (nx*ny*nz);;

let i = ref 0;;
for ix = 0 to nx-1 do
  for iy = 0 to ny-1 do
    for iz = 0 to nz-1 do(
      Printf.printf "%g " data.(ix).(iy).(iz);
      if !i = 2 then (
	i := 0;
	Printf.printf "\n";
      )
      else
	i := !i + 1
    )     
    done;
  done;
done;;

Printf.printf
"attribute \"dep\" string \"positions\"
object \"regular positions regular connections\" class field
component \"positions\" value 1
component \"connections\" value 2
component \"data\" value 3\n"
;;
