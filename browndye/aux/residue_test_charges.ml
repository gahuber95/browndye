(* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*)

(*
Called by bd_top.

Generates test charge for each charged residue; 
position is at center of charge for each residue. Uses standard input and
output.
*)


Arg.parse
  []
   (fun arg -> raise (Failure "no anonymous arguments"))
  "Generates test charge for each charged residue; position is at center of charge for each residue."
;;


module V = Vec3;;

type sphere = {
  pos: V.t;
  radius: float;
  charge: float;
  residue: string;
  atype: string;
  rnumber: int;
  anumber: int;
};;


module H = Hashtbl;;

let residues = H.create 0;;

let residue_names = H.create 0;;

 Atom_parser.apply_to_atoms Scanf.Scanning.stdin
   (fun ~rname:rname ~rnumber:rnumber ~aname:aname ~anumber:anum 
     ~x:x ~y:y ~z:z ~radius:r ~charge:q ->
     let pos = V.v3 x y z in
     let new_sphere = {
       charge = q;
       pos = pos;
       radius = r;
       residue = rname;
       atype = aname;
       rnumber = rnumber;
       anumber = anum;
     }       
     in
       if H.mem residues rnumber then
	 let rlist = H.find residues rnumber in
	   H.remove residues rnumber;
	   H.add residues rnumber (new_sphere :: rlist);
       else
	 H.add residues rnumber [new_sphere];

       if not (H.mem residue_names rnumber) then
	 H.add residue_names rnumber rname;
   )
;;

type residue = {
  rpos: V.t;
  rq: float;
  rrnumber: int;
}

let reses = 
  H.fold
    (fun rnumber spheres reses ->
      let charge, abs_charge, pos_sum = 
	List.fold_left 
	  (fun info sphere -> 
	    let q, abs_q, pos = info in	     	      
	    let qsum = q +. sphere.charge in
	    let aqsum = abs_q +. (abs_float sphere.charge) in
	    let pos_sum = V.vsum pos (V.vscaled (abs_float sphere.charge) sphere.pos) in
	      qsum, aqsum, pos_sum
	  ) 
	  (0.0, 0.0, (Vec3.v3 0.0 0.0 0.0)) 
	  spheres
      in
      let pos = V.vscaled (1.0/.abs_charge) pos_sum in
	if (abs_float charge) > 0.0001 then
	  let res = {
	    rpos = pos;
	    rrnumber = rnumber;
	    rq = charge;
	  }
	  in
	    res :: reses
	else
	  reses
    )
    residues
    []
;;

let total_q = 
  List.fold_left
    (fun sum res -> sum +. res.rq)
    0.0
    reses
;;

let pr = Printf.printf;;

pr "<root>\n";;
pr "  <total-charge> %g </total-charge>\n" total_q;;
List.iter
  (fun res ->
    let rname = H.find residue_names res.rrnumber in
      pr "  <point>\n";
      pr "    <residue> %s </residue>\n" rname;
      pr "    <residue_number> %d </residue_number>\n" res.rrnumber;
      pr "    <atom-type> charge-center </atom-type>\n";
      pr "    <x> %g </x> <y> %g </y> <z> %g </z>\n" res.rpos.V.x res.rpos.V.y res.rpos.V.z;
      pr "    <charge> %g </charge>\n" res.rq;
      pr "  </point>\n";
  )
  reses
;;


pr "</root>\n";;

