(* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*)

(* computes two ellipsoids for the molecule from the effective volumes
file.  Both share the same axes.  The hydrodynamic ellipsoid is 
computed from moments of the effective volume, and the bounding ellipsoid
is the ellipsoid with the same proportions but different size and center
which contains the spheres.

Inputs from standard input file from "compute_effective_volumes" and
outputs to standard output.

*)

module PP = Point_parser;;

module V = Vec3;;

type atom = {
  pos: V.t;
  radius: float;
  mutable volume: float;
};;

let alist = ref [];;

let get v i = 
  if i = 0 then
    v.V.x
  else if i = 1 then
    v.V.y
  else
    v.V.z
;;

let put v i a = 
  if i = 0 then
    v.V.x <- a
  else if i = 1 then
    v.V.y <- a
  else
    v.V.z <- a
;;

Arg.parse
  []
  (fun arg -> raise (Failure "no anonymous arguments"))
  "Generates bounding and hydrodynamic ellipsoids from effective volume file (output from compute_effective_volumes)"
;;

PP.apply_to_points "volume" Scanf.Scanning.stdin
  (fun ~x:x ~y:y ~z:z ~radius:r ~charge:vol ~ptype:ptype ->
    let atom = {
      pos = V.v3 x y z;
      radius = r;
      volume = vol;
    }
    in
      alist := atom :: (!alist);
  )
;;

let atoms = Array.of_list (!alist);;

let moments = 
  [| [| 0.0; 0.0; 0.0 |]; 
     [| 0.0; 0.0; 0.0 |]; 
     [| 0.0; 0.0; 0.0 |]; 
  |]
;;

let center = V.v3 0.0 0.0 0.0 ;;


let total_volume0 = 
  Array.fold_left
    (fun sum atom ->
      sum +. (abs_float atom.volume)
    )
    0.0
    atoms
;;

if total_volume0 = 0.0 then
    Array.iter
      (fun atom ->
	atom.volume <- 1.0;
      )
      atoms
;;

let len = Array.length;;

let total_volume = 
  if total_volume0 = 0.0 then
    (float_of_int (len atoms))
  else
    total_volume0
;;

Array.iter
  (fun atom ->
    for i = 0 to 2 do 
      put center i 
	((get center i) +. (get atom.pos i)*.atom.volume)
    done;
  )
  atoms
;;

let center =
  V.vscaled (1.0/.total_volume) center
;;

let kdel i j = if i = j then 1.0 else 0.0;;
let smomc = 0.07697;; (* ((3/(4pi))^(2/3))/5 *)
let tt = 2.0/.3.0;;

(* volume moments *)
Array.iter
  (fun atom ->
    for i = 0 to 2 do 
      let di = (get atom.pos i) -. (get center i) in
	for j = 0 to 2 do
	  let dj = (get atom.pos j) -. (get center j) in
	    moments.(i).(j) <- moments.(i).(j) +. 
	      (di*.dj +. (kdel i j)*.smomc*.(atom.volume**tt))*.atom.volume;
      done;
    done;
  )
  atoms
;;

for i = 0 to 2 do 
  for j = 0 to 2 do
    moments.(i).(j) <- moments.(i).(j) /. total_volume
  done
done
;;

(* principal axes *)
let (aaxis0, aaxis1, aaxis2), (mom0, mom1, mom2) = 
  Solve3.eigenvectors moments
;;

let vofa a = 
  V.v3 a.(0) a.(1) a.(2)
;;

let axis0, axis1, axis2 = vofa aaxis0, vofa aaxis1, vofa aaxis2;;

let scale1, scale2 = sqrt (mom1/.mom0), sqrt (mom2/.mom0);;
    
let transform scale1 scale2 pos =
  let d = V.vdiff pos center in
  let z0 = V.dot axis0 d
  and z1 = V.dot axis1 d
  and z2 = V.dot axis2 d
  in
  let new_pos = 
    V.vsum (V.vsum (V.vsum center 
		       (V.vscaled z0 axis0)) 
	       (V.vscaled (z1*.scale1) axis1))
      (V.vscaled (z2*.scale2) axis2)
  in
    for i = 0 to 2 do
      put pos i (get new_pos i)
    done
;;
    
(* transform atom positions to spherical shape from ellipsoidal *)
let pcopy pos = {
  V.x = pos.V.x;
  V.y = pos.V.y;
  V.z = pos.V.z;
};;

let satoms = Array.map 
  (fun atom -> 
    let new_atom = 
      let new_pos = pcopy atom.pos in
	{
	  atom with
	    pos = pcopy new_pos;
	}
    in
      transform (1.0/.scale1) (1.0/.scale2) new_atom.pos;
      new_atom
  )
  atoms
;;
    

(* find bounding sphere *)
let xyz v = 
  v.V.x, v.V.y, v.V.z
;;

module Point =
struct
  type t = V.t
  type reff = int
  type container = Vec3.t array
  let v3 = V.v3
  let xyz = xyz
  let sum = V.vsum
  let diff = V.vdiff
  let scaled = V.vscaled
  let dot = V.dot
  let cross = V.cross
  let distance = V.distance
  let position arr i = arr.(i)
  let to_array arr = Array.init (Array.length arr) (fun i -> i)
end
;;

let spoints = Array.map (fun atom -> atom.pos) satoms;;

module C = Circumspheres.M( Point);;

let scenter, r = C.smallest_enclosing_sphere spoints;;

let outer_r = Array.fold_left
  (fun outer_r atom ->
    let r = V.distance atom.pos scenter in
    let rr = atom.radius +. r in
      max rr outer_r
  )
  0.0
  satoms
;;

(* transform bounding sphere into bounding ellipsoid *)
transform scale1 scale2 scenter;;

let eval0 = outer_r;;
let eval1 = outer_r*.scale1;;
let eval2 = outer_r*.scale2;;

let max_distance = 
  Array.fold_left
    (fun res atom ->
       let d = (V.distance center atom.pos) +. atom.radius in
	 max res d
    )
    0.0
    atoms
;;


Printf.printf "<roottag>\n";;

Printf.printf "  <bound-center> %g %g %g </bound-center>\n" 
  scenter.V.x scenter.V.y scenter.V.z
;;

Printf.printf "  <hydro-center> %g %g %g </hydro-center>\n"
  center.V.x center.V.y center.V.z
;;

Printf.printf "  <axes>\n";;

let pv v = 
  Printf.printf "    <axis> %g %g %g </axis>\n" v.V.x v.V.y v.V.z
;;

pv axis0;;
pv axis1;;
pv axis2;;

Printf.printf "  </axes>\n";

Printf.printf "  <bound-distances> %g %g %g </bound-distances>\n" 
  eval0 eval1 eval2
;;

Printf.printf "  <hydro-radii> %g %g %g </hydro-radii>\n" 
  (sqrt (5.0*.mom0)) (sqrt (5.0*.mom1)) (sqrt (5.0*.mom2));;

Printf.printf "  <max-distance> %g </max-distance>\n" max_distance;;

Printf.printf "</roottag>\n";;
