module V = Vec3
let len = Array.length

type atom_t = {
  pos: V.t;
  mutable radius: float;
  rnum: int;
  rname: string;
  anum: int;
  aname: string;
  charge: float;
}

let atoms_of_file afile = 
  let lst = ref [] in 
  Atom_parser.apply_to_atoms 
    ~buffer:(Scanf.Scanning.from_file afile)
    ~f:(fun ~rname:rname ~rnumber:rnum ~aname:aname ~anumber:anum 
      ~x:x ~y:y ~z:z ~radius:r ~charge:q ->
	let atom = {
	  pos = V.v3 x y z;
	  radius = r;
	  rnum = rnum;
	  rname = rname;
	  anum = anum;
	  aname = aname;
	  charge = q;
	} 
	in
	lst := atom :: (!lst);
    );
  Array.of_list (List.rev !lst)

let file0, file1, out0, out1 = 
  
  let mol0_file_ref = ref ""
  and mol1_file_ref = ref ""
  and out0_file_ref = ref ""
  and out1_file_ref = ref ""
  in
  
  Arg.parse
    [("-mol0", Arg.Set_string mol0_file_ref, "molecule0 pqrxml file");
     ("-mol1", Arg.Set_string mol1_file_ref, "molecule1 pqrxml file");
     ("-out0", Arg.Set_string out0_file_ref, "output file 0 (default: input molecule0)");
     ("-out1", Arg.Set_string out1_file_ref, "output file 1 (default: input molecule1)");
    ]
    (fun arg -> raise (Failure "no anonymous arguments"))
    "Shrinks atoms so reactive configuration is possible";

  let mol0_file = !mol0_file_ref
  and mol1_file = !mol1_file_ref in
  
  let out0_file = 
    if !out0_file_ref = "" then  
      mol0_file
    else
      !out0_file_ref
  and out1_file = 
    if !out1_file_ref = "" then  
      mol1_file
    else
      !out1_file_ref
  in
  mol0_file, mol1_file, out0_file, out1_file 
    

let atoms0 = atoms_of_file file0
let atoms1 = atoms_of_file file1
;;

for i0 = 0 to (len atoms0)-1 do
  let atom0 = atoms0.(i0) in
  let r0 = atom0.radius in
  for i1 = 0 to (len atoms1)-1 do
    let atom1 = atoms1.(i1) in
    let r1 = atom1.radius in
    let d = V.distance atom0.pos atom1.pos in
    let rr = r0 +. r1 in 
    if d < rr then
      let scale = d/.rr in
      atom0.radius <- atom0.radius*.scale;
      atom1.radius <- atom1.radius*.scale;
  done;
done
  
let output atoms file =
  let f atom = 
    atom.rnum, atom.rname, atom.anum, atom.aname, 
    atom.pos, atom.charge, atom.radius
  in
  Output_pqrxml.sphere_array ~spheres:atoms ~info:f ~channel:(open_out file)
;;

output atoms0 out0;;
output atoms1 out1;;
