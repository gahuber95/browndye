(* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*)

(* Used internally. Computes rate constant of diffusion to start-radius by 
integrating the Smoluchowski equation.
Arguments are the solvent node and molecule node from results XML file.
*)

module JP = Jam_xml_ml_pull_parser;;
module NI = Node_info;;

let f snode mnode = 

  let pi = 3.1415926 in
  let ffn = NI.float_of_child mnode in
    
  let ffn_opt node tag default = 
    let child_opt = JP.child node tag in
      match child_opt with
	| Some child -> 
	    JP.float_from_stream (JP.node_stream child)
	| None -> default
  in

  let hi = 
    let child_opt = JP.child mnode "hydrodynamic-interactions" in
    let res = 
      match child_opt with
	| Some child ->
	  JP.string_from_stream (JP.node_stream child)
	| None -> raise (Failure "file should have hydrodynamic-interactions")
    in
    if res = "true" || res = "True" || res = "TRUE" then
      true
    else if res = "false" || res = "False" || res = "FALSE" then
      false
    else
      raise (Failure "hydrodynamic interactions should be true or false")
	
  in

  let q0 = ffn_opt mnode "charge-molecule0" 0.0 in
  let q1 = ffn_opt mnode "charge-molecule1" 0.0 in
    
  let dL = ffn_opt snode "debye-length" infinity in
  let solvdi = ffn_opt snode "dielectric" 1.0 in
    
  let b = ffn "b-radius" in
  let a0 = ffn "radius-molecule0" in
  let a1 = ffn "radius-molecule1" in
    
  let rel_mu = ffn_opt snode "relative-viscosity" 1.0 in
  let water_mu = ffn_opt snode "water-viscosity" 0.243 in
  let mu = rel_mu*.water_mu in
    
  let kT = ffn_opt snode "kT" 1.0 in
  let vperm = ffn_opt snode "vacuum-permittivity" 0.000142 in
    
  let dpre = kT/.(6.0*.pi*.mu) in
    
  let a2 = 0.5*.(a0*.a0 +. a1*.a1) in 
  let ainv = 1.0/.a0 +. 1.0/.a1 in
    
  let dpara r =
    if hi then
      dpre*.( ainv -. 3.0/.r +. 2.0*.a2/.(r*.r*.r)) 
    else
      dpre*.ainv
  in

  let tol = 1.0e-6 in
  
  let integrand s =
    if s = 0.0 then
      1.0/.(dpre*.ainv)
    else
      let r = 1.0/.s in    
      let v = q0*.q1*.(exp (-.1.0/.(s*.dL)))*.s/.(4.0*.pi*.vperm*.solvdi) in
	(exp (v/.kT))/.(dpara r)
  in
    
  let igral r = 
    Romberg.integral integrand 0.0 (1.0/.r) tol
  in
    
  let kd r = 4.0*.pi/.(igral r) in    
    kd b 
      
