
(* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*)

(* 
Used internally by XML parser.

This is a "stack of strings" which is used to concatenate many
small strings together while avoiding quadratic scaling of effort.
As strings are added to the stack, they are gradually coalesced
into one big string.

Exported functions are described in the interface file.
*)

let is_spaces str = 
  let n = String.length str in
  let res = ref true in
  let i = ref 0 in
    while (!i < n) && !res do
      let c = String.get str !i in
	res := (c = ' ') || (c = '\n') || (c = '\t');
	i := !i + 1;
    done;
    !res
;;


let adjust_str_stack stack = 

  let rec loop () = 
    let str1 = 
      let s1 = Stack.pop stack in 
	if is_spaces s1 then
	  " "
	else
	  s1
	
    in
      if Stack.is_empty stack then
	Stack.push str1 stack
      else
	let str0 = Stack.top stack in
	  if (String.length str1)*2 > (String.length str0) then
	    let str0 = Stack.pop stack in
	      Stack.push (str0 ^ str1) stack;
	      loop();
	  else 
	    Stack.push str1 stack;
  in
    loop()
;;
	  

let add_str_to_stack str stack = 
  Stack.push str stack;
  adjust_str_stack stack
;;
      

let rec str_from_stack stack = 
  if Stack.is_empty stack then
    ""
  else
    let str1 = Stack.pop stack in
    let str0 = str_from_stack stack in
      str0 ^ str1
;;

