(* 
Used internally. Higher level XML parser than ocamlexpat, on which it sits.
   
The parser has a user-defined internal state type denoted by 'a in
the type definition.  The user defines a function to be called 
by the parser when
a tag begins, and a function to be called with a tag ends.
The "begin tag" function takes as arguments the state, the tag name,
and list of attributes.  The "end tag" function takes as arguments
the state, name of tag, and a buffer containing the contents.

*)

type 'a begin_tag_fun =
    'a -> string -> (string * string) list -> unit
type 'a end_tag_fun =
    'a -> string -> Scanf.Scanning.scanbuf -> unit

type 'a parser 

(* helper functions to extract data from the buffer *)
val get_int : Scanf.Scanning.scanbuf -> int
val get_float : Scanf.Scanning.scanbuf -> float
val get_string : Scanf.Scanning.scanbuf -> string

val new_parser : 'a -> ('a begin_tag_fun) -> ('a end_tag_fun) -> 'a parser

(* parse the whole buffer *)
val parse : 'a parser -> Scanf.Scanning.scanbuf -> unit

(* parse some of the buffer and return "true" upon completion *)
val parse_some : 'a parser -> Scanf.Scanning.scanbuf -> bool

val parse_some_of_channel : 'a parser -> in_channel -> bool

(* sometimes you need to parse a stream or buffer where the opening tag is 
missing. This function can be used to insert some text into the beginning
so every closing tag has a match. *)
 
val pre_parse: 'a parser -> string -> unit
