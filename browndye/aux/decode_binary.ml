(* Use internally. Encodes and decodes an array of floating point numbers to and from
a string in MIME |Base64 format.  The two visible functions are 
"string_of_floats"and "floats_of_string".
*)



let digit_start = 48;;
let lowercase_start = 97;;
let uppercase_start = 65;;

let last_chars = [| '+'; '/' |];;
let pad = '=';;

let digit_end = digit_start + 10;;
let lowercase_end = lowercase_start + 26;;
let uppercase_end = uppercase_start + 26;;

let rtable = Array.init 64 
  (fun i -> 
    if i < 26 then
      Char.chr (i + uppercase_start)
    else if i < 52 then
      Char.chr (i - 26 + lowercase_start)
    else if i < 62 then
      Char.chr (i - 52 + digit_start)
    else
      last_chars.( i - 62)
  )
;;

let table = Array.init 128 
  (fun i ->
    if i < digit_start then (
      let c = Char.chr i in
      if c = last_chars.(0) then
	62
      else if c = last_chars.(1) then
	63
      else
	-1
    )
    else if (i < digit_end) then
      i - digit_start + 52 
    else if (uppercase_start <= i) && (i < uppercase_end) then
      i - uppercase_start
    else if (lowercase_start <= i) && (i < lowercase_end) then
      i - lowercase_start + 26
    else
      -1
  )
;;

module I = Int32;;

let bytes_of_string str = 
  let n = String.length str in
  let n4 = n/4 in
  let n34 = 3*n4 in 
  let res = Array.init n34 (fun i -> Char.chr 0) in
    
  let ibyte_block = ref 0 in
  for ichar_block = 0 to n4-1 do
    let ichar = ichar_block*4 in
    let ci c = I.of_int table.(Char.code c) in
    let ch0, ch1, ch2, ch3 = 
      str.[ichar], str.[ichar+1], str.[ichar+2], str.[ichar+3]
    in
    let i0,i1,i2,i3 = ci ch0, ci ch1, ci ch2, ci ch3 in

    let s0a = i0
    and s0b = I.shift_right i1 4
    and s1a = I.logand i1 (I.of_int 0b1111)
    and s1b = I.shift_right i2 2
    and s2a = I.logand i2 (I.of_int 0b11)
    and s2b = i3
    in
    let b0 = I.logor (I.shift_left s0a 2) s0b
    and b1 = I.logor (I.shift_left s1a 4) s1b
    and b2 = I.logor (I.shift_left s2a 6) s2b
    in
    let cc i = Char.chr (I.to_int i) in
      res.(!ibyte_block) <- cc b0;
      ibyte_block := !ibyte_block + 1;       

      if not (ch2 = pad) then (
	res.(!ibyte_block) <- cc b1;
        ibyte_block := !ibyte_block + 1;      

	if not (ch3 = pad) then (
	  res.(!ibyte_block) <- cc b2;	  
          ibyte_block := !ibyte_block + 1;
	)      
      )
      
  done;
    res
;;


(* i is position in array if array is treated as blocks of 4  *)
let float_of_bytes bytes i = 
  let j = i*4 in
  let iof c = I.of_int (Char.code c) in
  let i0 = iof bytes.(j)
  and i1 = iof bytes.(j+1)
  and i2 = iof bytes.(j+2)
  and i3 = iof bytes.(j+3)
  in
  let k0 = I.shift_left i0 0
  and k1 = I.shift_left i1 8
  and k2 = I.shift_left i2 16
  and k3 = I.shift_left i3 24
  in
  let ires = I.add (I.add k0 k1) (I.add k2 k3) in
    I.float_of_bits ires
;;

let floats_of_string str = 
  let bytes = bytes_of_string str in
  let nb = Array.length bytes in
  let nf = nb/4 in
    Array.init nf (fun i -> float_of_bytes bytes i) 
;;


let chars_of_floats floats = 
  let nf = Array.length floats in
  let nb = nf*4 in

  let ch i =
    Char.chr (I.to_int i) 
  in

  let cht i = 
    rtable.( I.to_int i)
  in

  let bytes = Array.init nb (fun i -> ' ') in
    for iblock = 0 to nf-1 do
      let ival = I.bits_of_float floats.(iblock) in
      let j = iblock*4 in
      let mask = I.of_int 0b11111111 in
	bytes.(j+0) <- ch (I.logand mask ival);
	bytes.(j+1) <- ch (I.logand mask (I.shift_right ival 8)); 
	bytes.(j+2) <- ch (I.logand mask (I.shift_right ival 16)); 
	bytes.(j+3) <- ch (I.logand mask (I.shift_right ival 24)); 
    done;

    let ci c = I.of_int (Char.code c) in 
    let nc = 4*((nb+2)/3) in      
    let res = Array.init nc (fun i -> ' ') in
      for iblock = 0 to nb/3 - 1 do

	let b0 = ci bytes.( 3*iblock+0)
	and b1 = ci bytes.( 3*iblock+1)
	and b2 = ci bytes.( 3*iblock+2)
	in
	let i0a = I.shift_right b0 2
	and i0b = I.logand b0 (I.of_int 0b11)
	and i1a = I.shift_right b1 4
	and i1b = I.logand b1 (I.of_int 0b1111)
	and i2a = I.shift_right b2 6
	and i2b = I.logand b2 (I.of_int 0b111111) in
	let c0 = i0a
	and c1 = I.logor (I.shift_left i0b 4) i1a
	and c2 = I.logor (I.shift_left i1b 2) i2a
	and c3 = i2b 
	in
	  res.( 4*iblock+0) <- cht c0;
	  res.( 4*iblock+1) <- cht c1;
	  res.( 4*iblock+2) <- cht c2;
	  res.( 4*iblock+3) <- cht c3;
      done;

      if (nb mod 3) == 1 then
	let b0 = ci bytes.( nb-1) in
	let i0a = I.shift_right b0 2
	and i0b = I.logand b0 (I.of_int 0b11)
	in
	let c0 = i0a
	and c1 = I.shift_left i0b 4
	in
	  res.( nc-4) <- cht c0;
	  res.( nc-3) <- cht c1;
	  res.( nc-2) <- pad;
	  res.( nc-1) <- pad;

      else if (nb mod 3) == 2 then
	let b0 = ci bytes.( nb-2) 
	and b1 = ci bytes.( nb-1)
	in
	let i0a = I.shift_right b0 2
	and i0b = I.logand b0 (I.of_int 0b11)
	and i1a = I.shift_right b1 4
	and i1b = I.logand b1 (I.of_int 0b1111)
	in
	let c0 = i0a
	and c1 = I.logor (I.shift_left i0b 4) i1a
	and c2 = I.shift_left i1b 2
	in
	  res.( nc-4) <- cht c0;
	  res.( nc-3) <- cht c1;
	  res.( nc-2) <- cht c2;
	  res.( nc-1) <- pad;
      else
	();

      res
;;

let string_of_floats fts = 
  let chars = chars_of_floats fts in
  let n = Array.length chars in
  let str = Bytes.create n in
    for i = 0 to n-1 do
      (* str.[i] <- chars.(i) *)
      Bytes.set str i chars.(i);
    done;
    str
;;

(* ******************************************************************* *)
(* Test code *)
(*
let pr = Printf.printf;;
let str = "zcyMP83MDEA=";;
let fts = floats_of_string str;;

Array.iter
  (fun x -> pr "%g " x)
  fts
;;

pr "\n";;
*)

(*
let pr = Printf.printf;;

let fts = [| 1.1; 2.2; 3.3; 4.4; 5.5|];;

let css = string_of_floats fts;;

let fts2 = floats_of_string css;;

Array.iter (fun x -> pr "%g " x) fts2;;
pr "\n";;
*)
