(* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*)

(*
Called by bd_top.

This program generates a pqrxml file of molecule spheres that reside
on the surface.  It includes surface spheres determined by "surface_spheres",
and also includes all spheres that are part of a reaction criterion. It
takes the following arguments, with flags:

-surface : surface file output from surface_spheres
-spheres : XML file of spheres output from pqr2xml
-rxn0 : XML reaction criteria file if molecule is first one of pair
-rxn1 : XML reaction criteria file if molecule is second one of pair
  
*)

(*******************************************************************)
let surface_file_ref = ref "";;
let sphere_file_ref = ref "";;
let rxn1_file_ref = ref "";;
let rxn2_file_ref = ref "";;

let pr = Printf.printf;;

Arg.parse
  [("-surface", (Arg.Set_string surface_file_ref), ": surface file (output from surface_spheres");
   ("-spheres", (Arg.Set_string sphere_file_ref), ": XML file of spheres (output from pqr2xml)"); 
   ("-rxn0", (Arg.Set_string rxn1_file_ref), ": XML reaction criteria file if molecule is first one of pair\n");
   ("-rxn1", (Arg.Set_string rxn2_file_ref), ": XML reaction criteria file if molecule is second one of pair\n");
  ]
  (fun a -> Printf.fprintf stderr "needs args\n";)
  "Generates XML list (equivalent to PQR) of surface and dangler spheres\n"
;;

let surface_file = !surface_file_ref;;
let sphere_file  = !sphere_file_ref;;
let rxn1_file = !rxn1_file_ref;;
let rxn2_file = !rxn2_file_ref;;

module JP = Jam_xml_ml_pull_parser;;

let from_file = Scanf.Scanning.from_file;;

let parser = JP.new_parser (from_file surface_file);;

let snode = 
  match (JP.node_of_tag parser "spheres") with
    | None -> raise (Failure "no spheres defined")
    | Some node -> node
;;

JP.complete_current_node parser;;

module NI = Node_info;;

let sphere_indices tag = 

  let surnode = NI.checked_child snode tag in 
  let n = NI.int_of_child surnode "n" in
    if n > 0 then
      let inode = NI.checked_child surnode "indices" in
      let stm = JP.node_stream inode in
	Array.init n
	  (fun i -> 
	    let res = ref (-1) in
	      Scanf.bscanf stm " %d " (fun i -> res := i);
	      !res
	  )
    else
      [||]
;;

let sindices = sphere_indices "surface";;
let dindices = sphere_indices "danglers";;
let sdindices = Array.append sindices dindices;;

module Int = 
  struct
    type t = int
    let compare x y = compare x y
  end

module Indices = Set.Make( Int);;

let rindices_from_file rn rxn_file =
  let iset0 = 
    Array.fold_left
      (fun res i ->
	Indices.add i res
      )
      Indices.empty
      sdindices
  in

  let parser = JP.new_parser (from_file rxn_file) in
    JP.complete_current_node parser;
    let node = JP.current_node parser in
    let rsnode = NI.checked_child node "reactions" in
    let rnodes = JP.children rsnode "reaction" in
    let iset = 
      List.fold_left
	(fun res rnode ->
	  let cnode = NI.checked_child rnode "criterion" in
	  let pnodes = JP.children cnode "pair" in
	    List.fold_left
	      (fun res pnode -> 
		let anode = NI.checked_child pnode "atoms" in
		let stm = JP.node_stream anode in
		let i1 = JP.int_from_stream stm in
		let i2 = JP.int_from_stream stm in
		let i = 
		  match rn with
		    | 1 -> i1
		    | 2 -> i2
		    | _ -> raise (Failure "rindices_from_file: not get here")
		in
		  Indices.add i res
	      )
	      res
	      pnodes;
	)
	iset0
	rnodes
    in
      Array.of_list (Indices.elements iset)
;;

let indices = 
  if not (rxn1_file = "") then
    rindices_from_file 1 rxn1_file
  else if not (rxn2_file = "") then
    rindices_from_file 2 rxn2_file
  else
    sdindices
;;

Array.sort compare indices;;


type atom = {
  x: float;
  y: float;
  z: float;
  r: float;
  q: float;
  anumber: int;
  aname: string;
  rnumber: int;
  rname: string;
};;


let atoms = 
  let atom_list = ref [] in
    Atom_parser.apply_to_atoms 
      (from_file sphere_file)
      (fun ~rname:rname ~rnumber:rnumber ~aname:aname ~anumber:anumber 
	~x:x ~y:y ~z:z ~radius:r ~charge:q ->
	let new_atom = {
	  x = x;
	  y = y;
	  z = z;
	  r = r;
	  q = q;
	  anumber = anumber;
	  aname = aname;
	  rname = rname;
	  rnumber = rnumber;
	}
	in
	  atom_list := new_atom :: (!atom_list)
      );    

    let atoms = Array.of_list (List.rev !atom_list) in
      Array.sort (fun a0 a1 -> compare a0.anumber a1.anumber) atoms;
      atoms
;;



let satoms = 
  let satom_list = ref [] in
  let na = Array.length atoms in
  let ia = ref 0 in
    Array.iter
      (fun index ->
	while (!ia < na) && (atoms.(!ia).anumber < index) do
	  ia := !ia + 1
	done;
	if !ia < na then
	  let atom = atoms.(!ia) in
	    if atom.anumber = index then
	      satom_list := atom :: (!satom_list);
      )
      indices;
    Array.of_list (List.rev !satom_list)
;;
	      
module Atom =
  struct
    type t = atom
    type container = atom array
    type index = int
    let atom_name atom = atom.aname
    let atom_number atom = atom.anumber
    let residue_name atom = atom.rname
    let residue_number atom = atom.rnumber
    let position atom = Vec3.v3 atom.x atom.y atom.z
    let charge atom = atom.q
    let radius atom = atom.r
    let first_index atom = 0
    let next atoms i = 
      if i < (Array.length atoms) then
	Some atoms.(i), i+1 
      else
	None, 0
  end
;;

module Outputter = Output_pqrxml.M( Atom);;

Outputter.f satoms stdout;;


