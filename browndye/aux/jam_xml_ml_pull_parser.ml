(* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*)


(* Used internally.  This is a hybrid-style parser.  It's like SAX in
that it reads in a bit at a time, but like DOM in that you can build
a tree on the spot.
*)

type node = {
  tag: string;
  nchildren: (string, node) Hashtbl.t;
  mutable child_nodes: node Queue.t;
  mutable nstream: Scanf.Scanning.scanbuf option;
  attributes: (string*string) list;
  mutable completed: bool;
  mutable merged: bool;
  mutable traversed: bool;
  parent: node option;
}
;;

let error msg = raise (Failure msg);;

let some_of opt = 
  match opt with
    | Some obj -> obj
    | None -> error "some_of: nothing here"
;;

let node_attributes node = 
  node.attributes
;;

let node_stream node = 
  some_of node.nstream
;;

let node_tag node = 
  node.tag
;;

let child node tag =
  try
    Some (Hashtbl.find node.nchildren tag)
  with Not_found ->
    None
;;

let gchild node tag0 tag1 = 
  match child node tag0 with
    | Some node -> child node tag1
    | None -> None
;;

let ggchild node tag0 tag1 tag2 = 
  match gchild node tag0 tag1 with
    | Some node -> child node tag2
    | None -> None
;;

let children node tag = 
  List.rev (Hashtbl.find_all node.nchildren tag)
;;

(*
fold : ('a -> 'b -> 'c -> 'c) -> ('a, 'b) t -> 'c -> 'c
*)

let all_children node = 
  Hashtbl.fold 
    (fun key node lst ->
      node :: lst
    )
    node.nchildren []
;;

type input_type = 
    | Buffer of Scanf.Scanning.scanbuf
    | Channel of in_channel
;;

type state = {
  mutable sparser: parser option;
  mutable snode: node option;
}

and parser = {
  input: input_type;
  jparser: state Jam_xml_ml_1f_parser.parser;
  pstate: state;
  mutable parsing_done: bool;
  mutable traversal_done: bool;
  mutable top_node: node option;
  entries: (node Queue.t) Stack.t
}
;;

module JP = Jam_xml_ml_1f_parser;;

let parse_some parser = 
  let input = parser.input in
  let jparser = parser.jparser in
    match input with
      | Buffer buffer -> JP.parse_some jparser buffer
      | Channel channel -> JP.parse_some_of_channel jparser channel
;;

let finished parser =
  parser.traversal_done
;;

let int_from_stream buf =
  let str = Scanf.bscanf buf " %s" (fun s -> s) in
  try
    Scanf.sscanf str "%d" (fun i -> i)
  with ex -> (
    Printf.fprintf stderr "tried to get an integer but got %s\n" str;
    raise ex
  )

  (* Scanf.bscanf buf " %d" (fun i -> i) *)
;;

let int64_from_stream buf = 
  let str = Scanf.bscanf buf " %s" (fun s -> s) in
  try
    Scanf.sscanf str "%Ld" (fun i -> i)
  with ex -> (
    Printf.fprintf stderr "tried to get an 64-bit integer but got %s\n" str;
    raise ex
  )

  (* Scanf.bscanf buf " %Ld" (fun i -> i) *)
 
let float_from_stream buf =
  let str = Scanf.bscanf buf " %s" (fun s -> s) in
  try
    Scanf.sscanf str "%f" (fun x -> x)
  with ex -> (
    Printf.fprintf stderr "tried to get a float but got %s\n" str;
    raise ex
  )

  (* Scanf.bscanf buf " %f" (fun x -> x) *)
;;
 
let float3_from_stream buf = 
  let str = Scanf.bscanf buf " %s %s %s" 
    (fun sx sy sz -> sx^" "^sy^" "^sz) 
  in
  try
    Scanf.sscanf str "%f %f %f" (fun x y z -> x,y,z)
  with ex -> (
    Printf.fprintf stderr "tried to get a three floats but got %s\n" str;
    raise ex
  )
  
  (* Scanf.bscanf buf " %f %f %f" (fun x y z -> x,y,z)  *)
;;

let string_from_stream buf =
  Scanf.bscanf buf " %s" (fun x -> x)
;;

let is_none obj = 
  match obj with
    | None -> true
    | _ -> false
;;


let current_node parser = 
  if Stack.is_empty parser.entries then
    some_of parser.top_node
  else
    Queue.top (Stack.top parser.entries)
;;

let is_current_node_tag parser tag =
  let node = current_node parser in
    node.tag = tag
;;

let begin_tag state tag attrs = 
  let node = {
    tag = tag;
    nchildren = Hashtbl.create 0;
    child_nodes = Queue.create();
    nstream = None;
    attributes = attrs;
    completed = false;
    merged = false;
    traversed = false;
    parent = state.snode;
  }
  in
  let sparser = some_of state.sparser in

    (match sparser.top_node with
      | None -> sparser.top_node <- Some node;
      | _ -> ()
    );
    
    (match state.snode with
       | Some snode -> Queue.add node snode.child_nodes 
       | _ -> ()
    );
    state.snode <- Some node
;;

let end_tag state tag stream = 
  let node = some_of state.snode in
    node.nstream <- Some stream;
    node.completed <- true;
    state.snode <- node.parent
;;

(* called after node is parsed *)
let rec complete_node node =
  while not (Queue.is_empty node.child_nodes) do
    let cnode = Queue.take node.child_nodes in
      complete_node cnode;
      Hashtbl.add node.nchildren cnode.tag cnode;
  done;
  node.merged <- true;
  node.traversed <- true;
;;
  
  
let complete_current_node parser = 
  let node = current_node parser in
  while not node.completed do
    parser.parsing_done <- parse_some parser;
  done;
  complete_node node;
;;

let read_node parser node = 
  while (Queue.is_empty node.child_nodes) && 
    (not node.completed) 
  do
    parser.parsing_done <- parse_some parser;
  done
;;


let next parser = 
  if parser.traversal_done then
    error "next: parser all done"
  else
    let top_node = some_of parser.top_node in
      if Stack.is_empty parser.entries then (
	read_node parser top_node;

	if (Queue.is_empty top_node.child_nodes) then (
	  parser.traversal_done <- true;
	  top_node.completed <- true;
	)
	else
	  Stack.push 
	    top_node.child_nodes 
	    parser.entries
      )
      else ( (* entries not empty *)
	let entry = Stack.top parser.entries in
	let node = Queue.top entry in	  read_node parser node;

	  if 
	    (not (Queue.is_empty node.child_nodes)) && 
	      (not node.merged)
	  then (* go down *) (
	    Stack.push node.child_nodes parser.entries;
	    read_node parser (Queue.top (Stack.top parser.entries));
	  )
	  else ( (* go over *)
	    let parent = some_of (Queue.top entry).parent in
	      ignore (Queue.take entry);

	      if Queue.is_empty parent.child_nodes then(
		read_node parser parent;

		ignore (Stack.pop parser.entries);
		Stack.push parent.child_nodes parser.entries;
		
	      );
	      
	      if Queue.is_empty parent.child_nodes then( (* go back up *)
		ignore (Stack.pop parser.entries);
		parent.traversed <- true;
		
		if Stack.is_empty parser.entries then (
		  top_node.completed <- true;
		  parser.traversal_done <- true;
		)	
	      )
	  )
      )
;;

let go_up parser = 
  ignore (Stack.pop parser.entries)
;;

(* needed if there is other text (comments, e.g.) at beginning *)
let find_top_node parser = 
  while is_none parser.top_node do
    parser.parsing_done <- parse_some parser;
  done
;;


let new_parser_gen input head =
  let state = {
    sparser = None;
    snode = None;
  }
  in
  let parser = {
    input = input;
    jparser = JP.new_parser state begin_tag end_tag;
    pstate = state;
    parsing_done = false;
    traversal_done = false;
    top_node = None;
    entries = Stack.create();
  }
  in
    state.sparser <- Some parser;

    if not (head = "") then
      JP.pre_parse parser.jparser head;

    parser.parsing_done <- parse_some parser;
    find_top_node parser; 
    parser
;;


let new_parser input = new_parser_gen (Buffer input) "";;
let new_parser_of_channel input head = new_parser_gen (Channel input) head;;

let node_of_tag parser tag =
 
  while not ((is_current_node_tag parser tag) || (finished parser)) do(
    next parser;
  )
  done;
  if finished parser then
    None
  else
    Some (current_node parser)
;;

let node_of_tags parser tags =
  let ntags = Array.length tags in
 
  let rec inner_loop itag =
    if itag = ntags then
      None
    else
      let tag = tags.(itag) in
	if is_current_node_tag parser tag then
	  Some (current_node parser, tag)
	else
	  inner_loop (itag+1)
  in
  let rec outer_loop () = 
    if finished parser then
      None
    else
      match inner_loop 0 with
	| Some res -> Some res
	| None ->
	    next parser;
	    outer_loop()
  in
    outer_loop()
;;



let node_of_tag_with_parent parser tag ptag = 
  let has_ptag cnode = 
    (some_of cnode.parent).tag = ptag
  in

  while 
    let cnode = current_node parser in
      (cnode.tag != tag) && (not (finished parser)) && (has_ptag cnode)
  do
    next parser;
  done;

  let cnode = current_node parser in
    if (has_ptag cnode) && (not (finished parser)) then
      Some cnode
    else
      None
;;


let skip_current_node parser = 
  let nlevel = Stack.length parser.entries in
  let node = current_node parser in
    while 
      ((Stack.length parser.entries) > nlevel) ||
      ((current_node parser) == node)
    do
      next parser
    done
;;


let skip_to_parent_end parser = 
  let parent = current_node parser in
    next parser;
    while not ((current_node parser) == parent) do
      next parser;
    done
;;

let level parser = 
  Stack.length parser.entries
;;


let move_up parser = 
  let children = Stack.pop parser.entries in
    ignore (Queue.take children)
;;
    

(* current node is parent of nodes to be processed *)
let apply_to_nodes_of_tag parser tag f  =

  let plevel = level parser in
    next parser;

    let finis = ref false in
      while not !finis do
	
	(* find next tag *)
	while
	  (not ((current_node parser).tag = tag)) && 
	    ((level parser) > plevel)	    
	do
	  next parser;
	done;
	
	finis := (level parser) <= plevel;
	if (not !finis) then (
	  f (current_node parser);
	  next parser;
	)	 	  
      done
;;


let fold_nodes_of_tag parser tag f x0 =
  let parent = current_node parser in

  let get_next_node () = 
    while
      let cnode = current_node parser in
	not ((cnode == parent) || (cnode.tag = tag))
    do
      next parser
    done;
  in
    next parser;
    get_next_node();
    let res = ref (f x0 (current_node parser)) in
      next parser;
      let finis = ref false in
      while not !finis do
        get_next_node();
        if not ((current_node parser) == parent) then (
	  res := f !res (current_node parser);
	  next parser;
        )
        else
          finis := true;        
      done;

      !res
;;

