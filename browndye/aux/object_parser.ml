(* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*)

(* 
used interally for parsing XML files.  

apply_to_objects otag buffer int_tags float_tags string_tags f:

For every node in an XML tree with tag "otag", applies the function
"f" to the following arguments:

f int_info float_info string_info

"int_info" contains a list of string names paired with integers;
the string names are the tags of the child nodes of the "otag" node,
and the integers are the integer contents.

"float_info" and "string_info" work the same way for floats and strings.

"f" is called for every "otag" node.  

The tags that are in the arguments to "f" are listed in the arguments
"int_tags", "float_tags", and "string_tags" to "apply_to_objects".

*)



module JP = Jam_xml_ml_pull_parser;;

let object_data int_tags float_tags string_tags node =
  let cnodes tags = 
    List.filter
      (fun node ->
	let tag = JP.node_tag node in
	  List.mem tag tags
      )
      (JP.all_children node) 
  in
  let int_res = 
    List.map
      (fun cnode ->
	let stm = JP.node_stream cnode in
	let res = Scanf.bscanf stm " %d " (fun x -> x)
	and tag = JP.node_tag cnode in
	  tag, res
      )
      (cnodes int_tags)

  and float_res = 
    List.map
      (fun cnode ->
	let stm = JP.node_stream cnode in
	let res = Scanf.bscanf stm " %f " (fun x -> x)
	and tag = JP.node_tag cnode in
	  tag, res
      )
      (cnodes float_tags)

  and string_res = 
    List.map
      (fun cnode ->
	let stm = JP.node_stream cnode in
	let res = Scanf.bscanf stm " %s " (fun x -> x)
	and tag = JP.node_tag cnode in
	  tag, res
      )
      (cnodes string_tags)
  in
    int_res, float_res, string_res


let apply_to_objects obj_tag buffer ints floats strings f = 
  let parser = JP.new_parser buffer in

  let apply_f () = 
    JP.complete_current_node parser;
    let int_res,float_res,string_res = 
      object_data ints floats strings (JP.current_node parser)
    in
      f int_res float_res string_res
  in
    while not (JP.finished parser) do
      JP.next parser;
      if JP.is_current_node_tag parser obj_tag then (
	apply_f();

      )
      else if JP.is_current_node_tag parser "residue" then (
	JP.apply_to_nodes_of_tag parser obj_tag
	    (fun node -> apply_f())
      )
    done
      
       

