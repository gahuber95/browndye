(* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*)
(* 
Convenience function.  Generates a test charge for each sphere.
Uses standard input and output.
*)


Arg.parse
  []
   (fun arg -> raise (Failure "no anonymous arguments"))
  "Generates test charge for each sphere."
;;


let pr = Printf.printf;;

pr "<root>\n";;

let total_q = ref 0.0;;

 Atom_parser.apply_to_atoms Scanf.Scanning.stdin
   (fun ~rname:rname ~rnumber:rnumber ~aname:aname ~anumber:anum 
     ~x:x ~y:y ~z:z ~radius:r ~charge:q ->

     if ((abs_float q) > 0.0) then(
       pr "  <point>\n";
       pr "    <residue> %s </residue>\n" rname;
       pr "    <residue_number> %d </residue_number>\n" rnumber;
       pr "    <atom-type> charge-center </atom-type>\n";
       pr "    <x> %g </x> <y> %g </y> <z> %g </z>\n" x y z;
       pr "    <charge> %g </charge>\n" q;
       pr "  </point>\n";
       total_q := !total_q +. q;
     )
   )
;;

pr "  <total-charge> %g </total-charge>\n" !total_q;;

pr "</root>\n";;

