(*
Used internally.  The Orchestrator is a "gmake"-like utility that
can call programs and update files.  It can also extract information
from XML files, process that information, and pass it on as arguments
to programs downstream.  It is used by "bd_top".
The beginnings of documentation is found in
doc/orchestrator.html
*)

type data =
    Int of int
  | Float of float
  | String of string
  | Float3 of float * float * float
  | Bool of bool
  | Null

type source

val file_name : source -> string
val new_command_source : string -> string -> source
val new_in_file_source : string -> source
val new_func2_source :
  (data -> data -> data) -> source -> source -> source
val new_func1_source : (data -> data) -> source -> source

val new_in_string_var_source : source -> string list -> source
val new_in_int_var_source : source -> string list -> source
val new_in_float_var_source : source -> string list -> source
val new_in_float3_var_source : source -> string list -> source

val new_in_2file_float_var_source :
  string -> string -> string list -> source
val new_in_2file_int_var_source :
  string -> string -> string list -> source
val new_in_2file_string_var_source :
  string -> string -> string list -> source

val new_alt_source:
  source->source -> source

val new_cond_source: (* condition is first arg *)
  source->source->source -> source

val new_file_source_of_string: source -> source

val const_float_source: float -> source
val const_string_source: string -> source

val add_prereq : source -> string -> source -> unit
val add_opt_prereq : source -> string -> source -> unit
val add_neg_opt_prereq : source -> string -> source -> unit
val add_stdin_prereq : source -> source -> unit

val source_value : source -> data

val update : source -> unit

val has_value: source -> bool
