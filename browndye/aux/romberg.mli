(*
Used internally.

Uses the Romberg method to integrate "f" from "low" to "high", with 
desired relative accuracy of "accuracy"

*)

val integral :
  f:(float -> float) -> low:float -> high:float -> accuracy:float -> float
