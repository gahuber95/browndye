(* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*)


(* Generic code for finding the smallest bounding sphere of a group
of points.  This implements the algorithm of E. Welzl
("Smallest Enclosing Disks (Balls And Ellipsoids)", 
Lect. Notes Comput. Sci. 555 (1991), 359-370.) 
*)

(* description in header file *)
module type Vector3 = sig
  type t
  type reff
  type container
  val v3: float->float->float -> t
  val xyz: t -> float*float*float
  val sum: t->t -> t
  val diff: t->t -> t
  val scaled: float->t -> t
  val dot: t->t -> float
  val cross: t->t -> t
  val distance: t->t -> float
  val position: container->reff -> t
  val to_array: container -> reff array
end


module M( V: Vector3) = struct 

let norm x = sqrt (V.dot x x)

(* circle defined by points p0,p1,p2 *)
let circumcircle p0 p1 p2 =
  let d1 = V.diff p1 p0
  and d2 = V.diff p2 p0 in
  let prp = V.cross d1 d2 in
  let nrm = norm prp in
    if nrm = 0.0 then (* collinear *)
      (V.v3 infinity infinity infinity), infinity
    else
      
      let perp = V.scaled nrm prp in
  
      let h1 = V.scaled 0.5 d1
      and h2 = V.scaled 0.5 d2
      in    
      let row d = 
	let x,y,z = V.xyz d in
	  [| x; y; z; |]
      in
      let mat = [| row d1; row d2; row perp |]
      and b = [| V.dot h1 d1; V.dot h2 d2; 0.0 |]
      in
      let relpos = [| nan; nan; nan; |] in
	Solve3.solve3 mat b relpos;
	
	let pc = 
	  let x,y,z = V.xyz p0 in
	  V.v3
	    (x +. relpos.(0))
	    (y +. relpos.(1))
	    (z +. relpos.(2))
	in
	  pc, (norm (V.diff pc p0))


(* sphere defined by points p0, p1, p2, p3 *)
let circumsphere p0 p1 p2 p3 = 
  let d1 = V.diff p1 p0
  and d2 = V.diff p2 p0
  and d3 = V.diff p3 p0
  in
  let h1 = (V.scaled 0.5 d1)
  and h2 = (V.scaled 0.5 d2)
  and h3 = (V.scaled 0.5 d3)
  in
    if V.dot h1 (V.cross h2 h3) = 0.0 then (* coplanar *)
      (V.v3 infinity infinity infinity), infinity
    else

      let row d = 
	let x,y,z = V.xyz d in
	  [| x; y; z; |]
      in
	
      let mat = [| (row d1); (row d2); (row d3); |] in
	
      let b = [| (V.dot h1 d1); (V.dot h2 d2); (V.dot h3 d3); |]    
      in
      let relpos = [| nan; nan; nan; |] in
	Solve3.solve3 mat b relpos;
	let pc = 
	  let x,y,z = V.xyz p0 in
	    V.v3
	      (x +. relpos.(0)) 
	      (y +. relpos.(1)) 
	      (z +. relpos.(2))
	in
	  pc, (norm (V.diff pc p0))


let avg p0 p1 = V.scaled 0.5 (V.sum p0 p1)

let eq p0 p1 = 
  let x0,y0,z0 = V.xyz p0
  and x1,y1,z1 = V.xyz p1
  in
    (x0 = x1) && (y0 = y1) && (z0 = z1)

(* smallest sphere that contains line segment defined by p0 and p1 *)
let smallest_line_sphere p0 p1 = 
  let c = avg p0 p1 in
    c, max (V.distance p0 c) (V.distance p1 c)

(* smallest sphere that contains triangle defined by p0,p1, and p2 *)
let smallest_triangle_sphere p0 p1 p2 =
  if eq p0 p1 then
    smallest_line_sphere p0 p1
  else if eq p0 p2 then
    smallest_line_sphere p0 p2
  else if eq p1 p2 then
    smallest_line_sphere p1 p2
  else      
    let d01, d12, d20 = (V.diff p1 p0), (V.diff p2 p1), (V.diff p0 p2) in
    let r01, r12, r20 = norm d01, norm d12, norm d20 in
      
    let maxr = max (max r01 r12) r20 in
      
    let trial_center, rem_pt = 
      if r01 = maxr then
	(avg p0 p1), p2
      else if r12 = maxr then
	(avg p1 p2), p0
      else
	(avg p2 p0), p1
    in
    let trial_radius = 0.5*.maxr in
      if (V.distance rem_pt trial_center) <= trial_radius then
	trial_center, trial_radius
      else
	let pc, r = circumcircle p0 p1 p2 in
	  (* adjust in case round-off error keeps points outside *)
	let max_r = 
	  let d0 = V.distance pc p0
	  and d1 = V.distance pc p1
	  and d2 = V.distance pc p2
	  in
	    max (max d0 d1) d2 
	in
	  pc, max_r
	    
	    
(* smallest sphere containing tetrahedron defined by p0,p1,p2, and p3 *)
let smallest_tetrahedron_sphere p0 p1 p2 p3 =
  if (eq p0 p1) || (eq p0 p2) || (eq p0 p3) then
    smallest_triangle_sphere p1 p2 p3
      
  else if (eq p1 p2) || (eq p1 p3) then
    smallest_triangle_sphere p0 p2 p3
      
  else if (eq p2 p3) then
    smallest_triangle_sphere p0 p1 p2
      
  else
    let ps = [| p0; p1; p2; p3 |] 
    and pair_ij = ref (1,0) 
    and max_pd = ref (V.distance p0 p1) in
      
      for i = 0 to 3 do
	let pi = ps.(i) in
	  for j = 0 to i-1 do
	    let pj = ps.(j) in
	    let d = V.distance pi pj in
	      if d > !max_pd then (
		max_pd := d;
		pair_ij := i,j
	      )
	  done;
      done;
      
      let i,j = !pair_ij in
      let trial_center = avg ps.(i) ps.(j) in
      let trial_radius = 0.5*.(!max_pd) in
	
      let rem_i, rem_j = 
	match i,j with
	  | 1,0 -> 2,3
	  | 2,0 -> 1,3
	  | 3,0 -> 1,2
	  | 2,1 -> 0,3
	  | 3,1 -> 0,2
	  | 3,2 -> 0,1
	  | _,_ -> raise (Failure "error")
      in
	
      let center_res, radius_res = 
	if 
	  ((V.distance ps.(rem_i) trial_center) <= trial_radius) &&
	    ((V.distance ps.(rem_j) trial_center) <= trial_radius)
	then
	  trial_center, trial_radius
	    
	else
	  (* try triangles now *)
	  let c0,r0 = smallest_triangle_sphere p1 p2 p3
	  and c1,r1 = smallest_triangle_sphere p0 p2 p3
	  and c2,r2 = smallest_triangle_sphere p0 p1 p3
	  and c3,r3 = smallest_triangle_sphere p0 p1 p2
	  in
	  let maxr = max (max (max r0 r1) r2) r3 in
	    
	    
	  let trial_center, rem_pt = 
	    if maxr = r0 then
	      c0,p0
	    else if maxr = r1 then
	      c1,p1
	    else if maxr = r2 then
	      c2,p2
	    else 
	      c3,p3
	  in
	    
	    if (V.distance rem_pt trial_center) <= maxr then
	      trial_center, maxr
	    else
	      let pc, r = circumsphere p0 p1 p2 p3 in
		(* adjust in case round-off error keeps points outside *)
	      let max_r = 
		let d0 = V.distance pc p0
		and d1 = V.distance pc p1
		and d2 = V.distance pc p2
		and d3 = V.distance pc p3
		in
		  max (max (max d0 d1) d2) d3 
	      in
		pc, max_r
      in
	center_res, radius_res
	    
	    
(* returns true if p is inside tetrahedron defined by p0 thru p3 *)
let inside_tetrahedron p0 p1 p2 p3 p = 
  let d01 = V.diff p1 p0
  and d02 = V.diff p2 p0
  and d03 = V.diff p3 p0
  and d12 = V.diff p2 p1
  and d13 = V.diff p3 p1
  in
  let perp3 = V.cross d01 d02
  and perp2 = V.cross d01 d03
  and perp1 = V.cross d02 d03
  and perp0 = V.cross d12 d13 
  in
  let stest origin perp vert = 
    (V.dot perp (V.diff p origin))*.(V.dot perp (V.diff vert origin)) > 0.0
  in

    (stest p1 perp0 p0) && (stest p0 perp1 p1) && 
      (stest p0 perp2 p2) && (stest p0 perp3 p3)  


(* not used, brute-force method for finding enclosing sphere; tests
each sphere defined by every possible tetrahedron *)

(*
let brute_force ctr =
  let pts = V.to_array ctr in
  let n = Array.length pts in
  let min_tet = ref None in
  let min_rad = ref infinity
  in
  let ipos i = 
    V.position ctr pts.(i)
  in

    for k0 = 0 to n-1 do
      for k1 = 0 to k0-1 do
	for k2 = 0 to k1-1 do
	  for k3 = 0 to k2-1 do
	    let c,r = smallest_tetrahedron_sphere 
	      (ipos k0) (ipos k1) (ipos k2) (ipos k3) 
	    in
	      
	    let ii = ref 0 in 
	    let finis = ref false in
	      while (not !finis) && !ii < n do
		let i = !ii in
		  if i != k0 && i != k1 && i != k2 && i != k3 then (
		    let p = pts.(i) in
		      if (V.distance (V.position ctr p) c) > r then
			finis := true  
		  );
		  ii := !ii + 1;
	      done;
	      
	      if not !finis then
		if r < !min_rad then (
		  min_rad := r;
		  min_tet := Some (k0,k1,k2,k3);
		)
	  done;
	done;
      done;
    done;
    
    let i0,i1,i2,i3 = 
      match !min_tet with
	| Some tet -> tet
	| None -> raise (Failure "brute: error")
	    
    in
      smallest_tetrahedron_sphere 
	(V.position ctr pts.(i0)) 
	(V.position ctr pts.(i1)) 
	(V.position ctr pts.(i2))
	(V.position ctr pts.(i3)) 

*)

(* returns center and radius of smallest containing sphere of points
defined by container ctr *)
let smallest_enclosing_sphere ctr =
  let pts = V.to_array ctr in
  let n = Array.length pts in
  let ipos i = 
    V.position ctr pts.(i)
  in
    match n with
      | 1 -> 
	  V.position ctr pts.(0), 0.0
      | 2 -> 
	  let v0 = ipos 0
	  and v1 = ipos 1
	  in
	    (avg v0 v1), 0.5*.(V.distance v0 v1)
      | 3 -> 
	  smallest_triangle_sphere (ipos 0) (ipos 1) (ipos 2)  
      | 4 ->  
	  smallest_tetrahedron_sphere (ipos 0) (ipos 1) (ipos 2) (ipos 3)

      | _ ->
	  let i0 = ref (n-4) in

	  let rmod i =
	    (i + n) mod n
	  in

	  let rec loop () =
	    let pti i = pts.(rmod (!i0+i)) in	      
	    let p0 = pti 0
	    and p1 = pti 1
	    and p2 = pti 2
	    and p3 = pti 3 in
	    let pc, r = 
	      smallest_tetrahedron_sphere 
		(V.position ctr p0) (V.position ctr p1) 
		(V.position ctr p2) (V.position ctr p3) 
	    and i = ref (rmod (!i0 + 4))  
	    and break = ref false 
	    and finis = ref false in 	      	      
	      while not !break do
		let p = pts.(!i) in
		let outsider = ref false in
		let ip = rmod (!i + 1) in
		let at_last = (ip = !i0) in		  
		  if (V.distance (V.position ctr p) pc) > r then (
		    outsider := true;
		    i0 := rmod (!i0 - 1);
		    let temp = pts.(!i0) in
		      pts.(!i0) <- p;
		      pts.(!i) <- temp;
		      break := true;		      
		  )
		  else ();
		  
		  if at_last  then (
		    break := true;
		    finis := not !outsider;
		  )
		  else
		    i := ip;		      
	      done;
	      
	      if not !finis then
		loop()
	      else
		pc, r
	  in
	    loop () 
	      
end

(************************************************************)
(* Testing code *)
(*
module My_Vector3 = struct 
  type t = {
    mutable x: float;
    mutable y: float;
    mutable z: float;
  }
      
  let v3 x y z = {
    x = x;
    y = y;
    z = z;
  }

  let xyz v = 
    v.x, v.y, v.z

  let scaled c a = 
    {
      x = c*.a.x;
      y = c*.a.y;
      z = c*.a.z;
    }
  
  let diff a b =  
    {
      x = a.x -. b.x;
      y = a.y -. b.y;
      z = a.z -. b.z;
    }
  
  let sum a b =  
    {
      x = a.x +. b.x;
      y = a.y +. b.y;
      z = a.z +. b.z;
    }

     

  let dot a b = 
    a.x*.b.x +. a.y*.b.y +. a.z*.b.z
  

  let distance a b = 
    let d = diff a b in
    sqrt (dot d d)
    
  let cross a b = {
    x = (a.y*.b.z -. a.z*.b.y);
    y = (a.z*.b.x -. a.x*.b.z);
    z = (a.x*.b.y -. a.y*.b.x);
  }
  
end


module MM = M( My_Vector3)

*)
(************************************************************)
(* Testing code *)
(*
let v3 = My_Vector3.v3;;

let p0 = v3 1.500000 2.500200 2.000400;;

let p1 = v3 1.000000 3.000300 2.000400;;
let p2 = v3 2.000000 2.500200 1.500200;;
let p3 = v3 1.500000 3.000300 1.500200;;
let p4 = v3 2.000000 3.000300 1.000200;;


let c,r = MM.smallest_tetrahedron_sphere p1 p2 p3 p4;;
let x,y,z = My_Vector3.xyz c;;
Printf.printf "center %f %f %f radius %f\n" x y z r;;

*)

(*

(*

let p0 = v3 0.0 0.0 0.0;;
let p1 = v3 1.0 0.0 0.0;;
let p2 = v3 0.0 1.0 0.0;;
let p3 = v3 0.0 0.0 1.0;;

(*
let pc, r = smallest_tetrahedron_sphere p0 p1 p2 p3;;
Printf.printf "%f %f %f, %f\n" pc.x pc.y pc.z r;;
*)

for i = 0 to 10 do
  let z = (float i)*.0.1 in
  let p = v3 0.1 0.1 z in
    Printf.printf "%f %b\n" z (inside_tetrahedron p0 p1 p2 p3 p);
done
;;

*)

Random.init 1111114;;

let pts = 
  Array.init 10000
    (fun i -> 
      v3 (Random.float 3.0) (Random.float 2.0) (Random.float 1.0) 
    )
;;

(*
Array.iter
  (fun p ->
    Printf.printf "%f %f %f\n" p.x p.y p.z
  )
pts
;;
*)

(
  let center, r = smallest_enclosing_sphere pts in
    Printf.printf "final center %f %f %f radius %f\n" 
      center.x center.y center.z r;
    flush stdout;
    Printf.printf "nops %d\n" !nops;
);


(*
(
  let center, r = brute_force pts in
    Printf.printf "final center %f %f %f radius %f brute\n" 
      center.x center.y center.z r;
);
*)




(*
Array.iter
  (fun pt ->
    Printf.printf "%f\n" (distance pt center)
  )
pts
;;
*)
*)
