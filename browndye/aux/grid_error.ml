

let interpolated vs alphas = 
  let v000, v001, v010, v011, v100, v101, v110, v111 = vs 
  and ax,ay,az = alphas in
  let axp,ayp,azp = 1.0 -. ax, 1.0 -. ay, 1.0 -. az in
  let v00 = azp*.v000 +. az*.v001
  and v01 = azp*.v010 +. az*.v011
  and v10 = azp*.v100 +. az*.v101
  and v11 = azp*.v110 +. az*.v111
  in
  let v0 = ayp*.v00 +. ay*.v01
  and v1 = ayp*.v10 +. ay*.v11
  in
    axp*.v0 +. ax*.v1
;;

let min_distance ~error:allowed_error ~debye:debye ~width:w = 

  (*
let allowed_error = 0.05;;
let debye = 1.0e20;; // 7.5;; 
   *)
  
  let g r = (exp (-.r/.debye))/.r in

  let length x y z = sqrt (x*.x +. y*.y +. z*.z) in

  let f p =
    let x,y,z = p in
    let r = length x y z in
    g r
  in
  
  let nbox = 11 in
  
  let box_error w c =
    let max_gen = 1 in
    
    let max_err = ref 0.0 in
    let max_err_pos = ref (Vec3.v3 nan nan nan) in
    
    let cx,cy,cz = c.Vec3.x, c.Vec3.y, c.Vec3.z in
    let fnbox = float (nbox - 1) in    
    let hw = 0.5*.w in
    
    let x0, x1 = cx -. hw, cx +. hw
    and y0, y1 = cy -. hw, cy +. hw
    and z0, z1 = cz -. hw, cz +. hw
    in
    
    let p000 = x0,y0,z0 
    and p001 = x0,y0,z1
    and p010 = x0,y1,z0
    and p011 = x0,y1,z1
    and p100 = x1,y0,z0
    and p101 = x1,y0,z1
    and p110 = x1,y1,z0
    and p111 = x1,y1,z1
    in
    let v000 = f p000
    and v001 = f p001
    and v010 = f p010
    and v011 = f p011
    and v100 = f p100
    and v101 = f p101
    and v110 = f p110
    and v111 = f p111
    in
    let vs = v000,v001,v010,v011,v100,v101,v110,v111 in
    
    let rec loop gen w c = 
      
      let cx,cy,cz = c.Vec3.x, c.Vec3.y, c.Vec3.z in
      let h = w/.fnbox in
      let hw = 0.5*.w in      
      
      let ainbound a = -0.1*.h <= a && a <= 1.0 +. 0.1*.h in
      
      for ix = 0 to nbox-1 do
	let x = cx +. (float ix)*.h -. hw in
	let ax = (x -. x0)/.(x1 -. x0) in
	if ainbound ax then
	  for iy = 0 to nbox-1 do
	    let y = cy +. (float iy)*.h -. hw in
	    let ay = (y -. y0)/.(y1 -. y0) in
	    if ainbound ay then
	      for iz = 0 to nbox-1 do
		let z = cz +. (float iz)*.h -. hw in
		let az = (z -. z0)/.(z1 -. z0) in
		if ainbound az then
		  let alphas = ax,ay,az in
		  let real_f = f (x, y, z) 
		  and interp_f = interpolated vs alphas in
		  (* Printf.printf "%g %g %g %g %g\n" x y z real_f interp_f;
		   *)
		  let err = abs_float ((interp_f -. real_f)/.real_f) in
		  
		  if err > !max_err then (
		    max_err := err;
		    max_err_pos := Vec3.v3 x y z;
		  );
	      done;
	  done;
      done;
      if gen < max_gen then
	loop (gen+1) (2.0*.h) !max_err_pos
    in
    loop 0 w c;
    !max_err
     
  in
  

  let error r w = 
    let acenters = Sphere.icoso_face_centers 2 (Vec3.v3 0.0 0.0 0.0) r in
    Array.fold_left
      (fun res apt ->
       let pt,area = apt in
       let err = box_error w pt in
       max res err
      )
      0.0
      acenters
  in
  
  
  let min_distance_inner w =
    let tol = 1.0e-6 in
    let f = (fun r -> (error r w) -. allowed_error) in
    let rlo = 0.01 in
    let rhi = Bisected.bracket f rlo in                
    Bisected.solution f rlo rhi tol
  in

  min_distance_inner w
;;



(*

Printf.printf "# debye length = %g\n" debye;;
let range = min 50.0 (4.0*.debye);;
for i = 1 to 20 do
  let r = (float i)*.range/.20.0 in   
    Printf.printf "%g %g\n" r (allowed_width r); flush stdout;
done
;;

 *)
