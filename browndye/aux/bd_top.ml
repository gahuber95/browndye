(* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*)

(* top-level program that calls the other auxilliary programs and
   keeps the intermediate files up-to-date with respect to each other.
   Sort of like "gmake".
*)

let len = Array.length;;

module JP = Jam_xml_ml_pull_parser;;
module NI = Node_info;;


(* hack needed because Scanf does not read in "inf" *)
let big_float = 
  let str = Printf.sprintf "%g" max_float in
  Scanf.sscanf str "%g" (fun x -> x)
;;

let top_node buffer = 
  let parser = JP.new_parser buffer in
  JP.complete_current_node parser;
  JP.current_node parser
;;

let path_ref = ref "";;

let input_fname_ref = ref "";;
Arg.parse [ ("-path", Arg.Set_string path_ref, "full path for functions (optional)")] 
  (fun fname -> input_fname_ref := fname;)
  "\"bd_top input-file\" runs the auxilliary programs prior to the simulation"
;;

let path = !path_ref
;;

let fp cname = 
  if path = "" then
    cname
  else
    Filename.concat path cname
;;

let new_command_source command output = 
  Orchestrator.new_command_source (fp command) output
;;

let input_fname = !input_fname_ref;;
let input_buffer = Scanf.Scanning.from_file input_fname;;

let input_node = top_node input_buffer;;

let mol0_node = NI.checked_child input_node "molecule0";;
let mol1_node = NI.checked_child input_node "molecule1";;

let prefix0 = NI.string_of_child mol0_node "prefix";;
let prefix1 = NI.string_of_child mol1_node "prefix";;

let prefices = [| prefix0; prefix1 |];;

let mol_nodes = [| mol0_node; mol1_node |];;

let prefix01 = prefix0 ^ "-" ^ prefix1 ^ "-";;

let fname i mid = 
  prefices.(i) ^ "-" ^ mid ^ ".xml"
;;

let fname01 mid = 
  prefix01 ^ mid ^ ".xml"
;;

let old_data_fname = fname01 "old-data";;

module H = Hashtbl;;

(* Update solvent file if needed *) 
let solvent_node = NI.checked_child input_node "solvent";;

let tf_node_value node tag = 
  try
    let res = NI.string_of_child node tag in
    if res = "true" || res = "True" || res = "TRUE" then
      true
    else
      false
  with NI.Not_found tag ->
    false
  
let is_protein = tf_node_value input_node "protein"

(* ************************************************* *)
(* information from the solvent file (one of the intermediate files) *)
let solvent_info_from_node node =

  let info = H.create 6 in

  let solvatt name default = 
    H.add info name default
  in
  solvatt "vacuum-permittivity" 0.000142;
  solvatt "dielectric" 78.0;
  solvatt "water-viscosity" 0.243;
  solvatt "relative-viscosity" 1.0;
  solvatt "debye-length" big_float;
  solvatt "kT" 1.0;

  let names = 
    H.fold (fun name value lst -> name :: lst) info []
  in
  List.iter
    (fun name -> 
      try 
	let value = NI.float_of_child node name in
	H.replace info name value
      with NI.Not_found tag ->
	()
    )
    names;
  info
;;

(* ************************************************* *)
let solvent_file = fname01 "solvent";;

let solvent_info = solvent_info_from_node solvent_node;;

let solvent_names = 
  H.fold (fun name value lst -> name :: lst) solvent_info []
;;

let pr = Printf.fprintf;;

let output_solvent_info() =
  let out = open_out solvent_file in
  pr out "<solvent>\n";
  H.iter 
    (fun name value ->
      pr out "  <%s> %g </%s>\n" name value name
    )
    solvent_info;
  pr out "</solvent>\n";
  close_out out
;;

(* update solvent file if there are changes in the main input file *)
if Sys.file_exists solvent_file then (
  let saved_solvent_node = top_node (Scanf.Scanning.from_file solvent_file) in
  let saved_solvent_info = solvent_info_from_node saved_solvent_node in
  let all_same = 
    List.fold_left
      (fun res name ->
	let val0 = H.find solvent_info name
	and val1 = H.find saved_solvent_info name in  
	let same = (val0 = val1) in
	same && res
      )
      true
      solvent_names
  in
  if not all_same then
    output_solvent_info()
)
else
  output_solvent_info()
;;

(*************************************************************************)
module OC = Orchestrator;;

let ifn = OC.new_in_file_source;;
let ifnsolv = ifn solvent_file;;

let h2o_visc_link = OC.new_in_float_var_source ifnsolv 
  ["water-viscosity"];;

let rel_visc_link = OC.new_in_float_var_source ifnsolv 
  ["relative-viscosity"];;

let kT_link = OC.new_in_float_var_source ifnsolv ["kT"];;

let vac_perm_link = OC.new_in_float_var_source ifnsolv 
  ["vacuum-permittivity"];;

let diel_link = OC.new_in_float_var_source ifnsolv ["dielectric"];;

let debye_link = OC.new_in_float_var_source ifnsolv 
  ["debye-length"];;

let desolv_link = OC.new_in_string_var_source (ifn input_fname)
  ["include-desolvation-forces"];;

let hi_link = OC.new_in_string_var_source (ifn input_fname) 
  ["hydrodynamic-interactions"];;

let hemi_link = OC.new_in_string_var_source (ifn input_fname)
                                            ["hemisphere"];;
  
let use_desolv =  
  if OC.has_value desolv_link then (
    let value = match OC.source_value desolv_link with
      | OC.String v -> v
      | _ -> raise (Failure "must be string value")
    in
    not (value = "false" || value = "False" || value = "FALSE")
  )
  else
    true
;;

let rxns_link = 
  let name = NI.string_of_child input_node "reactions" in
  OC.new_in_file_source name
;;

let field_names = 
  let got_field_names i =
    let mol_node = mol_nodes.(i) in
    
    let field_nodes =
      let apbs_node_opt = JP.child mol_node "apbs-grids" in
      match apbs_node_opt with
	| Some apbs_node -> JP.children apbs_node "grid"
	| None -> []
    in
    List.map 
      (fun node -> 
	JP.string_from_stream (JP.node_stream node))
      field_nodes
  in
  [| got_field_names 0; got_field_names 1 |] 
;;


type molecule_info = {
  mutable born_links: OC.source array;
  links_dict: (string, OC.source) H.t;
};;

let float_of_link link = 
  let cdata = OC.source_value link in
  match cdata with
    | OC.Float x -> x
    | _ -> raise (Failure "no float value in source")
;;


(* ************************************************* *)
(* two molecules: 0 and 1 *)
let update_molecule_info i = (
  let links_dict = H.create 0 in

  let fn = fname i in
  let ic = if i = 0 then "0" else "1" in
  let mol_node = mol_nodes.(i) in
  let atoms_file = NI.string_of_child mol_node "atoms" in

  let atoms_link = OC.new_in_file_source atoms_file
  in
  H.add links_dict "atoms" atoms_link;

  let surface_link = 
    new_command_source "surface_spheres" (fn "surface") in
  OC.add_stdin_prereq surface_link atoms_link;
  if tf_node_value mol_node "all-in-surface" then (
    OC.add_prereq surface_link "all" (OC.const_string_source "")
  );
  
  let solute_diel_link = 
    OC.new_in_2file_float_var_source input_fname old_data_fname
      ["molecule" ^ ic; "solute-dielectric"]
  in    

  let field_links = 
    Array.of_list 
      (List.map
	 (fun name -> OC.new_in_file_source name)
	 field_names.(i)
      )
  in
  
  let outmost_field_link_i =
    if (len field_links) = 0 then
      0
    else
      let module V = Vec3 in 
      let module GP = Grid_parser in 
      let corners_of i = 
        let link = field_links.(i) in
        let fname = OC.file_name link in
        let info = Grid_parser.grid_info (Scanf.Scanning.from_file fname) in
        let low_corner = info.GP.low in
        let high_corner = 
	  let nx,ny,nz = info.GP.n
	  and hx,hy,hz = info.GP.h in
	  let diff = 
	    V.v3 ((float nx)*.hx) ((float ny)*.hy) ((float nz)*.hz)
	  in
	  V.vsum low_corner diff
        in
        low_corner, high_corner
      in
      
      let left_of ca cb = 
        (ca.V.x <= cb.V.x) &&  (ca.V.y <= cb.V.y) &&  (ca.V.z <= cb.V.z)
      in
      
      let rec loop info i = 
        if i = (len field_links) then
	  info
        else
	  let io,(lco,hco) = info in
	  let lc,hc = corners_of i in 
	  if (left_of lc lco) && (left_of hco hc) then
	    loop (i,(lc,hc)) (i+1)
	  else
	    loop info (i+1)
      in
      let info0 = 0,(corners_of 0) in
      let i0, corners0 = loop info0 0 in
      i0
  in
  
  let eff_charge_link =
    let prog = 
      if is_protein then
	"protein_test_charges"
      else
	"residue_test_charges"
    in
    new_command_source prog (fn "charges") in
  OC.add_stdin_prereq eff_charge_link atoms_link;
  H.add links_dict "eff-charges" eff_charge_link;
  
  let after_slash_before_dot file = 
    let pos0 =
      try
	String.index file '/' 
      with Not_found -> -1	
    in
    let pos1 = String.rindex file '.' in
    String.sub file (pos0+1) ((pos1 - pos0) - 1)
  in  
  
  let ins_pts_links =
    let new_ins_link name = 
      let ins_pts_link = new_command_source "inside_points" name in
      OC.add_prereq ins_pts_link "spheres" atoms_link;
      OC.add_prereq ins_pts_link "surface" surface_link;
      OC.add_prereq ins_pts_link "exclusion_distance" (OC.const_float_source 0.0);
      ins_pts_link
    in
    if (len field_links) > 0 then
      Array.map
        (fun field_link ->
          let name = OC.file_name field_link in
          let ins_pts_link = new_ins_link ("ins-"^(after_slash_before_dot name)^".xml") in
          OC.add_prereq ins_pts_link "egrid" field_link;
          ins_pts_link
        )
        field_links
    else
      let ins_pts_link = new_ins_link ("ins-"^prefices.(i)^".xml") in
      [| ins_pts_link |]
      
  in

(*
  let dist_link = new_command_source "grid_distances" (fn "distances") in
  OC.add_stdin_prereq dist_link ins_pts_links.(0);
*)

  if i = 1 then (
    let cheby_link = new_command_source "lumped_charges" (fn "cheby") in
    OC.add_prereq cheby_link "pts" eff_charge_link;
      (* OC.add_prereq cheby_link "dist" dist_link; *)
    
    H.add links_dict "lumped_charges" cheby_link;
    OC.update cheby_link; 
  );

  let born_links =
    if use_desolv then
      Array.map
	(fun ins_pt_link -> 
	  let name = 
	    let inname = OC.file_name ins_pt_link in	 
	    let name0 = String.sub inname 4 ((String.length inname) - 4) in
	    String.sub name0 0 ((String.length name0) - 4) 
	  in
	  
	  let born_link = new_command_source "born_integral" 
	    ("born-"^name^".dx") 
	  in
	  OC.add_prereq born_link "in" ins_pt_link;
	  OC.add_prereq born_link "vperm" vac_perm_link; 
	  OC.add_prereq born_link "oeps" diel_link; 
	  OC.add_prereq born_link "ieps" solute_diel_link;
	  OC.add_prereq born_link "debye" debye_link;
	  OC.update born_link;
	  born_link
	)
	ins_pts_links
    else
      [||]
  in

  let q2_link = 
    new_command_source "compute_charges_squared" (fn "q2") in
  OC.add_stdin_prereq q2_link eff_charge_link;
  
  let q2_cheby_link = 
    new_command_source "lumped_charges" (fn "q2-cheby") 
  in
  OC.add_prereq q2_cheby_link "pts" q2_link;
  (* OC.add_prereq q2_cheby_link "dist" dist_link; *)
  OC.update q2_cheby_link;
  
  let volume_link =
    new_command_source "compute_effective_volumes" (fn "volume") in
  OC.add_prereq volume_link "spheres" atoms_link;
  OC.add_prereq volume_link "inside" ins_pts_links.(outmost_field_link_i);
  OC.add_prereq volume_link "solvent" diel_link;
  OC.add_prereq volume_link "solute" solute_diel_link;
  
  let ellipsoid_link = 
    new_command_source "ellipsoids" (fn "ellipsoids")
  in
  OC.add_stdin_prereq ellipsoid_link volume_link;
  OC.update ellipsoid_link;
  H.add links_dict "ellipsoid" ellipsoid_link;

  let center_link = 
    OC.new_in_float3_var_source ellipsoid_link ["hydro-center"] in

  let mpole_link = new_command_source "mpole_grid_fit" (fn "mpole") in

  OC.add_prereq mpole_link "debye" debye_link;
  OC.add_prereq mpole_link "solvdi" diel_link;
  OC.add_prereq mpole_link "vperm" vac_perm_link;
  OC.add_prereq mpole_link "center" center_link;

  if (len field_links) = 0 then 
    OC.add_prereq mpole_link "charges" eff_charge_link
  else
    OC.add_prereq mpole_link "dx" field_links.(outmost_field_link_i);
 
  H.add links_dict "mpole" mpole_link;

  let charge_link = 
    if i = 0 then
      OC.new_in_float_var_source mpole_link ["charge"]
    else
      OC.new_in_float_var_source eff_charge_link ["total-charge"]
  in
  let smooth_radius_link = 
    OC.new_in_float_var_source mpole_link ["smooth-radius"] in
  let max_distance_link = 
    OC.new_in_float_var_source ellipsoid_link ["max-distance"] in
  H.add links_dict "charge" charge_link;
  H.add links_dict "smooth-radius" smooth_radius_link;
  H.add links_dict "max-distance" max_distance_link;

  let rxn_flag = 
    "rxn" ^ (if i = 0 then "0" else "1")
  in
  
  let surface_atoms_link =

    let hard_surface_link = 
      let res = 
	new_command_source 
	  "make_surface_sphere_list" 
	  (fn "surface-atoms-hard") 
      in
      OC.add_prereq res rxn_flag rxns_link;
      OC.add_prereq res "surface" surface_link;
      OC.add_prereq res "spheres" atoms_link;
      res
    in


    let soft_file_name_link = 
      OC.new_in_2file_string_var_source 
	input_fname old_data_fname [("molecule"^ic); "soft-spheres"]	
	
    in

    let res = 
      new_command_source "make_soft_spheres" (fn "surface-atoms") in
    OC.add_stdin_prereq res hard_surface_link; 
    OC.add_opt_prereq res "soft" soft_file_name_link;
    res

  in

  OC.update surface_atoms_link; 

  let hydro_radius_link = 
    new_command_source "hydro_radius" (fn "hydro-radius")
  in
  OC.add_stdin_prereq hydro_radius_link ins_pts_links.(outmost_field_link_i);

  H.add links_dict "hydro_radius" 
    (OC.new_in_float_var_source hydro_radius_link []);

  {
    born_links = born_links;
    links_dict = links_dict;
  };

  
)
;;

(* ************************************************* *)

let mol_info = [| update_molecule_info 0; update_molecule_info 1; |];;

let link_of i name = 
  H.find mol_info.(i).links_dict name
;;


let hydro_radius_link i = link_of i "hydro_radius";;

let hydro_radius0_link = hydro_radius_link 0;;
let hydro_radius1_link = hydro_radius_link 1;;

let charge0_link = link_of 0 "charge";;
let charge1_link = link_of 1 "charge";;


(* ******************************************************* *)
(* compute appropriate b-radius *)
let b_radius_link = 
  let user_link = 
    OC.new_in_2file_float_var_source 
      input_fname old_data_fname ["b-radius"]

  and auto_link = 
    let max_distance0_link = link_of 0 "max-distance"
    and max_distance1_link = link_of 1 "max-distance"
    and smooth_radius0_link = link_of 0 "smooth-radius"
    and smooth_radius1_link = link_of 1 "smooth-radius"
    in
    let gop op = 
      (fun x0 x1 -> 
	match x0, x1 with
	  | OC.Float xf0, OC.Float xf1 -> OC.Float (op xf0 xf1)
	  | _,_ -> raise (Failure "b_radius_link: both args must be floats")
      )
    in
    let scaled x =
      match x with
	| OC.Float xf -> OC.Float (1.1*.xf)
	| _ -> raise (Failure "b_radius_link: arg must be float")
    in

    let plus = gop (+.)
    and gmax = gop max
    in
    
    let collision_distance_link = 
      OC.new_func2_source plus max_distance0_link max_distance1_link
    in
    let scaled_coll_dist_link = 
      OC.new_func1_source scaled collision_distance_link 
    in
    let charge_distance_link = 
      OC.new_func2_source gmax smooth_radius0_link smooth_radius1_link
    in
    OC.new_func2_source gmax scaled_coll_dist_link charge_distance_link
      
  in
  OC.new_alt_source user_link auto_link
    
;;

(* ******************************************************* *)
let return_dist_name = fname01 "return-dist";;

let return_dist_source = 
  new_command_source "sphere_return_distribution" return_dist_name;;

let aprds = OC.add_prereq return_dist_source;; 
aprds "arad0" hydro_radius0_link;;
aprds "arad1" hydro_radius1_link;;
aprds "ch0" charge0_link;;
aprds "ch1" charge1_link;;
aprds "rbegin" b_radius_link;;
aprds "debye" debye_link;;
aprds "diel" diel_link;;
aprds "vperm" vac_perm_link;;
aprds "kT" kT_link;;
aprds "h2ovisc" h2o_visc_link;;
aprds "visc" rel_visc_link;;
OC.add_neg_opt_prereq return_dist_source "hi" hi_link;;
OC.add_opt_prereq return_dist_source "hemi" hemi_link;;
OC.update return_dist_source;;

(* ************************************************* *)
(* print out molecule information *)
let print_mol_sim out i = 
  pr out "    <molecule%d>\n" i;
  pr out "      <atoms> %s </atoms>\n" (fname i "surface-atoms");
  if i = 1 then
    pr out "      <eff-charges> %s </eff-charges>\n" (fname i "cheby");

  pr out "      <charge-squared> %s </charge-squared>\n" 
    (fname i "q2-cheby");

  pr out "      <hydro-radius> %s </hydro-radius>\n" (fname i "hydro-radius");
  pr out "      <ellipsoids> %s </ellipsoids>\n" (fname i "ellipsoids");
  pr out "      <electric-field>\n";
  
  List.iter
    (fun name ->
      pr out "        <grid> %s </grid>\n" name;
    )
    field_names.(i);

  if i = 0 then (
    let mpole_file_name = OC.file_name (link_of 0 "mpole") in
    pr out "        <multipole-field> %s </multipole-field>\n" 
      mpole_file_name;
  );
  pr out "      </electric-field>\n";

  if i = 0 then (
    pr out "      <total-charge> %g </total-charge>\n" 
      (float_of_link charge0_link); 
  );

  if use_desolv then (
    pr out "      <desolvation-field>\n";
    
    let born_file_names = 
      Array.map
	OC.file_name
	mol_info.(i).born_links
    in
    Array.iter
      (fun name ->
	pr out "        <grid> %s </grid>\n" name;
      )
      born_file_names;
    
    pr out "      </desolvation-field>\n";
  );

  (
    let tag = "short-ranged-force-parameters" in
    let pnode_opt =  JP.child mol_nodes.(i) tag in
    match pnode_opt with
      | None -> ()
      | Some pnode ->
	let fname = JP.string_from_stream (JP.node_stream pnode) in
	pr out "      <%s> %s </%s>\n" tag fname tag;  
  );

  (
    let rnode_opt = JP.child mol_nodes.(i) "radii" in
    match rnode_opt with
      | None -> ()
      | Some rnode ->
	pr out "    <radii>\n";
	let pr_hs tag = 
	  let hsnode_opt = JP.child rnode tag in
	  match hsnode_opt with
	    | None -> ()
	    | Some hsnode -> 
	      let value = 
		JP.string_from_stream (JP.node_stream hsnode)
	      in
	      pr out "      <%s> %s </%s>\n" tag value tag;
	      
	in
	pr_hs "hard";
	pr_hs "soft";
	pr out "    </radii>\n";	      
  );
  

  (
    let tnode_opt = JP.child mol_nodes.(i) "tether" in
    match tnode_opt with
      | None -> ()
      | Some tnode ->
	pr out "      <tether>\n";
	if i = 1 then (
	  let iatom1 = NI.int_of_child tnode "atom1" in
	  pr out "       <atom1> %d </atom1>\n" iatom1;
	)
	else (
	  let iatom0 = NI.int_of_child tnode "atom0" in
	  pr out "       <atom0> %d </atom0>\n" iatom0;
	  let rlen = NI.float_of_child tnode "resting-length" in
	  let dlen = NI.float_of_child tnode "length-deviation" in
	  pr out "        <resting-length> %g </resting-length>\n" rlen;
	  pr out "        <length-deviation> %g </length-deviation>\n" dlen;
	);
	pr out "      </tether>\n";
  );
  
  pr out "    </molecule%d>\n" i
;;

(* ************************************************* *)
let sim_file= open_out (fname01 "simulation");;

(* just carry through verbatim information from input file *)
let pthru_int name = 
  try
    let value = NI.int_of_child input_node name in
    pr sim_file "  <%s> %d </%s>\n" name value name
  with NI.Not_found tag -> ()
;;

let pthru_flt name = 
  try
    let value = NI.float_of_child input_node name in
    pr sim_file "    <%s> %g </%s>\n" name value name
  with NI.Not_found tag -> ()
;;

let pthru_str name = 
  try
    pr sim_file "  <%s> %s </%s>\n" name 
      (NI.string_of_child input_node name) name
  with NI.Not_found tag -> ()
;;


pr sim_file "<root>\n";;

pthru_int "n-threads";;

pr sim_file "  <solvent-file> %s </solvent-file>\n" solvent_file;;

pthru_str "output";;
pthru_int "n-trajectories";;
pthru_int "n-trajectories-per-output";;
pthru_int "n-steps-per-output";;
pthru_int "n-copies";;
pthru_int "n-bin-copies";;
pthru_int "n-steps";;
pthru_int "seed";;
pthru_int "max-n-steps";;
pthru_int "n-we-steps-per-output";;
pthru_str "trajectory-file";;
pthru_str "min-rxn-dist-file";;

pr sim_file "  <molecule-pair>\n";;
print_mol_sim sim_file 0;;
print_mol_sim sim_file 1;;

let tnode_opt = JP.child input_node "time-step-tolerances";;
match tnode_opt with
  | None -> ()
  | Some tnode -> (
    pr sim_file "    <time-step-tolerances>\n";
    
    let cpr tag = 
      let cnode_opt = JP.child tnode tag in
      match cnode_opt with
	| None -> ()
	| Some cnode -> 
	  let stm = JP.node_stream cnode in 
	  let value = JP.float_from_stream stm in
	  pr sim_file "      <%s> %g </%s>\n" tag value tag;
    in
    cpr "force";
    cpr "reaction";
    cpr "collision";
    cpr "minimum-dx";
    cpr "minimum-reaction-dx";

    pr sim_file "    </time-step-tolerances>\n";
  )
;;      


pr sim_file "    <reaction-file> %s </reaction-file>\n" 
  (OC.file_name rxns_link);

pr sim_file "    <b-radius> %g </b-radius>\n" (float_of_link b_radius_link);;

pr sim_file "    <BD-propagation-file> %s </BD-propagation-file>\n" 
  return_dist_name;;


pthru_str "use-6-8-force";;
pthru_str "start-at-site";;
pthru_str "hydrodynamic-interactions";;
pthru_flt "desolvation-parameter";;

pr sim_file "  </molecule-pair>\n";;

pr sim_file "  <bin-file> %s </bin-file>\n" (fname01 "bins");;
pr sim_file "</root>\n";;


let copy_file fromf tof = 
  let inc = open_in fromf in
  let buf = Bytes.create 100000 in
  let n = input inc buf 0 (Bytes.length buf) in
  let ouc = open_out tof in
  output ouc buf 0 n;
  close_in inc;
  close_out ouc
;;

(* keep old copy of input file around so that if only one thing
   changes, not everything has to change *)
copy_file input_fname old_data_fname;;
