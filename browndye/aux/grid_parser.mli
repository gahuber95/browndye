(* Used internally. Extracts information from a dx-format file *)

type data3d =
    (float, Bigarray.float32_elt, Bigarray.c_layout)
    Bigarray.Array3.t
 
type t = {
  n : int * int * int; (* number of grid points in x,y, and z directions *)
  h : float * float * float; (* grid spacing in x,y, and z directions *)
  low : Vec3.t; (* position of lowest corner *)
  data : data3d option; (* actual data *)
}

(* Returns t data structure with shape information but no data. *)
val grid_info : Scanf.Scanning.scanbuf -> t

(* return structure with data *)
val new_grid : Scanf.Scanning.scanbuf -> t

(* For very large grids, the following functions avoid reading all
of the data into memory from the file; the data are processed directly
from the file. *)

(* (arguments: buffer, f, g) - applies f to t structure of grid,
and repeatedly feeds the result through (g a ix iy iz potential laplacian),
returning the final result. Arguments of g are the intermediate result,
the indices of the grid point (ix,iy,iz), the value at the grid point 
(potential), and the laplacian.
*)
val apply_to_laplacian :
  Scanf.Scanning.scanbuf -> (t -> 'a) ->
  ('a -> int -> int -> int -> float -> float -> 'a) -> 'a

(* (arguments: buffer, f, g) - applies f to t structure of grid,
and feeds the result to function g.  The arguments of g are
the output of f, the indices of the grid point (ix,iy,iz), and
the value at the grid point (potential). *)
val apply_to_potential :
  Scanf.Scanning.in_channel ->
  (t -> 'a) -> ('a -> int -> int -> int -> float -> 'a) -> 'a


(* Same as apply_to_potential, except that the quantity given
to g is the gradient at each point dotted with itself. *)
val apply_to_ee :
  Scanf.Scanning.scanbuf -> (t -> 'a) ->
  ('a -> int -> int -> int -> float -> 'b) -> unit
