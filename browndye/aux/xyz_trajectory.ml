(*
Converts a trajectory file into an XYZ file suitable for animating with
VMD.  It takes the following arguments, with flags:

-mol0 : molecule0 pqrxml file (optional)
-mol1 : molecule1 pqrxml file
-trajf : trajectory file
-pqr : output trajectory of concatenated PQR files instead of XYX
*)

let mol0_file_ref = ref "";;
let mol1_file_ref = ref "";;
let traj_file_ref = ref "";;
let do_pqr_ref = ref false;;
let to_out_ref = ref true;;

Arg.parse
  [("-mol0", Arg.Set_string mol0_file_ref, "molecule0 pqrxml file");
   ("-mol1", Arg.Set_string mol1_file_ref, "molecule1 pqrxml file");
   ("-trajf", Arg.Set_string traj_file_ref, "trajectory file");
    ("-pqr", Arg.Set do_pqr_ref, "output trajectory of PQR files");
    ("-trial",Arg.Clear to_out_ref, "output file size only");
  ]
  (fun arg -> raise (Failure "no anonymous arguments"))
  "Outputs trajectory in XYZ format for viewing with VMD"
;;

let mol0_file = !mol0_file_ref;;
let mol1_file = !mol1_file_ref;;
let traj_file = !traj_file_ref;;
let do_pqr = !do_pqr_ref;;
let to_out = !to_out_ref;;

let only1 = (mol0_file = "");;
  
let pr = Printf.printf;;
let sq x = x*.x;;

module SS = Scanf.Scanning;;
module V = Vec3;;
module JP = Jam_xml_ml_pull_parser;;
module NI = Node_info ;;

type atom = {
  mutable pos: V.t;
  aname: string;
  rname: string;
  anumber: int;
  rnumber: int;
  radius: float;
  charge: float;
};;

let mv_prod mat v =    
  let bx = mat.(0).V.x*.v.V.x +. mat.(0).V.y*.v.V.y +. mat.(0).V.z*.v.V.z;
  and by = mat.(1).V.x*.v.V.x +. mat.(1).V.y*.v.V.y +. mat.(1).V.z*.v.V.z;
  and bz = mat.(2).V.x*.v.V.x +. mat.(2).V.y*.v.V.y +. mat.(2).V.z*.v.V.z
  in
    V.v3 bx by bz
;;


let print_atom_xyz atom pos nc_ref = 
  let output = Printf.sprintf "%s %.4g %.4g %.4g\n" 
    atom.aname pos.V.x pos.V.y pos.V.z in
  if to_out then
    Printf.printf "%s" output
  else
    nc_ref := !nc_ref + (String.length output)
;;
  
let print_atom_pqr atom pos nc_ref = 

  let aname = atom.aname in
    
  let apre,amid,apos = 
    match String.length aname with
      | 1 -> " ", aname, "  "
      | 2 -> " ", aname, " "
      | 3 -> 
          let first = aname.[0] in
            if 'A' <= first && first <= 'Z' then
              " ", aname,""
            else
              "", aname, " "
      | 4 -> "", aname, ""
      | _ -> raise (Failure "should not get here")
  in
  let output = 
    Printf.sprintf "ATOM %6d %s%s%s %-3s%6d     %7.3f %7.3f %7.3f %7.4f %7.3f\n" 
      atom.anumber apre amid apos atom.rname atom.rnumber 
      pos.V.x pos.V.y pos.V.z atom.charge atom.radius
  in
  if to_out then
    Printf.printf "%s" output
  else
    nc_ref := !nc_ref + (String.length output)
;;

let parser = JP.new_parser (SS.from_file traj_file);;

let found_node parser tag =
  while not (JP.is_current_node_tag parser tag) do
    JP.next parser;
  done;
  JP.current_node parser
;;

let node_int node = 
  JP.int_from_stream (JP.node_stream node)
;;

let quat_to_mat quat = 
  let mat = [|
    V.v3 nan nan nan;
    V.v3 nan nan nan;
    V.v3 nan nan nan;
  |]
  in    
  let qw = quat.(0)
  and qx = quat.(1)
  and qy = quat.(2)
  and qz = quat.(3)
  in    
    mat.(0).V.x <- 1.0 -. 2.0*.qy*.qy -. 2.0*.qz*.qz;
    mat.(0).V.y <- 2.0*.qx*.qy -. 2.0*.qz*.qw;
    mat.(0).V.z <- 2.0*.qx*.qz +. 2.0*.qy*.qw;
    
    mat.(1).V.x <- 2.0*.qx*.qy +. 2.0*.qz*.qw;
    mat.(1).V.y <- 1.0 -. 2.0*.qx*.qx -. 2.0*.qz*.qz;
    mat.(1).V.z <- 2.0*.qy*.qz -. 2.0*.qx*.qw;
    
    mat.(2).V.x <- 2.0*.qx*.qz -. 2.0*.qy*.qw;
    mat.(2).V.y <- 2.0*.qy*.qz +. 2.0*.qx*.qw;
    mat.(2).V.z <- 1.0 -. 2.0*.qx*.qx -. 2.0*.qy*.qy;
    mat
;;

let atoms_of_file mol_file = 
  let alist = ref [] in
    Atom_parser.apply_to_atoms (SS.from_file mol_file)
      (fun ~rname:rname ~rnumber:rnumber ~aname:aname ~anumber:anumber 
	~x:x ~y:y ~z:z ~radius:r ~charge:q -> 
	let pos = V.v3 x y z in
	let a = {
	  pos = pos;
	  aname = aname;
	  rname = rname;
	  anumber = anumber;
	  rnumber = rnumber;
	  radius = r;
	  charge = q;
	}
	in
	  alist := a :: (!alist);
      );
    Array.of_list (List.rev !alist)
;;
  
let atoms0 = if only1 then [||] else atoms_of_file mol0_file;;
let atoms1 = atoms_of_file mol1_file;;
    
let print_header nc_ref = 
  let out0 = 
    Printf.sprintf "%d\n" ((Array.length atoms0) + (Array.length atoms1))
  in
  let out1 = Printf.sprintf "Molecule\n"
  in
  if to_out then 
    pr "%s%s" out0 out1
  else
    nc_ref := !nc_ref + (String.length out0) + (String.length out1)
;;


let tv t = 
  let x,y,z = t in
    V.v3 x y z
;;
    
let vec3 node = 
  let stm = JP.node_stream node in
    tv (JP.float3_from_stream stm)
;;
    
let c0 = 
  let cm_node = found_node parser "center-molecule0" in
    JP.complete_current_node parser;
    vec3 cm_node
;;
  
let c1 = 
  let cm_node = found_node parser "center-molecule1" in
    JP.complete_current_node parser;
    vec3 cm_node
;;
    
let translate_atoms atoms c =
  for i = 0 to (Array.length atoms)-1 do
    let atom = atoms.(i) in
    atom.pos <- V.vdiff atom.pos c;
  done
;;
  
translate_atoms atoms1 c1;;
translate_atoms atoms0 c0;;
  
(*
if only1 then (
  let c0m = V.vscaled (-1.0) c0 in
  translate_atoms atoms1 c0m;
)
else (
  translate_atoms atoms0 c0;
);;
 *)
  
let nc_ref = ref 0;;

JP.go_up parser;;
JP.apply_to_nodes_of_tag parser "state"
  (fun node ->
    JP.complete_current_node parser; 
    print_header nc_ref;
    let trans = 
      tv (NI.float3_of_child node "translation") 
    in
    let rot = 
      let qnode_opt = JP.child node "quaternion" in
      match qnode_opt with
      | Some qnode -> 
	 let quat3 = vec3 qnode in
	 let quat4 = 
	   let q1,q2,q3 = quat3.V.x, quat3.V.y, quat3.V.z in
	   let q0 = 
	     let q0sq = 1.0 -. ((sq q1) +. (sq q2) +. (sq q3)) in
	     sqrt (max 0.0 q0sq)
	   in
	   [| q0; q1; q2; q3; |]
	 in
	 quat_to_mat quat4
		     
      | None -> 			    
	 let rnode = NI.checked_child node "rotation" in
	 let rnodes = Array.of_list (JP.children rnode "row") in
	 Array.map (fun rnode -> vec3 rnode) rnodes 		    
    in
    if not only1 then
      Array.iteri
	(fun i atom ->
	  let pos = atom.pos in
	  if do_pqr then
	    print_atom_pqr atom pos nc_ref
	  else
	    print_atom_xyz atom pos nc_ref
	)
	atoms0;
    
    Array.iteri
      (fun i atom ->
	let pos0 = atom.pos in
	let pos1 = mv_prod rot pos0 in
	let pos = V.vsum pos1 trans in
	if do_pqr then
	  print_atom_pqr atom pos nc_ref
	else
	  print_atom_xyz atom pos nc_ref
      )
      atoms1;		    
  )   
;;
  
let mbyte = 1 lsl 20;;
  
if not to_out then
  pr "Estimated file size: %iM\n" (!nc_ref/mbyte);

 
