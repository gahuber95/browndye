(* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*)

(* used internally to generate polyhedra and points on the surface
of spheres. Exported functions are described in the interface file. *)


open Vec3

type ball_t = {
  order: int;
  center: Vec3.t;
  radius: float;
}

type element_t = {
  pos: Vec3.t;
  perp: Vec3.t;
  area: float;
}


let sq a = a*.a

let rshifted ball p = 
  vscaled (1.0/.ball.radius) (vdiff p ball.center) 
 
let shifted ball p = 
  vsum (vscaled ball.radius p) ball.center
 
let normalized ball v =
  let v = rshifted ball v in
  let vx,vy,vz = v.x, v.y, v.z in
  let rm1 = 1.0 /. (sqrt ((sq vx) +. (sq vy) +. (sq vz))) in
  let v = vscaled rm1 v in
    shifted ball v

let line_center ball p0 p1 =
  normalized ball (vscaled 0.5 (vsum p0 p1))
  
let pi = 2.0 *. (acos 0.0)
let pi2 = 2.0 *. pi

let triangle_area cen p0 p1 p2 = 
  let r2 = distance2 cen p0 in
  let v0 = vdiff p0 cen
  and v1 = vdiff p1 cen
  and v2 = vdiff p2 cen
  in
  let angle va vb = acos ((dot va vb)/.r2) in
  let a01 = angle v0 v1
  and a12 = angle v1 v2
  and a20 = angle v2 v0
  in
  let eangle aa ab ac =
    let ca = cos aa
    and cb = cos ab
    and cc = cos ac 
    and sb = sin ab
    and sc = sin ac in 
    acos ((ca -. cb*.cc)/.(sb*.sc))
  in
  let ea0 = eangle a12 a20 a01
  and ea1 = eangle a20 a01 a12
  and ea2 = eangle a01 a12 a20
  in
  r2*.(ea0 +. ea1 +. ea2 -. pi)

let quadrisected ball triangle = 
  let p0, p1, p2 = triangle in
  let lc = line_center ball in
  let p01 = lc p0 p1
  and p12 = lc p1 p2
  and p20 = lc p2 p0
  in
    [(p01,p12,p20); (p0,p01,p20); (p1,p12,p01); (p2,p12,p20)]

let centroid ball p0 p1 p2 =
 
  let mean p0 p1 = 
    vscaled 0.5 (vsum p0 p1) 
  in
  let avg3 p0 p1 = 
    vsum (vscaled (2.0/.3.0) p0) (vscaled (1.0/.3.0) p1)
  in 

  let d1 = vdiff p1 p0
  and d2 = vdiff p2 p0 
  and d12 = vdiff p1 p2 in
    if (vnorm d1) = 0.0 then
      avg3 p0 p2
    else if (vnorm d2) = 0.0 then
      avg3 p0 p1
    else if (vnorm d12) = 0.0 then
      avg3 p1 p0
    else
      let perp = normed (cross d1 d2) in
      let m1 = mean p0 p1
      and m2 = mean p0 p2
      in
      let c1 = vdiff p1 m2
      and c2 = vdiff p2 m1
      in
      let t1 = cross perp c1
      and t2 = cross perp c2
      in
      let row0 = [| perp.x; perp.y; perp.z |]
      and row1 = [| t1.x; t1.y; t1.z |]
      and row2 = [| t2.x; t2.y; t2.z |] in
      let b = [| dot perp p0; dot t1 p1; dot t2 p2 |] in
      let m = [| row0; row1; row2 |] in    
      let c = [| nan; nan; nan |] in
	Solve3.solve3 m b c;
	normalized ball (v3 c.(0) c.(1) c.(2))

let rim_height = 1.0/.(sqrt 5.0)
let rim_radius = 2.0/.(sqrt 5.0)

let top = v3 0.0 0.0 1.0
and top_rim = 
  (Array.init 
     5 
     (fun i -> 
	let th = 0.2*.pi2 *.(float i) in
	let cth = (cos th)
	and sth = (sin th) in
	  v3 (rim_radius*.cth) (rim_radius*.sth) rim_height
     )
  )

and bottom = v3 0.0 0.0 (-1.0)
and bottom_rim = 
  (Array.init 
     5 
     (fun i -> 
	let th = 0.2*.pi2 *.((float i) +. 0.5) in
	let cth = (cos th)
	and sth = (sin th) in
	  v3 (rim_radius*.cth) (rim_radius*.sth) (-.rim_height)
     )
  )
	  
let top_cap = 
  (Array.init 
     5
     (fun i -> 
	top_rim.(i), top, top_rim.((i+1) mod 5)
     )
  )

let bottom_cap = 
  (Array.init 
     5
     (fun i -> 
	bottom_rim.(i), bottom, bottom_rim.((i+1) mod 5)
     )
  )

let up_belt = 
  (Array.init
     5
     (fun i ->
	bottom_rim.(i), top_rim.((i+1) mod 5), bottom_rim.((i+1) mod 5) 
     )
  )

let down_belt = 
  (Array.init
     5
     (fun i ->
	top_rim.(i), bottom_rim.(i), top_rim.((i+1) mod 5)
     )
  )


let icosahedron ball =
  List.map
    (fun verts -> 
       let shf = shifted ball in
       let v0,v1,v2 = verts in
	 (shf v0), (shf v1), (shf v2)
    )
    (Array.to_list
       (Array.concat [top_cap; up_belt; down_belt; bottom_cap;])
    )


let oct_rim = 
  [| (v3 1.0 0.0 0.0); (v3 0.0 1.0 0.0); 
     (v3 (-.1.0) 0.0 0.0); (v3 0.0 (-.1.0) 0.0);
  |] 
 
let oct_top = (v3 0.0 0.0 1.0)
let oct_bot = (v3 0.0 0.0 (-.1.0))

let oct_tcap = [|
  (oct_top, oct_rim.(0), oct_rim.(1)); 
  (oct_top, oct_rim.(1), oct_rim.(2)); 
  (oct_top, oct_rim.(2), oct_rim.(3)); 
  (oct_top, oct_rim.(3), oct_rim.(0));
|]

let oct_bcap = [|
  (oct_bot, oct_rim.(0), oct_rim.(1)); 
  (oct_bot, oct_rim.(1), oct_rim.(2)); 
  (oct_bot, oct_rim.(2), oct_rim.(3)); 
  (oct_bot, oct_rim.(3), oct_rim.(0));
|]

let octahedron ball = 
  List.map
    (fun verts -> 
       let shf = shifted ball in
       let v0,v1,v2 = verts in
	 (shf v0), (shf v1), (shf v2)
    )
    (Array.to_list
       (Array.concat [ oct_tcap; oct_bcap])
    )


let divided ball triangles = 
  (List.fold_left 
     (fun list tri -> list @ (quadrisected ball tri))
     []
     triangles
  )

let rec recursively_divided ball n triangles = 
  if 
    n == 0
  then
    triangles
  else
    (recursively_divided ball (n-1) (divided ball triangles))


let made ~order ~radius ~center= {
  order = order;
  radius = radius;
  center = center
}


let icoso_triangle_vertices ball = 
  let verts = 
    (recursively_divided ball ball.order (icosahedron ball))
  in
    Array.of_list verts

let octo_triangle_vertices ball = 
  let verts = 
    (recursively_divided ball ball.order (octahedron ball))
  in
    Array.of_list verts


let normal = rshifted

(*
let triangle_area p0 p1 p2 = 
  let d1 = vdiff p1 p0
  and d2 = vdiff p2 p0 in
    0.5*.(vnorm (cross d1 d2))
 *)

let icoso_elements sph = 
  let tris = icoso_triangle_vertices sph in
    Array.map
      (fun tri ->
	let v0,v1,v2 = tri in
	let area = triangle_area sph.center v0 v1 v2 in
	let pos = centroid sph v0 v1 v2 in
	let perp = normed (vdiff pos sph.center) in
	  {
	    pos = pos;
	    perp = perp;
	    area = area;
	  }
      )
      tris

let icoso_face_centers_z0 pos radius = 
  let ball = {
    order = 0;
    center = pos;
    radius = radius;
  }
  in
  let elements = icoso_elements ball in
    Array.map
      (fun ele -> ele.pos, ele.area;)
      elements
;;

let icoso_face_centers order pos radius = 
  let ball = {
    order = order;
    center = pos;
    radius = radius;
  }
  in
  let elements = icoso_elements ball in
    Array.map
      (fun ele -> ele.pos, ele.area;)
      elements
;;


let with_no_dups ar = 
  let n = Array.length ar in
    Array.sort compare ar;
    let nd = ref 1 in
      for k = 1 to n-1 do
	if not (ar.(k) = ar.(k-1)) then
	  nd := !nd + 1
      done;
      
      let id = ref 0 in
	Array.init !nd
	  (fun i ->
	    let res = ar.(!id) in
	      id := !id + 1;
	      while (!id < n) && (ar.(!id) = ar.(!id-1)) do 
		id := !id + 1;	    
	      done;
	      res
	  )
;;

let icoso_vertices ball = 
  let tris = icoso_triangle_vertices ball in
  let dup_verts = 
    Array.init (3*(Array.length tris))
      (fun i ->
	let v0,v1,v2 = tris.(i/3) in
	  match i mod 3 with
	    | 0 -> v0
	    | 1 -> v1
	    | _ -> v2
      )
  in
    with_no_dups dup_verts
;;

