(* 
Used internally.
Applies "f" to all XML nodes labeled "point" in "buffer". 
The tags "x", "y", "z", 
"radius", and "type" are used to feed the corresponding labeled arguments
to "f". The tag used to extract the argument "charge" is the "qtag" argument
to "apply_to_points".
*)

type ftype = (x: float -> y: float -> z: float -> radius: float -> charge: float -> ptype: string -> unit)

val apply_to_points : qtag: string -> buffer: Scanf.Scanning.scanbuf ->
  f: ftype -> unit
