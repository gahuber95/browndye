let pair_file, mol0_file, mol1_file = 

  let pair_file_ref = ref ""
  and mol0_file_ref = ref ""
  and mol1_file_ref = ref ""
  in    
  
  Arg.parse 
    [
      ("-pairs", Arg.Set_string pair_file_ref, "file of reaction pairs (output from make_rxn_pairs)");
      ("-mol0", Arg.Set_string mol0_file_ref, "pqrxml file of molecule 0");
      ("-mol1", Arg.Set_string mol1_file_ref, "pqrxml file of molecule 1");
    ]
    (fun a -> Printf.fprintf stderr "needs args\n";)
    "Generates XML file describing reaction pathway from the output of make_rxn_pairs";
  !pair_file_ref, !mol0_file_ref, !mol1_file_ref
;;

let buf = Scanf.Scanning.from_file pair_file;;

module JP = Jam_xml_ml_pull_parser;;
let parser = JP.new_parser buf;;

let pr = Printf.printf;;

let pairs = 
  let lst = ref [] in 
  JP.apply_to_nodes_of_tag parser "pair"
  (fun pnode ->
    JP.complete_current_node parser;
    let stm = JP.node_stream pnode in
    Scanf.bscanf stm " %d %d" 
      (fun i0 i1 ->
	lst := (i0,i1) :: (!lst)
      )
  );
  Array.of_list (List.rev !lst)
;;

module V = Vec3;;

type atom = {
  pos: V.t;
  number: int;
};;

module H = Hashtbl;;

let atoms fname =
  let buf = Scanf.Scanning.from_file fname in
  let dict = H.create 0 in 
    Atom_parser.apply_to_atoms buf
      (fun ~rname:rname ~rnumber:rnumber ~aname:aname ~anumber:anumber 
	~x:x ~y:y ~z:z ~radius:r ~charge:q ->
	let new_atom = {
	  pos = V.v3 x y z;
	  number = anumber;
	}
	in
	H.add dict anumber new_atom;
      );
  dict
;;

let atoms0 = atoms mol0_file;;
let atoms1 = atoms mol1_file;;

let triangle = 
  let area i0 i1 i2 = 
    let pair_pos i = 
      let ia0,ia1 = pairs.(i) in
      let atom0 = H.find atoms0 ia0
      and atom1 = H.find atoms1 ia1 in
      V.vscaled 0.5 (V.vsum atom0.pos atom1.pos)
    in
    let p0 = pair_pos i0
    and p1 = pair_pos i1
    and p2 = pair_pos i2
    in
    let r01 = V.distance p0 p1
    and r12 = V.distance p1 p2
    and r20 = V.distance p2 p0
    in
    let s = 0.5*.(r01 +. r12 +. r20) in
    sqrt (s*.(s -. r01)*.(s -. r12)*.(s -. r20))
  in
  let tri_ref = ref (0,1,2) in
  let max_area = ref (area 0 1 2) in 
  let np = Array.length pairs in
  for ip0 = 0 to np-1 do
    for ip1 = 0 to ip0-1 do
      for ip2 = 0 to ip1-1 do
	let a = area ip0 ip1 ip2 in
	if a > !max_area then (
	  max_area := a;
	  tri_ref := (ip0,ip1,ip2);
	)
      done;
    done;
  done;
  !tri_ref
;;

let ip0,ip1,ip2 = triangle;;

pr "<rxn-pairs>\n";;

let prpair ip = 
  let ia0, ia1 = pairs.(ip) in
  pr "  <pair> %d %d </pair>\n" ia0 ia1
;;

prpair ip0;;
prpair ip1;;
prpair ip2;;

pr "</rxn-pairs>\n";;


