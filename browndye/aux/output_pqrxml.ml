(* 
Used internally. Outputs pqrxml file from information in a 
container of charged spheres (atoms).  The CHARGED_SPHERE
module type has functions for extracting pqrxml information
and for advancing along the container. The function "f"
is called to do the actual outputting.
*)

module type CHARGED_SPHERE =
  sig
    type t
    type container
    type index
    val atom_name: t -> string
    val residue_name: t -> string
    val atom_number: t -> int
    val residue_number: t -> int
    val position: t -> Vec3.t
    val charge: t -> float
    val radius: t -> float
    val first_index: container -> index
    val next: container -> index -> (t option)*index 
  end

let pr = Printf.fprintf

let print_info fp rnum x y z q r rname aname anum ires = 
  if not (rnum = !ires) then (
    
    if !ires > (-1) then
      pr fp  "  </residue>\n";
    
    pr fp  "  <residue>\n";
    pr fp  "    <residue_name>%s</residue_name>\n" rname;
    pr fp  "    <residue_number>%d</residue_number>\n" rnum;
    ires := rnum
  );
  
  pr fp  "    <atom>\n";
  pr fp  "      <atom_name>%s</atom_name>\n" aname;
  pr fp  "      <atom_number>%d</atom_number>\n" anum;
  pr fp  "      <x>%f</x>\n" x;
  pr fp  "      <y>%f</y>\n" y;
  pr fp  "      <z>%f</z>\n" z;
  pr fp  "      <charge>%f</charge>\n" q;
  pr fp  "      <radius>%f</radius>\n" r;
  pr fp  "    </atom>\n";
   

module M( CS: CHARGED_SPHERE) =
    struct
      
      let f atoms channel =		
	pr channel "<roottag>\n";
	let ires = ref (-1) in

	let rec loop index = 
	  let atom_opt, next_index = CS.next atoms index in
	    match atom_opt with
	      | None -> 
		  pr channel "  </residue>\n";
		  pr channel "</roottag>\n";
	      | Some atom ->
		  let rnum = CS.residue_number atom
		  and rname = CS.residue_name atom
		  and anum = CS.atom_number atom
		  and aname = CS.atom_name atom
		  and pos = CS.position atom 
		  and q = CS.charge atom
		  and r = CS.radius atom
		  in
		  let x,y,z = 
		    pos.Vec3.x, pos.Vec3.y, pos.Vec3.z 
		  in
		    print_info channel rnum x y z q r rname aname anum ires;
		    loop next_index
	in
	  loop (CS.first_index atoms)

    end

(* 
Does not use the module interface. The "info" function returns a tuple
with the following items:
residue number, residue name, atom number, atom name, position, charge, radius 
*)

let sphere_array ~spheres ~info ~channel = 
  pr channel "<roottag>\n";
  let n = Array.length spheres in
  let ires = ref (-1) in
    for i = 0 to n-1 do
      let atom = spheres.(i) in
      let rnum, rname, anum, aname, pos, q, r = info atom in
      let x,y,z = pos.Vec3.x, pos.Vec3.y, pos.Vec3.z in
	print_info channel rnum x y z q r rname aname anum ires;
    done;
    pr channel "  </residue>\n";
    pr channel "</roottag>\n";
		
  
