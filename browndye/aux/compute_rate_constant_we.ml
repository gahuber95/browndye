(* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*)


(*
Outputs rate constants and 95% confidence intervals. Input is results file from 
"we_simulation". Uses kdb_calculator for actual calculation.
*)


module JP = Jam_xml_ml_pull_parser;;
module NI = Node_info;;

let pi = 3.1415926;;


Arg.parse 
 []
(fun arg -> raise (Failure "no arguments"))
 "Outputs rate constant and confidence intervals. Input is results file from \"we_simulation\""
;;

 
let rparser = JP.new_parser (Scanf.Scanning.stdin);;

let found_node parser tag = 
  while not (JP.is_current_node_tag parser tag) do
    JP.next parser;
  done;
  JP.current_node parser
;;

let snode = found_node rparser "solvent";;
JP.complete_current_node rparser;;

let mnode = found_node rparser "molecule-info";;
JP.complete_current_node rparser;;

                                   
let kdb = Kdb_calculator.f snode mnode;;

let nd = JP.int_from_stream (JP.node_stream (found_node rparser "n"));;
let nrxns = nd-1;;

ignore (found_node rparser "data");;


let all_data = 
  JP.fold_nodes_of_tag rparser "d" 
      (fun res node ->
	let stm = JP.node_stream node in
	let row = Array.init nd (fun i -> 0.0) in
	  for i = 0 to nd-1 do
	    Scanf.bscanf stm " %g" (fun x -> row.(i) <- x)
	  done;
	  row :: res
      )
      []
;;


let total_flux = 
  let n = List.length all_data in
  let sum = 
    List.fold_left 
      (fun sum row -> 
	sum +. (Array.fold_left (+.) 0.0 row)
      ) 
      0.0
      all_data
  in
    sum /. (float n)
;;


let series_mean_n_bounds data = 

  let data = Array.of_list (List.rev data) in    
  let autoc = Fft.autocorrelation data in
  let ns = 1000 in

    Random.self_init();

    let n = Array.length data in
    let sq x = x*.x in

    let smean a =   
      let sum = Array.fold_left (+.) 0.0 a in
	sum/.(float n)
    in
      
    let ran_autocors = 
      let k = 1 in
	Array.init ns
	  (fun i -> 
	    let xs = 
	      Array.init n
		(fun i -> data.( Random.int n)) 
	    in
	    let mean = smean xs in
	      
	    let den_sum = 
	      Array.fold_left (fun sum x -> sum +. (sq (x -. mean))) 0.0 xs 
	    in
	    let num_sum =
	      let sum = ref 0.0 in
		for i = k to (n-1) do
		  sum := !sum +. (xs.(i) -. mean)*.(xs.(i-k) -. mean)
		done;
		!sum
	    in
	      (float n)*.num_sum /. ((float (n-k))*.den_sum)
	  )
    in
      
      Array.sort compare ran_autocors;

      let low, high = ran_autocors.( 25*ns/1000), ran_autocors.( 975*ns/1000) 
      in

      let cor_len =
	let na = Array.length autoc in
	let finis = ref false in
	let i = ref 0 in
	  while not !finis do
	    
	    if !i = na then (
	      Printf.fprintf stderr 
		"autocorrelation does not die away fast enough\n";
	      exit 0;
	    );
	    
	    let ac = autoc.(!i) in
	      finis := (low < ac) && (ac < high);
	      i := !i + 1;
	  done;
	  !i
      in
	

      let sample_mean() =
	let ix = ref 0 in
	let p = 1.0/.(float cor_len) in
	let sum = ref 0.0 in
	  for i = 0 to n-1 do 
	    if i = 0 || ((Random.float 1.0) < p) || (!ix = n-1) then 
	      ix := Random.int n
	    else 
	      ix := !ix + 1;
	    
	    sum := !sum +. data.(!ix)
	  done;
	  !sum /. (float n)
      in
	
      let sample_means = 
	Array.init ns (fun i -> sample_mean())
      in
	
	Array.sort compare sample_means;
  
	let lowb, highb = 
	  sample_means.( 25*ns/1000), sample_means.( 975*ns/1000)
	in
	  (* Printf.printf "betas %g %g\n" lowb highb; *)
	  lowb, (smean data), highb
;;


let val_and_bounds = 
  Array.init nd 
    (fun id ->
      let data = List.map (fun row -> row.(id)) all_data in
	series_mean_n_bounds data
    )
;;

let conv_factor = 602000000.0;; (* 1/M s, make it an option later *)
let escaped_low, escaped_mean, escaped_high = val_and_bounds.(nrxns);; 
let nsamp = 10000;;

(*
let escaped_sdev = (escaped_high -. escaped_low)/.4.0;;
*)

let gaussian () = 
  let u0 = 1.0 -. (Random.float 1.0) in
  let u1 = 1.0 -. (Random.float 1.0) in
    (sqrt (-2.0*.(log u0)))*.(cos (2.0*.pi*.u1))
;;

let pr = Printf.printf;;

pr "<rates>\n";;
pr "  <!-- 95%% confidence intervals -->\n";;

for irxn = 0 to nrxns-1 do
  let low, mean, high = val_and_bounds.(irxn) in
    
  let low_beta = low/.total_flux
  and high_beta = high/.total_flux
  and mean_beta = mean/.total_flux
  in

  let low_rate = conv_factor*.kdb*.low_beta
  and high_rate = conv_factor*.kdb*.high_beta in
  let mean_rate = conv_factor*.kdb*.mean_beta
    
    in
      pr "  <rate>\n";
      pr "    <number> %d </number>\n" irxn;
      pr "    <rate-constant>\n";
      pr "      <low> %g </low>\n" low_rate;
      pr "      <mean> %g </mean>\n" mean_rate;
      pr "      <high> %g </high>\n" high_rate;
      pr "    </rate-constant>\n";
      pr "    <reaction-probability>\n";
      pr "      <low> %g </low>\n" low_beta;
      pr "      <mean> %g </mean>\n" mean_beta;
      pr "      <high> %g </high>\n" high_beta;
      pr "    </reaction-probability>\n";
      pr "  </rate>\n";
      

done;;



pr "</rates>\n";;

