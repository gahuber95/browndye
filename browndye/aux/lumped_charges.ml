(* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*)

(*
Generates XML file of data structures of Chebyshev interpolation boxes
as described in paper.  This effectively lumps charges together to
make the simulation more efficient.

Takes the following arguments, with flags:

-max :  maximum number of points in box (default 40)
-pts : XML file of charged points;
-dist : optional distance grid from grid_distances program;
-thr : distance to diameter ratio for skipping inner boxes (default 2.0);
   
*)

let error msg = 
  raise (Failure msg)
;;

type particle = {
  pos: Vec3.t;
  q: float;
  induceable: bool;
}
;;

module JP = Jam_xml_ml_pull_parser;;

let float_from_node node = 
  JP.float_from_stream (JP.node_stream node)

;;

let some_of opt = 
  match opt with
    | Some obj -> obj
    | None -> error "some_of: nothing there"
;;



let childo node tag = 
  some_of (JP.child node tag)
;;


let vtags = ["x"; "y"; "z"];;

open Vec3;;

let filename_ref = ref "";;
let distname_ref = ref "";;
let threshhold_ref = ref 2.0;;

let max_n_in_box_ref = ref 40;; 


Arg.parse 
  [("-max", Arg.Set_int max_n_in_box_ref, 
   "maximum number of points in box, default 40");
   ("-pts", Arg.Set_string filename_ref, "charged points");
   ("-dist", Arg.Set_string distname_ref, "optional distance grid (from grid_distances program)");
   ("-thr", Arg.Set_float threshhold_ref, "distance to diameter ratio for skipping inner boxes (default 2.0)");
  ] 
  (fun fname -> Printf.fprintf stderr "no default arguments") 
  "Generates lumped force centers: lump_force_centers file.xml"
;;


let filename = !filename_ref;;
let distname = !distname_ref;;
let threshhold = !threshhold_ref;;

let gdistance_info_opt = 
  if (distname = "") then
    None
  else
    let fp = Scanf.Scanning.from_file distname in
      Some (Dgrid_parser.grid fp)
;;

let particles =
  let fp = Scanf.Scanning.from_file filename in
  let parts = ref [] in
    Point_parser.apply_to_points "charge" fp
      (fun ~x:x ~y:y ~z:z ~radius:r ~charge:q ~ptype:ptype ->
	let part = {
	  pos = v3 x y z;
	  q = q;
	  induceable = (ptype = "induceable");
	}
	in
	  parts := part :: (!parts)
      );
    !parts
;;

let total_charge = 
  List.fold_left
    (fun sum part ->
      sum +. part.q
    )
    0.0
    particles
;;

let centroid = 
  let total_abs_charge = 
    List.fold_left
      (fun sum part ->
	 sum +. (abs_float part.q) +. min_float
      )
      0.0
      particles
  in
    
  let abs_charge_pos_sum = 
    List.fold_left
      (fun sum part ->
	 vsum sum (vscaled ((abs_float part.q) +. min_float) part.pos)
      )
      (v3 0.0 0.0 0.0)
      particles
  in
    vscaled (1.0/.total_abs_charge) abs_charge_pos_sum
;;

type gen_box = 
  | Box of box
  | Particles of particle list
  | Empty

and box = {
  children: gen_box * gen_box;
  vcoefs: float array array array;
  fcoefs: Vec3.t array array array;
  tcoefs: Vec3.t array array array;
  positions:(float array)*(float array)*(float array);
  corners: Vec3.t * Vec3.t;
  parts: particle list;
}
;;

let corners_from_parts parts = 
  List.fold_left
    (fun res part ->
       let lcorn, hcorn = res in
       let pos = part.pos in
       let nlcorn = 
	 v3 (min lcorn.x pos.x) (min lcorn.y pos.y) (min lcorn.z pos.z) 
       and nhcorn = 
	 v3 (max hcorn.x pos.x) (max hcorn.y pos.y) (max hcorn.z pos.z) 
       in
	 nlcorn, nhcorn
    )
    ((v3 infinity infinity infinity), 
     (v3 neg_infinity neg_infinity neg_infinity)
    )
    parts
;;

let list_halves lst = 
  let nh = (List.length lst)/2 in
  let rec loop i lst0 lst1 = 
    if i = nh then
      lst0, lst1
    else
      match lst0 with
	| hd :: tail ->
	    loop (i+1) tail (hd::lst1)
	| [] -> lst0, lst1
  in
    loop 0 lst []
;;

let split dir parts = 
  let coord = 
    match dir with
      | 0 -> (fun part -> part.pos.x)
      | 1 -> (fun part -> part.pos.y)
      | 2 -> (fun part -> part.pos.z)
      | _ -> error "split"
  in
  let sparts = 
    List.sort 
      (fun p0 p1 -> 
	 compare (coord p0) (coord p1)
      ) 
      parts 
  in
    list_halves sparts 
;;
  
(****************************************************************)  
let array_init2 nx ny f =
  Array.init nx
    (fun ix ->
      Array.init ny (f ix)
    )
;;

let array_init3 nx ny nz f =
  Array.init nx
    (fun ix -> 
      array_init2 ny nz (f ix)
    )
;;

let top_order = 4;;

(* Chebyshev polynomials and their derivatives *)
let t0 = (fun x -> 1.0);;
let t1 = (fun x -> x);;
let t2 = (fun x -> 2.0*.x*.x -. 1.0);;
let t3 = (fun x -> 4.0*.x*.x*.x -. 3.0*.x);;

let dt0 = (fun x -> 0.0);;
let dt1 = (fun x -> 1.0);;
let dt2 = (fun x -> 4.0*.x);;
let dt3 = (fun x -> 12.0*.x*.x -. 3.0);;
let ts = [| t0; t1; t2; t3; |];;

let tt0 = (fun x -> 0.5)
let tts = [| tt0; t1; t2; t3; |];;


let dt0 = (fun x -> 0.0);;
let dt1 = (fun x -> 1.0);;
let dt2 = (fun x -> 4.0*.x);;
let dt3 = (fun x -> 12.0*.x*.x -. 3.0);;
let dts = [| dt0; dt1; dt2; dt3; |];;

let pi = 3.1415926;;

let zeros = 
  let zeros_f order =
    let n = order in
    let fn = float n in
    Array.init n
      (fun kkc ->
	let kk = (n-1)-kkc in
	let k = kk+1 in
        let fk = float k in
	let pif = pi*.(fk -. 0.5)/.fn in
	  cos pif
      )
  in
    Array.init (top_order+1) zeros_f
;;


(* left index is polynomial, right is location of zero *)
let ts_at_zeros = 
  let ts_at_zeros_f order =
    let n = order in
      array_init2 n n
	(fun i k -> 
	  ts.(i) zeros.(n).(k)
	)
  in
    Array.init (top_order+1) ts_at_zeros_f
;;
	
let ts_at_zeros_trans = 
  let ts_at_zeros_f order =
    let n = order in
      array_init2 n n
	(fun k i -> 
	  ts.(i) zeros.(n).(k)
	)
  in
    Array.init (top_order+1) ts_at_zeros_f
;;
	
let greens_fun3 = 
  Array.init (top_order+1)
    (fun n ->
      array_init3 n n n 
	(fun kx ky kz -> 
	  let ttxa = Array.init n (fun i -> 0.0)
	  and ttya = Array.init n (fun i -> 0.0)
	  and ttza = Array.init n (fun i -> 0.0)
	  in
	  let dttxa = Array.init n (fun i -> 0.0)
	  and dttya = Array.init n (fun i -> 0.0)
	  and dttza = Array.init n (fun i -> 0.0)
	  in
	    
	  let ts_at_zeros_n = ts_at_zeros_trans.(n) in
	  let ts_at_zeros_nx = ts_at_zeros_n.(kx) 
	  and ts_at_zeros_ny = ts_at_zeros_n.(ky) 
	  and ts_at_zeros_nz = ts_at_zeros_n.(kz) 
	  in  
	    (fun p ->
	      let x,y,z = p.x, p.y, p.z in
		
		for i = 0 to n-1 do
		  ttxa.(i) <- tts.(i) x;
		  ttya.(i) <- tts.(i) y;
		  ttza.(i) <- tts.(i) z;
		  dttxa.(i) <- dts.(i) x;
		  dttya.(i) <- dts.(i) y;
		  dttza.(i) <- dts.(i) z;
		done;
		
		let sum0, sumx, sumy, sumz = 
		  ref 0.0, ref 0.0, ref 0.0, ref 0.0 
		in
		  for ix = 0 to n-1 do
		    let ts_at_zeros_n_x = ts_at_zeros_nx.(ix) in
		    let ttx, dttx = ttxa.(ix), dttxa.(ix) in
		      for iy = 0 to n-1 do
			let ts_at_zeros_n_y = ts_at_zeros_ny.(iy) in
			let tty, dtty = ttya.(iy), dttya.(iy) in
			  for iz = 0 to n-1 do
			    let ts_at_zeros_n_z = ts_at_zeros_nz.(iz) in
			    let ttz, dttz = ttza.(iz), dttza.(iz) in
			    let tsro = 
			      ts_at_zeros_n_x*.ts_at_zeros_n_y*.ts_at_zeros_n_z
			    in
			      sum0 := !sum0 +. ttx*.tty*.ttz*.tsro;
			      sumx := !sumx +. dttx*.tty*.ttz*.tsro;
			      sumy := !sumy +. ttx*.dtty*.ttz*.tsro;
			      sumz := !sumz +. ttx*.tty*.dttz*.tsro;
			  done;
		      done;
		  done;
		  (!sum0), (-. !sumx), (-. !sumy), (-. !sumz)
	    )
	)
    )
;;

let force_n_torque_coefs order lc hc parts =
  let lx,ly,lz = lc.x, lc.y, lc.z in
  let wx = hc.x -. lx
  and wy = hc.y -. ly
  and wz = hc.z -. lz
  in
  let scaled_pos pos =
    let x = (2.0*.(pos.x -. lx)/.wx) -. 1.0
    and y = (2.0*.(pos.y -. ly)/.wy) -. 1.0
    and z = (2.0*.(pos.z -. lz)/.wz) -. 1.0
    in
      v3 x y z
  in
  let n = order in
  let fn = float n in
  let fn3 = fn*.fn*.fn in

  let ftcs = 
    array_init3 n n n
      (fun ix iy iz ->
	let g = greens_fun3.(n).(ix).(iy).(iz) 
	in
	let sumv, sumf, sumt = 
	  List.fold_left
	    (fun sum part ->
	      let sumv, sumf, sumt = sum in
	      let pos = scaled_pos part.pos in
	      (* let ex, ey, ez = gx pos, gy pos, gz pos in *)
	      let v,ex,ey,ez = g pos in
	      let e = v3 ex ey ez in

	      let sv = part.q *. v in
	      let sf = vscaled part.q e in
	      let f = v3 (2.0*.sf.x/.wx) (2.0*.sf.y/.wy) (2.0*.sf.z/.wz) in
		( sumv +. sv, (vsum sumf sf),(vsum sumt (cross part.pos f)) 
		)
	    )
	    (0.0, (v3 0.0 0.0 0.0), (v3 0.0 0.0 0.0))
	    parts
	in
	  ((8.0/.fn3)*.sumv,
	  (let scs = vscaled (8.0/.fn3) sumf in
	     v3 (2.0*.scs.x/.wx) (2.0*.scs.y/.wy) (2.0*.scs.z/.wz)
	  ), 
	  (vscaled (8.0/.fn3) sumt) 
	  )
      )
  in
  let vcs = 
    array_init3 n n n 
      (fun ix iy iz ->
	let vc,fc,tc = ftcs.(ix).(iy).(iz) in
	  vc
      )

  and fcs =
    array_init3 n n n 
      (fun ix iy iz ->
	let vc,fc,tc = ftcs.(ix).(iy).(iz) in
	  fc
      )

  and tcs =
    array_init3 n n n 
      (fun ix iy iz ->
	let vc,fc,tc = ftcs.(ix).(iy).(iz) in
	  tc
      )
  in
    vcs, fcs, tcs

;;

let max_n_in_box = !max_n_in_box_ref;;

let new_box order parts = 
  let lc, hc = corners_from_parts parts in 
  let d = vdiff hc lc in

  let n = order in

  let x_zeros = 
    Array.init n 
      (fun i -> 0.5*.d.x*.zeros.(n).(i) +. lc.x +. 0.5*.d.x)  
  and y_zeros = 
    Array.init n 
      (fun i -> 0.5*.d.y*.zeros.(n).(i) +. lc.y +. 0.5*.d.y)  
  and z_zeros = 
    Array.init n 
      (fun i -> 0.5*.d.z*.zeros.(n).(i) +. lc.z +. 0.5*.d.z)  
  in
  let vcoefs, fcoefs, tcoefs = force_n_torque_coefs n lc hc parts in

    {
      children = Empty, Empty;
      parts = parts;
      vcoefs = vcoefs; 
      fcoefs = fcoefs; (* force_coefs n lc hc parts; *)
      tcoefs = tcoefs; (* torque_coefs n lc hc parts; *)
      corners = lc,hc;
      positions = x_zeros, y_zeros, z_zeros;
	
    }
;;
    
let rec split_box box =

  let inner_split () = 
    let parts = box.parts in
      if (List.length parts) <= max_n_in_box then
	Particles parts
      else
	let lc,hc = box.corners in
	let d = vdiff hc lc in
	let dir = 
	  let dmax = max (max d.x d.y) d.z in
	    if dmax = d.x then
	      0
	    else if dmax = d.y then
	      1
	  else
	      2
	in
	let parts0, parts1 = split dir parts in
	let child0 = split_box (new_box top_order parts0)
	and child1 = split_box (new_box top_order parts1) in
	  Box {
	    box with
	      children = child0, child1;
	      parts = [];
	  }

  in
    match gdistance_info_opt with
      | None -> 
	  inner_split()

      | Some gdistance_info ->
	  let cx,cy,cz = 
	    (let c = gdistance_info.Dgrid_parser.corner in
	       c.Vec3.x, c.Vec3.y, c.Vec3.z);
	  and hx,hy,hz = gdistance_info.Dgrid_parser.spacing
	  and gridd = gdistance_info.Dgrid_parser.data
	  in
	  let diameter = 
	    let lcorn, hcorn = box.corners in
	      Vec3.distance lcorn hcorn
	  in

	  let fmin (a: float) (b:float) = 
	    if a < b then a else b
	  in

	  let center = 
	    let lc,hc = box.corners in
	      Vec3.vscaled 0.5 (Vec3.vsum lc hc)
	  in
	  let ix,iy,iz = 
	    let x,y,z = center.Vec3.x, center.Vec3.y, center.Vec3.z in
	    int_of_float ((x -. cx)/.hx),
	    int_of_float ((y -. cy)/.hy),
	    int_of_float ((z -. cz)/.hz)    
	  in

	  let box_dist = ref infinity in
	    for kx = 0 to 1 do
	      for ky = 0 to 1 do
		for kz = 0 to 1 do
		  box_dist := fmin !box_dist gridd.{ix+kx, iy+ky, iz+kz};
		done;
	      done;
	    done;

	    if !box_dist < threshhold*.diameter then
	      inner_split()
	    else (
	      if (List.length box.parts) < max_n_in_box then
		Particles box.parts
	      else
		Box {
		  box with
		    children = Empty, Empty;
		    parts = [];
		}
	    )	      
;;

let psp sp = 
  for i = 1 to sp do
    Printf.printf " ";
  done
;;

let print_vec3 sp v = 
  psp sp; Printf.printf "<x>%g</x> <y>%g</y> <z>%g</z>\n" v.x v.y v.z
;;

let print_float sp v = 
  psp sp; Printf.printf "%g\n" v;
;;

let print_coefs print_fun sp coefs = 
  Array.iteri 
    (fun ix plane ->
       psp sp; Printf.printf "<xc i=\"%d\">\n" ix;
       Array.iteri
	 (fun iy row ->
	    psp (sp+2); Printf.printf "<yc i=\"%d\">\n" iy;
	    Array.iteri 
	      (fun iz v -> 
		 psp (sp+4); Printf.printf "<zc i=\"%d\">\n" iz;
		 print_fun (sp+6) v;			  
	         psp (sp+4); Printf.printf "</zc>\n";
	      )
	      row;
	    psp (sp+2); Printf.printf "</yc>\n";
	 )
	 plane;
       psp sp; Printf.printf "</xc>\n";
    )
    coefs
;;

let print_particles sp parts = 
  let otag i tag = 
    psp (sp+2*i); Printf.printf "<%s>\n" tag;
  and ctag i tag = 
    psp (sp+2*i); Printf.printf "</%s>\n" tag
  in
    List.iter
      (fun part ->
	Printf.printf "          <point type=\"%s\">\n" 
	  (if part.induceable then "induced" else "permanent");
	
	otag 1 "pos";
	print_vec3 (sp + 2*2) part.pos;
	ctag 1 "pos";
	
	otag 1 "q";
	psp (sp + 2*2); Printf.printf "%g\n" part.q;
	ctag 1 "q";
	
	ctag 0 "point";
      )
      parts;
;;

let print_array sp a =
  psp sp;
  Array.iter (fun x -> Printf.printf "%g " x) a;
  Printf.printf "\n";
;;

let rec print_gbox order sp gbox =
  let otag i tag = 
    psp (sp+2*i); Printf.printf "<%s>\n" tag;
  and ctag i tag = 
    psp (sp+2*i); Printf.printf "</%s>\n" tag;
  in

    match gbox with
      | Empty ->
	  error "print gbox: Empty"

      | Box box -> (
	  otag 0 "cbox";
	  psp (sp + 2*1); Printf.printf "<order> %d </order>\n" order;
	    

	  let lc, hc = box.corners in
	    otag 1 "corners";
	    otag 2 "low";
	    print_vec3 (sp + 2*3) lc;
	    ctag 2 "low";
	    
	    otag 2 "high";
	    print_vec3 (sp + 2*3) hc;
	    ctag 2 "high";
	    
	    ctag 1 "corners";
	      
	    otag 1 "coefs";
	    otag 2 "potential";
	    print_coefs print_float (sp + 2*3) box.vcoefs;
	    ctag 2 "potential";

	    otag 2 "force";
	    print_coefs print_vec3 (sp + 2*3) box.fcoefs;
	    ctag 2 "force";
	    otag 2 "torque";
	    print_coefs print_vec3 (sp + 2*3) box.tcoefs;
	    ctag 2 "torque";
	    
	    ctag 1 "coefs";
	    
	    otag 1 "positions";
	    let xp,yp,zp = box.positions in
	      otag 2 "xc";
	      print_array (sp + 2*3) xp;
	      ctag 2 "xc";
	      otag 2 "yc";
	      print_array (sp + 2*3) yp;
	      ctag 2 "yc";
	      otag 2 "zc";
	      print_array (sp + 2*3) zp;
	      ctag 2 "zc";
	      ctag 1 "positions";
	      
	      let child0, child1 = box.children in
		(match child0, child1 with
		  | Empty, Empty -> ()
		  | _, _ -> 
		      otag 1 "children";
		      print_gbox order (sp+2*2) child0;
		      print_gbox order (sp+2*2) child1;
		      ctag 1 "children";
		);
		ctag 0 "cbox"
	);
	  
      | Particles parts -> (
	  otag 0 "pbox";
	  otag 1 "points";
	  psp (sp + 2*2); Printf.printf "<n> %d </n>\n" (List.length parts);
	  print_particles (sp + 2*2) parts;
	  ctag 1 "points";
	  ctag 0 "pbox";
	)
	  
;;


let box1 = new_box (top_order-1) particles;;

let box0 = split_box (new_box top_order particles);;


Printf.printf "<roottag>\n";
Printf.printf "  <total-charge> %g </total-charge>\n" total_charge;
Printf.printf "  <centroid>\n";
print_vec3 4 centroid;
Printf.printf "  </centroid>\n";

if (List.length particles) > max_n_in_box then (
  Printf.printf "  <om1_box>\n";
  print_gbox (top_order-1) 4 (Box box1);
  Printf.printf "  </om1_box>\n";
);  

Printf.printf "  <o_box>\n";
print_gbox top_order 4 box0;
Printf.printf "  </o_box>\n";

Printf.printf "</roottag>\n";

(********************************************************)
(* Test code *)
(*

let parts = 
  let poses = ref [] in
    for ix = 1 to 10 do
      for iy = 1 to 10 do
	for iz = 1 to 10 do
	  let x,y,z = float ix, float iy, float iz in
	    poses := (v3 x y z)::(!poses)
	done;
      done;
    done;
    List.map 
      (fun pos -> {
	 pos = pos;
	 fcenter = Charge (if pos.x > 5.0 then 1.0 else -1.0);
       }
      )
      !poses
;;

let charge_of part = 
  match part.fcenter with
    | Charge q -> q
    | _ -> error "charge_of_parg"
;;

Printf.printf "<rootag>\n";;
print_particles 2 parts;;
Printf.printf "</rootag>\n";;


let box0 = split_box (new_box parts);;

let qpos = v3 0.0 0.0 20.0;;

let deb = 10.0;;

let force = 
  let fref = ref (v3 0.0 0.0 0.0) in
    for ix = 0 to order-1 do
      for iy = 0 to order-1 do
	for iz = 0 to order-1 do
	  let r = vdiff box0.positions.(ix).(iy).(iz) qpos in
	  let rmag = vnorm r in
	  let v = (exp(-.rmag/.deb))/.rmag in
	    fref := vsum !fref (vscaled v box0.fcoefs.(ix).(iy).(iz))
	done;
      done;
    done;
    !fref
;;

Printf.printf "force %g %g %g\n" force.x force.y force.z;;


let real_force = 
  List.fold_left
    (fun sum part ->
       
       let r = vdiff part.pos qpos in
       let rmag = vnorm r in
       let dvdror = 
	 -.(exp (-.rmag/.deb))*.(1.0/.(deb*.rmag) +. 
				 1.0/.(rmag*.rmag))/.rmag in 
       let f = vscaled (dvdror*.(charge_of part)) r in
	 vsum sum f
    )
    (v3 0.0 0.0 0.0)
    parts
;;
  
Printf.printf "real force %g %g %g\n" real_force.x real_force.y real_force.z;;


let torque = 
  let fref = ref (v3 0.0 0.0 0.0) in
    for ix = 0 to order-1 do
      for iy = 0 to order-1 do
	for iz = 0 to order-1 do
	  let r = vdiff box0.positions.(ix).(iy).(iz) qpos in
	  let rmag = vnorm r in
	  let v = (exp (-.rmag/.deb))/.rmag in
	    fref := vsum !fref (vscaled v box0.tcoefs.(ix).(iy).(iz))
	done;
      done;
    done;
    !fref
;;

Printf.printf "torque %g %g %g\n" torque.x torque.y torque.z;;

let real_torque = 
  List.fold_left
    (fun sum part ->
       let ppos = part.pos in
       let r = vdiff ppos qpos in
       let rmag = vnorm r in
       let dvdror = -.(exp (-.rmag/.deb))*.(1.0/.(deb*.rmag) +. 1.0/.(rmag*.rmag))/.rmag in 
       let f = vscaled (dvdror*.(charge_of part)) r in
	 vsum sum (cross ppos f)
    )
    (v3 0.0 0.0 0.0)
    parts
;;

Printf.printf "real torque %g %g %g\n" 
  real_torque.x real_torque.y real_torque.z;;
*)
