(* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*)


(*
Generates effective volumes from spheres and solvent exclusion grid. If both dielectrics are set, result is multiplied by the dielectric-dependent scale factor
to be used for desolvation forces.

Takes the following arguments with flags:

-spheres: pqrxml file with spheres
-inside: inside points file
-rgyration: compute radius of gyration for whole system
-solvent: solvent dielectric (dimensionless), default 1
-solute: solute dielectric (dimensionless), default 1

and outputs the result to standard output
  
*)

let pr = Printf.printf;;
let fl() = flush stdout;;

let len = Array.length;;
let fold = Array.fold_left;;

let sphere_file_ref = ref "";;
let grid_file_ref = ref "";;
let rgyration_ref = ref false;;
let solvent_eps_ref = ref 1.0;;
let solute_eps_ref = ref 1.0;;

Arg.parse
  [("-spheres", Arg.Set_string sphere_file_ref, " xml file with spheres");
   ("-inside", Arg.Set_string grid_file_ref, " inside points file");
   ("-rgyration", Arg.Set rgyration_ref, " compute radius of gyration for whole system");
   ("-solvent", Arg.Set_float solvent_eps_ref, " solvent dielectric (dimensionless), default 1");
   ("-solute", Arg.Set_float solute_eps_ref, " solute dielectric (dimensionless), default 1");

  ]
  (fun arg -> raise (Failure "no anonymous arguments"))
  "Generates effective volumes from spheres and solvent exclusion grid. If both dielectrics are set, result is scaled to be used for desolvation forces"
;;

Random.init 1111111;;

let solvent_eps = !solvent_eps_ref;;
let solute_eps = !solute_eps_ref;;
let do_rgyration = !rgyration_ref;;
let sphere_file = !sphere_file_ref;;
let grid_file = !grid_file_ref;;

type sphere = {
  pos: Vec3.t;
  radius: float;
  mutable eff_volume: float;
};;

open Vec3;;

let spheres =
  let buf = Scanf.Scanning.from_file sphere_file in
  let lst = ref [] in
    Atom_parser.apply_to_atoms
      buf
      (fun ~rname:rname ~rnumber:rnumber ~aname:aname ~anumber:anum 
	~x:x ~y:y ~z:z ~radius:r ~charge:q ->
	let pos = v3 x y z in
	let new_sphere = {
	  pos = pos;
	  radius = r;
	  eff_volume = 0.0;
	}
	  
	in
	  lst := new_sphere :: (!lst)
      );
    Array.of_list (List.rev !lst)
;;

let array_init2 nx ny f =
  Array.init nx
    (fun ix ->
      Array.init ny (f ix)
    )
;;

let array_init3 nx ny nz f =
  Array.init nx
    (fun ix -> 
      array_init2 ny nz (f ix)
    )
;;

open Bigarray;;

let igrid = Igrid_parser.grid true (Scanf.Scanning.from_file grid_file);;
let sol_exclusion = igrid.Igrid_parser.data;;
let exrad = igrid.Igrid_parser.exclusion_radius;;
let nx,ny,nz = Array3.dim1 sol_exclusion, Array3.dim2 sol_exclusion, Array3.dim3 sol_exclusion
  (* (len sol_exclusion), (len sol_exclusion.(0)), (len sol_exclusion.(0).(0)) *)
;;


let hx,hy,hz = igrid.Igrid_parser.spacing;;

let lowx,lowy,lowz = igrid.Igrid_parser.corner ;;
let low_bound = v3 lowx lowy lowz ;;

let high_bound = 
  let hgx = lowx +. 1.00*.hx*.(float (nx-1))
  and hgy = lowy +. 1.00*.hy*.(float (ny-1))
  and hgz = lowz +. 1.00*.hz*.(float (nz-1))
  in
    v3 hgx hgy hgz
;;



(* ******************************************** *)
(* check input *)
(
  let chb low high x r = 
    if (x -.r < low || x +. r > high) then (
      raise (Failure "spheres are not in bounds")
    )
  in

  Array.iter
    (fun sph -> 
      chb low_bound.x high_bound.x  sph.pos.x sph.radius;
      chb low_bound.y high_bound.y  sph.pos.y sph.radius;
      chb low_bound.z high_bound.z  sph.pos.z sph.radius;
    )
    spheres
)
;;

(* ******************************************** *)

let sq x = x*.x;;

if do_rgyration then (* just compute radius of gyration *) (
  let pos = [| 0.0; 0.0; 0.0 |] in
  let np = ref 0 in
    
    for ix = 0 to nx-1 do
      let x = hx*.(float ix) in
      let solx = Array3.slice_left_2 sol_exclusion ix (* sol_exclusion.(ix) *) in 
	for iy = 0 to ny-1 do
	  let solxy = Array2.slice_left solx iy (* solx.(iy) *) in
	  let y = hy*.(float iy) in
	    for iz = 0 to nz-1 do
	      let sol = solxy.{iz} in
	      let z = hz*.(float iz) in
		if sol = 1 then ( 
		  np := !np + 1;
		  pos.(0) <- pos.(0) +. x;
		  pos.(1) <- pos.(1) +. y;
		  pos.(2) <- pos.(2) +. z;
		)  
		done;
	done;
    done;
    
    let fn = float !np in
      pos.(0) <- pos.(0)/.fn;
      pos.(1) <- pos.(1)/.fn;
      pos.(2) <- pos.(2)/.fn;
      
      let r2 = ref 0.0 in
	
	for ix = 0 to nx-1 do
	  let x = hx*.(float ix) in
          let solx = Array3.slice_left_2 sol_exclusion ix in 
	    for iy = 0 to ny-1 do
	      let solxy = Array2.slice_left solx iy in
	      let y = hy*.(float iy) in
		for iz = 0 to nz-1 do
		  let sol = solxy.{iz} in
		  let z = hz*.(float iz) in
		    if sol = 1 then 
		      r2 := !r2 +. 
			(sq (x -. pos.(0))) +. (sq (y -. pos.(1))) +. 
			(sq (z -. pos.(2))); 
		    
		done;
	    done;
	done;
	
	r2 := !r2/.fn;
	Printf.printf "<roottag>\n";
	Printf.printf "  <radius>%g </radius>\n" (sqrt !r2);
	Printf.printf "  <x>%g </x> <y>%g </y> <z>%g </z>\n"
	  pos.(0) pos.(1) pos.(2);
	Printf.printf "</roottag>\n";
)
else (* compute volumes for each atom *) (

  let width = vdiff high_bound low_bound 
  in
    
  (* bounding boxes for grid assignment search *)
  let boxes, pt_indices =
    
    let max_rad = 
      let r = 
	fold
	  (fun res sph ->
	    max res sph.radius
	  )
	  0.0
	  spheres
      in
	r +. exrad
    in
	
    let nbx = int_of_float (width.x/.max_rad)
    and nby = int_of_float (width.y/.max_rad)
    and nbz = int_of_float (width.z/.max_rad)
    in
      (* use a 3-D array of boxes containing the spheres, so 
	 that when a grid point can be assigned to a sphere,
	 only the neighboring boxes need to be searched *)


    let boxes = 
      array_init3 nbx nby nbz
	(fun ix iy iz -> ref [])
    in

    let pt_indices pos = 
      let ix = 
	int_of_float ((float_of_int nbx)*.(pos.x -. low_bound.x)/.width.x)
      and iy = 
	int_of_float ((float_of_int nby)*.(pos.y -. low_bound.y)/.width.y)
      and iz = 
	int_of_float ((float_of_int nbz)*.(pos.z -. low_bound.z)/.width.z)
      in
	ix,iy,iz
    in
      Array.iteri
	(fun i sph ->
	  let ix,iy,iz = pt_indices sph.pos in
	  let box = boxes.(ix).(iy).(iz) in
	    box := sph :: (!box)
	)
	spheres;
      boxes, pt_indices
  in

    (* apply f to spheres in neighboring boxes of pos *)
    let fold_nebors pos f res0 = 
      let nx,ny,nz = (len boxes), (len boxes.(0)), (len boxes.(0).(0)) in
	
      let ix,iy,iz = pt_indices pos in
	if (ix < 0 || nx <= ix) ||  (iy < 0 || ny <= iy) ||  
	  (iz < 0 || nz <= iz)
	then  
	  res0
	else
	  let nxb,nxe = max 0 (ix-1), min (nx-1) (ix+1) 
	  and nyb,nye = max 0 (iy-1), min (ny-1) (iy+1) 
	  and nzb,nze = max 0 (iz-1), min (nz-1) (iz+1)
	  in
	  let res = ref res0 in
	    for kx = nxb to nxe do
	      for ky = nyb to nye do
		for kz = nzb to nze do
		  let box = !(boxes.(kx).(ky).(kz)) in
		    res := List.fold_left f !res box;
		done;
	      done;
	    done;
	    !res
    in

    let h3 = hx*.hy*.hz in 
    let n_per_box = 10 in

      (* go through all grid-boxes and find those with an inside point *)
      for ix = 0 to nx-2 do
	for iy = 0 to ny-2 do
	  for iz = 0 to nz-2 do

	    let nsolex = 
	      let ns = ref 0 in 
		for kx = 0 to 1 do
		  for ky = 0 to 1 do
		    for kz = 0 to 1 do
		      ns := !ns + sol_exclusion.{ix+kx,iy+ky,iz+kz} 
		    done;
		  done;
		done;
		!ns
	    in
	      if nsolex > 0 then
		let x = low_bound.x +. hx*.( (float ix) +. 0.5)
		and y = low_bound.y +. hy*.( (float iy) +. 0.5)
		and z = low_bound.z +. hz*.( (float iz) +. 0.5)
		in
		  (* if is completely inside molecule, whole box is
		     assigned to sphere nearest point *)
		  if nsolex = 8 then
		    let pos = v3 x y z in
		    let d, min_sph_opt = 
		      fold_nebors pos
			(fun res sph ->
			  let min_d, min_sph_opt = res in
			  let d = distance sph.pos pos in
			    if d < min_d then 
			      d, (Some sph)
			    else
			      res
			)
			(infinity, None)
		    in
		      match min_sph_opt with
			| Some min_sph ->
			    min_sph.eff_volume <- min_sph.eff_volume +. h3
			| _ -> ()
		    
		  else (* nsolex > 0 *)
		    (* otherwise, random points are generated in
		       box, tested to see if they are inside, and
		    their portion of volume assigned to nearest sphere *)
		    for isamp = 1 to n_per_box do
		      let xx = x +. (Random.float hx) -. 0.5*.hx 
		      and yy = y +. (Random.float hy) -. 0.5*.hy 
		      and zz = z +. (Random.float hz) -. 0.5*.hz 
		      in
		      let pos = v3 xx yy zz in
		      let d, min_sph_opt = 
			fold_nebors pos
			  (fun res sph ->
			    let min_d, min_sph_opt = res in
			    let d = distance sph.pos pos in
			      match d < min_d, d < sph.radius +. exrad with
				| true, true -> 
				    d, (Some sph)
				| true, false ->
				    d, min_sph_opt
				| _,_ -> 
				    res
			  )
			  (infinity, None)
		      in
			match min_sph_opt with
			  | Some min_sph ->
			      min_sph.eff_volume <- min_sph.eff_volume +. 
				h3/.(float n_per_box)
			  | _ -> ()
			      

		    done;

	  done;
	done;
      done;

      let scale_for_desolve = 
	match classify_float solvent_eps, classify_float solute_eps with
	  | FP_nan, FP_nan -> false
	  | _, _ -> true
      in

	Printf.printf "<!-- \"charge\" is (scaled) effective volume -->\n";
	Printf.printf "<roottag>\n";
	if scale_for_desolve then (
	  Printf.printf "  <solvent-dielectric> %g </solvent-dielectric>\n" solvent_eps;
	  Printf.printf "  <solute-dielectric> %g </solute-dielectric>\n" solute_eps;
	);
	Array.iter
	  (fun sph ->
	    
	    let pos = sph.pos in
	      
	      Printf.printf "  <point>\n";
	      Printf.printf "    <type> permanent </type>\n";
	      Printf.printf "    <x> %g </x> <y> %g </y> <z> %g </z>\n" 
		pos.x pos.y pos.z; 

	      let vol = if scale_for_desolve then
		let debug_factor = 8.0/.6.7 in
		let factor = 
		  3.0*.solvent_eps*.( solvent_eps -. solute_eps)/.
		    ( 2.0*.solvent_eps +. solute_eps)
		in
		  debug_factor*.factor*.sph.eff_volume
		else
		  sph.eff_volume
	      in
		Printf.printf "    <charge> %g </charge>\n" vol;
		Printf.printf "    <volume> %g </volume>\n" sph.eff_volume;
		Printf.printf "    <radius> %g </radius>\n" sph.radius;
		Printf.printf "  </point>\n";
	  )
	  spheres
	;
	
	Printf.printf "</roottag>\n";
)

