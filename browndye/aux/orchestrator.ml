(* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*)

(*
  Used internally.  The Orchestrator is a "gmake"-like utility that
  can call programs and update files.  It can also extract information
  from XML files, process that information, and pass it on as arguments
  to programs downstream.  It is used by "bd_top".
  The beginnings of documentation is found in
  doc/orchestrator.html
*)

type data = 
  | Int of int
  | Float of float
  | String of string
  | Float3 of float*float*float
  | Bool of bool
  | Null

type source_option_t = Ordinary | Pos_Option | Neg_Option

type source = 
  | Func2_Source of func2_source
  | Func1_Source of func1_source
  | Command_Source of command_source
  | In_File_Source of in_file_source

  | In_Var_Source of in_var_source

  | Const_Float_Source of float
  | Const_Int_Source of int
  | Const_String_Source of string
  | In_2File_Var_Source of in_2file_var_source

  | Alt_Source of alt_source
  | Cond_Source of cond_source

and in_file_source = 
  | IFString of string
  | IFSource of source

and func2_source = {
  prereq0: source;
  prereq1: source;
  f2: data->data->data;
}

and func1_source = {
  prereq: source;
  f1: data->data;
}

and command_source = {
  command: string;
  prereqs: (string, source*source_option_t) Hashtbl.t;
  mutable stdin_prereq: source option; 
  std_out: string;
}

and comorif_source = Command of command_source | In_File of in_file_source

and alt_source = {
  source0: source;
  source1: source;
}

and cond_source = {
  isource0: source;
  isource1: source;
  csource: source;
}

and in_var_source = {
  fname: comorif_source;
  fpath: string list;
  findicator: data;
}

and in_2file_var_source = {
  ffname0: string;
  ffname1: string;
  f2path: string list;
  indicator: data;
}



(* ****************************************************** *)
let rec if_name ifn = 
  match ifn with
    | IFString name -> name
    | IFSource source -> file_name source

and cof_name cof = 
  match cof with
    | In_File iffile -> if_name iffile
    | Command csource -> csource.std_out

and file_name source = 
  match source with
    | In_File_Source ifsource -> if_name ifsource
    | In_Var_Source ivsource -> cof_name ivsource.fname
    | Command_Source csource -> csource.std_out
    | _ -> raise (Failure "no file name for this source type")

let new_command_source command std_out = 
  Command_Source
    {
      command = command;
      prereqs = Hashtbl.create 0;
      stdin_prereq = None;
      std_out = std_out;
    }

let new_alt_source source0 source1 = 
  Alt_Source {
    source0 = source0;
    source1 = source1;
  }

let new_cond_source csource source0 source1 = 
  Cond_Source {
    csource = csource;
    isource0 = source0;
    isource1 = source1;
  }
    


let new_in_file_source name = 
  In_File_Source (IFString name)

let in_file_of_source source = 
  In_File_Source (IFSource source)

let new_func2_source f2 prereq0 prereq1 =
  Func2_Source
    {
      f2 = f2;
      prereq0 = prereq0;
      prereq1 = prereq1;
    }

let new_func1_source f1 prereq =
  Func1_Source
    {
      f1 = f1;
      prereq = prereq;
    }

let comorif_of_source source = 
  match source with
    | Command_Source csource -> Command csource
    | In_File_Source ifsource -> In_File ifsource 
    | _ -> raise (Failure "comorif_source: type not used")

let new_in_string_var_source source path = 
  In_Var_Source
    {
      fname = comorif_of_source source;
      fpath = path;
      findicator = String "";
    }

let new_in_int_var_source source path =
  In_Var_Source
    {
      fname = comorif_of_source source;
      fpath = path;
      findicator = Int 0;
    }

let new_in_float_var_source source path =
  In_Var_Source
    {
      fname = comorif_of_source source;
      fpath = path;
      findicator = Float 0.0;
    }

let new_in_float3_var_source source path =
  In_Var_Source
    {
      fname = comorif_of_source source;
      fpath = path;
      findicator = Float3 (0.0,0.0,0.0);
    }

let new_in_2file_var_source fname0 fname1 path ind = 
  In_2File_Var_Source
    {
      ffname0 = fname0;
      ffname1 = fname1;
      f2path = path;
      indicator = ind;
    }
    
let new_in_2file_int_var_source fname0 fname1 path = 
  new_in_2file_var_source fname0 fname1 path (Int 0)

let new_in_2file_float_var_source fname0 fname1 path = 
  new_in_2file_var_source fname0 fname1 path (Float 0.0)

let new_in_2file_string_var_source fname0 fname1 path = 
  new_in_2file_var_source fname0 fname1 path (String "")


let new_file_source_of_string source = 
  In_File_Source (IFSource source) 

let add_prereq source flag psource = 
  match source with
    | Command_Source csource ->
      Hashtbl.add csource.prereqs flag (psource,Ordinary)
    | _ ->
      raise (Failure "add_prereq: must be command source")

let add_opt_prereq source flag psource = 
  match source with
    | Command_Source csource ->
      Hashtbl.add csource.prereqs flag (psource,Pos_Option)
    | _ ->
      raise (Failure "add_prereq: must be command source")

let add_neg_opt_prereq source flag psource = 
  match source with
    | Command_Source csource ->
      Hashtbl.add csource.prereqs flag (psource,Neg_Option)
    | _ ->
      raise (Failure "add_prereq: must be command source")

let add_stdin_prereq source psource = 
  match source with
    | Command_Source csource ->
      csource.stdin_prereq <- Some psource
    | _ ->
      raise (Failure "add_stdin_prereq: must be command source")
	
let string_of_data x =
  match x with
    | String s -> s
    | Int i -> 
      Printf.sprintf "%d" i
    | Float f ->
      Printf.sprintf "%g" f
    | Float3 (x,y,z) ->
      Printf.sprintf "%g %g %g" x y z
    | Bool tf -> Printf.sprintf "%b" tf
    | Null -> raise (Failure "string_of_data: no value")

let file_date name =
  if Sys.file_exists name then
    let stats = Unix.stat name in
    stats.Unix.st_mtime
  else
    0.0


let int_of_string s = 
  Scanf.sscanf s "%d" (fun i -> i)


let float_of_string s = 
  Scanf.sscanf s "%g" (fun x -> x)


module JP = Jam_xml_ml_pull_parser


let rec string_from_xml_source xsource path = 
  match path with
    | [] -> 
      String (JP.string_from_stream (JP.node_stream xsource))
    | head :: tail ->
      match JP.child xsource head with
	| None -> Null
	| Some csource -> 
	  string_from_xml_source csource tail

let str_is_true str = 
  str = "true" || str = "True" || str = "TRUE"

let str_is_false str = 
  str = "false" || str = "False" || str = "FALSE"

let bool_from_xml_source xsource path = 
  match string_from_xml_source xsource path with
    | String tfstring ->
       if str_is_true tfstring then
	 Bool true
       else if str_is_false tfstring then
	 Bool false
       else
	 raise (Failure ("string "^tfstring^" in xml file does not match boolean value"))
	  
    | _ -> raise (Failure "bool_from_xml_source: should not get here")
      

let rec float_from_xml_source xsource path = 
  match path with
    | [] -> 
      Float (JP.float_from_stream (JP.node_stream xsource))

    | head :: tail ->
      match JP.child xsource head with
	| None -> Null
	| Some csource -> 
	  float_from_xml_source csource tail
	    
let rec int_from_xml_source xsource path = 
  match path with
    | [] -> 
      Int (JP.int_from_stream (JP.node_stream xsource))

    | head :: tail ->
      match JP.child xsource head with
	| None -> Null
	| Some csource -> 
	  int_from_xml_source csource tail
	    
let rec float3_from_xml_source xsource path = 
  match path with
    | [] -> 
      let x,y,z = JP.float3_from_stream (JP.node_stream xsource) in
      Float3 (x,y,z)

    | head :: tail ->
      match JP.child xsource head with
	| None -> Null
	| Some csource -> 
	  float3_from_xml_source csource tail

let val_from_xml_file get filename path =
  if Sys.file_exists filename then
    let buf = Scanf.Scanning.from_file filename in
    let parser = JP.new_parser buf in
    JP.complete_current_node parser;
    let top_source = JP.current_node parser in
    try
      get top_source path
    with ex -> (
      Printf.fprintf stderr "problem in file %s at tags:\n" filename;
      Printf.fprintf stderr "%s\n" (JP.node_tag top_source);
      List.iter (fun tag -> Printf.fprintf stderr "%s " tag) path;
      Printf.fprintf stderr "\n";
      raise ex
    )  
  else
    Null
      
let string_from_xml_file = 
  val_from_xml_file string_from_xml_source

let float_from_xml_file = 
  val_from_xml_file float_from_xml_source

let int_from_xml_file = 
  val_from_xml_file int_from_xml_source

let float3_from_xml_file = 
  val_from_xml_file float3_from_xml_source

let bool_from_xml_file = 
  val_from_xml_file bool_from_xml_source

let in_var_source_value ivsource = 
  let fname = cof_name ivsource.fname in
  let path = ivsource.fpath in
  match ivsource.findicator with
    | String s -> string_from_xml_file fname path
    | Int i -> int_from_xml_file fname path
    | Float f -> float_from_xml_file fname path
    | Float3 (f0,f1,f2) -> float3_from_xml_file fname path
    | Bool tf -> bool_from_xml_file fname path
    | Null -> raise (Failure "in_var_source: should not get here")
      
(* the part that gets put after the -flag *)
let rec source_value source = 
  match source with
    | Command_Source csource ->
      String csource.std_out

    | In_File_Source ifsource -> if_source_value ifsource

    | In_Var_Source ivsource -> in_var_source_value ivsource

    | In_2File_Var_Source ivsource ->
      let fdate0 = file_date ivsource.ffname0
      and fdate1 = file_date ivsource.ffname1
      in
      let ffname = 
	if fdate0 > fdate1 then ivsource.ffname0 else ivsource.ffname1
      and path = ivsource.f2path 
      in
      (
	match ivsource.indicator with 
	  | Float f -> float_from_xml_file ffname path
	  | Int i -> int_from_xml_file ffname path
	  | String s -> string_from_xml_file ffname path
	  | Float3 (x0,x1,x2) -> float3_from_xml_file ffname path
	  | Bool tf -> bool_from_xml_file ffname path
	  | Null -> Null
      )
	

    | Func2_Source fsource ->
      fsource.f2 (source_value fsource.prereq0) (source_value fsource.prereq1)
    | Func1_Source fsource ->
      fsource.f1 (source_value fsource.prereq)

    | Const_Float_Source f -> Float f
    | Const_Int_Source i -> Int i
    | Const_String_Source s -> String s

    | Alt_Source asource ->
      let value = source_value asource.source0 in
      (match value with
	| Null -> source_value asource.source1
	| _ -> value
      )

    | Cond_Source isource ->
      let cond = source_value isource.csource in
      (match cond with
	| Bool tf ->
	  if tf then
	    source_value isource.isource0
	  else
	    source_value isource.isource1
	| _ ->
	  raise (Failure "Cond_Source condition source must be boolean")
      )

and if_source_value ifsource =
  match ifsource with
    | IFString name -> String name
    | IFSource source -> source_value source

let in_2file_var_source_date source =
  let exists0 = Sys.file_exists source.ffname0
  and exists1 = Sys.file_exists source.ffname1
  in
  if exists0 && exists1 then      
    let date0, date1 = file_date source.ffname0, file_date source.ffname1 in
    let value fname = 
      string_from_xml_file fname source.f2path
    in
    let value0, value1 = value source.ffname0, value source.ffname1 in
    if value0 = value1 then
      0.0
    else
      max date0 date1
	
  else (* one file does not exist *) (
    if exists0 then
      file_date source.ffname0
    else if exists1 then
      file_date source.ffname1
    else
      raise (Failure "in_2file_var_source_date: no file exists")	  
  )
    
    
(* returns date-time *)

let rec update_inner source =
  let if_date ifn = (
    (match ifn with
      | IFString name -> file_date name
      | IFSource source ->
	let nvalue = source_value source in
	match nvalue with
	  | String name -> 
	    let fdate = file_date name 
	    and ndate = update_inner source in
	    max fdate ndate
	  | _ -> 
	    raise (Failure "source of in file source must have string value")
    )
  )
  in

  let cof_date cof =
    match cof with
      | In_File iffile -> if_date iffile
      | Command csource ->
	update_inner  (Command_Source csource)
	  
  in
  match source with
    | Command_Source csource -> (

      let old_date = file_date csource.std_out in
      
      let stdin_prereq_date = 
	match csource.stdin_prereq with
	  | None -> 0.0
	  | Some stdin_prereq -> update_inner stdin_prereq
      in
      let prereqs_date =
	Hashtbl.fold
	  (fun flag sourceop date ->
	    let source, op = sourceop in
	    let source_date = update_inner  source in
	    max date source_date
	  )
	  csource.prereqs
	  (file_date csource.std_out)
      in
      
      let new_date = max stdin_prereq_date prereqs_date in	  	   
      if new_date > old_date then
	let command =
	  let pre_command = 
	    match csource.stdin_prereq with
	      | Some psource ->
		let prereq = string_of_data (source_value psource) in
		"cat " ^ prereq ^ " | " ^ csource.command
	      | None -> csource.command
	  in
	  let mid_command = 
	    Hashtbl.fold
	      (fun flag sourceop com ->
		let source, op = sourceop in
		let value = source_value source in
		match value with
		  | Null -> (
                     match op with
                     | Pos_Option -> com
                     | Neg_Option -> Printf.sprintf "%s -%s " com flag
                     | Ordinary -> 
		        raise
			  (Failure ("missing argument "^flag^
				      " to "^com))
                  ) 
		  | _ -> 
		    let svalue = string_of_data value in
                    if str_is_true svalue then
                      Printf.sprintf "%s -%s " com flag
                    else if str_is_false svalue then
                      Printf.sprintf "%s " com 
                    else
		      Printf.sprintf "%s -%s %s " com flag svalue  
	      )
	      csource.prereqs
	      pre_command
	  in
	  mid_command ^ " > " ^ csource.std_out
	in
	
	Printf.printf "command: %s\n" command;
	flush stdout;
	if not ((Sys.command command) = 0) then(
	  
	  if Sys.file_exists csource.std_out then
	    Sys.remove csource.std_out;
	  
	  raise (Failure ("command failed: " ^ command))
	)
	else
	  file_date csource.std_out
      else 
	old_date
    );
      
      
    | Func2_Source fsource ->
      max (update_inner  fsource.prereq0) (update_inner  fsource.prereq1)
	
    | Func1_Source fsource ->
      update_inner  fsource.prereq
	
    | In_File_Source ifsource -> 
      if_date ifsource		    

    | In_Var_Source vsource ->
      cof_date vsource.fname
	
    | In_2File_Var_Source vsource ->
      in_2file_var_source_date vsource  
	
    | Alt_Source asource ->
      let date0 = update_inner asource.source0 in
      let value0 = source_value asource.source0 in
      (match value0 with
	| Null ->
	  let date1 = update_inner asource.source1
	  in
	  max date0 date1
	| _ -> date0
      )
	
    | Cond_Source isource ->
      let datec = update_inner isource.csource in
      let cond = source_value isource.csource in
      let date01 = 
	(match cond with
	  | Bool tf ->
	    if tf then
	      update_inner isource.isource0
	    else
	      update_inner isource.isource1
	  | _ -> raise (Failure "Cond_Source condition source must be boolean")
	)
      in
      max datec date01

    | _ ->
      0.0 (* constants *)
	
	
let update source =
  ignore (update_inner source)

let const_float_source x = 
  Const_Float_Source x

let const_string_source x = 
  Const_String_Source x

let has_value source = 
  let value = source_value source in
  match value with
    | Null -> false
    | _ -> true

