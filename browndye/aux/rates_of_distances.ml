(* Copyright (c) 2008 Gary A. Huber, Howard Hughes Medical Institute 
   See the file COPYRIGHT for copying permission
*)

(*
Outputs rate constants and 95% confidence intervals. Input is results file from "nam_simulation". Uses kdb_calculator for actual calculation.
*)


module JP = Jam_xml_ml_pull_parser;;
module NI = Node_info;;

let res_file, coord_file, plain = 
  let res_file_ref = ref ""
  and coord_file_ref = ref ""
  and plain_ref = ref false
  in
  Arg.parse 
    [
      ("-res", Arg.Set_string res_file_ref, "results file from nam_simulation");
      ("-dist", Arg.Set_string coord_file_ref, "reaction distance file");
      ("-plain", Arg.Set plain_ref, "output as plain file (coordinate, low, mid, high rates) instead of xml");
    ]
    (fun arg -> raise (Failure "no anonymous arguments"))
    "Outputs rate constant as function of threshold reaction distance";
  !res_file_ref, !coord_file_ref, !plain_ref
;;

let rnode = 
  let rparser = JP.new_parser (Scanf.Scanning.from_file res_file) in
  JP.complete_current_node rparser;
  JP.current_node rparser
;;

let snode = NI.checked_child rnode "solvent";;
let mnode = NI.checked_child rnode "molecule-info";;

let rxnode = NI.checked_child rnode "reactions" ;;
let ntrajs = NI.int_of_child rxnode "n-trajectories";;
let cnodes = Array.of_list (JP.children rxnode "completed");;
let nrxns = Array.length cnodes;;

let kdb = Kdb_calculator.f snode mnode;;

let conv_factor = 602000000.0;; (* 1/M s, make it an option later *)

let pr = Printf.printf;;
  
let coords = 
  let cparser = JP.new_parser (Scanf.Scanning.from_file coord_file) in
  
  let lst = 
    JP.fold_nodes_of_tag cparser "min-dist" 
                         (fun lst cnode ->
	                  let coord = 
                            JP.float_from_stream (JP.node_stream cnode) in
	                  coord :: lst
                         )
                         []
  in
  let res = Array.of_list lst in
  Array.sort compare res;
  res
;;

  if not plain then
    pr "<rates>\n";
     
  for i = 10 to ntrajs-1 do
    let beta = (float i)/.(float ntrajs) in
    let rate beta = conv_factor*.kdb*.beta 
    in
    let sdev = sqrt ( beta*.(1.0 -. beta)/.(float ntrajs)) in
    let high_beta = beta +. 2.0*.sdev
    and low_beta = beta -. 2.0*.sdev
    in                                                          
    let coord = coords.(i) in
    let low = max 0.0 (rate low_beta)
    and high = rate high_beta
    and mid = rate beta 
    in

    if plain then
      pr "%g %g %g %g\n" coord low mid high
    else (
      pr " <rofc>\n";
      pr "   <dist> %g </dist>\n" coord;
      pr "   <rate-constant>\n";
      pr "      <low> %g </low>\n" low;
      pr "      <mean> %g </mean>\n" mid;
      pr "      <high> %g </high>\n" high;
      pr "   </rate-constant>\n";
      pr " </rofc>\n";  
    )      
  done
;;
  
  if not plain then
    pr "</rates>\n";
  
  



 
