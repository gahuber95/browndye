<html>
<body>
<h1>BrownDye Tutorial</h1>

<p>
This tutorial will walk you through two different types of BD simulations on 
the Thrombin-Thrombomodulin system, which is an important component of the
blood-clotting cascade.  Thrombin consists of 295 amino acids, and
thrombomodulin consists of 117 amino acids.  
(My thanks to Adam Van Wynsberghe
for the necessary data on these two molecules.)
Another part of the tutorial
deals with a bifunctional enzyme and substrate channeling.

<p>
BrownDye can be easily installed on Linux or Mac OS X.  
If you have Windows, you can
install <a href="http://cygwin.com/install.html"> Cygwin </a>,
which provides a free Linux emulator, and then to install BrownDye using the
Cygwin terminal.
Go <a href="#windows"> here </a> for more details.
Other software required will be <a href="http://www.poissonboltzmann.org/apbs/">
APBS </a>, <a href="http://caml.inria.fr/pub/docs/manual-ocaml/index.html">Ocaml</a>, and <a href="http://www.ks.uiuc.edu/Research/vmd/"> VMD </a> for the visualization.

<h2> Running on a Linux Computer </h2>
Other necessary software is the Ocaml compiler, which is free and can be obtained from <a href="http://caml.inria.fr/"> here </a>. If you are using Ubuntu or a similar Linux based on Debian, you can just type

<pre>
sudo apt-get install ocaml 
</pre>

You will also need APBS; this can also quickly be installed on Ubuntu by typing

<pre>
sudo apt-get install apbs 
</pre>

The following steps will also work on the Mac terminal and Windows Cygwin
terminal.
You can install BrownDye on your machine by going to the 
<a href="http://browndye.ucsd.edu"> website </a>, downloading 
the file <code>browndye.tar.gz</code>, moving the file to a directory of your choice, 
and typing

<pre>
tar xvfz browndye.tar.gz
cd browndye
make all
</pre>

All of the executables are now in the directory <code>browndye/bin</code>; you need
to add this directory to your path.  If you use a bash shell, this can
be done by typing

<pre>
PATH=$PATH:fullpath/browndye/bin
</pre>

where <code>fullpath</code> is the full path name of the directory containing the
browndye distribution.
If you now want to run the thrombin-thrombomodulin tutorial, go to
<code>browndye/thrombin-example</code>.

<p>
The atomic coordinates for thrombin and thrombomodulin are in
files <code>t.pqr</code> and <code>m.pqr</code>.  Because BrownDye works primarily with
XML files, you must convert this two files to an equivalent XML format:

<pre>
pqr2xml < t.pqr > t-atoms.xml
pqr2xml < m.pqr > m-atoms.xml
</pre>

Next, you must generate the electrostatic grids, in dx format, using
APBS:

<pre>
apbs t.in
apbs m.in
</pre>
where the input files <code>t.in</code> and <code>m.in</code> are provided.  The grids are
output to <code>t.dx</code> and <code>m.dx</code>.  
Throughout this session, thrombin will
be denoted by the prefix "t" while thrombomodulin will be denoted
by prefix "m".  
Be sure to take note of the Debye length
in the APBS output if you don't feel like calculating it by hand; it
will be needed later.  (Some older versions of APBS will name the output files 
<code>t-PE0.dx</code> and <code> m-PE0.dx</code>; if that happens, be sure to 
rename the files.)

<p>  In addition to atomic coordinates and grids, the third key input
is the set of reaction criteria.  This can be generated from the 
two coordinate files <code>t-atoms.xml</code> and <code>m-atoms.xml</code>, and a file,
<code>protein-protein-contacts.xml</code>, which describes which pairs of atom
types can define a contact 
(It is actually stored as <code>protein-protein-contacts.xml.bak</code>; you
need to copy from this file.)

The program <code>make_rxn_pairs</code> takes these
three files and a search distance to generate a file of reaction
pairs.  This assumes that the coordinates of the two molecules
are consistent with the bound state.
<pre>
make_rxn_pairs -mol0 t-atoms.xml -mol1 m-atoms.xml -ctypes protein-protein-contacts.xml -dist 6.0 > t-m-pairs.xml
</pre>
 The resulting file still is not suitable for input into the simulation
programs, however.  I've made the program general enough to 
have more than one reaction in a simulation, so one could envision having
several such reaction pair files that would need to combined into a final
reaction description file.  For now, you can use the program <code>make_rxn_file</code>,
which generates an input file for the case of one reaction:
<pre>
make_rxn_file -pairs t-m-pairs.xml -distance 4.9 -nneeded 3 > t-m-rxns.xml
</pre>
This generates a reaction description file which tells the simulation
programs that if any 3 of the atom pairs approach within 4.9 Angstroms,
a reaction occurs.

<p> A note: if you don't feel like typing the above commands in, especially
if you make changes and need to do it repeatedly, I have included a
Makefile in the example directory. Typing
<pre>
make all
</pre>
should run the above commands.

<p>
The remaining pieces of information are contained in the file <code>input.xml</code>.
(This must be copied from the file <code>input.xml.bak</code>.)
It contains, among other things, information on the solvent,
information on each molecule, and parameters governing the simulation
itself.
For the sake of efficiency, the larger molecule
should be "molecule0" if there is a large size difference.  
Also, each molecule is assigned a prefix as mentioned above; these
are used in naming the intermediate files that are generated in the
next step:
<pre>
bd_top input.xml
</pre>
The bd_top program is written in Ocaml using a Unix "make"-like utility
that I wrote to help orchestrate the creation of the files.
Like "make", if an intermediate file is changed or replaced, 
running bd_top (which is analogous to a Makefile) will run only 
those commands necessary to re-generate files that depend on the
updated file.  Unlike "make", this utility, which I call the
"Orchestrator", can also read information from xml files and 
have chains of dependent calculations (eventually I want to 
write a version in Python so it will look more familiar to most people).
So, when the command is executed, the following files are generated
for thrombin:

<ul>
<li><b>t-charges.xml</b> - a set of effective charges

</li><li><b>t-ellipsoids.xml</b> - bounding ellipoid using for estimating
hydrodynamic interactions

</li><li><b>t-hydro-radius.xml</b> - hydrodynamic radius  

</li><li><b>t-q2.xml</b> - effective charges squared; used for the desolvation forces 

</li><li><b>t-q2-cheby.xml</b> - Chebyshev interpolation data structure for
effective charges squared; used for desolvation forces

</li><li><b>t-surface.xml</b> - a list of atoms on the surface found by
rolling a probe sphere. 

</li><li><b>t-surface-atoms.xml</b> - actual list of surface atoms to be used
for collision detection 

</li><li><b>t-surface-atoms-hard.xml</b> - list of surface atoms that interact
via a hard-sphere interaction

</li><li><b>t-volume.xml</b> - list of effective volumes of each atom 

</li><li><b>t-mpole.xml</b> - multipole expansion of effective charges

</li> <li> <b> ins-t.xml </b> - grid of points denoting inside (1) or outside (0) 

</li><li><b> born-t.dx </b> - grid of values used to estimate the desolvation energy.

</li>

</ul>

The corresponding files with prefix "m" are also generated for
thrombomodulin.  In addition, the following file is generated:
<ul>
</li><li><b>m-cheby.xml</b> - Chebyshev interpolation data structure for
effective charges; used for Coulombic forces
</ul>

<p>
The following files are generated for both molecules:

<ul>

</li><li><b>t-m-solvent.xml</b> - solvent information 
</li><li><b>t-m-old-data.xml</b> - previous data, used for detecting changes in parameters  
</li><li><b>t-m-return-dist.xml</b> - information used for appropriately returning the molecule to the b-sphere; has data on the time and probability distributions.
</li><li><b>t-m-simulation.xml</b> - contains additional information generated by the auxilliary programs; is read directly into the simulation programs. 


</ul> 
The nice thing about these intermediate files is that any of them can
be replaced or changed, and then <code>bd_top</code> run again to update everything.
For example, right now I'm using a simple test-charge approximation by
default, but one could easily generate effective charges using another
program such as SDA, convert the output into the appropriate XML format,
replace <code>t-charges.xml</code> and <code>m-charges.xml</code>, and run <code>bd_top</code> again.

<p> Another useability note:  if you want to clean things up, you
can delete all *.dx and *.xml files; the two xml files that you
need to get started are also available as <code>input.xml.bak</code> and
<code>protein-protein-contacts.xml.bak</code>.

<p>
At this point, you can choose to do a simulation of one trajectory at a time,
or you can do a weighted-ensemble simulation.  In general, the weighted-ensemble
method is not as efficient at the single-trajectory method
unless the probability of a reaction event is
very low.

<h3>One trajectory at a time</h3>
<p>
To perform the single-trajectory simulation, the following is executed:
<pre>
nam_simulation t-m-simulation.xml
</pre>
The results end up in <code>results.xml</code>, as designated in <code>input.xml</code>.
As the simulation proceeds, you can look at <code>results.xml</code> to 
see the simulation progress.  (This is called <code>nam_simulation</code> after
Northrup, Allison, and McCammon, who came up with the first algorithm
of this type.  This code uses a fancy variation on the orginal 
algorithm.).

<p>
At any point, you can use <code>compute_rate_constant</code> to 
analyze <code>results.xml</code> and obtain an estimate of and 95% confidence bounds
on
the reaction rate constant in units of M/s.  The file <code>t-m-solvent.xml</code>
must also be given to this program:
<pre>
compute_rate_constant < results.xml 
</pre>
This will put the rate constant results to standard output.

<h3> Visualization </h3>
<p> 
To visualize a trajectory leading to a reaction, you must include
the line
<pre>
&lttrajectory-file&gt trajectory &lt/trajectory-file&gt
</pre>
in the input file, run <code>bd_top</code> and <code> nam_simulation </code>
again.  The value <code>trajectory</code> gives its names to the resulting trajectory
files, and can be what you want.
Because the files can get quite large, you might want
to also include a line such as
<pre>
&lt n-steps-per-output &gt 10 &lt /n-steps-per-output &gt
</pre>
which causes only every 10th configuration to be output.
At the end, the files <code> trajectory0.xml </code> and
<code> trajectory0.index.xml</code>
will be generated. The first file contains the actual trajectories in a 
compressed format, and the second file functions as an index to the
first file.
If you are running with more than one thread, more files
like this will also be generated, with names <code> trajectory1.xm1 </code>,
<code> trajectory1.index.xml </code>, etc., with one pair of files per thread.

After the initial trajectory files are generated, they must be processed further.  For example, if you wish to view a trajectory that results in a reaction, you
 must find such a trajectory by running 
<pre>
process_trajectories -traj trajectory0.xml -index trajectory0.index.xml -srxn association
</pre>
which prints out the numbers of reactive trajectories.  Then, pick one of
the numbers, say 8, and run
<pre>
process_trajectories -traj trajectory0.xml -index trajectory0.index.xml -n 8 > trajectory.xml
</pre>
This uncompresses the contents of <code>trajectory0.xml</code> and outputs 
the desired trajectory into <code>trajectory.xml</code>.
At this point, you can cut down even further on the number of configurations by
adding to the above to get
<pre>
process_trajectories -traj trajectory0.xml -index trajectory0.index.xml -n 8 -nstride 10 > trajectory.xml
</pre>
which outputs only every tenth configuration in <code>trajectory0.xml</code>.

The final step is to generate an xyz-format file for visualization by
VMD, which is obtained by running
<pre>
xyz_trajectory -mol0 t-atoms.xml -mol1 m-atoms.xml -trajf trajectory.xml > trajectory.xyz
</pre>
This file can be very large, since all of the atom positions for every configuration are output to <code> traj.xyz</code>, so it can be important to 
reduce the number of trajectories upstream.  This file then can be passed
to VMD for visualization.  If you are worried about the size of the file,
you can run 
<pre>
xyz_trajectory -mol0 t-atoms.xml -mol1 m-atoms.xml -trajf trajectory.xml -trial
</pre>
which will output nothing except an estimate of the file size. If it looks like
it might be too large, you can go back and increase the stride argument to
<code> process_trajectories </code>.

At last, you can start up VMD, load in the xyz file, and watch the animation.

<h3> Weighted Ensemble Method </h3>
<p>
To perform the weighted-ensemble method, you must first generate the
bins for the system copies:
<pre>
build_bins t-m-simulation.xml
</pre>
The  number of system copies used in the bin-building process are
given in <code>input.xml</code> in the <code>n-copies</code> tag.
As it runs, reaction coordinate numbers will go scrolling past; they should keep
getting smaller and eventually stop.  If that does not happen, i.e.,
the numbers keep on going, you might need to increase the number of system 
copies, or it might be that your reaction criterion is unattainable.
Assuming it converges, the bin information is placed in <code>t-m-bins.xml</code>.
The actual weighted-ensemble simulation is then run:
<pre>
we_simulation t-m-simulation.xml
</pre>
As before, the results are output to <code>results.xml</code>.  In each row of
output numbers, the right-most number is the flux of system copies that
escaped without reaction, while the other ones are reactive fluxes.
So, even for a rare reaction event, you should at least see small numbers for
the reactive fluxes after the system has reacted steady-state.
This can be visually examined at any point, and can also be analyzed as
above, but with a different program:
<pre>
compute_rate_constant_we -sim results.xml -solvent t-m-solvent.xml
</pre>
Because the streams of numbers are autocorrelated, a more sophisticated
approach for computing confidence intervals is used, and if there are
not enough data points, the program <code>compute_rate_constant_we</code> will
simply refuse to provide an answer.

<h3> Channeling Tutorial</h3>
We also present a very simple and crude model to demonstrate <i>substrate 
channeling</i>, which occurs when an enzyme molecule has two different
active sites for two different reactions, and the product of one reaction
serves as the substrate for the other reaction.  Often this situation is
realized when two separate enzymes are bound to each other.  One 
medically interesting system is that of <i>dihydrofolate reductase</i> (DHFR)
and <i> thymidylate synthase </i> (TS).  The thymidylate synthase
caries out the reaction
<p>
5,10-methylenetetrahydrofolate + deoxyuridine monophosphate &#8596 dihydrofolate + thymidine monophosphate
<p>
The dihydrofolate is then reduced by the DHFR.  In some organisms,
the two enzymes are joined together, and there is some evidence that
a positivly charged channel helps convey the negatively charged
dihydrofolate from one active site to another.  A Brownian dynamics
simulation by Elcock
et al. some years ago on enzyme complex from the protozoan that causes 
a nasty tropical disease, leishmaniasis,
showed a very pronounced channeling effect.  We unfortunately cannot 
reproduce that here since the coordinates of that enzyme complex are
not publicly available, but I have set up a simple system using
the complex from the agent of African sleeping sickness, another nasty
disease.  

<p>
The enzyme complex is 3QGT from the protein data bank. It is missing
many residues in the region between the two different enzymes, so
we may not have a very complete channel in this model.  Furthermore,
work would need to be done to find the binding mode of the dihydrofolate
to the DHFR active site. So, for now, we will need to settle for 
a small sphere with a -2 charge to assess the hypothesis of channeling.
The simulation starts the "substrate" in the TS active site.  The
complex itself is a dimer, with two TS and two DHFR units.  The subtrate
is advanced until it either reacts or escapes.  You can explore the
effect of electrostatic channeling by changing the charge on the substrate
sphere.  

<p>
You will find that the effect here is noticeable, but not 
dramatic; this may because of the incompleteness of the model or 
actual reality.  There are other structures of the DHFR-TS complex in
the protein data bank from medically interesting organisms. Perhaps
some refining of the model along with upcoming improvements in Browndye
such as flexible molecules will lead to some interesting results.

<p>
The data for the channeling simulation is found in the <code> channeling-example</code> directory in the Browndye distribution.

<h3> Ideas for fun </h3>

<p> You can change the random number generator seed, under the <code>seed</code> tag.
Good to do if you're bored but don't have the energy to do anything else.

<p>
One parameter to play with is the reaction criterion distance, which is the
<code>-distance</code> input to the program <code>make_rxn_file</code>.  The number given in 
the tutorial and the Makefile is 4.9, but you can change that by re-running
<code>make_rxn_file</code> or by changing it in the Makefile.

<p>
You can also change the ionic strength in files <code>t.in</code> and <code>m.in</code> and
generate new APBS grids.  Note: <i> you must take note of the new Debye length</i>
and put that value in the file <code>input.xml</code>.  So far, it is not possible to
automatically get the Debye length from the output DX file of APBS.

<p>
If your machine has several processors, you
can change the value under the tag <code>n-threads</code> in file <code>input.xml</code> and
see it run under several processors.  So far, it runs only on shared-memory
machines using <code>pthreads</code>.
Even on a single-processor
machine, you can still run several threads, but it does not make the
programs go any faster.

<p>
A final useability note:  Most of the programs will output a description of 
themselves and their options if you type in
<pre>
program -help
</pre>

<h2> Running on an Opal Server </h2>
The flow of events is the same as above, except it is done on one of the clusters via a web browser, and for now, the bd_top and nam_simulation steps are combined into one step.

<ul>
<li> Using the files <code>t.pqr</code> and <code>t.in</code> found in the 
<a href="."> input files</a>,
generate the APBS grid <code>t.dx</code>.
<li> Using the files <code>m.pqr</code> and <code>m.in</code> found in the <a> input files</a>,
generate the APBS grid <code>t.dx</code>.
<li> Go to the Opal server at <a href="http://nbcr-222.ucsd.edu/opal2/dashboard?command=serviceList">nbcr-222.ucsd.edu </a>.
<li> Select the service <code>Browndye: pqr2xml</code>, select the option <code>Input URL</code>,
using another browser tab go to the <a> input files</a>, and copy and paste the URL of <code>t.pqr</code> into the <code>Input URL</code> box. Copy and pasting URL's can easily be done by moving the curser over the link, pressing the right mouse button and selecting <code>Copy Link Location</code>.
<li> Click the <code>Submit</code> button
<li> When the page indicates the execution has finished, click on the Output Base URL, inspect the file <code>t-atoms.xml</code> in new browser tab, and <i> do not delete the tab.</i> 
<li> Repeat the previous three steps using <code>m.pqr</code> to generate the file <code>m-atoms.xml</code> in its own tab.
</ul>

<p> The following steps are used to generate the reaction pairs file:

<ul>
<li> Select the service <code>Browndye: make_rxn_pairs</code>.
<li> Copy the URL of <code>t-atoms.xml</code> from <code>Browndye: pqr2xml</code> service into the box for <code>Molecule 0 Input URL</code>.
<li> Copy the URL of <code>m-atoms.xml</code> from <code>Browndye: pqr2xml</code> service into the box for <code>Molecule 1 Input URL</code>.
<li> Copy the URL of <code>protein-protein-contacts.xml</code> in the input files into the
box for <code>Contacts Type URL</code>.
<li> Enter 5.5 into the <code>Search Distance</code> box
<li> Enter <code>t-m-rxn-pairs.xml</code> into the <code>Output File</code> box.
<li> Submit the job, and when finished, inspect the <code>t-m-rxn-pairs.xml</code> file.
Do not delete the tab.
</ul>

<p> The following steps are used to generate the reaction description file:
<ul>
<li> Select the service <code>Browndye: make_rxn_file</code>.
<li> Copy the URL of the the file <code>t-m-rxn-pairs.xml</code> from the output of 
<code>make_rxn_pairs</code> to the <code>Pairs Input URL</code> box. 
<li> Enter 5.5 into the <code>Reaction Distance</code> box.
<li> Enter 3 into the <code>Number of Required Contacts</code> box.
<li> Enter <code>t-m-rxns.xml</code> into the <code>Output File</code> box.
<li> Submit the job, and inspect the results when finished.
</ul>

<p> The following steps are used to run <code>bd_top</code> and <code>nam_simulation</code>
<ul>
<li> Select the service <code>bd_top</code>.
<li>As above, copy the URL's for <code>t-atoms.xml</code> and <code>m-atoms.xml</code> into
the boxes for <code>Molecule 0 URL</code> and <code>Molecule 1 URL</code>.
<li>Copy the URL for <code>t-m-rxns.xml</code> into the <code>Reaction File URL</code> box.
<li>Copy the URL's or files for <code>t.dx</code> and <code>m.dx</code> into the APBS boxes
for Molecule 0 and Molecule 1.
<li>Copy the URL of the input file <code>input.xml</code> in the input files into the
<code>Input File URL</code> box.
<li> Submit the job.
<li> When the file <code>results.xml</code> finally appears in the <code>Output Base URL</code>,
keep checking it if you want until 1000 trajectories have been run and
the job stops.
<li> At any time, copy the URL for <code>results.xml</code> into the input URL box
for the <code>Browndye: compute_rate_constant</code> service, and check the file
<code>stdout.txt</code> for the rate constant information.
</ul>

<h2><a name="windows"> Notes on Running BrownDye on Windows and Macintosh</a> </h2>
The Cygwin package is easy to install; you go to the website and download
and run the <code>setup.exe</code> file. The only part that requires some
effort is finding the necessary packages to install.  If you do nothing,
Cygwin installs a minimal subset, but to run Browndye you need gcc,
make, autoconf, and ocaml.  You could install all optional packages, but
it makes for a huge and lengthy download.  Once Cygwin is installed,
you can pop open a terminal window, giving you access to a directory
tree just like any other Unix system.  If you are missing packages,
you can run <code>setup.exe</code> again, which will again present you 
the choice of packages.

<p>
Another option for Windows is to install Ubuntu Linux using Wubi.  From Windows,
you download and run the Wubi installer, which in turn downloads and 
installs Ubuntu Linux and the boot-up tools necessary to boot into either
Windows or Ubuntu.  No disk partioning is necessary, making it very easy.
The download usually takes several minutes depending on
the speed of your connection.  Then, when you restart your computer, you
are given a choice between booting into Windows or Ubuntu.  Afterwards,
if you want, Ubuntu is easily deleted by running the Wubi Uninstall
program from within Windows.

<p>
In order to obtain gcc for OS X on the Mac, you need to go to Apple's website
and register as a developer. If you are doing your work in a university, this
is free.  Then, you install the XCode Developer tools, which include gcc.
Ocaml is available for the Mac from the Ocaml website. 

<h2> References </h2>

<p>
Huber, GA and McCammon, JA. <a href="http://dx.doi.org/10.1016/j.cpc.2010.07.022"> Browndye: A Software Package for Brownian
Dynamics.</a>  Computer Physics
Communications 181, 1896-1905 (2010)

<p>
Ermak, DL and McCammon, JA. Brownian Dynamics with Hydrodynamic Interactions,
J. Chem. Phys. 69, 1352-1360 (1978)

<p>
Northrup SH, Allison SA and McCammon JA.
Brownian Dynamics Simulation of Diffusion-Influenced Bimolecular Reactions
J. Chem. Phys. 80, 1517-1526

<p>
Luty BA, McCammon JA and Zhou HX. Diffusive Reaction-Rates From Brownian Dynamics Simulations - Replacing the Outer Cutoff Surface by an Analytical Treatment,
J. Chem. Phys. 97, 5682-5686 (1992)

<p>
Huber GA and Kim S. Weighted-ensemble Brownian dynamics simulations for protein association reactions, Biophys. J 70, 97-110 (1996)

<p>
Gabdoulline RR and Wade RC. Effective charges for macromolecules in solvent,
J. Phys. Chem 100, 3868-3878 (1996)


</body>
</html>

