<html>
<body>
<h1>BrownDye Tutorial</h1>

<p>
This tutorial will walk you through two different types of BD simulations on 
the Thrombin-Thrombomodulin system, which is an important component of the
blood-clotting cascade.  Thrombin consists of 295 amino acids, and
thrombomodulin consists of 117 amino acids.  (My thanks to Adam Van Wynsberghe
for the necessary data on these two molecules.)

<p>
First, in order to use BrownDye on these machines, you must set your
path to include the executable files:

<pre>
PATH=$PATH:/home/linux/ieng6/nbcr10/nbcr10/public/browndye/browndye/bin
</pre>
The new directory contains the executables of BrownDye.  
You also need to copy the file /u/ieng6/nbcr10/nbcr10/public/browndye/thrombin-example.tar.gz to your home directory, and do
<pre>
tar xvfz thrombin-example.tar.gz
</pre>
from within your home directory.  This will give you a directory "thrombin-example";
the commands of the tutorial example will be run from within this directory

<p>
The atomic coordinates for thrombin and thrombomodulin are in
files t.pqr and m.pqr.  Because BrownDye works primarily with
XML files, you must convert this two files to an equivalent XML format:

<pre>
pqr2xml < t.pqr > t-atoms.xml
pqr2xml < m.pqr > m-atoms.xml
</pre>

Next, you must generate the electrostatic grids, in dx format, using
APBS:

<pre>
apbs t.in
apbs m.in
</pre>
where the input files t.in and m.in are provided.  The grids are
output to t.dx and m.dx.  
Throughout this session, thrombin will
be denoted by the prefix "t" while thrombomodulin will be denoted
by prefix "m".  
Be sure to take note of the Debye length
in the APBS output if you don't feel like calculating it by hand; it
will be needed later.

<p>  In addition to atomic coordinates and grids, the third key input
is the set of reaction criteria.  This can be generated from the 
two coordinate files t-atoms.pqrxml and m-atoms.pqrxml, and a file,
protein-protein-contacts.xml, which describes which pairs of atom
types can define a contact.  The program make_rxn_pairs takes these
three files and a search distance to generate a file of reaction
pairs.  This assumes that the coordinates of the two molecules
are consistent with the bound state.
<pre>
make_rxn_pairs -mol0 t-atoms.pqrxml -mol1 m-atoms.pqrxml -ctypes protein-protein-contacts.xml -dist 5.5 > t-m-pairs.xml
</pre>
 The resulting file still is not suitable for input into the simulation
programs, however.  I've made the program general enough to 
have more than one reaction in a simulation, so one could envision having
several such reaction pair files that would need to combined into a final
reaction description file.  For now, you can use the program make_rxn_file,
which generates an input file for the case of one reaction:
<pre>
make_rxn_file -pairs t-m-pairs.xml -distance 5.5 -nneeded 3 > t-m-rxns.xml
</pre>
This generates a reaction description file which tells the simulation
programs that if any 3 of the atom pairs approach within 5.5 Angstroms,
a reaction occurs.

<p> A note: if you don't feel like typing the above commands in, especially
if you make changes and need to do it repeatedly, I have included a
Makefile in the example directory. Typing
<pre>
make all
</pre>
should run the above commands.

<p>
The remaining pieces of information are contained in the file input.xml.
It contains, among other things, information on the solvent,
information on each molecule, and parameters governing the simulation
itself.
For the sake of efficiency, the larger molecule
should be "molecule0" if there is a large size difference.  
Also, each molecule is assigned a prefix as mentioned above; these
are used in naming the intermediate files that are generated in the
next step:
<pre>
bd_top input.xml
</pre>
The bd_top program is written in Ocaml using a Unix "make"-like utility
that I wrote to help orchestrate the creation of the files.
Like "make", if an intermediate file is changed or replaced, 
running bd_top (which is analogous to a Makefile) will run only 
those commands necessary to re-generate files that depend on the
updated file.  Unlike "make", this utility, which I call the
"Orchestrator", can also read information from xml files and 
have chains of dependent calculations (eventually I want to 
write a version in Python so it will look more familiar to most people).
So, when the command is executed, the following files are generated
for thrombin:

<ul>
<li><b>t-charges.xml</b> - a set of effective charges

</li><li><b>t-cheby.xml </b> - Chebyshev interpolation data structure for 
effective charges

</li><li><b>t-distances</b> - a dx file containing the distances of
each exterior point to the nearest interior point, and vice-versa

</li><li><b>t-ellipsoids.xml</b> - bounding ellipoid using for estimating
hydrodynamic interactions

</li><li><b>t-hydro-radius.xml</b> - hydrodynamic radius  

</li><li><b>t-q2.xml</b> - effective charges squared; used for the desolvation forces 

</li><li><b>t-q2-cheby.xml</b> - Chebyshev interpolation data structure for
effective charges squared 

</li><li><b>t-surface.xml</b> - a list of atoms on the surface found by
rolling a probe sphere. 

</li><li><b>t-surface-atoms.xml</b> - actual list of surface atoms to be used
for collision detection 

</li><li><b>t-volume.xml</b> - list of effective volumes of each atom 

</li><li><b>t-mpole.xml</b> - multipole expansion of effective charges

 </li><li><b>t-monopole.xml</b> - total charge 

</li> <li> <b> ins-t.xml </b> - grid of points denoting inside (1) or outside (0) </li>

</ul>

The corresponding files with prefix "m" are also generated for
thrombomodulin.

<p>
The following files are generated for both molecules:

<ul>

</li><li><b>t-m-solvent.xml</b> - solvent information 
</li><li><b>t-m-old-data.xml</b> - previous data, used for detecting changes in parameters 
</li><li><b>t-m-normed-rxns.xml</b> - because only the surface atoms are read into the simulation programs, the atoms in the reaction criteria must be renumbered so that they are in sequence. This is the resulting file.
 
</li><li><b>t-m-return-dist.xml</b> - information used for appropriately returning the molecule to the b-sphere; has data on the time and probability distributions.

</li><li><b>t-m-simulation.xml</b> - contains additional information generated by the auxilliary programs; is read directly into the simulation programs. 


</ul> 
The nice thing about these intermediate files is that any of them can
be replaced or changed, and then "bd_top" run again to update everything.
For example, right now I'm using a simple test-charge approximation by
default, but one could easily generate effective charges using another
program such as SDA, convert the output into the appropriate XML format,
replace "t-charges.xml" and "m-charges.xml", and run "bd_top" again.

<p> Another useability note:  if you want to clean things up, you
can delete all *.dx and *.xml files; the two xml files that you
need to get started are also available as "input.xml.bak" and
"protein-protein-contacts.xml.bak".

<p>
At this point, you can choose to do a simulation of one trajectory at a time,
or you can do a weighted-ensemble simulation.  In general, the weighted-ensemble
method is not as efficient at the single-trajectory method
unless the probability of a reaction event is
very low.

<p>
To perform the single-trajectory simulation, the following is executed:
<pre>
nam_simulation t-m-simulation.xml
</pre>
The results end up in "results.xml", as designated in "input.xml".
As the simulation proceeds, you can look at "results.xml" to 
see the simulation progress.  (This is called "nam_simulation" after
Northrup, Allison, and McCammon, who came up with the first algorithm
of this type.  This code uses a fancy variation on the orginal 
algorithm.).

<p>
At any point, you can use "compute_rate_constant" to 
analyze "results.xml" and obtain an estimate of and 95% confidence bounds
on
the reaction rate constant in units of M/s.  The file "t-m-solvent.xml"
must also be given to this program:
<pre>
cat results.xml | compute_rate_constant 
</pre>
This will put the rate constant results to standard output.

<p>
To perform the weighted-ensemble method, you must first generate the
bins for the system copies:
<pre>
build_bins t-m-simulation.xml
</pre>
The  number of system copies used in the bin-building process are
given in "input.xml" in the "n-copies" tag.
As it runs, reaction coordinate numbers will go scrolling past; they should keep
getting smaller and eventually stop.  If that does not happen, i.e.,
the numbers keep on going, you might need to increase the number of system 
copies, or it might be that your reaction criterion is unattainable.
Assuming is converges, the bin information is place in "t-m-bins.xml".
The actual weighted-ensemble simulation is then run:
<pre>
we_simulation t-m-simulation.xml
</pre>
As before, the results are output to "results.xml".  In each row of
output numbers, the right-most number is the flux of system copies that
escaped without reaction, while the other ones are reactive fluxes.
So, even for a rare reaction event, you should at least see small numbers for
the reactive fluxes after the system has reacted steady-state.
This can be visually examined at any point, and can also be analyzed as
above, but with a different program:
<pre>
compute_rate_constant_we -sim results.xml -solvent t-m-solvent.xml
</pre>
Because the streams of numbers are autocorrelated, a more sophisticated
approach for computing confidence intervals is used, and if there are
not enough data points, the program "compute_rate_constant_we" will
simply refuse to provide an answer.

<h2> Ideas for fun </h2>

<p> You can change the random number generator seed, under the "seed" tag.
Good to do if you're bored but don't have the energy to do anything else.

<p>
One parameter to play with is the reaction criterion distance, which is the
"-distance" input to the program "make_rxn_file".  The number given in 
the tutorial and the Makefile is 5.0, but you can change that by re-running
"make_rxn_file" or by changing it in the Makefile.

<p>
You can also change the ionic strength in files "t.in" and "m.in" and
generate new APBS grids.  Note: <i> you must take note of the new Debye length</i>
and put that value in the file "input.xml".  So far, it is not possible to
automatically get the Debye length from the output DX file of APBS (another
good argument for using XML!).

<p>
Although I'm pretty sure that the machines you're working on are single-processor
machines, if you have access to a machine with several processors, you
can change the value under the tag "n-threads" in file "input.xml" and
see it run under several processors.  So far, it runs only on shared-memory
machines using "pthreads".  Let me know if you want to try it on another
machine; you can copy the BrownDye distribution over and compile it if
you have Ocaml, or you might be able to copy the executables over if you
don't. (Ocaml is quite easy to install.) 
Even on a single-processor
machine, you can still run several threads, but it does not make the
programs go any faster.

<p>
A final useability note:  Most of the programs will output a description of 
themselves and their options if you type in
<pre>
program -help
</pre>

<h2> Running on an Opal Server </h2>
The flow of events is the same as above, except it is done on one of the clusters via a web browser, and for now, the bd_top and nam_simulation steps are combined into one step.

<ul>
<li> Using the files "t.pqr" and "t.in" found in the 
<a href="http://browndye.ucsd.edu/input-files.html"> input files</a>,
generate the APBS grid "t.dx".
<li> Using the files "m.pqr" and "m.in" found in the <a> input files</a>,
generate the APBS grid "t.dx".
<li> Go to one of the Opal servers at <a href="http://rocce.ucsd.edu:8080/opal2/dashboard?command=serviceList">rocce.ucsd.edu </a> or
<a href="http://kryptonite.nbcr.net/opal2/dashboard?command=serviceList">kryptonite.ucsd.edu</a>.
<li> Select the service "Browndye: pqr2xml", select the option "Input URL",
using another browser tab go to the <a> input files</a>, and copy and paste the URL of "t.pqr" into the "Input URL" box. Copy and pasting URL's can easily be done by moving the curser over the link, pressing the right mouse button and selecting "Copy Link Location".
<li> Click the "Submit" button
<li> When the page indicates the execution has finished, click on the Output Base URL, inspect the file "t-atoms.pqrxml" in new browser tab, and <i> do not delete the tab.</i> 
<li> Repeat the previous three steps using "m.pqr" to generate the file "m-atoms.pqrxml" in its own tab.
</ul>

<p> The following steps are used to generate the reaction pairs file:

<ul>
<li> Select the service "Browndye: make_rxn_pairs".
<li> Copy the URL of "t-atoms.pqrxml" from "Browndye: pqr2xml" service into the box for "Molecule 0 Input URL".
<li> Copy the URL of "m-atoms.pqrxml" from "Browndye: pqr2xml" service into the box for "Molecule 1 Input URL".
<li> Copy the URL of "protein-protein-contacts.xml" in the input files into the
box for "Contacts Type URL".
<li> Enter 5.5 into the "Search Distance" box
<li> Enter "t-m-rxn-pairs.xml" into the "Output File" box.
<li> Submit the job, and when finished, inspect the "t-m-rxn-pairs.xml" file.
Do not delete the tab.
</ul>

<p> The following steps are used to generate the reaction description file:
<ul>
<li> Select the service "Browndye: make_rxn_file".
<li> Copy the URL of the the file "t-m-rxn-pairs.xml" from the output of 
"make_rxn_pairs" to the "Pairs Input URL" box. 
<li> Enter 5.5 into the "Reaction Distance" box.
<li> Enter 3 into the "Number of Required Contacts" box.
<li> Enter "t-m-rxns.xml" into the "Output File" box.
<li> Submit the job, and inspect the results when finished.
</ul>

<p> The following steps are used to run "bd_top" and "nam_simulation"
<ul>
<li>As above, copy the URL's for "t-atoms.pqrxml" and "m-atoms.pqrxml" into
the boxes for "Molecule 0 URL" and "Molecule 1 URL".
<li>Copy the URL for "t-m-rxns.xml" into the "Reaction File URL" box.
<li>Copy the URL's or files for "t.dx" and "m.dx" into the APBS boxes
for Molecule 0 and Molecule 1.
<li>Copy the URL of the input file "input.xml" in the input files into the
"Input File URL" box.
<li> Submit the job.
<li> When the file "results.xml" finally appears in the "Output Base URL",
keep checking it if you want until 1000 trajectories have been run and
the job stops.
<li> At any time, copy the URL for "results.xml" into the input URL box
for the "Browndye: compute_rate_constant" service, and check the file
"stdout.txt" for the rate constant information.
  

</ul>

  


<h2> References </h2>

<p>
Huber, GA and McCammon, JA. Browndye: A Software Package for Brownian
Dynamics.  Computer Physics
Communications, <a href="http://dx.doi.org/10.1016/j.cpc.2010.07.022">
in press </a>

<p>
Ermak, DL and McCammon, JA. Brownian Dynamics with Hydrodynamic Interactions,
J. Chem. Phys. 69, 1352-1360 (1978)

<p>
Northrup SH, Allison SA and McCammon JA.
Brownian Dynamics Simulation of Diffusion-Influenced Bimolecular Reactions
J. Chem. Phys. 80, 1517-1526

<p>
Luty BA, McCammon JA and Zhou HX. Diffusive Reaction-Rates From Brownian Dynamics Simulations - Replacing the Outer Cutoff Surface by an Analytical Treatment,
J. Chem. Phys. 97, 5682-5686 (1992)

<p>
Huber GA and Kim S. Weighted-ensemble Brownian dynamics simulations for protein association reactions, Biophys. J 70, 97-110 (1996)

<p>
Gabdoulline RR and Wade RC. Effective charges for macromolecules in solvent,
J. Phys. Chem 100, 3868-3878 (1996)


</body>
</html>

