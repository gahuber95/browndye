This is an example of a simulation of the association of 
thrombin and thrombomodulin, two proteins in the blood clotting 
cascade.

In order to run the example, you need to first have APBS installed
(found at www.poissonboltzmann.org) and have your path set to include 
"browndye/bin".  

Type "make all"; this will run APBS to generate the electric potential 
files for both molecules, pqr2xml to generate the corresponding xml files
from the pqr files, and 
make_rxn_pairs and make_rxn_file to generate the reaction file.

Type "bd_top input.xml" to run the auxilliary programs.

Type "nam_simulation t-m-simulation.xml" to run the simulation.

While the simulation is running, you can see how many trajectories
react by looking at "results.xml", and 
you can type "compute_rate_constant < results.xml" to get an estimate
of the rate constant.

If you want to see how it runs on more that one processor, 
edit the "input.xml" file, change the contents of the "<n-threads>"
element to the number of processors, and type "bd_top input.xml"
as above.

Note: the reaction criteria here have been loosened considerably so
the user can see some reactions take place in the space of a few
minutes at the expense of a realistic rate constant. The actual reaction 
takes place much less often.

The file tutorial09.html is a tutorial from a workshop at UCSD in the summer
of 2009.




