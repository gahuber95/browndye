pi = 3.1415926
r0 = 1.0
r1 = 1.0
R = 2.5
mu = 0.243
kT = 1.0
b = 10.0

def diff( r):
    return kT/(6*pi*mu*r)

d0 = diff( r0)
d1 = diff( r1)

d = d0 + d1

conv_factor = 602000000.0

print "rate", 4*pi*R*d*conv_factor
print "rxn prob", R/b

